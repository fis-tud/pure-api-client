from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.conflict_problem_details import ConflictProblemDetails
from ...models.problem_details import ProblemDetails
from ...models.research_output import ResearchOutput
from ...types import Response


def _get_kwargs(
    uuid: str,
    *,
    body: ResearchOutput,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}

    _kwargs: Dict[str, Any] = {
        "method": "put",
        "url": f"/research-outputs/{uuid}",
    }

    _body = body.to_dict()

    _kwargs["json"] = _body
    headers["Content-Type"] = "application/json"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[ConflictProblemDetails, ProblemDetails, ResearchOutput]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = ResearchOutput.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = ProblemDetails.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = ProblemDetails.from_dict(response.json())

        return response_404
    if response.status_code == HTTPStatus.CONFLICT:
        response_409 = ConflictProblemDetails.from_dict(response.json())

        return response_409
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[ConflictProblemDetails, ProblemDetails, ResearchOutput]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: ResearchOutput,
) -> Response[Union[ConflictProblemDetails, ProblemDetails, ResearchOutput]]:
    """Update research output

     Update research output with specific UUID.

    Args:
        uuid (str):
        body (ResearchOutput): Research output exists in many variations, from written to non-
            textual form. Templates are available for a range of these variations.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[ConflictProblemDetails, ProblemDetails, ResearchOutput]]
    """

    kwargs = _get_kwargs(
        uuid=uuid,
        body=body,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: ResearchOutput,
) -> Optional[Union[ConflictProblemDetails, ProblemDetails, ResearchOutput]]:
    """Update research output

     Update research output with specific UUID.

    Args:
        uuid (str):
        body (ResearchOutput): Research output exists in many variations, from written to non-
            textual form. Templates are available for a range of these variations.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[ConflictProblemDetails, ProblemDetails, ResearchOutput]
    """

    return sync_detailed(
        uuid=uuid,
        client=client,
        body=body,
    ).parsed


async def asyncio_detailed(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: ResearchOutput,
) -> Response[Union[ConflictProblemDetails, ProblemDetails, ResearchOutput]]:
    """Update research output

     Update research output with specific UUID.

    Args:
        uuid (str):
        body (ResearchOutput): Research output exists in many variations, from written to non-
            textual form. Templates are available for a range of these variations.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[ConflictProblemDetails, ProblemDetails, ResearchOutput]]
    """

    kwargs = _get_kwargs(
        uuid=uuid,
        body=body,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: ResearchOutput,
) -> Optional[Union[ConflictProblemDetails, ProblemDetails, ResearchOutput]]:
    """Update research output

     Update research output with specific UUID.

    Args:
        uuid (str):
        body (ResearchOutput): Research output exists in many variations, from written to non-
            textual form. Templates are available for a range of these variations.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[ConflictProblemDetails, ProblemDetails, ResearchOutput]
    """

    return (
        await asyncio_detailed(
            uuid=uuid,
            client=client,
            body=body,
        )
    ).parsed
