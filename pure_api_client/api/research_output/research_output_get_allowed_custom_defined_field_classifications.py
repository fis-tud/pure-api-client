from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.classification_ref_list import ClassificationRefList
from ...models.problem_details import ProblemDetails
from ...types import Response


def _get_kwargs(
    field_identifer: str,
) -> Dict[str, Any]:
    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": f"/research-outputs/allowed-custom-defined-field-values/{field_identifer}/classifications",
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[ClassificationRefList, ProblemDetails]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = ClassificationRefList.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = ProblemDetails.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = ProblemDetails.from_dict(response.json())

        return response_404
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[ClassificationRefList, ProblemDetails]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    field_identifer: str,
    *,
    client: Union[AuthenticatedClient, Client],
) -> Response[Union[ClassificationRefList, ProblemDetails]]:
    """Get allowed classifications for the custom-defined field associated with the research output

     Get allowed classifications for the custom-defined field associated with the research output.

    Args:
        field_identifer (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[ClassificationRefList, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        field_identifer=field_identifer,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    field_identifer: str,
    *,
    client: Union[AuthenticatedClient, Client],
) -> Optional[Union[ClassificationRefList, ProblemDetails]]:
    """Get allowed classifications for the custom-defined field associated with the research output

     Get allowed classifications for the custom-defined field associated with the research output.

    Args:
        field_identifer (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[ClassificationRefList, ProblemDetails]
    """

    return sync_detailed(
        field_identifer=field_identifer,
        client=client,
    ).parsed


async def asyncio_detailed(
    field_identifer: str,
    *,
    client: Union[AuthenticatedClient, Client],
) -> Response[Union[ClassificationRefList, ProblemDetails]]:
    """Get allowed classifications for the custom-defined field associated with the research output

     Get allowed classifications for the custom-defined field associated with the research output.

    Args:
        field_identifer (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[ClassificationRefList, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        field_identifer=field_identifer,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    field_identifer: str,
    *,
    client: Union[AuthenticatedClient, Client],
) -> Optional[Union[ClassificationRefList, ProblemDetails]]:
    """Get allowed classifications for the custom-defined field associated with the research output

     Get allowed classifications for the custom-defined field associated with the research output.

    Args:
        field_identifer (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[ClassificationRefList, ProblemDetails]
    """

    return (
        await asyncio_detailed(
            field_identifer=field_identifer,
            client=client,
        )
    ).parsed
