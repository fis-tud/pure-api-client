from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.external_person_list_result import ExternalPersonListResult
from ...models.external_persons_query import ExternalPersonsQuery
from ...models.problem_details import ProblemDetails
from ...types import Response


def _get_kwargs(
    *,
    body: ExternalPersonsQuery,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}

    _kwargs: Dict[str, Any] = {
        "method": "post",
        "url": "/external-persons/search",
    }

    _body = body.to_dict()

    _kwargs["json"] = _body
    headers["Content-Type"] = "application/json"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[ExternalPersonListResult, ProblemDetails]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = ExternalPersonListResult.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = ProblemDetails.from_dict(response.json())

        return response_400
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[ExternalPersonListResult, ProblemDetails]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    body: ExternalPersonsQuery,
) -> Response[Union[ExternalPersonListResult, ProblemDetails]]:
    """Query operation for external persons

     Lists external persons in the Pure instance that matches the provided query, similar to the GET
    version, instead of using parameters to alter the response, an JSON document is posted with the
    request. The JSON document contains fields for all the parameters available for the GET version, but
    also additional filtering options.

    Args:
        body (ExternalPersonsQuery): Create a query for external persons

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[ExternalPersonListResult, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        body=body,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: Union[AuthenticatedClient, Client],
    body: ExternalPersonsQuery,
) -> Optional[Union[ExternalPersonListResult, ProblemDetails]]:
    """Query operation for external persons

     Lists external persons in the Pure instance that matches the provided query, similar to the GET
    version, instead of using parameters to alter the response, an JSON document is posted with the
    request. The JSON document contains fields for all the parameters available for the GET version, but
    also additional filtering options.

    Args:
        body (ExternalPersonsQuery): Create a query for external persons

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[ExternalPersonListResult, ProblemDetails]
    """

    return sync_detailed(
        client=client,
        body=body,
    ).parsed


async def asyncio_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    body: ExternalPersonsQuery,
) -> Response[Union[ExternalPersonListResult, ProblemDetails]]:
    """Query operation for external persons

     Lists external persons in the Pure instance that matches the provided query, similar to the GET
    version, instead of using parameters to alter the response, an JSON document is posted with the
    request. The JSON document contains fields for all the parameters available for the GET version, but
    also additional filtering options.

    Args:
        body (ExternalPersonsQuery): Create a query for external persons

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[ExternalPersonListResult, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        body=body,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: Union[AuthenticatedClient, Client],
    body: ExternalPersonsQuery,
) -> Optional[Union[ExternalPersonListResult, ProblemDetails]]:
    """Query operation for external persons

     Lists external persons in the Pure instance that matches the provided query, similar to the GET
    version, instead of using parameters to alter the response, an JSON document is posted with the
    request. The JSON document contains fields for all the parameters available for the GET version, but
    also additional filtering options.

    Args:
        body (ExternalPersonsQuery): Create a query for external persons

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[ExternalPersonListResult, ProblemDetails]
    """

    return (
        await asyncio_detailed(
            client=client,
            body=body,
        )
    ).parsed
