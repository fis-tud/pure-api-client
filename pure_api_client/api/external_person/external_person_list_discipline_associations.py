from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.disciplines_association_list_result import DisciplinesAssociationListResult
from ...models.disciplines_associations_query import DisciplinesAssociationsQuery
from ...models.problem_details import ProblemDetails
from ...types import Response


def _get_kwargs(
    discipline_scheme: str,
    *,
    body: DisciplinesAssociationsQuery,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}

    _kwargs: Dict[str, Any] = {
        "method": "post",
        "url": f"/external-persons/disciplines/{discipline_scheme}/search",
    }

    _body = body.to_dict()

    _kwargs["json"] = _body
    headers["Content-Type"] = "application/json"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[DisciplinesAssociationListResult, ProblemDetails]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = DisciplinesAssociationListResult.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = ProblemDetails.from_dict(response.json())

        return response_400
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[DisciplinesAssociationListResult, ProblemDetails]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    discipline_scheme: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: DisciplinesAssociationsQuery,
) -> Response[Union[DisciplinesAssociationListResult, ProblemDetails]]:
    """Query operation for disciplines associated with external persons

     Lists disciplines from the discipline scheme associated with external persons in the Pure instance
    that matches the provided query.

    Args:
        discipline_scheme (str):
        body (DisciplinesAssociationsQuery): Create a query for discipline associations

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[DisciplinesAssociationListResult, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        discipline_scheme=discipline_scheme,
        body=body,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    discipline_scheme: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: DisciplinesAssociationsQuery,
) -> Optional[Union[DisciplinesAssociationListResult, ProblemDetails]]:
    """Query operation for disciplines associated with external persons

     Lists disciplines from the discipline scheme associated with external persons in the Pure instance
    that matches the provided query.

    Args:
        discipline_scheme (str):
        body (DisciplinesAssociationsQuery): Create a query for discipline associations

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[DisciplinesAssociationListResult, ProblemDetails]
    """

    return sync_detailed(
        discipline_scheme=discipline_scheme,
        client=client,
        body=body,
    ).parsed


async def asyncio_detailed(
    discipline_scheme: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: DisciplinesAssociationsQuery,
) -> Response[Union[DisciplinesAssociationListResult, ProblemDetails]]:
    """Query operation for disciplines associated with external persons

     Lists disciplines from the discipline scheme associated with external persons in the Pure instance
    that matches the provided query.

    Args:
        discipline_scheme (str):
        body (DisciplinesAssociationsQuery): Create a query for discipline associations

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[DisciplinesAssociationListResult, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        discipline_scheme=discipline_scheme,
        body=body,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    discipline_scheme: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: DisciplinesAssociationsQuery,
) -> Optional[Union[DisciplinesAssociationListResult, ProblemDetails]]:
    """Query operation for disciplines associated with external persons

     Lists disciplines from the discipline scheme associated with external persons in the Pure instance
    that matches the provided query.

    Args:
        discipline_scheme (str):
        body (DisciplinesAssociationsQuery): Create a query for discipline associations

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[DisciplinesAssociationListResult, ProblemDetails]
    """

    return (
        await asyncio_detailed(
            discipline_scheme=discipline_scheme,
            client=client,
            body=body,
        )
    ).parsed
