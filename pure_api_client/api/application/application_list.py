from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.application_list_result import ApplicationListResult
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
    order: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["size"] = size

    params["offset"] = offset

    params["order"] = order

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/applications",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[ApplicationListResult]:
    if response.status_code == HTTPStatus.OK:
        response_200 = ApplicationListResult.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[ApplicationListResult]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
    order: Union[Unset, str] = UNSET,
) -> Response[ApplicationListResult]:
    """Lists all applications

     Lists all applications in the Pure instance. If you need to filter the applications returned, see
    the POST version which supports additional filtering.

    Args:
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.
        order (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[ApplicationListResult]
    """

    kwargs = _get_kwargs(
        size=size,
        offset=offset,
        order=order,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
    order: Union[Unset, str] = UNSET,
) -> Optional[ApplicationListResult]:
    """Lists all applications

     Lists all applications in the Pure instance. If you need to filter the applications returned, see
    the POST version which supports additional filtering.

    Args:
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.
        order (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        ApplicationListResult
    """

    return sync_detailed(
        client=client,
        size=size,
        offset=offset,
        order=order,
    ).parsed


async def asyncio_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
    order: Union[Unset, str] = UNSET,
) -> Response[ApplicationListResult]:
    """Lists all applications

     Lists all applications in the Pure instance. If you need to filter the applications returned, see
    the POST version which supports additional filtering.

    Args:
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.
        order (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[ApplicationListResult]
    """

    kwargs = _get_kwargs(
        size=size,
        offset=offset,
        order=order,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
    order: Union[Unset, str] = UNSET,
) -> Optional[ApplicationListResult]:
    """Lists all applications

     Lists all applications in the Pure instance. If you need to filter the applications returned, see
    the POST version which supports additional filtering.

    Args:
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.
        order (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        ApplicationListResult
    """

    return (
        await asyncio_detailed(
            client=client,
            size=size,
            offset=offset,
            order=order,
        )
    ).parsed
