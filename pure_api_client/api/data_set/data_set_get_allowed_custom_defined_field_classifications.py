from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.classification_ref_list import ClassificationRefList
from ...models.problem_details import ProblemDetails
from ...types import Response


def _get_kwargs(
    property_name: str,
) -> Dict[str, Any]:
    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": f"/data-sets/allowed-custom-defined-field-values/{property_name}/classifications",
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[ClassificationRefList, ProblemDetails]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = ClassificationRefList.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = ProblemDetails.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = ProblemDetails.from_dict(response.json())

        return response_404
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[ClassificationRefList, ProblemDetails]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    property_name: str,
    *,
    client: Union[AuthenticatedClient, Client],
) -> Response[Union[ClassificationRefList, ProblemDetails]]:
    """Get allowed classifications for the custom-defined field associated with the data set

     Get allowed classifications for the custom-defined field associated with the data set.

    Args:
        property_name (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[ClassificationRefList, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        property_name=property_name,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    property_name: str,
    *,
    client: Union[AuthenticatedClient, Client],
) -> Optional[Union[ClassificationRefList, ProblemDetails]]:
    """Get allowed classifications for the custom-defined field associated with the data set

     Get allowed classifications for the custom-defined field associated with the data set.

    Args:
        property_name (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[ClassificationRefList, ProblemDetails]
    """

    return sync_detailed(
        property_name=property_name,
        client=client,
    ).parsed


async def asyncio_detailed(
    property_name: str,
    *,
    client: Union[AuthenticatedClient, Client],
) -> Response[Union[ClassificationRefList, ProblemDetails]]:
    """Get allowed classifications for the custom-defined field associated with the data set

     Get allowed classifications for the custom-defined field associated with the data set.

    Args:
        property_name (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[ClassificationRefList, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        property_name=property_name,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    property_name: str,
    *,
    client: Union[AuthenticatedClient, Client],
) -> Optional[Union[ClassificationRefList, ProblemDetails]]:
    """Get allowed classifications for the custom-defined field associated with the data set

     Get allowed classifications for the custom-defined field associated with the data set.

    Args:
        property_name (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[ClassificationRefList, ProblemDetails]
    """

    return (
        await asyncio_detailed(
            property_name=property_name,
            client=client,
        )
    ).parsed
