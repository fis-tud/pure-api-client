from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.disciplines_discipline_list_result import DisciplinesDisciplineListResult
from ...types import UNSET, Response, Unset


def _get_kwargs(
    discipline_scheme: str,
    *,
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["size"] = size

    params["offset"] = offset

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": f"/external-organizations/disciplines/{discipline_scheme}/allowed-disciplines",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[DisciplinesDisciplineListResult]:
    if response.status_code == HTTPStatus.OK:
        response_200 = DisciplinesDisciplineListResult.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[DisciplinesDisciplineListResult]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    discipline_scheme: str,
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
) -> Response[DisciplinesDisciplineListResult]:
    """A list of allowed disciplines for a specific discipline scheme

     Get a list of a allowed disciplines for specific discipline scheme for external organizations

    Args:
        discipline_scheme (str):
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[DisciplinesDisciplineListResult]
    """

    kwargs = _get_kwargs(
        discipline_scheme=discipline_scheme,
        size=size,
        offset=offset,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    discipline_scheme: str,
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
) -> Optional[DisciplinesDisciplineListResult]:
    """A list of allowed disciplines for a specific discipline scheme

     Get a list of a allowed disciplines for specific discipline scheme for external organizations

    Args:
        discipline_scheme (str):
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        DisciplinesDisciplineListResult
    """

    return sync_detailed(
        discipline_scheme=discipline_scheme,
        client=client,
        size=size,
        offset=offset,
    ).parsed


async def asyncio_detailed(
    discipline_scheme: str,
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
) -> Response[DisciplinesDisciplineListResult]:
    """A list of allowed disciplines for a specific discipline scheme

     Get a list of a allowed disciplines for specific discipline scheme for external organizations

    Args:
        discipline_scheme (str):
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[DisciplinesDisciplineListResult]
    """

    kwargs = _get_kwargs(
        discipline_scheme=discipline_scheme,
        size=size,
        offset=offset,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    discipline_scheme: str,
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
) -> Optional[DisciplinesDisciplineListResult]:
    """A list of allowed disciplines for a specific discipline scheme

     Get a list of a allowed disciplines for specific discipline scheme for external organizations

    Args:
        discipline_scheme (str):
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        DisciplinesDisciplineListResult
    """

    return (
        await asyncio_detailed(
            discipline_scheme=discipline_scheme,
            client=client,
            size=size,
            offset=offset,
        )
    ).parsed
