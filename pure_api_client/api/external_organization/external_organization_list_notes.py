from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.note_list_result import NoteListResult
from ...models.problem_details import ProblemDetails
from ...types import UNSET, Response, Unset


def _get_kwargs(
    uuid: str,
    *,
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["size"] = size

    params["offset"] = offset

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": f"/external-organizations/{uuid}/notes",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[NoteListResult, ProblemDetails]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = NoteListResult.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = ProblemDetails.from_dict(response.json())

        return response_404
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[NoteListResult, ProblemDetails]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
) -> Response[Union[NoteListResult, ProblemDetails]]:
    """Lists notes

     Lists notes associated with an external organization ordered by date (nulls last)

    Args:
        uuid (str):
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[NoteListResult, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        uuid=uuid,
        size=size,
        offset=offset,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
) -> Optional[Union[NoteListResult, ProblemDetails]]:
    """Lists notes

     Lists notes associated with an external organization ordered by date (nulls last)

    Args:
        uuid (str):
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[NoteListResult, ProblemDetails]
    """

    return sync_detailed(
        uuid=uuid,
        client=client,
        size=size,
        offset=offset,
    ).parsed


async def asyncio_detailed(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
) -> Response[Union[NoteListResult, ProblemDetails]]:
    """Lists notes

     Lists notes associated with an external organization ordered by date (nulls last)

    Args:
        uuid (str):
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[NoteListResult, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        uuid=uuid,
        size=size,
        offset=offset,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
) -> Optional[Union[NoteListResult, ProblemDetails]]:
    """Lists notes

     Lists notes associated with an external organization ordered by date (nulls last)

    Args:
        uuid (str):
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[NoteListResult, ProblemDetails]
    """

    return (
        await asyncio_detailed(
            uuid=uuid,
            client=client,
            size=size,
            offset=offset,
        )
    ).parsed
