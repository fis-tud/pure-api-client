from http import HTTPStatus
from typing import Any, Dict, Optional, Union, cast

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.problem_details import ProblemDetails
from ...types import UNSET, Response, Unset


def _get_kwargs(
    uuid: str,
    *,
    token_expiry_hours: Union[Unset, float] = 24.0,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["tokenExpiryHours"] = token_expiry_hours

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "post",
        "url": f"/users/{uuid}/actions/reset-password",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Any, ProblemDetails]]:
    if response.status_code == HTTPStatus.ACCEPTED:
        response_202 = cast(Any, None)
        return response_202
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = ProblemDetails.from_dict(response.json())

        return response_400
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Any, ProblemDetails]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    token_expiry_hours: Union[Unset, float] = 24.0,
) -> Response[Union[Any, ProblemDetails]]:
    """Reset user password

     Resets the user's password. Reset password email will be sent to the user's email. The token expiry
    hour defaults to 24 hours.

    Args:
        uuid (str):
        token_expiry_hours (Union[Unset, float]):  Default: 24.0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Any, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        uuid=uuid,
        token_expiry_hours=token_expiry_hours,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    token_expiry_hours: Union[Unset, float] = 24.0,
) -> Optional[Union[Any, ProblemDetails]]:
    """Reset user password

     Resets the user's password. Reset password email will be sent to the user's email. The token expiry
    hour defaults to 24 hours.

    Args:
        uuid (str):
        token_expiry_hours (Union[Unset, float]):  Default: 24.0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Any, ProblemDetails]
    """

    return sync_detailed(
        uuid=uuid,
        client=client,
        token_expiry_hours=token_expiry_hours,
    ).parsed


async def asyncio_detailed(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    token_expiry_hours: Union[Unset, float] = 24.0,
) -> Response[Union[Any, ProblemDetails]]:
    """Reset user password

     Resets the user's password. Reset password email will be sent to the user's email. The token expiry
    hour defaults to 24 hours.

    Args:
        uuid (str):
        token_expiry_hours (Union[Unset, float]):  Default: 24.0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Any, ProblemDetails]]
    """

    kwargs = _get_kwargs(
        uuid=uuid,
        token_expiry_hours=token_expiry_hours,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    uuid: str,
    *,
    client: Union[AuthenticatedClient, Client],
    token_expiry_hours: Union[Unset, float] = 24.0,
) -> Optional[Union[Any, ProblemDetails]]:
    """Reset user password

     Resets the user's password. Reset password email will be sent to the user's email. The token expiry
    hour defaults to 24 hours.

    Args:
        uuid (str):
        token_expiry_hours (Union[Unset, float]):  Default: 24.0.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Any, ProblemDetails]
    """

    return (
        await asyncio_detailed(
            uuid=uuid,
            client=client,
            token_expiry_hours=token_expiry_hours,
        )
    ).parsed
