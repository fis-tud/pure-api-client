from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.project_list_result import ProjectListResult
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
    order: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["size"] = size

    params["offset"] = offset

    params["order"] = order

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/projects",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[ProjectListResult]:
    if response.status_code == HTTPStatus.OK:
        response_200 = ProjectListResult.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[ProjectListResult]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
    order: Union[Unset, str] = UNSET,
) -> Response[ProjectListResult]:
    """Lists all projects

     Lists all projects in the Pure instance. If you need to filter the projects returned, see the POST
    version which supports additional filtering.

    Args:
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.
        order (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[ProjectListResult]
    """

    kwargs = _get_kwargs(
        size=size,
        offset=offset,
        order=order,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
    order: Union[Unset, str] = UNSET,
) -> Optional[ProjectListResult]:
    """Lists all projects

     Lists all projects in the Pure instance. If you need to filter the projects returned, see the POST
    version which supports additional filtering.

    Args:
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.
        order (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        ProjectListResult
    """

    return sync_detailed(
        client=client,
        size=size,
        offset=offset,
        order=order,
    ).parsed


async def asyncio_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
    order: Union[Unset, str] = UNSET,
) -> Response[ProjectListResult]:
    """Lists all projects

     Lists all projects in the Pure instance. If you need to filter the projects returned, see the POST
    version which supports additional filtering.

    Args:
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.
        order (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[ProjectListResult]
    """

    kwargs = _get_kwargs(
        size=size,
        offset=offset,
        order=order,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: Union[AuthenticatedClient, Client],
    size: Union[Unset, int] = 10,
    offset: Union[Unset, int] = 0,
    order: Union[Unset, str] = UNSET,
) -> Optional[ProjectListResult]:
    """Lists all projects

     Lists all projects in the Pure instance. If you need to filter the projects returned, see the POST
    version which supports additional filtering.

    Args:
        size (Union[Unset, int]):  Default: 10.
        offset (Union[Unset, int]):  Default: 0.
        order (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        ProjectListResult
    """

    return (
        await asyncio_detailed(
            client=client,
            size=size,
            offset=offset,
            order=order,
        )
    ).parsed
