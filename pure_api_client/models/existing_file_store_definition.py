from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.existing_file_store_definition_store_content_state import ExistingFileStoreDefinitionStoreContentState
from ..models.existing_file_store_definition_store_file_state import ExistingFileStoreDefinitionStoreFileState
from ..types import UNSET, Unset

T = TypeVar("T", bound="ExistingFileStoreDefinition")


@_attrs_define
class ExistingFileStoreDefinition:
    """
    Attributes:
        store_name (Union[Unset, str]):
        store_content_state (Union[Unset, ExistingFileStoreDefinitionStoreContentState]):
        store_content_id (Union[Unset, str]):
        store_file_id (Union[Unset, str]):
        store_file_state (Union[Unset, ExistingFileStoreDefinitionStoreFileState]):
        metadata_update_required (Union[Unset, bool]):
    """

    store_name: Union[Unset, str] = UNSET
    store_content_state: Union[Unset, ExistingFileStoreDefinitionStoreContentState] = UNSET
    store_content_id: Union[Unset, str] = UNSET
    store_file_id: Union[Unset, str] = UNSET
    store_file_state: Union[Unset, ExistingFileStoreDefinitionStoreFileState] = UNSET
    metadata_update_required: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        store_name = self.store_name

        store_content_state: Union[Unset, str] = UNSET
        if not isinstance(self.store_content_state, Unset):
            store_content_state = self.store_content_state.value

        store_content_id = self.store_content_id

        store_file_id = self.store_file_id

        store_file_state: Union[Unset, str] = UNSET
        if not isinstance(self.store_file_state, Unset):
            store_file_state = self.store_file_state.value

        metadata_update_required = self.metadata_update_required

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if store_name is not UNSET:
            field_dict["storeName"] = store_name
        if store_content_state is not UNSET:
            field_dict["storeContentState"] = store_content_state
        if store_content_id is not UNSET:
            field_dict["storeContentId"] = store_content_id
        if store_file_id is not UNSET:
            field_dict["storeFileId"] = store_file_id
        if store_file_state is not UNSET:
            field_dict["storeFileState"] = store_file_state
        if metadata_update_required is not UNSET:
            field_dict["metadataUpdateRequired"] = metadata_update_required

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        store_name = d.pop("storeName", UNSET)

        _store_content_state = d.pop("storeContentState", UNSET)
        store_content_state: Union[Unset, ExistingFileStoreDefinitionStoreContentState]
        if isinstance(_store_content_state, Unset):
            store_content_state = UNSET
        else:
            store_content_state = ExistingFileStoreDefinitionStoreContentState(_store_content_state)

        store_content_id = d.pop("storeContentId", UNSET)

        store_file_id = d.pop("storeFileId", UNSET)

        _store_file_state = d.pop("storeFileState", UNSET)
        store_file_state: Union[Unset, ExistingFileStoreDefinitionStoreFileState]
        if isinstance(_store_file_state, Unset):
            store_file_state = UNSET
        else:
            store_file_state = ExistingFileStoreDefinitionStoreFileState(_store_file_state)

        metadata_update_required = d.pop("metadataUpdateRequired", UNSET)

        existing_file_store_definition = cls(
            store_name=store_name,
            store_content_state=store_content_state,
            store_content_id=store_content_id,
            store_file_id=store_file_id,
            store_file_state=store_file_state,
            metadata_update_required=metadata_update_required,
        )

        existing_file_store_definition.additional_properties = d
        return existing_file_store_definition

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
