from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="GeoLocationType0")


@_attrs_define
class GeoLocationType0:
    """Geographical location

    Attributes:
        point (Union[None, Unset, str]): Location expressed as a point
        polygon (Union[None, Unset, str]): Location expressed as a polygon
        calculated_point (Union[Unset, str]): Used to determine whether or not we may update the point. An end-user has
            not entered a point manually if the value of the calculated point is the same as point
    """

    point: Union[None, Unset, str] = UNSET
    polygon: Union[None, Unset, str] = UNSET
    calculated_point: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        point: Union[None, Unset, str]
        if isinstance(self.point, Unset):
            point = UNSET
        else:
            point = self.point

        polygon: Union[None, Unset, str]
        if isinstance(self.polygon, Unset):
            polygon = UNSET
        else:
            polygon = self.polygon

        calculated_point = self.calculated_point

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if point is not UNSET:
            field_dict["point"] = point
        if polygon is not UNSET:
            field_dict["polygon"] = polygon
        if calculated_point is not UNSET:
            field_dict["calculatedPoint"] = calculated_point

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()

        def _parse_point(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        point = _parse_point(d.pop("point", UNSET))

        def _parse_polygon(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        polygon = _parse_polygon(d.pop("polygon", UNSET))

        calculated_point = d.pop("calculatedPoint", UNSET)

        geo_location_type_0 = cls(
            point=point,
            polygon=polygon,
            calculated_point=calculated_point,
        )

        geo_location_type_0.additional_properties = d
        return geo_location_type_0

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
