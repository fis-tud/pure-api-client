from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="OrderingsList")


@_attrs_define
class OrderingsList:
    """List of available orderings

    Attributes:
        orderings (Union[Unset, List[str]]): IDs of ways content can be ordered
    """

    orderings: Union[Unset, List[str]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        orderings: Union[Unset, List[str]] = UNSET
        if not isinstance(self.orderings, Unset):
            orderings = self.orderings

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if orderings is not UNSET:
            field_dict["orderings"] = orderings

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        orderings = cast(List[str], d.pop("orderings", UNSET))

        orderings_list = cls(
            orderings=orderings,
        )

        orderings_list.additional_properties = d
        return orderings_list

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
