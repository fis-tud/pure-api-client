from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.content_ref_type_0 import ContentRefType0


T = TypeVar("T", bound="ProjectAssociation")


@_attrs_define
class ProjectAssociation:
    """An association to a project.

    Attributes:
        project (Union['ContentRefType0', None]):
        relation_type (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
    """

    project: Union["ContentRefType0", None]
    relation_type: Union["ClassificationRefType0", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0

        project: Union[Dict[str, Any], None]
        if isinstance(self.project, ContentRefType0):
            project = self.project.to_dict()
        else:
            project = self.project

        relation_type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.relation_type, Unset):
            relation_type = UNSET
        elif isinstance(self.relation_type, ClassificationRefType0):
            relation_type = self.relation_type.to_dict()
        else:
            relation_type = self.relation_type

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "project": project,
            }
        )
        if relation_type is not UNSET:
            field_dict["relationType"] = relation_type

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0

        d = src_dict.copy()

        def _parse_project(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        project = _parse_project(d.pop("project"))

        def _parse_relation_type(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        relation_type = _parse_relation_type(d.pop("relationType", UNSET))

        project_association = cls(
            project=project,
            relation_type=relation_type,
        )

        project_association.additional_properties = d
        return project_association

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
