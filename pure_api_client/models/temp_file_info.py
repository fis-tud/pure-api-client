from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="TempFileInfo")


@_attrs_define
class TempFileInfo:
    """
    Attributes:
        temporary_id (Union[Unset, str]):
        digest_algorithm (Union[Unset, str]):
        size (Union[Unset, int]):
        digest (Union[Unset, str]):
        mime_type (Union[Unset, str]):
    """

    temporary_id: Union[Unset, str] = UNSET
    digest_algorithm: Union[Unset, str] = UNSET
    size: Union[Unset, int] = UNSET
    digest: Union[Unset, str] = UNSET
    mime_type: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        temporary_id = self.temporary_id

        digest_algorithm = self.digest_algorithm

        size = self.size

        digest = self.digest

        mime_type = self.mime_type

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if temporary_id is not UNSET:
            field_dict["temporaryId"] = temporary_id
        if digest_algorithm is not UNSET:
            field_dict["digestAlgorithm"] = digest_algorithm
        if size is not UNSET:
            field_dict["size"] = size
        if digest is not UNSET:
            field_dict["digest"] = digest
        if mime_type is not UNSET:
            field_dict["mimeType"] = mime_type

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        temporary_id = d.pop("temporaryId", UNSET)

        digest_algorithm = d.pop("digestAlgorithm", UNSET)

        size = d.pop("size", UNSET)

        digest = d.pop("digest", UNSET)

        mime_type = d.pop("mimeType", UNSET)

        temp_file_info = cls(
            temporary_id=temporary_id,
            digest_algorithm=digest_algorithm,
            size=size,
            digest=digest,
            mime_type=mime_type,
        )

        temp_file_info.additional_properties = d
        return temp_file_info

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
