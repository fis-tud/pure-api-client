import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.date_range import DateRange
    from ..models.formatted_localized_string_type_0 import FormattedLocalizedStringType0
    from ..models.geo_location_type_0 import GeoLocationType0
    from ..models.identifier import Identifier
    from ..models.keyword_group import KeywordGroup
    from ..models.link import Link
    from ..models.organization_or_external_organization_ref_type_0 import OrganizationOrExternalOrganizationRefType0
    from ..models.workflow import Workflow


T = TypeVar("T", bound="Event")


@_attrs_define
class Event:
    """An event typically a conference, workshop or similar

    Attributes:
        type (Union['ClassificationRefType0', None]): A reference to a classification value
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        uuid (Union[Unset, str]): UUID, this is the primary identity of the entity
        created_by (Union[Unset, str]): Username of creator
        created_date (Union[Unset, datetime.datetime]): Date and time of creation
        modified_by (Union[Unset, str]): Username of the user that performed a modification
        modified_date (Union[Unset, datetime.datetime]): Date and time of last modification
        portal_url (Union[Unset, str]): URL of the content on the Pure Portal
        pretty_url_identifiers (Union[Unset, List[str]]): All pretty URLs
        previous_uuids (Union[Unset, List[str]]): UUIDs of other content items which have been merged into this content
            item (or similar)
        version (Union[None, Unset, str]): Used to guard against conflicting updates. For new content this is null, and
            for existing content the current value. The property should never be modified by a client, except in the rare
            case where the client wants to perform an update irrespective of if other clients have made updates in the
            meantime, also known as a "dirty write". A dirty write is performed by not including the property value or
            setting the property to null
        abbreviated_title (Union['FormattedLocalizedStringType0', None, Unset]): A set of potentially formatted string
            values each localized for a specific submission locale. Please note that invalid locale values will be ignored.
            Example: {'en_GB': 'Some text'}.
        geo_location (Union['GeoLocationType0', None, Unset]): Geographical location
        city (Union[None, Unset, str]): The city of the event
        conference_number (Union[None, Unset, str]): The conference number of the event
        country (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        degree_of_recognition (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        description (Union['FormattedLocalizedStringType0', None, Unset]): A set of potentially formatted string values
            each localized for a specific submission locale. Please note that invalid locale values will be ignored.
            Example: {'en_GB': 'Some text'}.
        keyword_groups (Union[List['KeywordGroup'], None, Unset]): A group for each type of keyword present
        links (Union[List['Link'], None, Unset]): Links to information about the event
        location (Union[None, Unset, str]): The actual location of the event
        organizers (Union[List[Union['OrganizationOrExternalOrganizationRefType0', None]], None, Unset]): The organizers
            of the event
        lifecycle (Union[Unset, DateRange]): A date range
        events (Union[List[Union['ContentRefType0', None]], None, Unset]): Other events related to this event
        identifiers (Union[List['Identifier'], None, Unset]): IDs that this object corresponds to in external systems.
            Such as a Scopus ID. Used by Pure where it is necessary to identify objects to specific external systems
        sponsor_organizations (Union[List[Union['OrganizationOrExternalOrganizationRefType0', None]], None, Unset]): The
            event sponsors
        subdivision (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        title (Union['FormattedLocalizedStringType0', None, Unset]): A set of potentially formatted string values each
            localized for a specific submission locale. Please note that invalid locale values will be ignored. Example:
            {'en_GB': 'Some text'}.
        sub_title (Union['FormattedLocalizedStringType0', None, Unset]): A set of potentially formatted string values
            each localized for a specific submission locale. Please note that invalid locale values will be ignored.
            Example: {'en_GB': 'Some text'}.
        workflow (Union[Unset, Workflow]): Information about workflow
        system_name (Union[Unset, str]): The content system name
    """

    type: Union["ClassificationRefType0", None]
    pure_id: Union[Unset, int] = UNSET
    uuid: Union[Unset, str] = UNSET
    created_by: Union[Unset, str] = UNSET
    created_date: Union[Unset, datetime.datetime] = UNSET
    modified_by: Union[Unset, str] = UNSET
    modified_date: Union[Unset, datetime.datetime] = UNSET
    portal_url: Union[Unset, str] = UNSET
    pretty_url_identifiers: Union[Unset, List[str]] = UNSET
    previous_uuids: Union[Unset, List[str]] = UNSET
    version: Union[None, Unset, str] = UNSET
    abbreviated_title: Union["FormattedLocalizedStringType0", None, Unset] = UNSET
    geo_location: Union["GeoLocationType0", None, Unset] = UNSET
    city: Union[None, Unset, str] = UNSET
    conference_number: Union[None, Unset, str] = UNSET
    country: Union["ClassificationRefType0", None, Unset] = UNSET
    degree_of_recognition: Union["ClassificationRefType0", None, Unset] = UNSET
    description: Union["FormattedLocalizedStringType0", None, Unset] = UNSET
    keyword_groups: Union[List["KeywordGroup"], None, Unset] = UNSET
    links: Union[List["Link"], None, Unset] = UNSET
    location: Union[None, Unset, str] = UNSET
    organizers: Union[List[Union["OrganizationOrExternalOrganizationRefType0", None]], None, Unset] = UNSET
    lifecycle: Union[Unset, "DateRange"] = UNSET
    events: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    identifiers: Union[List["Identifier"], None, Unset] = UNSET
    sponsor_organizations: Union[List[Union["OrganizationOrExternalOrganizationRefType0", None]], None, Unset] = UNSET
    subdivision: Union["ClassificationRefType0", None, Unset] = UNSET
    title: Union["FormattedLocalizedStringType0", None, Unset] = UNSET
    sub_title: Union["FormattedLocalizedStringType0", None, Unset] = UNSET
    workflow: Union[Unset, "Workflow"] = UNSET
    system_name: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.formatted_localized_string_type_0 import FormattedLocalizedStringType0
        from ..models.geo_location_type_0 import GeoLocationType0
        from ..models.organization_or_external_organization_ref_type_0 import OrganizationOrExternalOrganizationRefType0

        type: Union[Dict[str, Any], None]
        if isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        pure_id = self.pure_id

        uuid = self.uuid

        created_by = self.created_by

        created_date: Union[Unset, str] = UNSET
        if not isinstance(self.created_date, Unset):
            created_date = self.created_date.isoformat()

        modified_by = self.modified_by

        modified_date: Union[Unset, str] = UNSET
        if not isinstance(self.modified_date, Unset):
            modified_date = self.modified_date.isoformat()

        portal_url = self.portal_url

        pretty_url_identifiers: Union[Unset, List[str]] = UNSET
        if not isinstance(self.pretty_url_identifiers, Unset):
            pretty_url_identifiers = self.pretty_url_identifiers

        previous_uuids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.previous_uuids, Unset):
            previous_uuids = self.previous_uuids

        version: Union[None, Unset, str]
        if isinstance(self.version, Unset):
            version = UNSET
        else:
            version = self.version

        abbreviated_title: Union[Dict[str, Any], None, Unset]
        if isinstance(self.abbreviated_title, Unset):
            abbreviated_title = UNSET
        elif isinstance(self.abbreviated_title, FormattedLocalizedStringType0):
            abbreviated_title = self.abbreviated_title.to_dict()
        else:
            abbreviated_title = self.abbreviated_title

        geo_location: Union[Dict[str, Any], None, Unset]
        if isinstance(self.geo_location, Unset):
            geo_location = UNSET
        elif isinstance(self.geo_location, GeoLocationType0):
            geo_location = self.geo_location.to_dict()
        else:
            geo_location = self.geo_location

        city: Union[None, Unset, str]
        if isinstance(self.city, Unset):
            city = UNSET
        else:
            city = self.city

        conference_number: Union[None, Unset, str]
        if isinstance(self.conference_number, Unset):
            conference_number = UNSET
        else:
            conference_number = self.conference_number

        country: Union[Dict[str, Any], None, Unset]
        if isinstance(self.country, Unset):
            country = UNSET
        elif isinstance(self.country, ClassificationRefType0):
            country = self.country.to_dict()
        else:
            country = self.country

        degree_of_recognition: Union[Dict[str, Any], None, Unset]
        if isinstance(self.degree_of_recognition, Unset):
            degree_of_recognition = UNSET
        elif isinstance(self.degree_of_recognition, ClassificationRefType0):
            degree_of_recognition = self.degree_of_recognition.to_dict()
        else:
            degree_of_recognition = self.degree_of_recognition

        description: Union[Dict[str, Any], None, Unset]
        if isinstance(self.description, Unset):
            description = UNSET
        elif isinstance(self.description, FormattedLocalizedStringType0):
            description = self.description.to_dict()
        else:
            description = self.description

        keyword_groups: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.keyword_groups, Unset):
            keyword_groups = UNSET
        elif isinstance(self.keyword_groups, list):
            keyword_groups = []
            for keyword_groups_type_0_item_data in self.keyword_groups:
                keyword_groups_type_0_item = keyword_groups_type_0_item_data.to_dict()
                keyword_groups.append(keyword_groups_type_0_item)

        else:
            keyword_groups = self.keyword_groups

        links: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.links, Unset):
            links = UNSET
        elif isinstance(self.links, list):
            links = []
            for links_type_0_item_data in self.links:
                links_type_0_item = links_type_0_item_data.to_dict()
                links.append(links_type_0_item)

        else:
            links = self.links

        location: Union[None, Unset, str]
        if isinstance(self.location, Unset):
            location = UNSET
        else:
            location = self.location

        organizers: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.organizers, Unset):
            organizers = UNSET
        elif isinstance(self.organizers, list):
            organizers = []
            for organizers_type_0_item_data in self.organizers:
                organizers_type_0_item: Union[Dict[str, Any], None]
                if isinstance(organizers_type_0_item_data, OrganizationOrExternalOrganizationRefType0):
                    organizers_type_0_item = organizers_type_0_item_data.to_dict()
                else:
                    organizers_type_0_item = organizers_type_0_item_data
                organizers.append(organizers_type_0_item)

        else:
            organizers = self.organizers

        lifecycle: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.lifecycle, Unset):
            lifecycle = self.lifecycle.to_dict()

        events: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.events, Unset):
            events = UNSET
        elif isinstance(self.events, list):
            events = []
            for events_type_0_item_data in self.events:
                events_type_0_item: Union[Dict[str, Any], None]
                if isinstance(events_type_0_item_data, ContentRefType0):
                    events_type_0_item = events_type_0_item_data.to_dict()
                else:
                    events_type_0_item = events_type_0_item_data
                events.append(events_type_0_item)

        else:
            events = self.events

        identifiers: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.identifiers, Unset):
            identifiers = UNSET
        elif isinstance(self.identifiers, list):
            identifiers = []
            for identifiers_type_0_item_data in self.identifiers:
                identifiers_type_0_item = identifiers_type_0_item_data.to_dict()
                identifiers.append(identifiers_type_0_item)

        else:
            identifiers = self.identifiers

        sponsor_organizations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.sponsor_organizations, Unset):
            sponsor_organizations = UNSET
        elif isinstance(self.sponsor_organizations, list):
            sponsor_organizations = []
            for sponsor_organizations_type_0_item_data in self.sponsor_organizations:
                sponsor_organizations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(sponsor_organizations_type_0_item_data, OrganizationOrExternalOrganizationRefType0):
                    sponsor_organizations_type_0_item = sponsor_organizations_type_0_item_data.to_dict()
                else:
                    sponsor_organizations_type_0_item = sponsor_organizations_type_0_item_data
                sponsor_organizations.append(sponsor_organizations_type_0_item)

        else:
            sponsor_organizations = self.sponsor_organizations

        subdivision: Union[Dict[str, Any], None, Unset]
        if isinstance(self.subdivision, Unset):
            subdivision = UNSET
        elif isinstance(self.subdivision, ClassificationRefType0):
            subdivision = self.subdivision.to_dict()
        else:
            subdivision = self.subdivision

        title: Union[Dict[str, Any], None, Unset]
        if isinstance(self.title, Unset):
            title = UNSET
        elif isinstance(self.title, FormattedLocalizedStringType0):
            title = self.title.to_dict()
        else:
            title = self.title

        sub_title: Union[Dict[str, Any], None, Unset]
        if isinstance(self.sub_title, Unset):
            sub_title = UNSET
        elif isinstance(self.sub_title, FormattedLocalizedStringType0):
            sub_title = self.sub_title.to_dict()
        else:
            sub_title = self.sub_title

        workflow: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.workflow, Unset):
            workflow = self.workflow.to_dict()

        system_name = self.system_name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "type": type,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if uuid is not UNSET:
            field_dict["uuid"] = uuid
        if created_by is not UNSET:
            field_dict["createdBy"] = created_by
        if created_date is not UNSET:
            field_dict["createdDate"] = created_date
        if modified_by is not UNSET:
            field_dict["modifiedBy"] = modified_by
        if modified_date is not UNSET:
            field_dict["modifiedDate"] = modified_date
        if portal_url is not UNSET:
            field_dict["portalUrl"] = portal_url
        if pretty_url_identifiers is not UNSET:
            field_dict["prettyUrlIdentifiers"] = pretty_url_identifiers
        if previous_uuids is not UNSET:
            field_dict["previousUuids"] = previous_uuids
        if version is not UNSET:
            field_dict["version"] = version
        if abbreviated_title is not UNSET:
            field_dict["abbreviatedTitle"] = abbreviated_title
        if geo_location is not UNSET:
            field_dict["geoLocation"] = geo_location
        if city is not UNSET:
            field_dict["city"] = city
        if conference_number is not UNSET:
            field_dict["conferenceNumber"] = conference_number
        if country is not UNSET:
            field_dict["country"] = country
        if degree_of_recognition is not UNSET:
            field_dict["degreeOfRecognition"] = degree_of_recognition
        if description is not UNSET:
            field_dict["description"] = description
        if keyword_groups is not UNSET:
            field_dict["keywordGroups"] = keyword_groups
        if links is not UNSET:
            field_dict["links"] = links
        if location is not UNSET:
            field_dict["location"] = location
        if organizers is not UNSET:
            field_dict["organizers"] = organizers
        if lifecycle is not UNSET:
            field_dict["lifecycle"] = lifecycle
        if events is not UNSET:
            field_dict["events"] = events
        if identifiers is not UNSET:
            field_dict["identifiers"] = identifiers
        if sponsor_organizations is not UNSET:
            field_dict["sponsorOrganizations"] = sponsor_organizations
        if subdivision is not UNSET:
            field_dict["subdivision"] = subdivision
        if title is not UNSET:
            field_dict["title"] = title
        if sub_title is not UNSET:
            field_dict["subTitle"] = sub_title
        if workflow is not UNSET:
            field_dict["workflow"] = workflow
        if system_name is not UNSET:
            field_dict["systemName"] = system_name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.date_range import DateRange
        from ..models.formatted_localized_string_type_0 import FormattedLocalizedStringType0
        from ..models.geo_location_type_0 import GeoLocationType0
        from ..models.identifier import Identifier
        from ..models.keyword_group import KeywordGroup
        from ..models.link import Link
        from ..models.organization_or_external_organization_ref_type_0 import OrganizationOrExternalOrganizationRefType0
        from ..models.workflow import Workflow

        d = src_dict.copy()

        def _parse_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        type = _parse_type(d.pop("type"))

        pure_id = d.pop("pureId", UNSET)

        uuid = d.pop("uuid", UNSET)

        created_by = d.pop("createdBy", UNSET)

        _created_date = d.pop("createdDate", UNSET)
        created_date: Union[Unset, datetime.datetime]
        if isinstance(_created_date, Unset):
            created_date = UNSET
        else:
            created_date = isoparse(_created_date)

        modified_by = d.pop("modifiedBy", UNSET)

        _modified_date = d.pop("modifiedDate", UNSET)
        modified_date: Union[Unset, datetime.datetime]
        if isinstance(_modified_date, Unset):
            modified_date = UNSET
        else:
            modified_date = isoparse(_modified_date)

        portal_url = d.pop("portalUrl", UNSET)

        pretty_url_identifiers = cast(List[str], d.pop("prettyUrlIdentifiers", UNSET))

        previous_uuids = cast(List[str], d.pop("previousUuids", UNSET))

        def _parse_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        version = _parse_version(d.pop("version", UNSET))

        def _parse_abbreviated_title(data: object) -> Union["FormattedLocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_formatted_localized_string_type_0 = FormattedLocalizedStringType0.from_dict(data)

                return componentsschemas_formatted_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["FormattedLocalizedStringType0", None, Unset], data)

        abbreviated_title = _parse_abbreviated_title(d.pop("abbreviatedTitle", UNSET))

        def _parse_geo_location(data: object) -> Union["GeoLocationType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_geo_location_type_0 = GeoLocationType0.from_dict(data)

                return componentsschemas_geo_location_type_0
            except:  # noqa: E722
                pass
            return cast(Union["GeoLocationType0", None, Unset], data)

        geo_location = _parse_geo_location(d.pop("geoLocation", UNSET))

        def _parse_city(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        city = _parse_city(d.pop("city", UNSET))

        def _parse_conference_number(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        conference_number = _parse_conference_number(d.pop("conferenceNumber", UNSET))

        def _parse_country(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        country = _parse_country(d.pop("country", UNSET))

        def _parse_degree_of_recognition(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        degree_of_recognition = _parse_degree_of_recognition(d.pop("degreeOfRecognition", UNSET))

        def _parse_description(data: object) -> Union["FormattedLocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_formatted_localized_string_type_0 = FormattedLocalizedStringType0.from_dict(data)

                return componentsschemas_formatted_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["FormattedLocalizedStringType0", None, Unset], data)

        description = _parse_description(d.pop("description", UNSET))

        def _parse_keyword_groups(data: object) -> Union[List["KeywordGroup"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                keyword_groups_type_0 = []
                _keyword_groups_type_0 = data
                for keyword_groups_type_0_item_data in _keyword_groups_type_0:
                    keyword_groups_type_0_item = KeywordGroup.from_dict(keyword_groups_type_0_item_data)

                    keyword_groups_type_0.append(keyword_groups_type_0_item)

                return keyword_groups_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["KeywordGroup"], None, Unset], data)

        keyword_groups = _parse_keyword_groups(d.pop("keywordGroups", UNSET))

        def _parse_links(data: object) -> Union[List["Link"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                links_type_0 = []
                _links_type_0 = data
                for links_type_0_item_data in _links_type_0:
                    links_type_0_item = Link.from_dict(links_type_0_item_data)

                    links_type_0.append(links_type_0_item)

                return links_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Link"], None, Unset], data)

        links = _parse_links(d.pop("links", UNSET))

        def _parse_location(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        location = _parse_location(d.pop("location", UNSET))

        def _parse_organizers(
            data: object,
        ) -> Union[List[Union["OrganizationOrExternalOrganizationRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                organizers_type_0 = []
                _organizers_type_0 = data
                for organizers_type_0_item_data in _organizers_type_0:

                    def _parse_organizers_type_0_item(
                        data: object,
                    ) -> Union["OrganizationOrExternalOrganizationRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_organization_or_external_organization_ref_type_0 = (
                                OrganizationOrExternalOrganizationRefType0.from_dict(data)
                            )

                            return componentsschemas_organization_or_external_organization_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["OrganizationOrExternalOrganizationRefType0", None], data)

                    organizers_type_0_item = _parse_organizers_type_0_item(organizers_type_0_item_data)

                    organizers_type_0.append(organizers_type_0_item)

                return organizers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["OrganizationOrExternalOrganizationRefType0", None]], None, Unset], data)

        organizers = _parse_organizers(d.pop("organizers", UNSET))

        _lifecycle = d.pop("lifecycle", UNSET)
        lifecycle: Union[Unset, DateRange]
        if isinstance(_lifecycle, Unset):
            lifecycle = UNSET
        else:
            lifecycle = DateRange.from_dict(_lifecycle)

        def _parse_events(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                events_type_0 = []
                _events_type_0 = data
                for events_type_0_item_data in _events_type_0:

                    def _parse_events_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    events_type_0_item = _parse_events_type_0_item(events_type_0_item_data)

                    events_type_0.append(events_type_0_item)

                return events_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        events = _parse_events(d.pop("events", UNSET))

        def _parse_identifiers(data: object) -> Union[List["Identifier"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                identifiers_type_0 = []
                _identifiers_type_0 = data
                for identifiers_type_0_item_data in _identifiers_type_0:
                    identifiers_type_0_item = Identifier.from_dict(identifiers_type_0_item_data)

                    identifiers_type_0.append(identifiers_type_0_item)

                return identifiers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Identifier"], None, Unset], data)

        identifiers = _parse_identifiers(d.pop("identifiers", UNSET))

        def _parse_sponsor_organizations(
            data: object,
        ) -> Union[List[Union["OrganizationOrExternalOrganizationRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                sponsor_organizations_type_0 = []
                _sponsor_organizations_type_0 = data
                for sponsor_organizations_type_0_item_data in _sponsor_organizations_type_0:

                    def _parse_sponsor_organizations_type_0_item(
                        data: object,
                    ) -> Union["OrganizationOrExternalOrganizationRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_organization_or_external_organization_ref_type_0 = (
                                OrganizationOrExternalOrganizationRefType0.from_dict(data)
                            )

                            return componentsschemas_organization_or_external_organization_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["OrganizationOrExternalOrganizationRefType0", None], data)

                    sponsor_organizations_type_0_item = _parse_sponsor_organizations_type_0_item(
                        sponsor_organizations_type_0_item_data
                    )

                    sponsor_organizations_type_0.append(sponsor_organizations_type_0_item)

                return sponsor_organizations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["OrganizationOrExternalOrganizationRefType0", None]], None, Unset], data)

        sponsor_organizations = _parse_sponsor_organizations(d.pop("sponsorOrganizations", UNSET))

        def _parse_subdivision(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        subdivision = _parse_subdivision(d.pop("subdivision", UNSET))

        def _parse_title(data: object) -> Union["FormattedLocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_formatted_localized_string_type_0 = FormattedLocalizedStringType0.from_dict(data)

                return componentsschemas_formatted_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["FormattedLocalizedStringType0", None, Unset], data)

        title = _parse_title(d.pop("title", UNSET))

        def _parse_sub_title(data: object) -> Union["FormattedLocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_formatted_localized_string_type_0 = FormattedLocalizedStringType0.from_dict(data)

                return componentsschemas_formatted_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["FormattedLocalizedStringType0", None, Unset], data)

        sub_title = _parse_sub_title(d.pop("subTitle", UNSET))

        _workflow = d.pop("workflow", UNSET)
        workflow: Union[Unset, Workflow]
        if isinstance(_workflow, Unset):
            workflow = UNSET
        else:
            workflow = Workflow.from_dict(_workflow)

        system_name = d.pop("systemName", UNSET)

        event = cls(
            type=type,
            pure_id=pure_id,
            uuid=uuid,
            created_by=created_by,
            created_date=created_date,
            modified_by=modified_by,
            modified_date=modified_date,
            portal_url=portal_url,
            pretty_url_identifiers=pretty_url_identifiers,
            previous_uuids=previous_uuids,
            version=version,
            abbreviated_title=abbreviated_title,
            geo_location=geo_location,
            city=city,
            conference_number=conference_number,
            country=country,
            degree_of_recognition=degree_of_recognition,
            description=description,
            keyword_groups=keyword_groups,
            links=links,
            location=location,
            organizers=organizers,
            lifecycle=lifecycle,
            events=events,
            identifiers=identifiers,
            sponsor_organizations=sponsor_organizations,
            subdivision=subdivision,
            title=title,
            sub_title=sub_title,
            workflow=workflow,
            system_name=system_name,
        )

        event.additional_properties = d
        return event

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
