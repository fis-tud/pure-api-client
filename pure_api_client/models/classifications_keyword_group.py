from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.localized_string_type_0 import LocalizedStringType0


T = TypeVar("T", bound="ClassificationsKeywordGroup")


@_attrs_define
class ClassificationsKeywordGroup:
    """De-normalized version of the full keyword group representation for instances where only structured keywords can be
    selected

        Attributes:
            logical_name (str): Unique name of the configuration that specifies this keyword group
            type_discriminator (str):
            pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
                entity
            name (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission locale. Note:
                invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
            classifications (Union[List[Union['ClassificationRefType0', None]], None, Unset]): A list classifications
                extracted from the keyword group structured keyword concept
    """

    logical_name: str
    type_discriminator: str
    pure_id: Union[Unset, int] = UNSET
    name: Union["LocalizedStringType0", None, Unset] = UNSET
    classifications: Union[List[Union["ClassificationRefType0", None]], None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        logical_name = self.logical_name

        type_discriminator = self.type_discriminator

        pure_id = self.pure_id

        name: Union[Dict[str, Any], None, Unset]
        if isinstance(self.name, Unset):
            name = UNSET
        elif isinstance(self.name, LocalizedStringType0):
            name = self.name.to_dict()
        else:
            name = self.name

        classifications: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.classifications, Unset):
            classifications = UNSET
        elif isinstance(self.classifications, list):
            classifications = []
            for classifications_type_0_item_data in self.classifications:
                classifications_type_0_item: Union[Dict[str, Any], None]
                if isinstance(classifications_type_0_item_data, ClassificationRefType0):
                    classifications_type_0_item = classifications_type_0_item_data.to_dict()
                else:
                    classifications_type_0_item = classifications_type_0_item_data
                classifications.append(classifications_type_0_item)

        else:
            classifications = self.classifications

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "logicalName": logical_name,
                "typeDiscriminator": type_discriminator,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if name is not UNSET:
            field_dict["name"] = name
        if classifications is not UNSET:
            field_dict["classifications"] = classifications

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        d = src_dict.copy()
        logical_name = d.pop("logicalName")

        type_discriminator = d.pop("typeDiscriminator")

        pure_id = d.pop("pureId", UNSET)

        def _parse_name(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        name = _parse_name(d.pop("name", UNSET))

        def _parse_classifications(data: object) -> Union[List[Union["ClassificationRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                classifications_type_0 = []
                _classifications_type_0 = data
                for classifications_type_0_item_data in _classifications_type_0:

                    def _parse_classifications_type_0_item(data: object) -> Union["ClassificationRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                            return componentsschemas_classification_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ClassificationRefType0", None], data)

                    classifications_type_0_item = _parse_classifications_type_0_item(classifications_type_0_item_data)

                    classifications_type_0.append(classifications_type_0_item)

                return classifications_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ClassificationRefType0", None]], None, Unset], data)

        classifications = _parse_classifications(d.pop("classifications", UNSET))

        classifications_keyword_group = cls(
            logical_name=logical_name,
            type_discriminator=type_discriminator,
            pure_id=pure_id,
            name=name,
            classifications=classifications,
        )

        classifications_keyword_group.additional_properties = d
        return classifications_keyword_group

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
