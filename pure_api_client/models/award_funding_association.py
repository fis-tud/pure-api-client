from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.visibility import Visibility


T = TypeVar("T", bound="AwardFundingAssociation")


@_attrs_define
class AwardFundingAssociation:
    """Funding associations exists in two variants, financial or non-financial.

    Attributes:
        type_discriminator (str):
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        cost_code (Union[None, Unset, str]): The cost code of the funding.
        funder (Union['ContentRefType0', None, Unset]):
        internal_funder (Union['ContentRefType0', None, Unset]):
        classifications (Union[Unset, List[Union['ClassificationRefType0', None]]]):
        funding_project_scheme (Union[None, Unset, str]): The funding project scheme of the funding, also known as
            research program.
        visibility (Union[Unset, Visibility]): Visibility of an object
    """

    type_discriminator: str
    pure_id: Union[Unset, int] = UNSET
    cost_code: Union[None, Unset, str] = UNSET
    funder: Union["ContentRefType0", None, Unset] = UNSET
    internal_funder: Union["ContentRefType0", None, Unset] = UNSET
    classifications: Union[Unset, List[Union["ClassificationRefType0", None]]] = UNSET
    funding_project_scheme: Union[None, Unset, str] = UNSET
    visibility: Union[Unset, "Visibility"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0

        type_discriminator = self.type_discriminator

        pure_id = self.pure_id

        cost_code: Union[None, Unset, str]
        if isinstance(self.cost_code, Unset):
            cost_code = UNSET
        else:
            cost_code = self.cost_code

        funder: Union[Dict[str, Any], None, Unset]
        if isinstance(self.funder, Unset):
            funder = UNSET
        elif isinstance(self.funder, ContentRefType0):
            funder = self.funder.to_dict()
        else:
            funder = self.funder

        internal_funder: Union[Dict[str, Any], None, Unset]
        if isinstance(self.internal_funder, Unset):
            internal_funder = UNSET
        elif isinstance(self.internal_funder, ContentRefType0):
            internal_funder = self.internal_funder.to_dict()
        else:
            internal_funder = self.internal_funder

        classifications: Union[Unset, List[Union[Dict[str, Any], None]]] = UNSET
        if not isinstance(self.classifications, Unset):
            classifications = []
            for classifications_item_data in self.classifications:
                classifications_item: Union[Dict[str, Any], None]
                if isinstance(classifications_item_data, ClassificationRefType0):
                    classifications_item = classifications_item_data.to_dict()
                else:
                    classifications_item = classifications_item_data
                classifications.append(classifications_item)

        funding_project_scheme: Union[None, Unset, str]
        if isinstance(self.funding_project_scheme, Unset):
            funding_project_scheme = UNSET
        else:
            funding_project_scheme = self.funding_project_scheme

        visibility: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.visibility, Unset):
            visibility = self.visibility.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "typeDiscriminator": type_discriminator,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if cost_code is not UNSET:
            field_dict["costCode"] = cost_code
        if funder is not UNSET:
            field_dict["funder"] = funder
        if internal_funder is not UNSET:
            field_dict["internalFunder"] = internal_funder
        if classifications is not UNSET:
            field_dict["classifications"] = classifications
        if funding_project_scheme is not UNSET:
            field_dict["fundingProjectScheme"] = funding_project_scheme
        if visibility is not UNSET:
            field_dict["visibility"] = visibility

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.visibility import Visibility

        d = src_dict.copy()
        type_discriminator = d.pop("typeDiscriminator")

        pure_id = d.pop("pureId", UNSET)

        def _parse_cost_code(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        cost_code = _parse_cost_code(d.pop("costCode", UNSET))

        def _parse_funder(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        funder = _parse_funder(d.pop("funder", UNSET))

        def _parse_internal_funder(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        internal_funder = _parse_internal_funder(d.pop("internalFunder", UNSET))

        classifications = []
        _classifications = d.pop("classifications", UNSET)
        for classifications_item_data in _classifications or []:

            def _parse_classifications_item(data: object) -> Union["ClassificationRefType0", None]:
                if data is None:
                    return data
                try:
                    if not isinstance(data, dict):
                        raise TypeError()
                    componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                    return componentsschemas_classification_ref_type_0
                except:  # noqa: E722
                    pass
                return cast(Union["ClassificationRefType0", None], data)

            classifications_item = _parse_classifications_item(classifications_item_data)

            classifications.append(classifications_item)

        def _parse_funding_project_scheme(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        funding_project_scheme = _parse_funding_project_scheme(d.pop("fundingProjectScheme", UNSET))

        _visibility = d.pop("visibility", UNSET)
        visibility: Union[Unset, Visibility]
        if isinstance(_visibility, Unset):
            visibility = UNSET
        else:
            visibility = Visibility.from_dict(_visibility)

        award_funding_association = cls(
            type_discriminator=type_discriminator,
            pure_id=pure_id,
            cost_code=cost_code,
            funder=funder,
            internal_funder=internal_funder,
            classifications=classifications,
            funding_project_scheme=funding_project_scheme,
            visibility=visibility,
        )

        award_funding_association.additional_properties = d
        return award_funding_association

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
