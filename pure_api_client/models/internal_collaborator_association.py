from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.content_ref_type_0 import ContentRefType0


T = TypeVar("T", bound="InternalCollaboratorAssociation")


@_attrs_define
class InternalCollaboratorAssociation:
    """An internal collaborator associated with the content.

    Attributes:
        type_discriminator (str):
        organization (Union['ContentRefType0', None]):
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        type (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        lead_collaborator (Union[None, Unset, bool]): Whether this collaborator is lead collaborator or not.
    """

    type_discriminator: str
    organization: Union["ContentRefType0", None]
    pure_id: Union[Unset, int] = UNSET
    type: Union["ClassificationRefType0", None, Unset] = UNSET
    lead_collaborator: Union[None, Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0

        type_discriminator = self.type_discriminator

        organization: Union[Dict[str, Any], None]
        if isinstance(self.organization, ContentRefType0):
            organization = self.organization.to_dict()
        else:
            organization = self.organization

        pure_id = self.pure_id

        type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.type, Unset):
            type = UNSET
        elif isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        lead_collaborator: Union[None, Unset, bool]
        if isinstance(self.lead_collaborator, Unset):
            lead_collaborator = UNSET
        else:
            lead_collaborator = self.lead_collaborator

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "typeDiscriminator": type_discriminator,
                "organization": organization,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if type is not UNSET:
            field_dict["type"] = type
        if lead_collaborator is not UNSET:
            field_dict["leadCollaborator"] = lead_collaborator

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0

        d = src_dict.copy()
        type_discriminator = d.pop("typeDiscriminator")

        def _parse_organization(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        organization = _parse_organization(d.pop("organization"))

        pure_id = d.pop("pureId", UNSET)

        def _parse_type(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        type = _parse_type(d.pop("type", UNSET))

        def _parse_lead_collaborator(data: object) -> Union[None, Unset, bool]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, bool], data)

        lead_collaborator = _parse_lead_collaborator(d.pop("leadCollaborator", UNSET))

        internal_collaborator_association = cls(
            type_discriminator=type_discriminator,
            organization=organization,
            pure_id=pure_id,
            type=type,
            lead_collaborator=lead_collaborator,
        )

        internal_collaborator_association.additional_properties = d
        return internal_collaborator_association

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
