from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.classified_address import ClassifiedAddress
    from ..models.classified_localized_value import ClassifiedLocalizedValue
    from ..models.classified_value import ClassifiedValue
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.date_range import DateRange
    from ..models.keyword_group import KeywordGroup
    from ..models.person_supervisor_association import PersonSupervisorAssociation


T = TypeVar("T", bound="PersonOrganizationAssociationType0")


@_attrs_define
class PersonOrganizationAssociationType0:
    """
    Attributes:
        type_discriminator (str):
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        addresses (Union[List['ClassifiedAddress'], None, Unset]): List of physical addresses.
        affiliation_id (Union[None, Unset, str]): Affiliation identification. This could be a student ID or a staff ID.
        employment_type (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        organization (Union['ContentRefType0', None, Unset]):
        emails (Union[List['ClassifiedValue'], None, Unset]): List of emails.
        web_addresses (Union[List['ClassifiedLocalizedValue'], None, Unset]): List of web addresses.
        phone_numbers (Union[Unset, List['ClassifiedValue']]):
        period (Union[Unset, DateRange]): A date range
        supervisor_associations (Union[List['PersonSupervisorAssociation'], None, Unset]): List of associations to
            persons who supervise this person.
        keyword_groups (Union[List['KeywordGroup'], None, Unset]): List of keyword groups.
        primary_association (Union[Unset, bool]): A boolean value indicating whether this is the primary association or
            not. Only one of the associations from a person to an organization will be primary.
    """

    type_discriminator: str
    pure_id: Union[Unset, int] = UNSET
    addresses: Union[List["ClassifiedAddress"], None, Unset] = UNSET
    affiliation_id: Union[None, Unset, str] = UNSET
    employment_type: Union["ClassificationRefType0", None, Unset] = UNSET
    organization: Union["ContentRefType0", None, Unset] = UNSET
    emails: Union[List["ClassifiedValue"], None, Unset] = UNSET
    web_addresses: Union[List["ClassifiedLocalizedValue"], None, Unset] = UNSET
    phone_numbers: Union[Unset, List["ClassifiedValue"]] = UNSET
    period: Union[Unset, "DateRange"] = UNSET
    supervisor_associations: Union[List["PersonSupervisorAssociation"], None, Unset] = UNSET
    keyword_groups: Union[List["KeywordGroup"], None, Unset] = UNSET
    primary_association: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0

        type_discriminator = self.type_discriminator

        pure_id = self.pure_id

        addresses: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.addresses, Unset):
            addresses = UNSET
        elif isinstance(self.addresses, list):
            addresses = []
            for addresses_type_0_item_data in self.addresses:
                addresses_type_0_item = addresses_type_0_item_data.to_dict()
                addresses.append(addresses_type_0_item)

        else:
            addresses = self.addresses

        affiliation_id: Union[None, Unset, str]
        if isinstance(self.affiliation_id, Unset):
            affiliation_id = UNSET
        else:
            affiliation_id = self.affiliation_id

        employment_type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.employment_type, Unset):
            employment_type = UNSET
        elif isinstance(self.employment_type, ClassificationRefType0):
            employment_type = self.employment_type.to_dict()
        else:
            employment_type = self.employment_type

        organization: Union[Dict[str, Any], None, Unset]
        if isinstance(self.organization, Unset):
            organization = UNSET
        elif isinstance(self.organization, ContentRefType0):
            organization = self.organization.to_dict()
        else:
            organization = self.organization

        emails: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.emails, Unset):
            emails = UNSET
        elif isinstance(self.emails, list):
            emails = []
            for emails_type_0_item_data in self.emails:
                emails_type_0_item = emails_type_0_item_data.to_dict()
                emails.append(emails_type_0_item)

        else:
            emails = self.emails

        web_addresses: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.web_addresses, Unset):
            web_addresses = UNSET
        elif isinstance(self.web_addresses, list):
            web_addresses = []
            for web_addresses_type_0_item_data in self.web_addresses:
                web_addresses_type_0_item = web_addresses_type_0_item_data.to_dict()
                web_addresses.append(web_addresses_type_0_item)

        else:
            web_addresses = self.web_addresses

        phone_numbers: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.phone_numbers, Unset):
            phone_numbers = []
            for phone_numbers_item_data in self.phone_numbers:
                phone_numbers_item = phone_numbers_item_data.to_dict()
                phone_numbers.append(phone_numbers_item)

        period: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.period, Unset):
            period = self.period.to_dict()

        supervisor_associations: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.supervisor_associations, Unset):
            supervisor_associations = UNSET
        elif isinstance(self.supervisor_associations, list):
            supervisor_associations = []
            for supervisor_associations_type_0_item_data in self.supervisor_associations:
                supervisor_associations_type_0_item = supervisor_associations_type_0_item_data.to_dict()
                supervisor_associations.append(supervisor_associations_type_0_item)

        else:
            supervisor_associations = self.supervisor_associations

        keyword_groups: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.keyword_groups, Unset):
            keyword_groups = UNSET
        elif isinstance(self.keyword_groups, list):
            keyword_groups = []
            for keyword_groups_type_0_item_data in self.keyword_groups:
                keyword_groups_type_0_item = keyword_groups_type_0_item_data.to_dict()
                keyword_groups.append(keyword_groups_type_0_item)

        else:
            keyword_groups = self.keyword_groups

        primary_association = self.primary_association

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "typeDiscriminator": type_discriminator,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if addresses is not UNSET:
            field_dict["addresses"] = addresses
        if affiliation_id is not UNSET:
            field_dict["affiliationId"] = affiliation_id
        if employment_type is not UNSET:
            field_dict["employmentType"] = employment_type
        if organization is not UNSET:
            field_dict["organization"] = organization
        if emails is not UNSET:
            field_dict["emails"] = emails
        if web_addresses is not UNSET:
            field_dict["webAddresses"] = web_addresses
        if phone_numbers is not UNSET:
            field_dict["phoneNumbers"] = phone_numbers
        if period is not UNSET:
            field_dict["period"] = period
        if supervisor_associations is not UNSET:
            field_dict["supervisorAssociations"] = supervisor_associations
        if keyword_groups is not UNSET:
            field_dict["keywordGroups"] = keyword_groups
        if primary_association is not UNSET:
            field_dict["primaryAssociation"] = primary_association

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.classified_address import ClassifiedAddress
        from ..models.classified_localized_value import ClassifiedLocalizedValue
        from ..models.classified_value import ClassifiedValue
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.date_range import DateRange
        from ..models.keyword_group import KeywordGroup
        from ..models.person_supervisor_association import PersonSupervisorAssociation

        d = src_dict.copy()
        type_discriminator = d.pop("typeDiscriminator")

        pure_id = d.pop("pureId", UNSET)

        def _parse_addresses(data: object) -> Union[List["ClassifiedAddress"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                addresses_type_0 = []
                _addresses_type_0 = data
                for addresses_type_0_item_data in _addresses_type_0:
                    addresses_type_0_item = ClassifiedAddress.from_dict(addresses_type_0_item_data)

                    addresses_type_0.append(addresses_type_0_item)

                return addresses_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedAddress"], None, Unset], data)

        addresses = _parse_addresses(d.pop("addresses", UNSET))

        def _parse_affiliation_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        affiliation_id = _parse_affiliation_id(d.pop("affiliationId", UNSET))

        def _parse_employment_type(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        employment_type = _parse_employment_type(d.pop("employmentType", UNSET))

        def _parse_organization(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        organization = _parse_organization(d.pop("organization", UNSET))

        def _parse_emails(data: object) -> Union[List["ClassifiedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                emails_type_0 = []
                _emails_type_0 = data
                for emails_type_0_item_data in _emails_type_0:
                    emails_type_0_item = ClassifiedValue.from_dict(emails_type_0_item_data)

                    emails_type_0.append(emails_type_0_item)

                return emails_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedValue"], None, Unset], data)

        emails = _parse_emails(d.pop("emails", UNSET))

        def _parse_web_addresses(data: object) -> Union[List["ClassifiedLocalizedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                web_addresses_type_0 = []
                _web_addresses_type_0 = data
                for web_addresses_type_0_item_data in _web_addresses_type_0:
                    web_addresses_type_0_item = ClassifiedLocalizedValue.from_dict(web_addresses_type_0_item_data)

                    web_addresses_type_0.append(web_addresses_type_0_item)

                return web_addresses_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedLocalizedValue"], None, Unset], data)

        web_addresses = _parse_web_addresses(d.pop("webAddresses", UNSET))

        phone_numbers = []
        _phone_numbers = d.pop("phoneNumbers", UNSET)
        for phone_numbers_item_data in _phone_numbers or []:
            phone_numbers_item = ClassifiedValue.from_dict(phone_numbers_item_data)

            phone_numbers.append(phone_numbers_item)

        _period = d.pop("period", UNSET)
        period: Union[Unset, DateRange]
        if isinstance(_period, Unset):
            period = UNSET
        else:
            period = DateRange.from_dict(_period)

        def _parse_supervisor_associations(data: object) -> Union[List["PersonSupervisorAssociation"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                supervisor_associations_type_0 = []
                _supervisor_associations_type_0 = data
                for supervisor_associations_type_0_item_data in _supervisor_associations_type_0:
                    supervisor_associations_type_0_item = PersonSupervisorAssociation.from_dict(
                        supervisor_associations_type_0_item_data
                    )

                    supervisor_associations_type_0.append(supervisor_associations_type_0_item)

                return supervisor_associations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["PersonSupervisorAssociation"], None, Unset], data)

        supervisor_associations = _parse_supervisor_associations(d.pop("supervisorAssociations", UNSET))

        def _parse_keyword_groups(data: object) -> Union[List["KeywordGroup"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                keyword_groups_type_0 = []
                _keyword_groups_type_0 = data
                for keyword_groups_type_0_item_data in _keyword_groups_type_0:
                    keyword_groups_type_0_item = KeywordGroup.from_dict(keyword_groups_type_0_item_data)

                    keyword_groups_type_0.append(keyword_groups_type_0_item)

                return keyword_groups_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["KeywordGroup"], None, Unset], data)

        keyword_groups = _parse_keyword_groups(d.pop("keywordGroups", UNSET))

        primary_association = d.pop("primaryAssociation", UNSET)

        person_organization_association_type_0 = cls(
            type_discriminator=type_discriminator,
            pure_id=pure_id,
            addresses=addresses,
            affiliation_id=affiliation_id,
            employment_type=employment_type,
            organization=organization,
            emails=emails,
            web_addresses=web_addresses,
            phone_numbers=phone_numbers,
            period=period,
            supervisor_associations=supervisor_associations,
            keyword_groups=keyword_groups,
            primary_association=primary_association,
        )

        person_organization_association_type_0.additional_properties = d
        return person_organization_association_type_0

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
