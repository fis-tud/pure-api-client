from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

if TYPE_CHECKING:
    from ..models.content_ref_type_0 import ContentRefType0


T = TypeVar("T", bound="DataSetStudentThesisAssociation")


@_attrs_define
class DataSetStudentThesisAssociation:
    """An association to a student thesis.

    Attributes:
        student_thesis (Union['ContentRefType0', None]):
    """

    student_thesis: Union["ContentRefType0", None]
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.content_ref_type_0 import ContentRefType0

        student_thesis: Union[Dict[str, Any], None]
        if isinstance(self.student_thesis, ContentRefType0):
            student_thesis = self.student_thesis.to_dict()
        else:
            student_thesis = self.student_thesis

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "studentThesis": student_thesis,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.content_ref_type_0 import ContentRefType0

        d = src_dict.copy()

        def _parse_student_thesis(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        student_thesis = _parse_student_thesis(d.pop("studentThesis"))

        data_set_student_thesis_association = cls(
            student_thesis=student_thesis,
        )

        data_set_student_thesis_association.additional_properties = d
        return data_set_student_thesis_association

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
