import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.date_range import DateRange
    from ..models.electronic_version_file import ElectronicVersionFile


T = TypeVar("T", bound="AdditionalFileElectronicVersion")


@_attrs_define
class AdditionalFileElectronicVersion:
    """An additional file related to a research output.

    Attributes:
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        access_type (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        embargo_period (Union[Unset, DateRange]): A date range
        license_type (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        user_defined_license (Union[None, Unset, str]): License defined by the user.
        visible_on_portal_date (Union[Unset, datetime.date]): Date where this document is/will be visible on the portal.
        creator (Union[Unset, str]): Username of creator.
        created (Union[Unset, datetime.datetime]): Date and time of creation.
        title (Union[None, Unset, str]): The title of the file.
        file (Union[Unset, ElectronicVersionFile]): Information about an electronic version file
    """

    pure_id: Union[Unset, int] = UNSET
    access_type: Union["ClassificationRefType0", None, Unset] = UNSET
    embargo_period: Union[Unset, "DateRange"] = UNSET
    license_type: Union["ClassificationRefType0", None, Unset] = UNSET
    user_defined_license: Union[None, Unset, str] = UNSET
    visible_on_portal_date: Union[Unset, datetime.date] = UNSET
    creator: Union[Unset, str] = UNSET
    created: Union[Unset, datetime.datetime] = UNSET
    title: Union[None, Unset, str] = UNSET
    file: Union[Unset, "ElectronicVersionFile"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0

        pure_id = self.pure_id

        access_type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.access_type, Unset):
            access_type = UNSET
        elif isinstance(self.access_type, ClassificationRefType0):
            access_type = self.access_type.to_dict()
        else:
            access_type = self.access_type

        embargo_period: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.embargo_period, Unset):
            embargo_period = self.embargo_period.to_dict()

        license_type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.license_type, Unset):
            license_type = UNSET
        elif isinstance(self.license_type, ClassificationRefType0):
            license_type = self.license_type.to_dict()
        else:
            license_type = self.license_type

        user_defined_license: Union[None, Unset, str]
        if isinstance(self.user_defined_license, Unset):
            user_defined_license = UNSET
        else:
            user_defined_license = self.user_defined_license

        visible_on_portal_date: Union[Unset, str] = UNSET
        if not isinstance(self.visible_on_portal_date, Unset):
            visible_on_portal_date = self.visible_on_portal_date.isoformat()

        creator = self.creator

        created: Union[Unset, str] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat()

        title: Union[None, Unset, str]
        if isinstance(self.title, Unset):
            title = UNSET
        else:
            title = self.title

        file: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.file, Unset):
            file = self.file.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if access_type is not UNSET:
            field_dict["accessType"] = access_type
        if embargo_period is not UNSET:
            field_dict["embargoPeriod"] = embargo_period
        if license_type is not UNSET:
            field_dict["licenseType"] = license_type
        if user_defined_license is not UNSET:
            field_dict["userDefinedLicense"] = user_defined_license
        if visible_on_portal_date is not UNSET:
            field_dict["visibleOnPortalDate"] = visible_on_portal_date
        if creator is not UNSET:
            field_dict["creator"] = creator
        if created is not UNSET:
            field_dict["created"] = created
        if title is not UNSET:
            field_dict["title"] = title
        if file is not UNSET:
            field_dict["file"] = file

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.date_range import DateRange
        from ..models.electronic_version_file import ElectronicVersionFile

        d = src_dict.copy()
        pure_id = d.pop("pureId", UNSET)

        def _parse_access_type(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        access_type = _parse_access_type(d.pop("accessType", UNSET))

        _embargo_period = d.pop("embargoPeriod", UNSET)
        embargo_period: Union[Unset, DateRange]
        if isinstance(_embargo_period, Unset):
            embargo_period = UNSET
        else:
            embargo_period = DateRange.from_dict(_embargo_period)

        def _parse_license_type(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        license_type = _parse_license_type(d.pop("licenseType", UNSET))

        def _parse_user_defined_license(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        user_defined_license = _parse_user_defined_license(d.pop("userDefinedLicense", UNSET))

        _visible_on_portal_date = d.pop("visibleOnPortalDate", UNSET)
        visible_on_portal_date: Union[Unset, datetime.date]
        if isinstance(_visible_on_portal_date, Unset):
            visible_on_portal_date = UNSET
        else:
            visible_on_portal_date = isoparse(_visible_on_portal_date).date()

        creator = d.pop("creator", UNSET)

        _created = d.pop("created", UNSET)
        created: Union[Unset, datetime.datetime]
        if isinstance(_created, Unset):
            created = UNSET
        else:
            created = isoparse(_created)

        def _parse_title(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        title = _parse_title(d.pop("title", UNSET))

        _file = d.pop("file", UNSET)
        file: Union[Unset, ElectronicVersionFile]
        if isinstance(_file, Unset):
            file = UNSET
        else:
            file = ElectronicVersionFile.from_dict(_file)

        additional_file_electronic_version = cls(
            pure_id=pure_id,
            access_type=access_type,
            embargo_period=embargo_period,
            license_type=license_type,
            user_defined_license=user_defined_license,
            visible_on_portal_date=visible_on_portal_date,
            creator=creator,
            created=created,
            title=title,
            file=file,
        )

        additional_file_electronic_version.additional_properties = d
        return additional_file_electronic_version

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
