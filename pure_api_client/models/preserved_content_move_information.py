import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.preserved_file_move_information import PreservedFileMoveInformation


T = TypeVar("T", bound="PreservedContentMoveInformation")


@_attrs_define
class PreservedContentMoveInformation:
    """
    Attributes:
        target_id (Union[Unset, int]):
        source_id (Union[Unset, int]):
        metadata_update_required (Union[Unset, bool]):
        preserved_file_move_information (Union[Unset, List['PreservedFileMoveInformation']]):
        uuid (Union[Unset, str]):
        created (Union[Unset, datetime.datetime]):
        creator (Union[Unset, str]):
        modified (Union[Unset, datetime.datetime]):
        modified_by (Union[Unset, str]):
        new_content (Union[Unset, bool]):
        replaced_content_id (Union[Unset, int]):
        id (Union[Unset, int]):
        version (Union[Unset, int]):
    """

    target_id: Union[Unset, int] = UNSET
    source_id: Union[Unset, int] = UNSET
    metadata_update_required: Union[Unset, bool] = UNSET
    preserved_file_move_information: Union[Unset, List["PreservedFileMoveInformation"]] = UNSET
    uuid: Union[Unset, str] = UNSET
    created: Union[Unset, datetime.datetime] = UNSET
    creator: Union[Unset, str] = UNSET
    modified: Union[Unset, datetime.datetime] = UNSET
    modified_by: Union[Unset, str] = UNSET
    new_content: Union[Unset, bool] = UNSET
    replaced_content_id: Union[Unset, int] = UNSET
    id: Union[Unset, int] = UNSET
    version: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        target_id = self.target_id

        source_id = self.source_id

        metadata_update_required = self.metadata_update_required

        preserved_file_move_information: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.preserved_file_move_information, Unset):
            preserved_file_move_information = []
            for preserved_file_move_information_item_data in self.preserved_file_move_information:
                preserved_file_move_information_item = preserved_file_move_information_item_data.to_dict()
                preserved_file_move_information.append(preserved_file_move_information_item)

        uuid = self.uuid

        created: Union[Unset, str] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat()

        creator = self.creator

        modified: Union[Unset, str] = UNSET
        if not isinstance(self.modified, Unset):
            modified = self.modified.isoformat()

        modified_by = self.modified_by

        new_content = self.new_content

        replaced_content_id = self.replaced_content_id

        id = self.id

        version = self.version

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if target_id is not UNSET:
            field_dict["targetId"] = target_id
        if source_id is not UNSET:
            field_dict["sourceId"] = source_id
        if metadata_update_required is not UNSET:
            field_dict["metadataUpdateRequired"] = metadata_update_required
        if preserved_file_move_information is not UNSET:
            field_dict["preservedFileMoveInformation"] = preserved_file_move_information
        if uuid is not UNSET:
            field_dict["uuid"] = uuid
        if created is not UNSET:
            field_dict["created"] = created
        if creator is not UNSET:
            field_dict["creator"] = creator
        if modified is not UNSET:
            field_dict["modified"] = modified
        if modified_by is not UNSET:
            field_dict["modifiedBy"] = modified_by
        if new_content is not UNSET:
            field_dict["newContent"] = new_content
        if replaced_content_id is not UNSET:
            field_dict["replacedContentId"] = replaced_content_id
        if id is not UNSET:
            field_dict["id"] = id
        if version is not UNSET:
            field_dict["version"] = version

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.preserved_file_move_information import PreservedFileMoveInformation

        d = src_dict.copy()
        target_id = d.pop("targetId", UNSET)

        source_id = d.pop("sourceId", UNSET)

        metadata_update_required = d.pop("metadataUpdateRequired", UNSET)

        preserved_file_move_information = []
        _preserved_file_move_information = d.pop("preservedFileMoveInformation", UNSET)
        for preserved_file_move_information_item_data in _preserved_file_move_information or []:
            preserved_file_move_information_item = PreservedFileMoveInformation.from_dict(
                preserved_file_move_information_item_data
            )

            preserved_file_move_information.append(preserved_file_move_information_item)

        uuid = d.pop("uuid", UNSET)

        _created = d.pop("created", UNSET)
        created: Union[Unset, datetime.datetime]
        if isinstance(_created, Unset):
            created = UNSET
        else:
            created = isoparse(_created)

        creator = d.pop("creator", UNSET)

        _modified = d.pop("modified", UNSET)
        modified: Union[Unset, datetime.datetime]
        if isinstance(_modified, Unset):
            modified = UNSET
        else:
            modified = isoparse(_modified)

        modified_by = d.pop("modifiedBy", UNSET)

        new_content = d.pop("newContent", UNSET)

        replaced_content_id = d.pop("replacedContentId", UNSET)

        id = d.pop("id", UNSET)

        version = d.pop("version", UNSET)

        preserved_content_move_information = cls(
            target_id=target_id,
            source_id=source_id,
            metadata_update_required=metadata_update_required,
            preserved_file_move_information=preserved_file_move_information,
            uuid=uuid,
            created=created,
            creator=creator,
            modified=modified,
            modified_by=modified_by,
            new_content=new_content,
            replaced_content_id=replaced_content_id,
            id=id,
            version=version,
        )

        preserved_content_move_information.additional_properties = d
        return preserved_content_move_information

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
