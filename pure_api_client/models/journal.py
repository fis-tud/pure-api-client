import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.additional_issn_type_0 import AdditionalISSNType0
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.identifier import Identifier
    from ..models.issn import ISSN
    from ..models.journal_title import JournalTitle
    from ..models.keyword_group import KeywordGroup
    from ..models.link import Link
    from ..models.sherpa_romeo_policy import SherpaRomeoPolicy
    from ..models.workflow import Workflow


T = TypeVar("T", bound="Journal")


@_attrs_define
class Journal:
    """A periodical presenting articles on a particular subject.

    Attributes:
        titles (List['JournalTitle']): Titles of the journal, these titles will be available for use when relating the
            journal to other content.
        type (Union['ClassificationRefType0', None]): A reference to a classification value
        workflow (Workflow): Information about workflow
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        uuid (Union[Unset, str]): UUID, this is the primary identity of the entity
        created_by (Union[Unset, str]): Username of creator
        created_date (Union[Unset, datetime.datetime]): Date and time of creation
        modified_by (Union[Unset, str]): Username of the user that performed a modification
        modified_date (Union[Unset, datetime.datetime]): Date and time of last modification
        portal_url (Union[Unset, str]): URL of the content on the Pure Portal
        pretty_url_identifiers (Union[Unset, List[str]]): All pretty URLs
        previous_uuids (Union[Unset, List[str]]): UUIDs of other content items which have been merged into this content
            item (or similar)
        version (Union[None, Unset, str]): Used to guard against conflicting updates. For new content this is null, and
            for existing content the current value. The property should never be modified by a client, except in the rare
            case where the client wants to perform an update irrespective of if other clients have made updates in the
            meantime, also known as a "dirty write". A dirty write is performed by not including the property value or
            setting the property to null
        issns (Union[List['ISSN'], None, Unset]): International Standard Serial Numbers of the journal, ISSNs in this
            list are searchable and selectable when relating the journal to other content
        additional_searchable_issns (Union[List[Union['AdditionalISSNType0', None]], None, Unset]): Additional
            International Standard Serial Numbers of the journal, ISSNs in this list are searchable but not selectable when
            relating the journal to other content
        country (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        indexed_in_doaj (Union[Unset, bool]): Whether the journal is present in the Directory of Open Access Journals
        keyword_groups (Union[List['KeywordGroup'], None, Unset]): Keywords associated with the journal
        links (Union[List['Link'], None, Unset]): Links to websites or other URLs associated with the journal
        publisher (Union['ContentRefType0', None, Unset]):
        identifiers (Union[List['Identifier'], None, Unset]): Identifiers of the Journal, often used to identify the
            journal in other sources, an example could be the Scopus Id of the journal
        additional_searchable_titles (Union[List[str], None, Unset]): Additional searchable titles of the journal, often
            abbreviations that are not used to show a journal, cannot be used when relating the journal to other content.
        sherpa_romeo_policy (Union[Unset, SherpaRomeoPolicy]): Describes a SHERPA RoMEO open access policy.
        additional_open_access_information (Union[None, Unset, str]): Additional open access information for this
            journal.
        system_name (Union[Unset, str]): The content system name
    """

    titles: List["JournalTitle"]
    type: Union["ClassificationRefType0", None]
    workflow: "Workflow"
    pure_id: Union[Unset, int] = UNSET
    uuid: Union[Unset, str] = UNSET
    created_by: Union[Unset, str] = UNSET
    created_date: Union[Unset, datetime.datetime] = UNSET
    modified_by: Union[Unset, str] = UNSET
    modified_date: Union[Unset, datetime.datetime] = UNSET
    portal_url: Union[Unset, str] = UNSET
    pretty_url_identifiers: Union[Unset, List[str]] = UNSET
    previous_uuids: Union[Unset, List[str]] = UNSET
    version: Union[None, Unset, str] = UNSET
    issns: Union[List["ISSN"], None, Unset] = UNSET
    additional_searchable_issns: Union[List[Union["AdditionalISSNType0", None]], None, Unset] = UNSET
    country: Union["ClassificationRefType0", None, Unset] = UNSET
    indexed_in_doaj: Union[Unset, bool] = UNSET
    keyword_groups: Union[List["KeywordGroup"], None, Unset] = UNSET
    links: Union[List["Link"], None, Unset] = UNSET
    publisher: Union["ContentRefType0", None, Unset] = UNSET
    identifiers: Union[List["Identifier"], None, Unset] = UNSET
    additional_searchable_titles: Union[List[str], None, Unset] = UNSET
    sherpa_romeo_policy: Union[Unset, "SherpaRomeoPolicy"] = UNSET
    additional_open_access_information: Union[None, Unset, str] = UNSET
    system_name: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.additional_issn_type_0 import AdditionalISSNType0
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0

        titles = []
        for titles_item_data in self.titles:
            titles_item = titles_item_data.to_dict()
            titles.append(titles_item)

        type: Union[Dict[str, Any], None]
        if isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        workflow = self.workflow.to_dict()

        pure_id = self.pure_id

        uuid = self.uuid

        created_by = self.created_by

        created_date: Union[Unset, str] = UNSET
        if not isinstance(self.created_date, Unset):
            created_date = self.created_date.isoformat()

        modified_by = self.modified_by

        modified_date: Union[Unset, str] = UNSET
        if not isinstance(self.modified_date, Unset):
            modified_date = self.modified_date.isoformat()

        portal_url = self.portal_url

        pretty_url_identifiers: Union[Unset, List[str]] = UNSET
        if not isinstance(self.pretty_url_identifiers, Unset):
            pretty_url_identifiers = self.pretty_url_identifiers

        previous_uuids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.previous_uuids, Unset):
            previous_uuids = self.previous_uuids

        version: Union[None, Unset, str]
        if isinstance(self.version, Unset):
            version = UNSET
        else:
            version = self.version

        issns: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.issns, Unset):
            issns = UNSET
        elif isinstance(self.issns, list):
            issns = []
            for issns_type_0_item_data in self.issns:
                issns_type_0_item = issns_type_0_item_data.to_dict()
                issns.append(issns_type_0_item)

        else:
            issns = self.issns

        additional_searchable_issns: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.additional_searchable_issns, Unset):
            additional_searchable_issns = UNSET
        elif isinstance(self.additional_searchable_issns, list):
            additional_searchable_issns = []
            for additional_searchable_issns_type_0_item_data in self.additional_searchable_issns:
                additional_searchable_issns_type_0_item: Union[Dict[str, Any], None]
                if isinstance(additional_searchable_issns_type_0_item_data, AdditionalISSNType0):
                    additional_searchable_issns_type_0_item = additional_searchable_issns_type_0_item_data.to_dict()
                else:
                    additional_searchable_issns_type_0_item = additional_searchable_issns_type_0_item_data
                additional_searchable_issns.append(additional_searchable_issns_type_0_item)

        else:
            additional_searchable_issns = self.additional_searchable_issns

        country: Union[Dict[str, Any], None, Unset]
        if isinstance(self.country, Unset):
            country = UNSET
        elif isinstance(self.country, ClassificationRefType0):
            country = self.country.to_dict()
        else:
            country = self.country

        indexed_in_doaj = self.indexed_in_doaj

        keyword_groups: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.keyword_groups, Unset):
            keyword_groups = UNSET
        elif isinstance(self.keyword_groups, list):
            keyword_groups = []
            for keyword_groups_type_0_item_data in self.keyword_groups:
                keyword_groups_type_0_item = keyword_groups_type_0_item_data.to_dict()
                keyword_groups.append(keyword_groups_type_0_item)

        else:
            keyword_groups = self.keyword_groups

        links: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.links, Unset):
            links = UNSET
        elif isinstance(self.links, list):
            links = []
            for links_type_0_item_data in self.links:
                links_type_0_item = links_type_0_item_data.to_dict()
                links.append(links_type_0_item)

        else:
            links = self.links

        publisher: Union[Dict[str, Any], None, Unset]
        if isinstance(self.publisher, Unset):
            publisher = UNSET
        elif isinstance(self.publisher, ContentRefType0):
            publisher = self.publisher.to_dict()
        else:
            publisher = self.publisher

        identifiers: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.identifiers, Unset):
            identifiers = UNSET
        elif isinstance(self.identifiers, list):
            identifiers = []
            for identifiers_type_0_item_data in self.identifiers:
                identifiers_type_0_item = identifiers_type_0_item_data.to_dict()
                identifiers.append(identifiers_type_0_item)

        else:
            identifiers = self.identifiers

        additional_searchable_titles: Union[List[str], None, Unset]
        if isinstance(self.additional_searchable_titles, Unset):
            additional_searchable_titles = UNSET
        elif isinstance(self.additional_searchable_titles, list):
            additional_searchable_titles = self.additional_searchable_titles

        else:
            additional_searchable_titles = self.additional_searchable_titles

        sherpa_romeo_policy: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.sherpa_romeo_policy, Unset):
            sherpa_romeo_policy = self.sherpa_romeo_policy.to_dict()

        additional_open_access_information: Union[None, Unset, str]
        if isinstance(self.additional_open_access_information, Unset):
            additional_open_access_information = UNSET
        else:
            additional_open_access_information = self.additional_open_access_information

        system_name = self.system_name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "titles": titles,
                "type": type,
                "workflow": workflow,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if uuid is not UNSET:
            field_dict["uuid"] = uuid
        if created_by is not UNSET:
            field_dict["createdBy"] = created_by
        if created_date is not UNSET:
            field_dict["createdDate"] = created_date
        if modified_by is not UNSET:
            field_dict["modifiedBy"] = modified_by
        if modified_date is not UNSET:
            field_dict["modifiedDate"] = modified_date
        if portal_url is not UNSET:
            field_dict["portalUrl"] = portal_url
        if pretty_url_identifiers is not UNSET:
            field_dict["prettyUrlIdentifiers"] = pretty_url_identifiers
        if previous_uuids is not UNSET:
            field_dict["previousUuids"] = previous_uuids
        if version is not UNSET:
            field_dict["version"] = version
        if issns is not UNSET:
            field_dict["issns"] = issns
        if additional_searchable_issns is not UNSET:
            field_dict["additionalSearchableIssns"] = additional_searchable_issns
        if country is not UNSET:
            field_dict["country"] = country
        if indexed_in_doaj is not UNSET:
            field_dict["indexedInDoaj"] = indexed_in_doaj
        if keyword_groups is not UNSET:
            field_dict["keywordGroups"] = keyword_groups
        if links is not UNSET:
            field_dict["links"] = links
        if publisher is not UNSET:
            field_dict["publisher"] = publisher
        if identifiers is not UNSET:
            field_dict["identifiers"] = identifiers
        if additional_searchable_titles is not UNSET:
            field_dict["additionalSearchableTitles"] = additional_searchable_titles
        if sherpa_romeo_policy is not UNSET:
            field_dict["sherpaRomeoPolicy"] = sherpa_romeo_policy
        if additional_open_access_information is not UNSET:
            field_dict["additionalOpenAccessInformation"] = additional_open_access_information
        if system_name is not UNSET:
            field_dict["systemName"] = system_name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.additional_issn_type_0 import AdditionalISSNType0
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.identifier import Identifier
        from ..models.issn import ISSN
        from ..models.journal_title import JournalTitle
        from ..models.keyword_group import KeywordGroup
        from ..models.link import Link
        from ..models.sherpa_romeo_policy import SherpaRomeoPolicy
        from ..models.workflow import Workflow

        d = src_dict.copy()
        titles = []
        _titles = d.pop("titles")
        for titles_item_data in _titles:
            titles_item = JournalTitle.from_dict(titles_item_data)

            titles.append(titles_item)

        def _parse_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        type = _parse_type(d.pop("type"))

        workflow = Workflow.from_dict(d.pop("workflow"))

        pure_id = d.pop("pureId", UNSET)

        uuid = d.pop("uuid", UNSET)

        created_by = d.pop("createdBy", UNSET)

        _created_date = d.pop("createdDate", UNSET)
        created_date: Union[Unset, datetime.datetime]
        if isinstance(_created_date, Unset):
            created_date = UNSET
        else:
            created_date = isoparse(_created_date)

        modified_by = d.pop("modifiedBy", UNSET)

        _modified_date = d.pop("modifiedDate", UNSET)
        modified_date: Union[Unset, datetime.datetime]
        if isinstance(_modified_date, Unset):
            modified_date = UNSET
        else:
            modified_date = isoparse(_modified_date)

        portal_url = d.pop("portalUrl", UNSET)

        pretty_url_identifiers = cast(List[str], d.pop("prettyUrlIdentifiers", UNSET))

        previous_uuids = cast(List[str], d.pop("previousUuids", UNSET))

        def _parse_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        version = _parse_version(d.pop("version", UNSET))

        def _parse_issns(data: object) -> Union[List["ISSN"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                issns_type_0 = []
                _issns_type_0 = data
                for issns_type_0_item_data in _issns_type_0:
                    issns_type_0_item = ISSN.from_dict(issns_type_0_item_data)

                    issns_type_0.append(issns_type_0_item)

                return issns_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ISSN"], None, Unset], data)

        issns = _parse_issns(d.pop("issns", UNSET))

        def _parse_additional_searchable_issns(
            data: object,
        ) -> Union[List[Union["AdditionalISSNType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                additional_searchable_issns_type_0 = []
                _additional_searchable_issns_type_0 = data
                for additional_searchable_issns_type_0_item_data in _additional_searchable_issns_type_0:

                    def _parse_additional_searchable_issns_type_0_item(
                        data: object,
                    ) -> Union["AdditionalISSNType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_additional_issn_type_0 = AdditionalISSNType0.from_dict(data)

                            return componentsschemas_additional_issn_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["AdditionalISSNType0", None], data)

                    additional_searchable_issns_type_0_item = _parse_additional_searchable_issns_type_0_item(
                        additional_searchable_issns_type_0_item_data
                    )

                    additional_searchable_issns_type_0.append(additional_searchable_issns_type_0_item)

                return additional_searchable_issns_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["AdditionalISSNType0", None]], None, Unset], data)

        additional_searchable_issns = _parse_additional_searchable_issns(d.pop("additionalSearchableIssns", UNSET))

        def _parse_country(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        country = _parse_country(d.pop("country", UNSET))

        indexed_in_doaj = d.pop("indexedInDoaj", UNSET)

        def _parse_keyword_groups(data: object) -> Union[List["KeywordGroup"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                keyword_groups_type_0 = []
                _keyword_groups_type_0 = data
                for keyword_groups_type_0_item_data in _keyword_groups_type_0:
                    keyword_groups_type_0_item = KeywordGroup.from_dict(keyword_groups_type_0_item_data)

                    keyword_groups_type_0.append(keyword_groups_type_0_item)

                return keyword_groups_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["KeywordGroup"], None, Unset], data)

        keyword_groups = _parse_keyword_groups(d.pop("keywordGroups", UNSET))

        def _parse_links(data: object) -> Union[List["Link"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                links_type_0 = []
                _links_type_0 = data
                for links_type_0_item_data in _links_type_0:
                    links_type_0_item = Link.from_dict(links_type_0_item_data)

                    links_type_0.append(links_type_0_item)

                return links_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Link"], None, Unset], data)

        links = _parse_links(d.pop("links", UNSET))

        def _parse_publisher(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        publisher = _parse_publisher(d.pop("publisher", UNSET))

        def _parse_identifiers(data: object) -> Union[List["Identifier"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                identifiers_type_0 = []
                _identifiers_type_0 = data
                for identifiers_type_0_item_data in _identifiers_type_0:
                    identifiers_type_0_item = Identifier.from_dict(identifiers_type_0_item_data)

                    identifiers_type_0.append(identifiers_type_0_item)

                return identifiers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Identifier"], None, Unset], data)

        identifiers = _parse_identifiers(d.pop("identifiers", UNSET))

        def _parse_additional_searchable_titles(data: object) -> Union[List[str], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                additional_searchable_titles_type_0 = cast(List[str], data)

                return additional_searchable_titles_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[str], None, Unset], data)

        additional_searchable_titles = _parse_additional_searchable_titles(d.pop("additionalSearchableTitles", UNSET))

        _sherpa_romeo_policy = d.pop("sherpaRomeoPolicy", UNSET)
        sherpa_romeo_policy: Union[Unset, SherpaRomeoPolicy]
        if isinstance(_sherpa_romeo_policy, Unset):
            sherpa_romeo_policy = UNSET
        else:
            sherpa_romeo_policy = SherpaRomeoPolicy.from_dict(_sherpa_romeo_policy)

        def _parse_additional_open_access_information(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        additional_open_access_information = _parse_additional_open_access_information(
            d.pop("additionalOpenAccessInformation", UNSET)
        )

        system_name = d.pop("systemName", UNSET)

        journal = cls(
            titles=titles,
            type=type,
            workflow=workflow,
            pure_id=pure_id,
            uuid=uuid,
            created_by=created_by,
            created_date=created_date,
            modified_by=modified_by,
            modified_date=modified_date,
            portal_url=portal_url,
            pretty_url_identifiers=pretty_url_identifiers,
            previous_uuids=previous_uuids,
            version=version,
            issns=issns,
            additional_searchable_issns=additional_searchable_issns,
            country=country,
            indexed_in_doaj=indexed_in_doaj,
            keyword_groups=keyword_groups,
            links=links,
            publisher=publisher,
            identifiers=identifiers,
            additional_searchable_titles=additional_searchable_titles,
            sherpa_romeo_policy=sherpa_romeo_policy,
            additional_open_access_information=additional_open_access_information,
            system_name=system_name,
        )

        journal.additional_properties = d
        return journal

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
