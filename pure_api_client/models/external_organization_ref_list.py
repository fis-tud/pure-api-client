from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.content_ref_type_0 import ContentRefType0


T = TypeVar("T", bound="ExternalOrganizationRefList")


@_attrs_define
class ExternalOrganizationRefList:
    """List of references to organizations external to the institution.

    Attributes:
        items (Union[Unset, List[Union['ContentRefType0', None]]]): External organization references
    """

    items: Union[Unset, List[Union["ContentRefType0", None]]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.content_ref_type_0 import ContentRefType0

        items: Union[Unset, List[Union[Dict[str, Any], None]]] = UNSET
        if not isinstance(self.items, Unset):
            items = []
            for items_item_data in self.items:
                items_item: Union[Dict[str, Any], None]
                if isinstance(items_item_data, ContentRefType0):
                    items_item = items_item_data.to_dict()
                else:
                    items_item = items_item_data
                items.append(items_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if items is not UNSET:
            field_dict["items"] = items

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.content_ref_type_0 import ContentRefType0

        d = src_dict.copy()
        items = []
        _items = d.pop("items", UNSET)
        for items_item_data in _items or []:

            def _parse_items_item(data: object) -> Union["ContentRefType0", None]:
                if data is None:
                    return data
                try:
                    if not isinstance(data, dict):
                        raise TypeError()
                    componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                    return componentsschemas_content_ref_type_0
                except:  # noqa: E722
                    pass
                return cast(Union["ContentRefType0", None], data)

            items_item = _parse_items_item(items_item_data)

            items.append(items_item)

        external_organization_ref_list = cls(
            items=items,
        )

        external_organization_ref_list.additional_properties = d
        return external_organization_ref_list

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
