from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.year_month_type_0 import YearMonthType0


T = TypeVar("T", bound="ResearcherCommitmentType0")


@_attrs_define
class ResearcherCommitmentType0:
    """Planned and actual research commitment.

    Attributes:
        year_month (Union['YearMonthType0', None]): A date that can be defined by only year and month
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        actual_researcher_commitment (Union[None, Unset, float]): The actual researcher commitment.
        planned_researcher_commitment (Union[None, Unset, float]): The planned researcher commitment.
    """

    year_month: Union["YearMonthType0", None]
    pure_id: Union[Unset, int] = UNSET
    actual_researcher_commitment: Union[None, Unset, float] = UNSET
    planned_researcher_commitment: Union[None, Unset, float] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.year_month_type_0 import YearMonthType0

        year_month: Union[Dict[str, Any], None]
        if isinstance(self.year_month, YearMonthType0):
            year_month = self.year_month.to_dict()
        else:
            year_month = self.year_month

        pure_id = self.pure_id

        actual_researcher_commitment: Union[None, Unset, float]
        if isinstance(self.actual_researcher_commitment, Unset):
            actual_researcher_commitment = UNSET
        else:
            actual_researcher_commitment = self.actual_researcher_commitment

        planned_researcher_commitment: Union[None, Unset, float]
        if isinstance(self.planned_researcher_commitment, Unset):
            planned_researcher_commitment = UNSET
        else:
            planned_researcher_commitment = self.planned_researcher_commitment

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "yearMonth": year_month,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if actual_researcher_commitment is not UNSET:
            field_dict["actualResearcherCommitment"] = actual_researcher_commitment
        if planned_researcher_commitment is not UNSET:
            field_dict["plannedResearcherCommitment"] = planned_researcher_commitment

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.year_month_type_0 import YearMonthType0

        d = src_dict.copy()

        def _parse_year_month(data: object) -> Union["YearMonthType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_year_month_type_0 = YearMonthType0.from_dict(data)

                return componentsschemas_year_month_type_0
            except:  # noqa: E722
                pass
            return cast(Union["YearMonthType0", None], data)

        year_month = _parse_year_month(d.pop("yearMonth"))

        pure_id = d.pop("pureId", UNSET)

        def _parse_actual_researcher_commitment(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        actual_researcher_commitment = _parse_actual_researcher_commitment(d.pop("actualResearcherCommitment", UNSET))

        def _parse_planned_researcher_commitment(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        planned_researcher_commitment = _parse_planned_researcher_commitment(
            d.pop("plannedResearcherCommitment", UNSET)
        )

        researcher_commitment_type_0 = cls(
            year_month=year_month,
            pure_id=pure_id,
            actual_researcher_commitment=actual_researcher_commitment,
            planned_researcher_commitment=planned_researcher_commitment,
        )

        researcher_commitment_type_0.additional_properties = d
        return researcher_commitment_type_0

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
