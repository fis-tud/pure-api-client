import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.classified_address import ClassifiedAddress
    from ..models.classified_localized_value import ClassifiedLocalizedValue
    from ..models.classified_value import ClassifiedValue
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
    from ..models.date_range import DateRange
    from ..models.identifier import Identifier
    from ..models.image_file import ImageFile
    from ..models.keyword_group import KeywordGroup
    from ..models.link import Link
    from ..models.localized_string_type_0 import LocalizedStringType0
    from ..models.visibility import Visibility


T = TypeVar("T", bound="Organization")


@_attrs_define
class Organization:
    """An organization in the institution

    Attributes:
        name (Union['LocalizedStringType0', None]): A set of string values, one for each submission locale. Note:
            invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        type (Union['ClassificationRefType0', None]): A reference to a classification value
        lifecycle (DateRange): A date range
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        uuid (Union[Unset, str]): UUID, this is the primary identity of the entity
        created_by (Union[Unset, str]): Username of creator
        created_date (Union[Unset, datetime.datetime]): Date and time of creation
        modified_by (Union[Unset, str]): Username of the user that performed a modification
        modified_date (Union[Unset, datetime.datetime]): Date and time of last modification
        portal_url (Union[Unset, str]): URL of the content on the Pure Portal
        pretty_url_identifiers (Union[Unset, List[str]]): All pretty URLs
        previous_uuids (Union[Unset, List[str]]): UUIDs of other content items which have been merged into this content
            item (or similar)
        version (Union[None, Unset, str]): Used to guard against conflicting updates. For new content this is null, and
            for existing content the current value. The property should never be modified by a client, except in the rare
            case where the client wants to perform an update irrespective of if other clients have made updates in the
            meantime, also known as a "dirty write". A dirty write is performed by not including the property value or
            setting the property to null
        identifiers (Union[List['Identifier'], None, Unset]): IDs that this object corresponds to in external systems.
            Such as a Scopus ID. Used by Pure where it is necessary to identify objects to specific external systems
        name_variants (Union[List['ClassifiedLocalizedValue'], None, Unset]): A list of organization name variants
        profile_informations (Union[List['ClassifiedLocalizedValue'], None, Unset]): A list of organization profile
            information entries
        photos (Union[List['ImageFile'], None, Unset]): A list of organization photos
        addresses (Union[List['ClassifiedAddress'], None, Unset]): A list of organization addresses
        phone_numbers (Union[List['ClassifiedValue'], None, Unset]): A list of organization phone numbers
        emails (Union[List['ClassifiedValue'], None, Unset]): A list of organization email addresses
        web_addresses (Union[List['ClassifiedLocalizedValue'], None, Unset]): A list of organization web addresses
        taken_over_by (Union['ContentRefType0', None, Unset]):
        parents (Union[List[Union['ContentRefType0', None]], None, Unset]): A list of parent organizations
        contact_persons (Union[List[Union['ContentRefType0', None]], None, Unset]): A list of organization contact
            persons
        keyword_groups (Union[List['KeywordGroup'], None, Unset]): A group for each type of keyword present
        cost_centers (Union[List[Union['ClassificationRefType0', None]], None, Unset]): A list of cost center
            classifications
        visibility (Union[Unset, Visibility]): Visibility of an object
        custom_defined_fields (Union['CustomDefinedFieldsType0', None, Unset]): Map of CustomDefinedField values, where
            the key is the field identifier Example: { "fieldName1": "typeDiscriminator": "Integer", "value" : 1}.
        links (Union[List['Link'], None, Unset]): Links associated with this organization.
        main_research_area (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        system_name (Union[Unset, str]): The content system name
    """

    name: Union["LocalizedStringType0", None]
    type: Union["ClassificationRefType0", None]
    lifecycle: "DateRange"
    pure_id: Union[Unset, int] = UNSET
    uuid: Union[Unset, str] = UNSET
    created_by: Union[Unset, str] = UNSET
    created_date: Union[Unset, datetime.datetime] = UNSET
    modified_by: Union[Unset, str] = UNSET
    modified_date: Union[Unset, datetime.datetime] = UNSET
    portal_url: Union[Unset, str] = UNSET
    pretty_url_identifiers: Union[Unset, List[str]] = UNSET
    previous_uuids: Union[Unset, List[str]] = UNSET
    version: Union[None, Unset, str] = UNSET
    identifiers: Union[List["Identifier"], None, Unset] = UNSET
    name_variants: Union[List["ClassifiedLocalizedValue"], None, Unset] = UNSET
    profile_informations: Union[List["ClassifiedLocalizedValue"], None, Unset] = UNSET
    photos: Union[List["ImageFile"], None, Unset] = UNSET
    addresses: Union[List["ClassifiedAddress"], None, Unset] = UNSET
    phone_numbers: Union[List["ClassifiedValue"], None, Unset] = UNSET
    emails: Union[List["ClassifiedValue"], None, Unset] = UNSET
    web_addresses: Union[List["ClassifiedLocalizedValue"], None, Unset] = UNSET
    taken_over_by: Union["ContentRefType0", None, Unset] = UNSET
    parents: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    contact_persons: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    keyword_groups: Union[List["KeywordGroup"], None, Unset] = UNSET
    cost_centers: Union[List[Union["ClassificationRefType0", None]], None, Unset] = UNSET
    visibility: Union[Unset, "Visibility"] = UNSET
    custom_defined_fields: Union["CustomDefinedFieldsType0", None, Unset] = UNSET
    links: Union[List["Link"], None, Unset] = UNSET
    main_research_area: Union["ClassificationRefType0", None, Unset] = UNSET
    system_name: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        name: Union[Dict[str, Any], None]
        if isinstance(self.name, LocalizedStringType0):
            name = self.name.to_dict()
        else:
            name = self.name

        type: Union[Dict[str, Any], None]
        if isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        lifecycle = self.lifecycle.to_dict()

        pure_id = self.pure_id

        uuid = self.uuid

        created_by = self.created_by

        created_date: Union[Unset, str] = UNSET
        if not isinstance(self.created_date, Unset):
            created_date = self.created_date.isoformat()

        modified_by = self.modified_by

        modified_date: Union[Unset, str] = UNSET
        if not isinstance(self.modified_date, Unset):
            modified_date = self.modified_date.isoformat()

        portal_url = self.portal_url

        pretty_url_identifiers: Union[Unset, List[str]] = UNSET
        if not isinstance(self.pretty_url_identifiers, Unset):
            pretty_url_identifiers = self.pretty_url_identifiers

        previous_uuids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.previous_uuids, Unset):
            previous_uuids = self.previous_uuids

        version: Union[None, Unset, str]
        if isinstance(self.version, Unset):
            version = UNSET
        else:
            version = self.version

        identifiers: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.identifiers, Unset):
            identifiers = UNSET
        elif isinstance(self.identifiers, list):
            identifiers = []
            for identifiers_type_0_item_data in self.identifiers:
                identifiers_type_0_item = identifiers_type_0_item_data.to_dict()
                identifiers.append(identifiers_type_0_item)

        else:
            identifiers = self.identifiers

        name_variants: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.name_variants, Unset):
            name_variants = UNSET
        elif isinstance(self.name_variants, list):
            name_variants = []
            for name_variants_type_0_item_data in self.name_variants:
                name_variants_type_0_item = name_variants_type_0_item_data.to_dict()
                name_variants.append(name_variants_type_0_item)

        else:
            name_variants = self.name_variants

        profile_informations: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.profile_informations, Unset):
            profile_informations = UNSET
        elif isinstance(self.profile_informations, list):
            profile_informations = []
            for profile_informations_type_0_item_data in self.profile_informations:
                profile_informations_type_0_item = profile_informations_type_0_item_data.to_dict()
                profile_informations.append(profile_informations_type_0_item)

        else:
            profile_informations = self.profile_informations

        photos: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.photos, Unset):
            photos = UNSET
        elif isinstance(self.photos, list):
            photos = []
            for photos_type_0_item_data in self.photos:
                photos_type_0_item = photos_type_0_item_data.to_dict()
                photos.append(photos_type_0_item)

        else:
            photos = self.photos

        addresses: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.addresses, Unset):
            addresses = UNSET
        elif isinstance(self.addresses, list):
            addresses = []
            for addresses_type_0_item_data in self.addresses:
                addresses_type_0_item = addresses_type_0_item_data.to_dict()
                addresses.append(addresses_type_0_item)

        else:
            addresses = self.addresses

        phone_numbers: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.phone_numbers, Unset):
            phone_numbers = UNSET
        elif isinstance(self.phone_numbers, list):
            phone_numbers = []
            for phone_numbers_type_0_item_data in self.phone_numbers:
                phone_numbers_type_0_item = phone_numbers_type_0_item_data.to_dict()
                phone_numbers.append(phone_numbers_type_0_item)

        else:
            phone_numbers = self.phone_numbers

        emails: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.emails, Unset):
            emails = UNSET
        elif isinstance(self.emails, list):
            emails = []
            for emails_type_0_item_data in self.emails:
                emails_type_0_item = emails_type_0_item_data.to_dict()
                emails.append(emails_type_0_item)

        else:
            emails = self.emails

        web_addresses: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.web_addresses, Unset):
            web_addresses = UNSET
        elif isinstance(self.web_addresses, list):
            web_addresses = []
            for web_addresses_type_0_item_data in self.web_addresses:
                web_addresses_type_0_item = web_addresses_type_0_item_data.to_dict()
                web_addresses.append(web_addresses_type_0_item)

        else:
            web_addresses = self.web_addresses

        taken_over_by: Union[Dict[str, Any], None, Unset]
        if isinstance(self.taken_over_by, Unset):
            taken_over_by = UNSET
        elif isinstance(self.taken_over_by, ContentRefType0):
            taken_over_by = self.taken_over_by.to_dict()
        else:
            taken_over_by = self.taken_over_by

        parents: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.parents, Unset):
            parents = UNSET
        elif isinstance(self.parents, list):
            parents = []
            for parents_type_0_item_data in self.parents:
                parents_type_0_item: Union[Dict[str, Any], None]
                if isinstance(parents_type_0_item_data, ContentRefType0):
                    parents_type_0_item = parents_type_0_item_data.to_dict()
                else:
                    parents_type_0_item = parents_type_0_item_data
                parents.append(parents_type_0_item)

        else:
            parents = self.parents

        contact_persons: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.contact_persons, Unset):
            contact_persons = UNSET
        elif isinstance(self.contact_persons, list):
            contact_persons = []
            for contact_persons_type_0_item_data in self.contact_persons:
                contact_persons_type_0_item: Union[Dict[str, Any], None]
                if isinstance(contact_persons_type_0_item_data, ContentRefType0):
                    contact_persons_type_0_item = contact_persons_type_0_item_data.to_dict()
                else:
                    contact_persons_type_0_item = contact_persons_type_0_item_data
                contact_persons.append(contact_persons_type_0_item)

        else:
            contact_persons = self.contact_persons

        keyword_groups: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.keyword_groups, Unset):
            keyword_groups = UNSET
        elif isinstance(self.keyword_groups, list):
            keyword_groups = []
            for keyword_groups_type_0_item_data in self.keyword_groups:
                keyword_groups_type_0_item = keyword_groups_type_0_item_data.to_dict()
                keyword_groups.append(keyword_groups_type_0_item)

        else:
            keyword_groups = self.keyword_groups

        cost_centers: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.cost_centers, Unset):
            cost_centers = UNSET
        elif isinstance(self.cost_centers, list):
            cost_centers = []
            for cost_centers_type_0_item_data in self.cost_centers:
                cost_centers_type_0_item: Union[Dict[str, Any], None]
                if isinstance(cost_centers_type_0_item_data, ClassificationRefType0):
                    cost_centers_type_0_item = cost_centers_type_0_item_data.to_dict()
                else:
                    cost_centers_type_0_item = cost_centers_type_0_item_data
                cost_centers.append(cost_centers_type_0_item)

        else:
            cost_centers = self.cost_centers

        visibility: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.visibility, Unset):
            visibility = self.visibility.to_dict()

        custom_defined_fields: Union[Dict[str, Any], None, Unset]
        if isinstance(self.custom_defined_fields, Unset):
            custom_defined_fields = UNSET
        elif isinstance(self.custom_defined_fields, CustomDefinedFieldsType0):
            custom_defined_fields = self.custom_defined_fields.to_dict()
        else:
            custom_defined_fields = self.custom_defined_fields

        links: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.links, Unset):
            links = UNSET
        elif isinstance(self.links, list):
            links = []
            for links_type_0_item_data in self.links:
                links_type_0_item = links_type_0_item_data.to_dict()
                links.append(links_type_0_item)

        else:
            links = self.links

        main_research_area: Union[Dict[str, Any], None, Unset]
        if isinstance(self.main_research_area, Unset):
            main_research_area = UNSET
        elif isinstance(self.main_research_area, ClassificationRefType0):
            main_research_area = self.main_research_area.to_dict()
        else:
            main_research_area = self.main_research_area

        system_name = self.system_name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "type": type,
                "lifecycle": lifecycle,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if uuid is not UNSET:
            field_dict["uuid"] = uuid
        if created_by is not UNSET:
            field_dict["createdBy"] = created_by
        if created_date is not UNSET:
            field_dict["createdDate"] = created_date
        if modified_by is not UNSET:
            field_dict["modifiedBy"] = modified_by
        if modified_date is not UNSET:
            field_dict["modifiedDate"] = modified_date
        if portal_url is not UNSET:
            field_dict["portalUrl"] = portal_url
        if pretty_url_identifiers is not UNSET:
            field_dict["prettyUrlIdentifiers"] = pretty_url_identifiers
        if previous_uuids is not UNSET:
            field_dict["previousUuids"] = previous_uuids
        if version is not UNSET:
            field_dict["version"] = version
        if identifiers is not UNSET:
            field_dict["identifiers"] = identifiers
        if name_variants is not UNSET:
            field_dict["nameVariants"] = name_variants
        if profile_informations is not UNSET:
            field_dict["profileInformations"] = profile_informations
        if photos is not UNSET:
            field_dict["photos"] = photos
        if addresses is not UNSET:
            field_dict["addresses"] = addresses
        if phone_numbers is not UNSET:
            field_dict["phoneNumbers"] = phone_numbers
        if emails is not UNSET:
            field_dict["emails"] = emails
        if web_addresses is not UNSET:
            field_dict["webAddresses"] = web_addresses
        if taken_over_by is not UNSET:
            field_dict["takenOverBy"] = taken_over_by
        if parents is not UNSET:
            field_dict["parents"] = parents
        if contact_persons is not UNSET:
            field_dict["contactPersons"] = contact_persons
        if keyword_groups is not UNSET:
            field_dict["keywordGroups"] = keyword_groups
        if cost_centers is not UNSET:
            field_dict["costCenters"] = cost_centers
        if visibility is not UNSET:
            field_dict["visibility"] = visibility
        if custom_defined_fields is not UNSET:
            field_dict["customDefinedFields"] = custom_defined_fields
        if links is not UNSET:
            field_dict["links"] = links
        if main_research_area is not UNSET:
            field_dict["mainResearchArea"] = main_research_area
        if system_name is not UNSET:
            field_dict["systemName"] = system_name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.classified_address import ClassifiedAddress
        from ..models.classified_localized_value import ClassifiedLocalizedValue
        from ..models.classified_value import ClassifiedValue
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
        from ..models.date_range import DateRange
        from ..models.identifier import Identifier
        from ..models.image_file import ImageFile
        from ..models.keyword_group import KeywordGroup
        from ..models.link import Link
        from ..models.localized_string_type_0 import LocalizedStringType0
        from ..models.visibility import Visibility

        d = src_dict.copy()

        def _parse_name(data: object) -> Union["LocalizedStringType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None], data)

        name = _parse_name(d.pop("name"))

        def _parse_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        type = _parse_type(d.pop("type"))

        lifecycle = DateRange.from_dict(d.pop("lifecycle"))

        pure_id = d.pop("pureId", UNSET)

        uuid = d.pop("uuid", UNSET)

        created_by = d.pop("createdBy", UNSET)

        _created_date = d.pop("createdDate", UNSET)
        created_date: Union[Unset, datetime.datetime]
        if isinstance(_created_date, Unset):
            created_date = UNSET
        else:
            created_date = isoparse(_created_date)

        modified_by = d.pop("modifiedBy", UNSET)

        _modified_date = d.pop("modifiedDate", UNSET)
        modified_date: Union[Unset, datetime.datetime]
        if isinstance(_modified_date, Unset):
            modified_date = UNSET
        else:
            modified_date = isoparse(_modified_date)

        portal_url = d.pop("portalUrl", UNSET)

        pretty_url_identifiers = cast(List[str], d.pop("prettyUrlIdentifiers", UNSET))

        previous_uuids = cast(List[str], d.pop("previousUuids", UNSET))

        def _parse_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        version = _parse_version(d.pop("version", UNSET))

        def _parse_identifiers(data: object) -> Union[List["Identifier"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                identifiers_type_0 = []
                _identifiers_type_0 = data
                for identifiers_type_0_item_data in _identifiers_type_0:
                    identifiers_type_0_item = Identifier.from_dict(identifiers_type_0_item_data)

                    identifiers_type_0.append(identifiers_type_0_item)

                return identifiers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Identifier"], None, Unset], data)

        identifiers = _parse_identifiers(d.pop("identifiers", UNSET))

        def _parse_name_variants(data: object) -> Union[List["ClassifiedLocalizedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                name_variants_type_0 = []
                _name_variants_type_0 = data
                for name_variants_type_0_item_data in _name_variants_type_0:
                    name_variants_type_0_item = ClassifiedLocalizedValue.from_dict(name_variants_type_0_item_data)

                    name_variants_type_0.append(name_variants_type_0_item)

                return name_variants_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedLocalizedValue"], None, Unset], data)

        name_variants = _parse_name_variants(d.pop("nameVariants", UNSET))

        def _parse_profile_informations(data: object) -> Union[List["ClassifiedLocalizedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                profile_informations_type_0 = []
                _profile_informations_type_0 = data
                for profile_informations_type_0_item_data in _profile_informations_type_0:
                    profile_informations_type_0_item = ClassifiedLocalizedValue.from_dict(
                        profile_informations_type_0_item_data
                    )

                    profile_informations_type_0.append(profile_informations_type_0_item)

                return profile_informations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedLocalizedValue"], None, Unset], data)

        profile_informations = _parse_profile_informations(d.pop("profileInformations", UNSET))

        def _parse_photos(data: object) -> Union[List["ImageFile"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                photos_type_0 = []
                _photos_type_0 = data
                for photos_type_0_item_data in _photos_type_0:
                    photos_type_0_item = ImageFile.from_dict(photos_type_0_item_data)

                    photos_type_0.append(photos_type_0_item)

                return photos_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ImageFile"], None, Unset], data)

        photos = _parse_photos(d.pop("photos", UNSET))

        def _parse_addresses(data: object) -> Union[List["ClassifiedAddress"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                addresses_type_0 = []
                _addresses_type_0 = data
                for addresses_type_0_item_data in _addresses_type_0:
                    addresses_type_0_item = ClassifiedAddress.from_dict(addresses_type_0_item_data)

                    addresses_type_0.append(addresses_type_0_item)

                return addresses_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedAddress"], None, Unset], data)

        addresses = _parse_addresses(d.pop("addresses", UNSET))

        def _parse_phone_numbers(data: object) -> Union[List["ClassifiedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                phone_numbers_type_0 = []
                _phone_numbers_type_0 = data
                for phone_numbers_type_0_item_data in _phone_numbers_type_0:
                    phone_numbers_type_0_item = ClassifiedValue.from_dict(phone_numbers_type_0_item_data)

                    phone_numbers_type_0.append(phone_numbers_type_0_item)

                return phone_numbers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedValue"], None, Unset], data)

        phone_numbers = _parse_phone_numbers(d.pop("phoneNumbers", UNSET))

        def _parse_emails(data: object) -> Union[List["ClassifiedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                emails_type_0 = []
                _emails_type_0 = data
                for emails_type_0_item_data in _emails_type_0:
                    emails_type_0_item = ClassifiedValue.from_dict(emails_type_0_item_data)

                    emails_type_0.append(emails_type_0_item)

                return emails_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedValue"], None, Unset], data)

        emails = _parse_emails(d.pop("emails", UNSET))

        def _parse_web_addresses(data: object) -> Union[List["ClassifiedLocalizedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                web_addresses_type_0 = []
                _web_addresses_type_0 = data
                for web_addresses_type_0_item_data in _web_addresses_type_0:
                    web_addresses_type_0_item = ClassifiedLocalizedValue.from_dict(web_addresses_type_0_item_data)

                    web_addresses_type_0.append(web_addresses_type_0_item)

                return web_addresses_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedLocalizedValue"], None, Unset], data)

        web_addresses = _parse_web_addresses(d.pop("webAddresses", UNSET))

        def _parse_taken_over_by(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        taken_over_by = _parse_taken_over_by(d.pop("takenOverBy", UNSET))

        def _parse_parents(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                parents_type_0 = []
                _parents_type_0 = data
                for parents_type_0_item_data in _parents_type_0:

                    def _parse_parents_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    parents_type_0_item = _parse_parents_type_0_item(parents_type_0_item_data)

                    parents_type_0.append(parents_type_0_item)

                return parents_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        parents = _parse_parents(d.pop("parents", UNSET))

        def _parse_contact_persons(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                contact_persons_type_0 = []
                _contact_persons_type_0 = data
                for contact_persons_type_0_item_data in _contact_persons_type_0:

                    def _parse_contact_persons_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    contact_persons_type_0_item = _parse_contact_persons_type_0_item(contact_persons_type_0_item_data)

                    contact_persons_type_0.append(contact_persons_type_0_item)

                return contact_persons_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        contact_persons = _parse_contact_persons(d.pop("contactPersons", UNSET))

        def _parse_keyword_groups(data: object) -> Union[List["KeywordGroup"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                keyword_groups_type_0 = []
                _keyword_groups_type_0 = data
                for keyword_groups_type_0_item_data in _keyword_groups_type_0:
                    keyword_groups_type_0_item = KeywordGroup.from_dict(keyword_groups_type_0_item_data)

                    keyword_groups_type_0.append(keyword_groups_type_0_item)

                return keyword_groups_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["KeywordGroup"], None, Unset], data)

        keyword_groups = _parse_keyword_groups(d.pop("keywordGroups", UNSET))

        def _parse_cost_centers(data: object) -> Union[List[Union["ClassificationRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                cost_centers_type_0 = []
                _cost_centers_type_0 = data
                for cost_centers_type_0_item_data in _cost_centers_type_0:

                    def _parse_cost_centers_type_0_item(data: object) -> Union["ClassificationRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                            return componentsschemas_classification_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ClassificationRefType0", None], data)

                    cost_centers_type_0_item = _parse_cost_centers_type_0_item(cost_centers_type_0_item_data)

                    cost_centers_type_0.append(cost_centers_type_0_item)

                return cost_centers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ClassificationRefType0", None]], None, Unset], data)

        cost_centers = _parse_cost_centers(d.pop("costCenters", UNSET))

        _visibility = d.pop("visibility", UNSET)
        visibility: Union[Unset, Visibility]
        if isinstance(_visibility, Unset):
            visibility = UNSET
        else:
            visibility = Visibility.from_dict(_visibility)

        def _parse_custom_defined_fields(data: object) -> Union["CustomDefinedFieldsType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_custom_defined_fields_type_0 = CustomDefinedFieldsType0.from_dict(data)

                return componentsschemas_custom_defined_fields_type_0
            except:  # noqa: E722
                pass
            return cast(Union["CustomDefinedFieldsType0", None, Unset], data)

        custom_defined_fields = _parse_custom_defined_fields(d.pop("customDefinedFields", UNSET))

        def _parse_links(data: object) -> Union[List["Link"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                links_type_0 = []
                _links_type_0 = data
                for links_type_0_item_data in _links_type_0:
                    links_type_0_item = Link.from_dict(links_type_0_item_data)

                    links_type_0.append(links_type_0_item)

                return links_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Link"], None, Unset], data)

        links = _parse_links(d.pop("links", UNSET))

        def _parse_main_research_area(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        main_research_area = _parse_main_research_area(d.pop("mainResearchArea", UNSET))

        system_name = d.pop("systemName", UNSET)

        organization = cls(
            name=name,
            type=type,
            lifecycle=lifecycle,
            pure_id=pure_id,
            uuid=uuid,
            created_by=created_by,
            created_date=created_date,
            modified_by=modified_by,
            modified_date=modified_date,
            portal_url=portal_url,
            pretty_url_identifiers=pretty_url_identifiers,
            previous_uuids=previous_uuids,
            version=version,
            identifiers=identifiers,
            name_variants=name_variants,
            profile_informations=profile_informations,
            photos=photos,
            addresses=addresses,
            phone_numbers=phone_numbers,
            emails=emails,
            web_addresses=web_addresses,
            taken_over_by=taken_over_by,
            parents=parents,
            contact_persons=contact_persons,
            keyword_groups=keyword_groups,
            cost_centers=cost_centers,
            visibility=visibility,
            custom_defined_fields=custom_defined_fields,
            links=links,
            main_research_area=main_research_area,
            system_name=system_name,
        )

        organization.additional_properties = d
        return organization

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
