import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.cerif_address_type_0 import CERIFAddressType0
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.date_range import DateRange
    from ..models.document import Document
    from ..models.identifier import Identifier
    from ..models.image_file import ImageFile
    from ..models.keyword_group import KeywordGroup
    from ..models.link import Link
    from ..models.localized_string_type_0 import LocalizedStringType0
    from ..models.organization_or_external_organization_ref_type_0 import OrganizationOrExternalOrganizationRefType0
    from ..models.visibility import Visibility
    from ..models.workflow import Workflow


T = TypeVar("T", bound="ExternalOrganization")


@_attrs_define
class ExternalOrganization:
    """An organization external to the institution

    Attributes:
        name (Union['LocalizedStringType0', None]): A set of string values, one for each submission locale. Note:
            invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        type (Union['ClassificationRefType0', None]): A reference to a classification value
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        uuid (Union[Unset, str]): UUID, this is the primary identity of the entity
        created_by (Union[Unset, str]): Username of creator
        created_date (Union[Unset, datetime.datetime]): Date and time of creation
        modified_by (Union[Unset, str]): Username of the user that performed a modification
        modified_date (Union[Unset, datetime.datetime]): Date and time of last modification
        portal_url (Union[Unset, str]): URL of the content on the Pure Portal
        pretty_url_identifiers (Union[Unset, List[str]]): All pretty URLs
        previous_uuids (Union[Unset, List[str]]): UUIDs of other content items which have been merged into this content
            item (or similar)
        version (Union[None, Unset, str]): Used to guard against conflicting updates. For new content this is null, and
            for existing content the current value. The property should never be modified by a client, except in the rare
            case where the client wants to perform an update irrespective of if other clients have made updates in the
            meantime, also known as a "dirty write". A dirty write is performed by not including the property value or
            setting the property to null
        nature_types (Union[List[Union['ClassificationRefType0', None]], None, Unset]): Nature of the organizations work
        acronym (Union[None, Unset, str]): Acronym of organization name
        alternative_names (Union[List[str], None, Unset]): Alternative names of organization
        identifiers (Union[List['Identifier'], None, Unset]): IDs that this object corresponds to in external systems.
            Such as a Scopus ID. Used by Pure where it is necessary to identify objects to specific external systems
        address (Union['CERIFAddressType0', None, Unset]): A physical address
        phone_number (Union[None, Unset, str]): Phone number
        mobile_phone_number (Union[None, Unset, str]): Phone number (mobile)
        fax (Union[None, Unset, str]): Fax number
        email (Union[None, Unset, str]): Email address
        bank_account_number (Union[None, Unset, str]): Bank account number
        vat_number (Union[None, Unset, str]): VAT number
        documents (Union[List['Document'], None, Unset]): Arbitrary documents relevant to the organization
        images (Union[List['ImageFile'], None, Unset]): External organization image
        links (Union[List['Link'], None, Unset]): Links to information about the organization
        keyword_groups (Union[List['KeywordGroup'], None, Unset]): A group for each type of keyword present
        note (Union[None, Unset, str]): A free-form note about the organization
        visibility (Union[Unset, Visibility]): Visibility of an object
        workflow (Union[Unset, Workflow]): Information about workflow
        taken_over_by (Union['OrganizationOrExternalOrganizationRefType0', None, Unset]): A reference to an organization
            in the institution or an external organization
        lifecycle (Union[Unset, DateRange]): A date range
        parent (Union['ContentRefType0', None, Unset]):
        system_name (Union[Unset, str]): The content system name
    """

    name: Union["LocalizedStringType0", None]
    type: Union["ClassificationRefType0", None]
    pure_id: Union[Unset, int] = UNSET
    uuid: Union[Unset, str] = UNSET
    created_by: Union[Unset, str] = UNSET
    created_date: Union[Unset, datetime.datetime] = UNSET
    modified_by: Union[Unset, str] = UNSET
    modified_date: Union[Unset, datetime.datetime] = UNSET
    portal_url: Union[Unset, str] = UNSET
    pretty_url_identifiers: Union[Unset, List[str]] = UNSET
    previous_uuids: Union[Unset, List[str]] = UNSET
    version: Union[None, Unset, str] = UNSET
    nature_types: Union[List[Union["ClassificationRefType0", None]], None, Unset] = UNSET
    acronym: Union[None, Unset, str] = UNSET
    alternative_names: Union[List[str], None, Unset] = UNSET
    identifiers: Union[List["Identifier"], None, Unset] = UNSET
    address: Union["CERIFAddressType0", None, Unset] = UNSET
    phone_number: Union[None, Unset, str] = UNSET
    mobile_phone_number: Union[None, Unset, str] = UNSET
    fax: Union[None, Unset, str] = UNSET
    email: Union[None, Unset, str] = UNSET
    bank_account_number: Union[None, Unset, str] = UNSET
    vat_number: Union[None, Unset, str] = UNSET
    documents: Union[List["Document"], None, Unset] = UNSET
    images: Union[List["ImageFile"], None, Unset] = UNSET
    links: Union[List["Link"], None, Unset] = UNSET
    keyword_groups: Union[List["KeywordGroup"], None, Unset] = UNSET
    note: Union[None, Unset, str] = UNSET
    visibility: Union[Unset, "Visibility"] = UNSET
    workflow: Union[Unset, "Workflow"] = UNSET
    taken_over_by: Union["OrganizationOrExternalOrganizationRefType0", None, Unset] = UNSET
    lifecycle: Union[Unset, "DateRange"] = UNSET
    parent: Union["ContentRefType0", None, Unset] = UNSET
    system_name: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.cerif_address_type_0 import CERIFAddressType0
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.localized_string_type_0 import LocalizedStringType0
        from ..models.organization_or_external_organization_ref_type_0 import OrganizationOrExternalOrganizationRefType0

        name: Union[Dict[str, Any], None]
        if isinstance(self.name, LocalizedStringType0):
            name = self.name.to_dict()
        else:
            name = self.name

        type: Union[Dict[str, Any], None]
        if isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        pure_id = self.pure_id

        uuid = self.uuid

        created_by = self.created_by

        created_date: Union[Unset, str] = UNSET
        if not isinstance(self.created_date, Unset):
            created_date = self.created_date.isoformat()

        modified_by = self.modified_by

        modified_date: Union[Unset, str] = UNSET
        if not isinstance(self.modified_date, Unset):
            modified_date = self.modified_date.isoformat()

        portal_url = self.portal_url

        pretty_url_identifiers: Union[Unset, List[str]] = UNSET
        if not isinstance(self.pretty_url_identifiers, Unset):
            pretty_url_identifiers = self.pretty_url_identifiers

        previous_uuids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.previous_uuids, Unset):
            previous_uuids = self.previous_uuids

        version: Union[None, Unset, str]
        if isinstance(self.version, Unset):
            version = UNSET
        else:
            version = self.version

        nature_types: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.nature_types, Unset):
            nature_types = UNSET
        elif isinstance(self.nature_types, list):
            nature_types = []
            for nature_types_type_0_item_data in self.nature_types:
                nature_types_type_0_item: Union[Dict[str, Any], None]
                if isinstance(nature_types_type_0_item_data, ClassificationRefType0):
                    nature_types_type_0_item = nature_types_type_0_item_data.to_dict()
                else:
                    nature_types_type_0_item = nature_types_type_0_item_data
                nature_types.append(nature_types_type_0_item)

        else:
            nature_types = self.nature_types

        acronym: Union[None, Unset, str]
        if isinstance(self.acronym, Unset):
            acronym = UNSET
        else:
            acronym = self.acronym

        alternative_names: Union[List[str], None, Unset]
        if isinstance(self.alternative_names, Unset):
            alternative_names = UNSET
        elif isinstance(self.alternative_names, list):
            alternative_names = self.alternative_names

        else:
            alternative_names = self.alternative_names

        identifiers: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.identifiers, Unset):
            identifiers = UNSET
        elif isinstance(self.identifiers, list):
            identifiers = []
            for identifiers_type_0_item_data in self.identifiers:
                identifiers_type_0_item = identifiers_type_0_item_data.to_dict()
                identifiers.append(identifiers_type_0_item)

        else:
            identifiers = self.identifiers

        address: Union[Dict[str, Any], None, Unset]
        if isinstance(self.address, Unset):
            address = UNSET
        elif isinstance(self.address, CERIFAddressType0):
            address = self.address.to_dict()
        else:
            address = self.address

        phone_number: Union[None, Unset, str]
        if isinstance(self.phone_number, Unset):
            phone_number = UNSET
        else:
            phone_number = self.phone_number

        mobile_phone_number: Union[None, Unset, str]
        if isinstance(self.mobile_phone_number, Unset):
            mobile_phone_number = UNSET
        else:
            mobile_phone_number = self.mobile_phone_number

        fax: Union[None, Unset, str]
        if isinstance(self.fax, Unset):
            fax = UNSET
        else:
            fax = self.fax

        email: Union[None, Unset, str]
        if isinstance(self.email, Unset):
            email = UNSET
        else:
            email = self.email

        bank_account_number: Union[None, Unset, str]
        if isinstance(self.bank_account_number, Unset):
            bank_account_number = UNSET
        else:
            bank_account_number = self.bank_account_number

        vat_number: Union[None, Unset, str]
        if isinstance(self.vat_number, Unset):
            vat_number = UNSET
        else:
            vat_number = self.vat_number

        documents: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.documents, Unset):
            documents = UNSET
        elif isinstance(self.documents, list):
            documents = []
            for documents_type_0_item_data in self.documents:
                documents_type_0_item = documents_type_0_item_data.to_dict()
                documents.append(documents_type_0_item)

        else:
            documents = self.documents

        images: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.images, Unset):
            images = UNSET
        elif isinstance(self.images, list):
            images = []
            for images_type_0_item_data in self.images:
                images_type_0_item = images_type_0_item_data.to_dict()
                images.append(images_type_0_item)

        else:
            images = self.images

        links: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.links, Unset):
            links = UNSET
        elif isinstance(self.links, list):
            links = []
            for links_type_0_item_data in self.links:
                links_type_0_item = links_type_0_item_data.to_dict()
                links.append(links_type_0_item)

        else:
            links = self.links

        keyword_groups: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.keyword_groups, Unset):
            keyword_groups = UNSET
        elif isinstance(self.keyword_groups, list):
            keyword_groups = []
            for keyword_groups_type_0_item_data in self.keyword_groups:
                keyword_groups_type_0_item = keyword_groups_type_0_item_data.to_dict()
                keyword_groups.append(keyword_groups_type_0_item)

        else:
            keyword_groups = self.keyword_groups

        note: Union[None, Unset, str]
        if isinstance(self.note, Unset):
            note = UNSET
        else:
            note = self.note

        visibility: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.visibility, Unset):
            visibility = self.visibility.to_dict()

        workflow: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.workflow, Unset):
            workflow = self.workflow.to_dict()

        taken_over_by: Union[Dict[str, Any], None, Unset]
        if isinstance(self.taken_over_by, Unset):
            taken_over_by = UNSET
        elif isinstance(self.taken_over_by, OrganizationOrExternalOrganizationRefType0):
            taken_over_by = self.taken_over_by.to_dict()
        else:
            taken_over_by = self.taken_over_by

        lifecycle: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.lifecycle, Unset):
            lifecycle = self.lifecycle.to_dict()

        parent: Union[Dict[str, Any], None, Unset]
        if isinstance(self.parent, Unset):
            parent = UNSET
        elif isinstance(self.parent, ContentRefType0):
            parent = self.parent.to_dict()
        else:
            parent = self.parent

        system_name = self.system_name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "type": type,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if uuid is not UNSET:
            field_dict["uuid"] = uuid
        if created_by is not UNSET:
            field_dict["createdBy"] = created_by
        if created_date is not UNSET:
            field_dict["createdDate"] = created_date
        if modified_by is not UNSET:
            field_dict["modifiedBy"] = modified_by
        if modified_date is not UNSET:
            field_dict["modifiedDate"] = modified_date
        if portal_url is not UNSET:
            field_dict["portalUrl"] = portal_url
        if pretty_url_identifiers is not UNSET:
            field_dict["prettyUrlIdentifiers"] = pretty_url_identifiers
        if previous_uuids is not UNSET:
            field_dict["previousUuids"] = previous_uuids
        if version is not UNSET:
            field_dict["version"] = version
        if nature_types is not UNSET:
            field_dict["natureTypes"] = nature_types
        if acronym is not UNSET:
            field_dict["acronym"] = acronym
        if alternative_names is not UNSET:
            field_dict["alternativeNames"] = alternative_names
        if identifiers is not UNSET:
            field_dict["identifiers"] = identifiers
        if address is not UNSET:
            field_dict["address"] = address
        if phone_number is not UNSET:
            field_dict["phoneNumber"] = phone_number
        if mobile_phone_number is not UNSET:
            field_dict["mobilePhoneNumber"] = mobile_phone_number
        if fax is not UNSET:
            field_dict["fax"] = fax
        if email is not UNSET:
            field_dict["email"] = email
        if bank_account_number is not UNSET:
            field_dict["bankAccountNumber"] = bank_account_number
        if vat_number is not UNSET:
            field_dict["vatNumber"] = vat_number
        if documents is not UNSET:
            field_dict["documents"] = documents
        if images is not UNSET:
            field_dict["images"] = images
        if links is not UNSET:
            field_dict["links"] = links
        if keyword_groups is not UNSET:
            field_dict["keywordGroups"] = keyword_groups
        if note is not UNSET:
            field_dict["note"] = note
        if visibility is not UNSET:
            field_dict["visibility"] = visibility
        if workflow is not UNSET:
            field_dict["workflow"] = workflow
        if taken_over_by is not UNSET:
            field_dict["takenOverBy"] = taken_over_by
        if lifecycle is not UNSET:
            field_dict["lifecycle"] = lifecycle
        if parent is not UNSET:
            field_dict["parent"] = parent
        if system_name is not UNSET:
            field_dict["systemName"] = system_name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.cerif_address_type_0 import CERIFAddressType0
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.date_range import DateRange
        from ..models.document import Document
        from ..models.identifier import Identifier
        from ..models.image_file import ImageFile
        from ..models.keyword_group import KeywordGroup
        from ..models.link import Link
        from ..models.localized_string_type_0 import LocalizedStringType0
        from ..models.organization_or_external_organization_ref_type_0 import OrganizationOrExternalOrganizationRefType0
        from ..models.visibility import Visibility
        from ..models.workflow import Workflow

        d = src_dict.copy()

        def _parse_name(data: object) -> Union["LocalizedStringType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None], data)

        name = _parse_name(d.pop("name"))

        def _parse_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        type = _parse_type(d.pop("type"))

        pure_id = d.pop("pureId", UNSET)

        uuid = d.pop("uuid", UNSET)

        created_by = d.pop("createdBy", UNSET)

        _created_date = d.pop("createdDate", UNSET)
        created_date: Union[Unset, datetime.datetime]
        if isinstance(_created_date, Unset):
            created_date = UNSET
        else:
            created_date = isoparse(_created_date)

        modified_by = d.pop("modifiedBy", UNSET)

        _modified_date = d.pop("modifiedDate", UNSET)
        modified_date: Union[Unset, datetime.datetime]
        if isinstance(_modified_date, Unset):
            modified_date = UNSET
        else:
            modified_date = isoparse(_modified_date)

        portal_url = d.pop("portalUrl", UNSET)

        pretty_url_identifiers = cast(List[str], d.pop("prettyUrlIdentifiers", UNSET))

        previous_uuids = cast(List[str], d.pop("previousUuids", UNSET))

        def _parse_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        version = _parse_version(d.pop("version", UNSET))

        def _parse_nature_types(data: object) -> Union[List[Union["ClassificationRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                nature_types_type_0 = []
                _nature_types_type_0 = data
                for nature_types_type_0_item_data in _nature_types_type_0:

                    def _parse_nature_types_type_0_item(data: object) -> Union["ClassificationRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                            return componentsschemas_classification_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ClassificationRefType0", None], data)

                    nature_types_type_0_item = _parse_nature_types_type_0_item(nature_types_type_0_item_data)

                    nature_types_type_0.append(nature_types_type_0_item)

                return nature_types_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ClassificationRefType0", None]], None, Unset], data)

        nature_types = _parse_nature_types(d.pop("natureTypes", UNSET))

        def _parse_acronym(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        acronym = _parse_acronym(d.pop("acronym", UNSET))

        def _parse_alternative_names(data: object) -> Union[List[str], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                alternative_names_type_0 = cast(List[str], data)

                return alternative_names_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[str], None, Unset], data)

        alternative_names = _parse_alternative_names(d.pop("alternativeNames", UNSET))

        def _parse_identifiers(data: object) -> Union[List["Identifier"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                identifiers_type_0 = []
                _identifiers_type_0 = data
                for identifiers_type_0_item_data in _identifiers_type_0:
                    identifiers_type_0_item = Identifier.from_dict(identifiers_type_0_item_data)

                    identifiers_type_0.append(identifiers_type_0_item)

                return identifiers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Identifier"], None, Unset], data)

        identifiers = _parse_identifiers(d.pop("identifiers", UNSET))

        def _parse_address(data: object) -> Union["CERIFAddressType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_cerif_address_type_0 = CERIFAddressType0.from_dict(data)

                return componentsschemas_cerif_address_type_0
            except:  # noqa: E722
                pass
            return cast(Union["CERIFAddressType0", None, Unset], data)

        address = _parse_address(d.pop("address", UNSET))

        def _parse_phone_number(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        phone_number = _parse_phone_number(d.pop("phoneNumber", UNSET))

        def _parse_mobile_phone_number(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        mobile_phone_number = _parse_mobile_phone_number(d.pop("mobilePhoneNumber", UNSET))

        def _parse_fax(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        fax = _parse_fax(d.pop("fax", UNSET))

        def _parse_email(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        email = _parse_email(d.pop("email", UNSET))

        def _parse_bank_account_number(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        bank_account_number = _parse_bank_account_number(d.pop("bankAccountNumber", UNSET))

        def _parse_vat_number(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        vat_number = _parse_vat_number(d.pop("vatNumber", UNSET))

        def _parse_documents(data: object) -> Union[List["Document"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                documents_type_0 = []
                _documents_type_0 = data
                for documents_type_0_item_data in _documents_type_0:
                    documents_type_0_item = Document.from_dict(documents_type_0_item_data)

                    documents_type_0.append(documents_type_0_item)

                return documents_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Document"], None, Unset], data)

        documents = _parse_documents(d.pop("documents", UNSET))

        def _parse_images(data: object) -> Union[List["ImageFile"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                images_type_0 = []
                _images_type_0 = data
                for images_type_0_item_data in _images_type_0:
                    images_type_0_item = ImageFile.from_dict(images_type_0_item_data)

                    images_type_0.append(images_type_0_item)

                return images_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ImageFile"], None, Unset], data)

        images = _parse_images(d.pop("images", UNSET))

        def _parse_links(data: object) -> Union[List["Link"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                links_type_0 = []
                _links_type_0 = data
                for links_type_0_item_data in _links_type_0:
                    links_type_0_item = Link.from_dict(links_type_0_item_data)

                    links_type_0.append(links_type_0_item)

                return links_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Link"], None, Unset], data)

        links = _parse_links(d.pop("links", UNSET))

        def _parse_keyword_groups(data: object) -> Union[List["KeywordGroup"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                keyword_groups_type_0 = []
                _keyword_groups_type_0 = data
                for keyword_groups_type_0_item_data in _keyword_groups_type_0:
                    keyword_groups_type_0_item = KeywordGroup.from_dict(keyword_groups_type_0_item_data)

                    keyword_groups_type_0.append(keyword_groups_type_0_item)

                return keyword_groups_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["KeywordGroup"], None, Unset], data)

        keyword_groups = _parse_keyword_groups(d.pop("keywordGroups", UNSET))

        def _parse_note(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        note = _parse_note(d.pop("note", UNSET))

        _visibility = d.pop("visibility", UNSET)
        visibility: Union[Unset, Visibility]
        if isinstance(_visibility, Unset):
            visibility = UNSET
        else:
            visibility = Visibility.from_dict(_visibility)

        _workflow = d.pop("workflow", UNSET)
        workflow: Union[Unset, Workflow]
        if isinstance(_workflow, Unset):
            workflow = UNSET
        else:
            workflow = Workflow.from_dict(_workflow)

        def _parse_taken_over_by(data: object) -> Union["OrganizationOrExternalOrganizationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_organization_or_external_organization_ref_type_0 = (
                    OrganizationOrExternalOrganizationRefType0.from_dict(data)
                )

                return componentsschemas_organization_or_external_organization_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["OrganizationOrExternalOrganizationRefType0", None, Unset], data)

        taken_over_by = _parse_taken_over_by(d.pop("takenOverBy", UNSET))

        _lifecycle = d.pop("lifecycle", UNSET)
        lifecycle: Union[Unset, DateRange]
        if isinstance(_lifecycle, Unset):
            lifecycle = UNSET
        else:
            lifecycle = DateRange.from_dict(_lifecycle)

        def _parse_parent(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        parent = _parse_parent(d.pop("parent", UNSET))

        system_name = d.pop("systemName", UNSET)

        external_organization = cls(
            name=name,
            type=type,
            pure_id=pure_id,
            uuid=uuid,
            created_by=created_by,
            created_date=created_date,
            modified_by=modified_by,
            modified_date=modified_date,
            portal_url=portal_url,
            pretty_url_identifiers=pretty_url_identifiers,
            previous_uuids=previous_uuids,
            version=version,
            nature_types=nature_types,
            acronym=acronym,
            alternative_names=alternative_names,
            identifiers=identifiers,
            address=address,
            phone_number=phone_number,
            mobile_phone_number=mobile_phone_number,
            fax=fax,
            email=email,
            bank_account_number=bank_account_number,
            vat_number=vat_number,
            documents=documents,
            images=images,
            links=links,
            keyword_groups=keyword_groups,
            note=note,
            visibility=visibility,
            workflow=workflow,
            taken_over_by=taken_over_by,
            lifecycle=lifecycle,
            parent=parent,
            system_name=system_name,
        )

        external_organization.additional_properties = d
        return external_organization

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
