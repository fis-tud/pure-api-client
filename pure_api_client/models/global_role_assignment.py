from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

if TYPE_CHECKING:
    from ..models.assignable_role_ref import AssignableRoleRef


T = TypeVar("T", bound="GlobalRoleAssignment")


@_attrs_define
class GlobalRoleAssignment:
    """Assignment of a global role

    Attributes:
        role_definition (AssignableRoleRef): A reference to an assignable role that can be either local or global
    """

    role_definition: "AssignableRoleRef"
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        role_definition = self.role_definition.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "roleDefinition": role_definition,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.assignable_role_ref import AssignableRoleRef

        d = src_dict.copy()
        role_definition = AssignableRoleRef.from_dict(d.pop("roleDefinition"))

        global_role_assignment = cls(
            role_definition=role_definition,
        )

        global_role_assignment.additional_properties = d
        return global_role_assignment

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
