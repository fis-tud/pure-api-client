from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.preserved_content_move_information import PreservedContentMoveInformation


T = TypeVar("T", bound="PreservedFileMoveInformation")


@_attrs_define
class PreservedFileMoveInformation:
    """
    Attributes:
        preserved_content_move_information (Union[Unset, PreservedContentMoveInformation]):
        source_file_id (Union[Unset, int]):
        target_file_id (Union[Unset, int]):
        id (Union[Unset, int]):
    """

    preserved_content_move_information: Union[Unset, "PreservedContentMoveInformation"] = UNSET
    source_file_id: Union[Unset, int] = UNSET
    target_file_id: Union[Unset, int] = UNSET
    id: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        preserved_content_move_information: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.preserved_content_move_information, Unset):
            preserved_content_move_information = self.preserved_content_move_information.to_dict()

        source_file_id = self.source_file_id

        target_file_id = self.target_file_id

        id = self.id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if preserved_content_move_information is not UNSET:
            field_dict["preservedContentMoveInformation"] = preserved_content_move_information
        if source_file_id is not UNSET:
            field_dict["sourceFileId"] = source_file_id
        if target_file_id is not UNSET:
            field_dict["targetFileId"] = target_file_id
        if id is not UNSET:
            field_dict["id"] = id

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.preserved_content_move_information import PreservedContentMoveInformation

        d = src_dict.copy()
        _preserved_content_move_information = d.pop("preservedContentMoveInformation", UNSET)
        preserved_content_move_information: Union[Unset, PreservedContentMoveInformation]
        if isinstance(_preserved_content_move_information, Unset):
            preserved_content_move_information = UNSET
        else:
            preserved_content_move_information = PreservedContentMoveInformation.from_dict(
                _preserved_content_move_information
            )

        source_file_id = d.pop("sourceFileId", UNSET)

        target_file_id = d.pop("targetFileId", UNSET)

        id = d.pop("id", UNSET)

        preserved_file_move_information = cls(
            preserved_content_move_information=preserved_content_move_information,
            source_file_id=source_file_id,
            target_file_id=target_file_id,
            id=id,
        )

        preserved_file_move_information.additional_properties = d
        return preserved_file_move_information

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
