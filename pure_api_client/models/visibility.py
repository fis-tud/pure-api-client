from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.visibility_key import VisibilityKey
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.system_localized_string_type_0 import SystemLocalizedStringType0


T = TypeVar("T", bound="Visibility")


@_attrs_define
class Visibility:
    """Visibility of an object

    Attributes:
        key (VisibilityKey): Visibility value
        description (Union['SystemLocalizedStringType0', None, Unset]): A set of localized string values each for a
            specific UI locale. Example: {'en_GB': 'Some text'}.
    """

    key: VisibilityKey
    description: Union["SystemLocalizedStringType0", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.system_localized_string_type_0 import SystemLocalizedStringType0

        key = self.key.value

        description: Union[Dict[str, Any], None, Unset]
        if isinstance(self.description, Unset):
            description = UNSET
        elif isinstance(self.description, SystemLocalizedStringType0):
            description = self.description.to_dict()
        else:
            description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "key": key,
            }
        )
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.system_localized_string_type_0 import SystemLocalizedStringType0

        d = src_dict.copy()
        key = VisibilityKey(d.pop("key"))

        def _parse_description(data: object) -> Union["SystemLocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_system_localized_string_type_0 = SystemLocalizedStringType0.from_dict(data)

                return componentsschemas_system_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["SystemLocalizedStringType0", None, Unset], data)

        description = _parse_description(d.pop("description", UNSET))

        visibility = cls(
            key=key,
            description=description,
        )

        visibility.additional_properties = d
        return visibility

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
