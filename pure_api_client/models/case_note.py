import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classified_case_note_source import ClassifiedCaseNoteSource
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.localized_string_type_0 import LocalizedStringType0


T = TypeVar("T", bound="CaseNote")


@_attrs_define
class CaseNote:
    """A note on a case, detailing title, judgement data and court processing case.

    Attributes:
        court (Union['ContentRefType0', None]):
        judgement_date (datetime.date): The judgement date.
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        title (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission locale.
            Note: invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        sources (Union[List['ClassifiedCaseNoteSource'], None, Unset]): Classified case note sources
    """

    court: Union["ContentRefType0", None]
    judgement_date: datetime.date
    pure_id: Union[Unset, int] = UNSET
    title: Union["LocalizedStringType0", None, Unset] = UNSET
    sources: Union[List["ClassifiedCaseNoteSource"], None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        court: Union[Dict[str, Any], None]
        if isinstance(self.court, ContentRefType0):
            court = self.court.to_dict()
        else:
            court = self.court

        judgement_date = self.judgement_date.isoformat()

        pure_id = self.pure_id

        title: Union[Dict[str, Any], None, Unset]
        if isinstance(self.title, Unset):
            title = UNSET
        elif isinstance(self.title, LocalizedStringType0):
            title = self.title.to_dict()
        else:
            title = self.title

        sources: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.sources, Unset):
            sources = UNSET
        elif isinstance(self.sources, list):
            sources = []
            for sources_type_0_item_data in self.sources:
                sources_type_0_item = sources_type_0_item_data.to_dict()
                sources.append(sources_type_0_item)

        else:
            sources = self.sources

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "court": court,
                "judgementDate": judgement_date,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if title is not UNSET:
            field_dict["title"] = title
        if sources is not UNSET:
            field_dict["sources"] = sources

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classified_case_note_source import ClassifiedCaseNoteSource
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        d = src_dict.copy()

        def _parse_court(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        court = _parse_court(d.pop("court"))

        judgement_date = isoparse(d.pop("judgementDate")).date()

        pure_id = d.pop("pureId", UNSET)

        def _parse_title(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        title = _parse_title(d.pop("title", UNSET))

        def _parse_sources(data: object) -> Union[List["ClassifiedCaseNoteSource"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                sources_type_0 = []
                _sources_type_0 = data
                for sources_type_0_item_data in _sources_type_0:
                    sources_type_0_item = ClassifiedCaseNoteSource.from_dict(sources_type_0_item_data)

                    sources_type_0.append(sources_type_0_item)

                return sources_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedCaseNoteSource"], None, Unset], data)

        sources = _parse_sources(d.pop("sources", UNSET))

        case_note = cls(
            court=court,
            judgement_date=judgement_date,
            pure_id=pure_id,
            title=title,
            sources=sources,
        )

        case_note.additional_properties = d
        return case_note

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
