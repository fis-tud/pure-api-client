import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.abstract_award_holder_association import AbstractAwardHolderAssociation
    from ..models.award_document import AwardDocument
    from ..models.award_funding_association import AwardFundingAssociation
    from ..models.award_status import AwardStatus
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.classified_localized_value import ClassifiedLocalizedValue
    from ..models.collaborator_association import CollaboratorAssociation
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
    from ..models.date_range import DateRange
    from ..models.identifier import Identifier
    from ..models.keyword_group import KeywordGroup
    from ..models.link import Link
    from ..models.localized_string_type_0 import LocalizedStringType0
    from ..models.system_currency_amount import SystemCurrencyAmount
    from ..models.visibility import Visibility
    from ..models.workflow import Workflow


T = TypeVar("T", bound="AwardManagementAward")


@_attrs_define
class AwardManagementAward:
    """The Award Management award template is used when the Award Management module is enabled for the Pure installation.

    Attributes:
        award_date (datetime.date): The date the award was granted by the funder.
        fundings (List['AwardFundingAssociation']): A collection of funding information for the award.
        organizations (List[Union['ContentRefType0', None]]): A collection of organizational unit affiliations.
        managing_organization (Union['ContentRefType0', None]):
        title (Union['LocalizedStringType0', None]): A set of string values, one for each submission locale. Note:
            invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        type (Union['ClassificationRefType0', None]): A reference to a classification value
        type_discriminator (str):
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        uuid (Union[Unset, str]): UUID, this is the primary identity of the entity
        created_by (Union[Unset, str]): Username of creator
        created_date (Union[Unset, datetime.datetime]): Date and time of creation
        modified_by (Union[Unset, str]): Username of the user that performed a modification
        modified_date (Union[Unset, datetime.datetime]): Date and time of last modification
        portal_url (Union[Unset, str]): URL of the content on the Pure Portal
        pretty_url_identifiers (Union[Unset, List[str]]): All pretty URLs
        previous_uuids (Union[Unset, List[str]]): UUIDs of other content items which have been merged into this content
            item (or similar)
        version (Union[None, Unset, str]): Used to guard against conflicting updates. For new content this is null, and
            for existing content the current value. The property should never be modified by a client, except in the rare
            case where the client wants to perform an update irrespective of if other clients have made updates in the
            meantime, also known as a "dirty write". A dirty write is performed by not including the property value or
            setting the property to null
        acronym (Union[None, Unset, str]): The acronym of the award
        award_holders (Union[Unset, List['AbstractAwardHolderAssociation']]):
        external_organizations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of external
            organization affiliations.
        awards (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of related grants.
        budget_awarded_amount_difference (Union[Unset, SystemCurrencyAmount]): A monetary value in the Pure
            installation's system currency as defined by the W3C's Payment Request standard: https://www.w3.org/TR/payment-
            request/#paymentcurrencyamount-dictionary
        co_managing_organizations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of co-
            managing organizational unit affiliations.
        collaborators (Union[List['CollaboratorAssociation'], None, Unset]): A collection of collaborators.
        descriptions (Union[List['ClassifiedLocalizedValue'], None, Unset]): A collection of descriptions for the award.
            Query the /awards/allowed-description-types endpoint for allowed types.
        actual_period (Union[Unset, DateRange]): A date range
        expected_period (Union[Unset, DateRange]): A date range
        effective_period (Union[Unset, DateRange]): A date range
        identifiers (Union[List['Identifier'], None, Unset]): Identifiers related to the award.
        nature_types (Union[List[Union['ClassificationRefType0', None]], None, Unset]): Nature of activity types for the
            award.
        applications (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of related applications.
        short_title (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission
            locale. Note: invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        total_academic_ownership (Union[Unset, float]): DEPRECATED - Total academic ownership of the award.
        total_awarded_amount (Union[Unset, SystemCurrencyAmount]): A monetary value in the Pure installation's system
            currency as defined by the W3C's Payment Request standard: https://www.w3.org/TR/payment-
            request/#paymentcurrencyamount-dictionary
        total_spent_amount (Union[Unset, SystemCurrencyAmount]): A monetary value in the Pure installation's system
            currency as defined by the W3C's Payment Request standard: https://www.w3.org/TR/payment-
            request/#paymentcurrencyamount-dictionary
        workflow (Union[Unset, Workflow]): Information about workflow
        visibility (Union[Unset, Visibility]): Visibility of an object
        links (Union[List['Link'], None, Unset]): Links associated with the award.
        keyword_groups (Union[List['KeywordGroup'], None, Unset]): Groups of keywords associated with the award.
        cluster (Union['ContentRefType0', None, Unset]):
        documents (Union[List['AwardDocument'], None, Unset]): A collection of documents related to the award.
        custom_defined_fields (Union['CustomDefinedFieldsType0', None, Unset]): Map of CustomDefinedField values, where
            the key is the field identifier Example: { "fieldName1": "typeDiscriminator": "Integer", "value" : 1}.
        system_name (Union[Unset, str]): The content system name
        status (Union[Unset, AwardStatus]): The award status and related information.
        allocated_time (Union[None, Unset, int]): The allocated time for the award.
    """

    award_date: datetime.date
    fundings: List["AwardFundingAssociation"]
    organizations: List[Union["ContentRefType0", None]]
    managing_organization: Union["ContentRefType0", None]
    title: Union["LocalizedStringType0", None]
    type: Union["ClassificationRefType0", None]
    type_discriminator: str
    pure_id: Union[Unset, int] = UNSET
    uuid: Union[Unset, str] = UNSET
    created_by: Union[Unset, str] = UNSET
    created_date: Union[Unset, datetime.datetime] = UNSET
    modified_by: Union[Unset, str] = UNSET
    modified_date: Union[Unset, datetime.datetime] = UNSET
    portal_url: Union[Unset, str] = UNSET
    pretty_url_identifiers: Union[Unset, List[str]] = UNSET
    previous_uuids: Union[Unset, List[str]] = UNSET
    version: Union[None, Unset, str] = UNSET
    acronym: Union[None, Unset, str] = UNSET
    award_holders: Union[Unset, List["AbstractAwardHolderAssociation"]] = UNSET
    external_organizations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    awards: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    budget_awarded_amount_difference: Union[Unset, "SystemCurrencyAmount"] = UNSET
    co_managing_organizations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    collaborators: Union[List["CollaboratorAssociation"], None, Unset] = UNSET
    descriptions: Union[List["ClassifiedLocalizedValue"], None, Unset] = UNSET
    actual_period: Union[Unset, "DateRange"] = UNSET
    expected_period: Union[Unset, "DateRange"] = UNSET
    effective_period: Union[Unset, "DateRange"] = UNSET
    identifiers: Union[List["Identifier"], None, Unset] = UNSET
    nature_types: Union[List[Union["ClassificationRefType0", None]], None, Unset] = UNSET
    applications: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    short_title: Union["LocalizedStringType0", None, Unset] = UNSET
    total_academic_ownership: Union[Unset, float] = UNSET
    total_awarded_amount: Union[Unset, "SystemCurrencyAmount"] = UNSET
    total_spent_amount: Union[Unset, "SystemCurrencyAmount"] = UNSET
    workflow: Union[Unset, "Workflow"] = UNSET
    visibility: Union[Unset, "Visibility"] = UNSET
    links: Union[List["Link"], None, Unset] = UNSET
    keyword_groups: Union[List["KeywordGroup"], None, Unset] = UNSET
    cluster: Union["ContentRefType0", None, Unset] = UNSET
    documents: Union[List["AwardDocument"], None, Unset] = UNSET
    custom_defined_fields: Union["CustomDefinedFieldsType0", None, Unset] = UNSET
    system_name: Union[Unset, str] = UNSET
    status: Union[Unset, "AwardStatus"] = UNSET
    allocated_time: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        award_date = self.award_date.isoformat()

        fundings = []
        for fundings_item_data in self.fundings:
            fundings_item = fundings_item_data.to_dict()
            fundings.append(fundings_item)

        organizations = []
        for organizations_item_data in self.organizations:
            organizations_item: Union[Dict[str, Any], None]
            if isinstance(organizations_item_data, ContentRefType0):
                organizations_item = organizations_item_data.to_dict()
            else:
                organizations_item = organizations_item_data
            organizations.append(organizations_item)

        managing_organization: Union[Dict[str, Any], None]
        if isinstance(self.managing_organization, ContentRefType0):
            managing_organization = self.managing_organization.to_dict()
        else:
            managing_organization = self.managing_organization

        title: Union[Dict[str, Any], None]
        if isinstance(self.title, LocalizedStringType0):
            title = self.title.to_dict()
        else:
            title = self.title

        type: Union[Dict[str, Any], None]
        if isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        type_discriminator = self.type_discriminator

        pure_id = self.pure_id

        uuid = self.uuid

        created_by = self.created_by

        created_date: Union[Unset, str] = UNSET
        if not isinstance(self.created_date, Unset):
            created_date = self.created_date.isoformat()

        modified_by = self.modified_by

        modified_date: Union[Unset, str] = UNSET
        if not isinstance(self.modified_date, Unset):
            modified_date = self.modified_date.isoformat()

        portal_url = self.portal_url

        pretty_url_identifiers: Union[Unset, List[str]] = UNSET
        if not isinstance(self.pretty_url_identifiers, Unset):
            pretty_url_identifiers = self.pretty_url_identifiers

        previous_uuids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.previous_uuids, Unset):
            previous_uuids = self.previous_uuids

        version: Union[None, Unset, str]
        if isinstance(self.version, Unset):
            version = UNSET
        else:
            version = self.version

        acronym: Union[None, Unset, str]
        if isinstance(self.acronym, Unset):
            acronym = UNSET
        else:
            acronym = self.acronym

        award_holders: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.award_holders, Unset):
            award_holders = []
            for award_holders_item_data in self.award_holders:
                award_holders_item = award_holders_item_data.to_dict()
                award_holders.append(award_holders_item)

        external_organizations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.external_organizations, Unset):
            external_organizations = UNSET
        elif isinstance(self.external_organizations, list):
            external_organizations = []
            for external_organizations_type_0_item_data in self.external_organizations:
                external_organizations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(external_organizations_type_0_item_data, ContentRefType0):
                    external_organizations_type_0_item = external_organizations_type_0_item_data.to_dict()
                else:
                    external_organizations_type_0_item = external_organizations_type_0_item_data
                external_organizations.append(external_organizations_type_0_item)

        else:
            external_organizations = self.external_organizations

        awards: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.awards, Unset):
            awards = UNSET
        elif isinstance(self.awards, list):
            awards = []
            for awards_type_0_item_data in self.awards:
                awards_type_0_item: Union[Dict[str, Any], None]
                if isinstance(awards_type_0_item_data, ContentRefType0):
                    awards_type_0_item = awards_type_0_item_data.to_dict()
                else:
                    awards_type_0_item = awards_type_0_item_data
                awards.append(awards_type_0_item)

        else:
            awards = self.awards

        budget_awarded_amount_difference: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.budget_awarded_amount_difference, Unset):
            budget_awarded_amount_difference = self.budget_awarded_amount_difference.to_dict()

        co_managing_organizations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.co_managing_organizations, Unset):
            co_managing_organizations = UNSET
        elif isinstance(self.co_managing_organizations, list):
            co_managing_organizations = []
            for co_managing_organizations_type_0_item_data in self.co_managing_organizations:
                co_managing_organizations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(co_managing_organizations_type_0_item_data, ContentRefType0):
                    co_managing_organizations_type_0_item = co_managing_organizations_type_0_item_data.to_dict()
                else:
                    co_managing_organizations_type_0_item = co_managing_organizations_type_0_item_data
                co_managing_organizations.append(co_managing_organizations_type_0_item)

        else:
            co_managing_organizations = self.co_managing_organizations

        collaborators: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.collaborators, Unset):
            collaborators = UNSET
        elif isinstance(self.collaborators, list):
            collaborators = []
            for collaborators_type_0_item_data in self.collaborators:
                collaborators_type_0_item = collaborators_type_0_item_data.to_dict()
                collaborators.append(collaborators_type_0_item)

        else:
            collaborators = self.collaborators

        descriptions: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.descriptions, Unset):
            descriptions = UNSET
        elif isinstance(self.descriptions, list):
            descriptions = []
            for descriptions_type_0_item_data in self.descriptions:
                descriptions_type_0_item = descriptions_type_0_item_data.to_dict()
                descriptions.append(descriptions_type_0_item)

        else:
            descriptions = self.descriptions

        actual_period: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.actual_period, Unset):
            actual_period = self.actual_period.to_dict()

        expected_period: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.expected_period, Unset):
            expected_period = self.expected_period.to_dict()

        effective_period: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.effective_period, Unset):
            effective_period = self.effective_period.to_dict()

        identifiers: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.identifiers, Unset):
            identifiers = UNSET
        elif isinstance(self.identifiers, list):
            identifiers = []
            for identifiers_type_0_item_data in self.identifiers:
                identifiers_type_0_item = identifiers_type_0_item_data.to_dict()
                identifiers.append(identifiers_type_0_item)

        else:
            identifiers = self.identifiers

        nature_types: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.nature_types, Unset):
            nature_types = UNSET
        elif isinstance(self.nature_types, list):
            nature_types = []
            for nature_types_type_0_item_data in self.nature_types:
                nature_types_type_0_item: Union[Dict[str, Any], None]
                if isinstance(nature_types_type_0_item_data, ClassificationRefType0):
                    nature_types_type_0_item = nature_types_type_0_item_data.to_dict()
                else:
                    nature_types_type_0_item = nature_types_type_0_item_data
                nature_types.append(nature_types_type_0_item)

        else:
            nature_types = self.nature_types

        applications: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.applications, Unset):
            applications = UNSET
        elif isinstance(self.applications, list):
            applications = []
            for applications_type_0_item_data in self.applications:
                applications_type_0_item: Union[Dict[str, Any], None]
                if isinstance(applications_type_0_item_data, ContentRefType0):
                    applications_type_0_item = applications_type_0_item_data.to_dict()
                else:
                    applications_type_0_item = applications_type_0_item_data
                applications.append(applications_type_0_item)

        else:
            applications = self.applications

        short_title: Union[Dict[str, Any], None, Unset]
        if isinstance(self.short_title, Unset):
            short_title = UNSET
        elif isinstance(self.short_title, LocalizedStringType0):
            short_title = self.short_title.to_dict()
        else:
            short_title = self.short_title

        total_academic_ownership = self.total_academic_ownership

        total_awarded_amount: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.total_awarded_amount, Unset):
            total_awarded_amount = self.total_awarded_amount.to_dict()

        total_spent_amount: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.total_spent_amount, Unset):
            total_spent_amount = self.total_spent_amount.to_dict()

        workflow: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.workflow, Unset):
            workflow = self.workflow.to_dict()

        visibility: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.visibility, Unset):
            visibility = self.visibility.to_dict()

        links: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.links, Unset):
            links = UNSET
        elif isinstance(self.links, list):
            links = []
            for links_type_0_item_data in self.links:
                links_type_0_item = links_type_0_item_data.to_dict()
                links.append(links_type_0_item)

        else:
            links = self.links

        keyword_groups: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.keyword_groups, Unset):
            keyword_groups = UNSET
        elif isinstance(self.keyword_groups, list):
            keyword_groups = []
            for keyword_groups_type_0_item_data in self.keyword_groups:
                keyword_groups_type_0_item = keyword_groups_type_0_item_data.to_dict()
                keyword_groups.append(keyword_groups_type_0_item)

        else:
            keyword_groups = self.keyword_groups

        cluster: Union[Dict[str, Any], None, Unset]
        if isinstance(self.cluster, Unset):
            cluster = UNSET
        elif isinstance(self.cluster, ContentRefType0):
            cluster = self.cluster.to_dict()
        else:
            cluster = self.cluster

        documents: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.documents, Unset):
            documents = UNSET
        elif isinstance(self.documents, list):
            documents = []
            for documents_type_0_item_data in self.documents:
                documents_type_0_item = documents_type_0_item_data.to_dict()
                documents.append(documents_type_0_item)

        else:
            documents = self.documents

        custom_defined_fields: Union[Dict[str, Any], None, Unset]
        if isinstance(self.custom_defined_fields, Unset):
            custom_defined_fields = UNSET
        elif isinstance(self.custom_defined_fields, CustomDefinedFieldsType0):
            custom_defined_fields = self.custom_defined_fields.to_dict()
        else:
            custom_defined_fields = self.custom_defined_fields

        system_name = self.system_name

        status: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.to_dict()

        allocated_time: Union[None, Unset, int]
        if isinstance(self.allocated_time, Unset):
            allocated_time = UNSET
        else:
            allocated_time = self.allocated_time

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "awardDate": award_date,
                "fundings": fundings,
                "organizations": organizations,
                "managingOrganization": managing_organization,
                "title": title,
                "type": type,
                "typeDiscriminator": type_discriminator,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if uuid is not UNSET:
            field_dict["uuid"] = uuid
        if created_by is not UNSET:
            field_dict["createdBy"] = created_by
        if created_date is not UNSET:
            field_dict["createdDate"] = created_date
        if modified_by is not UNSET:
            field_dict["modifiedBy"] = modified_by
        if modified_date is not UNSET:
            field_dict["modifiedDate"] = modified_date
        if portal_url is not UNSET:
            field_dict["portalUrl"] = portal_url
        if pretty_url_identifiers is not UNSET:
            field_dict["prettyUrlIdentifiers"] = pretty_url_identifiers
        if previous_uuids is not UNSET:
            field_dict["previousUuids"] = previous_uuids
        if version is not UNSET:
            field_dict["version"] = version
        if acronym is not UNSET:
            field_dict["acronym"] = acronym
        if award_holders is not UNSET:
            field_dict["awardHolders"] = award_holders
        if external_organizations is not UNSET:
            field_dict["externalOrganizations"] = external_organizations
        if awards is not UNSET:
            field_dict["awards"] = awards
        if budget_awarded_amount_difference is not UNSET:
            field_dict["budgetAwardedAmountDifference"] = budget_awarded_amount_difference
        if co_managing_organizations is not UNSET:
            field_dict["coManagingOrganizations"] = co_managing_organizations
        if collaborators is not UNSET:
            field_dict["collaborators"] = collaborators
        if descriptions is not UNSET:
            field_dict["descriptions"] = descriptions
        if actual_period is not UNSET:
            field_dict["actualPeriod"] = actual_period
        if expected_period is not UNSET:
            field_dict["expectedPeriod"] = expected_period
        if effective_period is not UNSET:
            field_dict["effectivePeriod"] = effective_period
        if identifiers is not UNSET:
            field_dict["identifiers"] = identifiers
        if nature_types is not UNSET:
            field_dict["natureTypes"] = nature_types
        if applications is not UNSET:
            field_dict["applications"] = applications
        if short_title is not UNSET:
            field_dict["shortTitle"] = short_title
        if total_academic_ownership is not UNSET:
            field_dict["totalAcademicOwnership"] = total_academic_ownership
        if total_awarded_amount is not UNSET:
            field_dict["totalAwardedAmount"] = total_awarded_amount
        if total_spent_amount is not UNSET:
            field_dict["totalSpentAmount"] = total_spent_amount
        if workflow is not UNSET:
            field_dict["workflow"] = workflow
        if visibility is not UNSET:
            field_dict["visibility"] = visibility
        if links is not UNSET:
            field_dict["links"] = links
        if keyword_groups is not UNSET:
            field_dict["keywordGroups"] = keyword_groups
        if cluster is not UNSET:
            field_dict["cluster"] = cluster
        if documents is not UNSET:
            field_dict["documents"] = documents
        if custom_defined_fields is not UNSET:
            field_dict["customDefinedFields"] = custom_defined_fields
        if system_name is not UNSET:
            field_dict["systemName"] = system_name
        if status is not UNSET:
            field_dict["status"] = status
        if allocated_time is not UNSET:
            field_dict["allocatedTime"] = allocated_time

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.abstract_award_holder_association import AbstractAwardHolderAssociation
        from ..models.award_document import AwardDocument
        from ..models.award_funding_association import AwardFundingAssociation
        from ..models.award_status import AwardStatus
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.classified_localized_value import ClassifiedLocalizedValue
        from ..models.collaborator_association import CollaboratorAssociation
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
        from ..models.date_range import DateRange
        from ..models.identifier import Identifier
        from ..models.keyword_group import KeywordGroup
        from ..models.link import Link
        from ..models.localized_string_type_0 import LocalizedStringType0
        from ..models.system_currency_amount import SystemCurrencyAmount
        from ..models.visibility import Visibility
        from ..models.workflow import Workflow

        d = src_dict.copy()
        award_date = isoparse(d.pop("awardDate")).date()

        fundings = []
        _fundings = d.pop("fundings")
        for fundings_item_data in _fundings:
            fundings_item = AwardFundingAssociation.from_dict(fundings_item_data)

            fundings.append(fundings_item)

        organizations = []
        _organizations = d.pop("organizations")
        for organizations_item_data in _organizations:

            def _parse_organizations_item(data: object) -> Union["ContentRefType0", None]:
                if data is None:
                    return data
                try:
                    if not isinstance(data, dict):
                        raise TypeError()
                    componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                    return componentsschemas_content_ref_type_0
                except:  # noqa: E722
                    pass
                return cast(Union["ContentRefType0", None], data)

            organizations_item = _parse_organizations_item(organizations_item_data)

            organizations.append(organizations_item)

        def _parse_managing_organization(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        managing_organization = _parse_managing_organization(d.pop("managingOrganization"))

        def _parse_title(data: object) -> Union["LocalizedStringType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None], data)

        title = _parse_title(d.pop("title"))

        def _parse_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        type = _parse_type(d.pop("type"))

        type_discriminator = d.pop("typeDiscriminator")

        pure_id = d.pop("pureId", UNSET)

        uuid = d.pop("uuid", UNSET)

        created_by = d.pop("createdBy", UNSET)

        _created_date = d.pop("createdDate", UNSET)
        created_date: Union[Unset, datetime.datetime]
        if isinstance(_created_date, Unset):
            created_date = UNSET
        else:
            created_date = isoparse(_created_date)

        modified_by = d.pop("modifiedBy", UNSET)

        _modified_date = d.pop("modifiedDate", UNSET)
        modified_date: Union[Unset, datetime.datetime]
        if isinstance(_modified_date, Unset):
            modified_date = UNSET
        else:
            modified_date = isoparse(_modified_date)

        portal_url = d.pop("portalUrl", UNSET)

        pretty_url_identifiers = cast(List[str], d.pop("prettyUrlIdentifiers", UNSET))

        previous_uuids = cast(List[str], d.pop("previousUuids", UNSET))

        def _parse_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        version = _parse_version(d.pop("version", UNSET))

        def _parse_acronym(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        acronym = _parse_acronym(d.pop("acronym", UNSET))

        award_holders = []
        _award_holders = d.pop("awardHolders", UNSET)
        for award_holders_item_data in _award_holders or []:
            award_holders_item = AbstractAwardHolderAssociation.from_dict(award_holders_item_data)

            award_holders.append(award_holders_item)

        def _parse_external_organizations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                external_organizations_type_0 = []
                _external_organizations_type_0 = data
                for external_organizations_type_0_item_data in _external_organizations_type_0:

                    def _parse_external_organizations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    external_organizations_type_0_item = _parse_external_organizations_type_0_item(
                        external_organizations_type_0_item_data
                    )

                    external_organizations_type_0.append(external_organizations_type_0_item)

                return external_organizations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        external_organizations = _parse_external_organizations(d.pop("externalOrganizations", UNSET))

        def _parse_awards(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                awards_type_0 = []
                _awards_type_0 = data
                for awards_type_0_item_data in _awards_type_0:

                    def _parse_awards_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    awards_type_0_item = _parse_awards_type_0_item(awards_type_0_item_data)

                    awards_type_0.append(awards_type_0_item)

                return awards_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        awards = _parse_awards(d.pop("awards", UNSET))

        _budget_awarded_amount_difference = d.pop("budgetAwardedAmountDifference", UNSET)
        budget_awarded_amount_difference: Union[Unset, SystemCurrencyAmount]
        if isinstance(_budget_awarded_amount_difference, Unset):
            budget_awarded_amount_difference = UNSET
        else:
            budget_awarded_amount_difference = SystemCurrencyAmount.from_dict(_budget_awarded_amount_difference)

        def _parse_co_managing_organizations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                co_managing_organizations_type_0 = []
                _co_managing_organizations_type_0 = data
                for co_managing_organizations_type_0_item_data in _co_managing_organizations_type_0:

                    def _parse_co_managing_organizations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    co_managing_organizations_type_0_item = _parse_co_managing_organizations_type_0_item(
                        co_managing_organizations_type_0_item_data
                    )

                    co_managing_organizations_type_0.append(co_managing_organizations_type_0_item)

                return co_managing_organizations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        co_managing_organizations = _parse_co_managing_organizations(d.pop("coManagingOrganizations", UNSET))

        def _parse_collaborators(data: object) -> Union[List["CollaboratorAssociation"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                collaborators_type_0 = []
                _collaborators_type_0 = data
                for collaborators_type_0_item_data in _collaborators_type_0:
                    collaborators_type_0_item = CollaboratorAssociation.from_dict(collaborators_type_0_item_data)

                    collaborators_type_0.append(collaborators_type_0_item)

                return collaborators_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["CollaboratorAssociation"], None, Unset], data)

        collaborators = _parse_collaborators(d.pop("collaborators", UNSET))

        def _parse_descriptions(data: object) -> Union[List["ClassifiedLocalizedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                descriptions_type_0 = []
                _descriptions_type_0 = data
                for descriptions_type_0_item_data in _descriptions_type_0:
                    descriptions_type_0_item = ClassifiedLocalizedValue.from_dict(descriptions_type_0_item_data)

                    descriptions_type_0.append(descriptions_type_0_item)

                return descriptions_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedLocalizedValue"], None, Unset], data)

        descriptions = _parse_descriptions(d.pop("descriptions", UNSET))

        _actual_period = d.pop("actualPeriod", UNSET)
        actual_period: Union[Unset, DateRange]
        if isinstance(_actual_period, Unset):
            actual_period = UNSET
        else:
            actual_period = DateRange.from_dict(_actual_period)

        _expected_period = d.pop("expectedPeriod", UNSET)
        expected_period: Union[Unset, DateRange]
        if isinstance(_expected_period, Unset):
            expected_period = UNSET
        else:
            expected_period = DateRange.from_dict(_expected_period)

        _effective_period = d.pop("effectivePeriod", UNSET)
        effective_period: Union[Unset, DateRange]
        if isinstance(_effective_period, Unset):
            effective_period = UNSET
        else:
            effective_period = DateRange.from_dict(_effective_period)

        def _parse_identifiers(data: object) -> Union[List["Identifier"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                identifiers_type_0 = []
                _identifiers_type_0 = data
                for identifiers_type_0_item_data in _identifiers_type_0:
                    identifiers_type_0_item = Identifier.from_dict(identifiers_type_0_item_data)

                    identifiers_type_0.append(identifiers_type_0_item)

                return identifiers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Identifier"], None, Unset], data)

        identifiers = _parse_identifiers(d.pop("identifiers", UNSET))

        def _parse_nature_types(data: object) -> Union[List[Union["ClassificationRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                nature_types_type_0 = []
                _nature_types_type_0 = data
                for nature_types_type_0_item_data in _nature_types_type_0:

                    def _parse_nature_types_type_0_item(data: object) -> Union["ClassificationRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                            return componentsschemas_classification_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ClassificationRefType0", None], data)

                    nature_types_type_0_item = _parse_nature_types_type_0_item(nature_types_type_0_item_data)

                    nature_types_type_0.append(nature_types_type_0_item)

                return nature_types_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ClassificationRefType0", None]], None, Unset], data)

        nature_types = _parse_nature_types(d.pop("natureTypes", UNSET))

        def _parse_applications(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                applications_type_0 = []
                _applications_type_0 = data
                for applications_type_0_item_data in _applications_type_0:

                    def _parse_applications_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    applications_type_0_item = _parse_applications_type_0_item(applications_type_0_item_data)

                    applications_type_0.append(applications_type_0_item)

                return applications_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        applications = _parse_applications(d.pop("applications", UNSET))

        def _parse_short_title(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        short_title = _parse_short_title(d.pop("shortTitle", UNSET))

        total_academic_ownership = d.pop("totalAcademicOwnership", UNSET)

        _total_awarded_amount = d.pop("totalAwardedAmount", UNSET)
        total_awarded_amount: Union[Unset, SystemCurrencyAmount]
        if isinstance(_total_awarded_amount, Unset):
            total_awarded_amount = UNSET
        else:
            total_awarded_amount = SystemCurrencyAmount.from_dict(_total_awarded_amount)

        _total_spent_amount = d.pop("totalSpentAmount", UNSET)
        total_spent_amount: Union[Unset, SystemCurrencyAmount]
        if isinstance(_total_spent_amount, Unset):
            total_spent_amount = UNSET
        else:
            total_spent_amount = SystemCurrencyAmount.from_dict(_total_spent_amount)

        _workflow = d.pop("workflow", UNSET)
        workflow: Union[Unset, Workflow]
        if isinstance(_workflow, Unset):
            workflow = UNSET
        else:
            workflow = Workflow.from_dict(_workflow)

        _visibility = d.pop("visibility", UNSET)
        visibility: Union[Unset, Visibility]
        if isinstance(_visibility, Unset):
            visibility = UNSET
        else:
            visibility = Visibility.from_dict(_visibility)

        def _parse_links(data: object) -> Union[List["Link"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                links_type_0 = []
                _links_type_0 = data
                for links_type_0_item_data in _links_type_0:
                    links_type_0_item = Link.from_dict(links_type_0_item_data)

                    links_type_0.append(links_type_0_item)

                return links_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Link"], None, Unset], data)

        links = _parse_links(d.pop("links", UNSET))

        def _parse_keyword_groups(data: object) -> Union[List["KeywordGroup"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                keyword_groups_type_0 = []
                _keyword_groups_type_0 = data
                for keyword_groups_type_0_item_data in _keyword_groups_type_0:
                    keyword_groups_type_0_item = KeywordGroup.from_dict(keyword_groups_type_0_item_data)

                    keyword_groups_type_0.append(keyword_groups_type_0_item)

                return keyword_groups_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["KeywordGroup"], None, Unset], data)

        keyword_groups = _parse_keyword_groups(d.pop("keywordGroups", UNSET))

        def _parse_cluster(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        cluster = _parse_cluster(d.pop("cluster", UNSET))

        def _parse_documents(data: object) -> Union[List["AwardDocument"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                documents_type_0 = []
                _documents_type_0 = data
                for documents_type_0_item_data in _documents_type_0:
                    documents_type_0_item = AwardDocument.from_dict(documents_type_0_item_data)

                    documents_type_0.append(documents_type_0_item)

                return documents_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["AwardDocument"], None, Unset], data)

        documents = _parse_documents(d.pop("documents", UNSET))

        def _parse_custom_defined_fields(data: object) -> Union["CustomDefinedFieldsType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_custom_defined_fields_type_0 = CustomDefinedFieldsType0.from_dict(data)

                return componentsschemas_custom_defined_fields_type_0
            except:  # noqa: E722
                pass
            return cast(Union["CustomDefinedFieldsType0", None, Unset], data)

        custom_defined_fields = _parse_custom_defined_fields(d.pop("customDefinedFields", UNSET))

        system_name = d.pop("systemName", UNSET)

        _status = d.pop("status", UNSET)
        status: Union[Unset, AwardStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = AwardStatus.from_dict(_status)

        def _parse_allocated_time(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        allocated_time = _parse_allocated_time(d.pop("allocatedTime", UNSET))

        award_management_award = cls(
            award_date=award_date,
            fundings=fundings,
            organizations=organizations,
            managing_organization=managing_organization,
            title=title,
            type=type,
            type_discriminator=type_discriminator,
            pure_id=pure_id,
            uuid=uuid,
            created_by=created_by,
            created_date=created_date,
            modified_by=modified_by,
            modified_date=modified_date,
            portal_url=portal_url,
            pretty_url_identifiers=pretty_url_identifiers,
            previous_uuids=previous_uuids,
            version=version,
            acronym=acronym,
            award_holders=award_holders,
            external_organizations=external_organizations,
            awards=awards,
            budget_awarded_amount_difference=budget_awarded_amount_difference,
            co_managing_organizations=co_managing_organizations,
            collaborators=collaborators,
            descriptions=descriptions,
            actual_period=actual_period,
            expected_period=expected_period,
            effective_period=effective_period,
            identifiers=identifiers,
            nature_types=nature_types,
            applications=applications,
            short_title=short_title,
            total_academic_ownership=total_academic_ownership,
            total_awarded_amount=total_awarded_amount,
            total_spent_amount=total_spent_amount,
            workflow=workflow,
            visibility=visibility,
            links=links,
            keyword_groups=keyword_groups,
            cluster=cluster,
            documents=documents,
            custom_defined_fields=custom_defined_fields,
            system_name=system_name,
            status=status,
            allocated_time=allocated_time,
        )

        award_management_award.additional_properties = d
        return award_management_award

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
