from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="DisciplinesDiscipline")


@_attrs_define
class DisciplinesDiscipline:
    """A discipline within a specific discipline scheme

    Attributes:
        discipline_id (Union[Unset, str]): The discipline identifier
        title (Union[Unset, str]): The discipline title
        description (Union[Unset, str]): The discipline description
    """

    discipline_id: Union[Unset, str] = UNSET
    title: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        discipline_id = self.discipline_id

        title = self.title

        description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if discipline_id is not UNSET:
            field_dict["disciplineId"] = discipline_id
        if title is not UNSET:
            field_dict["title"] = title
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        discipline_id = d.pop("disciplineId", UNSET)

        title = d.pop("title", UNSET)

        description = d.pop("description", UNSET)

        disciplines_discipline = cls(
            discipline_id=discipline_id,
            title=title,
            description=description,
        )

        disciplines_discipline.additional_properties = d
        return disciplines_discipline

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
