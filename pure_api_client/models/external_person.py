import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.gender_type_0 import GenderType0
    from ..models.identifier import Identifier
    from ..models.image_file import ImageFile
    from ..models.keyword_group import KeywordGroup
    from ..models.localized_string_type_0 import LocalizedStringType0
    from ..models.name import Name
    from ..models.workflow import Workflow


T = TypeVar("T", bound="ExternalPerson")


@_attrs_define
class ExternalPerson:
    """A person not known to the institution

    Attributes:
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        uuid (Union[Unset, str]): UUID, this is the primary identity of the entity
        created_by (Union[Unset, str]): Username of creator
        created_date (Union[Unset, datetime.datetime]): Date and time of creation
        modified_by (Union[Unset, str]): Username of the user that performed a modification
        modified_date (Union[Unset, datetime.datetime]): Date and time of last modification
        portal_url (Union[Unset, str]): URL of the content on the Pure Portal
        pretty_url_identifiers (Union[Unset, List[str]]): All pretty URLs
        previous_uuids (Union[Unset, List[str]]): UUIDs of other content items which have been merged into this content
            item (or similar)
        version (Union[None, Unset, str]): Used to guard against conflicting updates. For new content this is null, and
            for existing content the current value. The property should never be modified by a client, except in the rare
            case where the client wants to perform an update irrespective of if other clients have made updates in the
            meantime, also known as a "dirty write". A dirty write is performed by not including the property value or
            setting the property to null
        identifiers (Union[List['Identifier'], None, Unset]): IDs that this object corresponds to in external systems.
            Such as a Scopus ID. Used by Pure where it is necessary to identify objects to specific external systems
        name (Union[Unset, Name]): A name describing a person, made up of given- and family name
        type (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        title (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission locale.
            Note: invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        country (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        gender (Union['GenderType0', None, Unset]): A list of possible genders
        external_organizations (Union[Unset, List[Union['ContentRefType0', None]]]):
        keyword_groups (Union[Unset, List['KeywordGroup']]):
        workflow (Union[Unset, Workflow]): Information about workflow
        images (Union[List['ImageFile'], None, Unset]): Image files with a maximum file size of 1MB
        system_name (Union[Unset, str]): The content system name
    """

    pure_id: Union[Unset, int] = UNSET
    uuid: Union[Unset, str] = UNSET
    created_by: Union[Unset, str] = UNSET
    created_date: Union[Unset, datetime.datetime] = UNSET
    modified_by: Union[Unset, str] = UNSET
    modified_date: Union[Unset, datetime.datetime] = UNSET
    portal_url: Union[Unset, str] = UNSET
    pretty_url_identifiers: Union[Unset, List[str]] = UNSET
    previous_uuids: Union[Unset, List[str]] = UNSET
    version: Union[None, Unset, str] = UNSET
    identifiers: Union[List["Identifier"], None, Unset] = UNSET
    name: Union[Unset, "Name"] = UNSET
    type: Union["ClassificationRefType0", None, Unset] = UNSET
    title: Union["LocalizedStringType0", None, Unset] = UNSET
    country: Union["ClassificationRefType0", None, Unset] = UNSET
    gender: Union["GenderType0", None, Unset] = UNSET
    external_organizations: Union[Unset, List[Union["ContentRefType0", None]]] = UNSET
    keyword_groups: Union[Unset, List["KeywordGroup"]] = UNSET
    workflow: Union[Unset, "Workflow"] = UNSET
    images: Union[List["ImageFile"], None, Unset] = UNSET
    system_name: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.gender_type_0 import GenderType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        pure_id = self.pure_id

        uuid = self.uuid

        created_by = self.created_by

        created_date: Union[Unset, str] = UNSET
        if not isinstance(self.created_date, Unset):
            created_date = self.created_date.isoformat()

        modified_by = self.modified_by

        modified_date: Union[Unset, str] = UNSET
        if not isinstance(self.modified_date, Unset):
            modified_date = self.modified_date.isoformat()

        portal_url = self.portal_url

        pretty_url_identifiers: Union[Unset, List[str]] = UNSET
        if not isinstance(self.pretty_url_identifiers, Unset):
            pretty_url_identifiers = self.pretty_url_identifiers

        previous_uuids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.previous_uuids, Unset):
            previous_uuids = self.previous_uuids

        version: Union[None, Unset, str]
        if isinstance(self.version, Unset):
            version = UNSET
        else:
            version = self.version

        identifiers: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.identifiers, Unset):
            identifiers = UNSET
        elif isinstance(self.identifiers, list):
            identifiers = []
            for identifiers_type_0_item_data in self.identifiers:
                identifiers_type_0_item = identifiers_type_0_item_data.to_dict()
                identifiers.append(identifiers_type_0_item)

        else:
            identifiers = self.identifiers

        name: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.name, Unset):
            name = self.name.to_dict()

        type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.type, Unset):
            type = UNSET
        elif isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        title: Union[Dict[str, Any], None, Unset]
        if isinstance(self.title, Unset):
            title = UNSET
        elif isinstance(self.title, LocalizedStringType0):
            title = self.title.to_dict()
        else:
            title = self.title

        country: Union[Dict[str, Any], None, Unset]
        if isinstance(self.country, Unset):
            country = UNSET
        elif isinstance(self.country, ClassificationRefType0):
            country = self.country.to_dict()
        else:
            country = self.country

        gender: Union[Dict[str, Any], None, Unset]
        if isinstance(self.gender, Unset):
            gender = UNSET
        elif isinstance(self.gender, GenderType0):
            gender = self.gender.to_dict()
        else:
            gender = self.gender

        external_organizations: Union[Unset, List[Union[Dict[str, Any], None]]] = UNSET
        if not isinstance(self.external_organizations, Unset):
            external_organizations = []
            for external_organizations_item_data in self.external_organizations:
                external_organizations_item: Union[Dict[str, Any], None]
                if isinstance(external_organizations_item_data, ContentRefType0):
                    external_organizations_item = external_organizations_item_data.to_dict()
                else:
                    external_organizations_item = external_organizations_item_data
                external_organizations.append(external_organizations_item)

        keyword_groups: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.keyword_groups, Unset):
            keyword_groups = []
            for keyword_groups_item_data in self.keyword_groups:
                keyword_groups_item = keyword_groups_item_data.to_dict()
                keyword_groups.append(keyword_groups_item)

        workflow: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.workflow, Unset):
            workflow = self.workflow.to_dict()

        images: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.images, Unset):
            images = UNSET
        elif isinstance(self.images, list):
            images = []
            for images_type_0_item_data in self.images:
                images_type_0_item = images_type_0_item_data.to_dict()
                images.append(images_type_0_item)

        else:
            images = self.images

        system_name = self.system_name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if uuid is not UNSET:
            field_dict["uuid"] = uuid
        if created_by is not UNSET:
            field_dict["createdBy"] = created_by
        if created_date is not UNSET:
            field_dict["createdDate"] = created_date
        if modified_by is not UNSET:
            field_dict["modifiedBy"] = modified_by
        if modified_date is not UNSET:
            field_dict["modifiedDate"] = modified_date
        if portal_url is not UNSET:
            field_dict["portalUrl"] = portal_url
        if pretty_url_identifiers is not UNSET:
            field_dict["prettyUrlIdentifiers"] = pretty_url_identifiers
        if previous_uuids is not UNSET:
            field_dict["previousUuids"] = previous_uuids
        if version is not UNSET:
            field_dict["version"] = version
        if identifiers is not UNSET:
            field_dict["identifiers"] = identifiers
        if name is not UNSET:
            field_dict["name"] = name
        if type is not UNSET:
            field_dict["type"] = type
        if title is not UNSET:
            field_dict["title"] = title
        if country is not UNSET:
            field_dict["country"] = country
        if gender is not UNSET:
            field_dict["gender"] = gender
        if external_organizations is not UNSET:
            field_dict["externalOrganizations"] = external_organizations
        if keyword_groups is not UNSET:
            field_dict["keywordGroups"] = keyword_groups
        if workflow is not UNSET:
            field_dict["workflow"] = workflow
        if images is not UNSET:
            field_dict["images"] = images
        if system_name is not UNSET:
            field_dict["systemName"] = system_name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.gender_type_0 import GenderType0
        from ..models.identifier import Identifier
        from ..models.image_file import ImageFile
        from ..models.keyword_group import KeywordGroup
        from ..models.localized_string_type_0 import LocalizedStringType0
        from ..models.name import Name
        from ..models.workflow import Workflow

        d = src_dict.copy()
        pure_id = d.pop("pureId", UNSET)

        uuid = d.pop("uuid", UNSET)

        created_by = d.pop("createdBy", UNSET)

        _created_date = d.pop("createdDate", UNSET)
        created_date: Union[Unset, datetime.datetime]
        if isinstance(_created_date, Unset):
            created_date = UNSET
        else:
            created_date = isoparse(_created_date)

        modified_by = d.pop("modifiedBy", UNSET)

        _modified_date = d.pop("modifiedDate", UNSET)
        modified_date: Union[Unset, datetime.datetime]
        if isinstance(_modified_date, Unset):
            modified_date = UNSET
        else:
            modified_date = isoparse(_modified_date)

        portal_url = d.pop("portalUrl", UNSET)

        pretty_url_identifiers = cast(List[str], d.pop("prettyUrlIdentifiers", UNSET))

        previous_uuids = cast(List[str], d.pop("previousUuids", UNSET))

        def _parse_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        version = _parse_version(d.pop("version", UNSET))

        def _parse_identifiers(data: object) -> Union[List["Identifier"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                identifiers_type_0 = []
                _identifiers_type_0 = data
                for identifiers_type_0_item_data in _identifiers_type_0:
                    identifiers_type_0_item = Identifier.from_dict(identifiers_type_0_item_data)

                    identifiers_type_0.append(identifiers_type_0_item)

                return identifiers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Identifier"], None, Unset], data)

        identifiers = _parse_identifiers(d.pop("identifiers", UNSET))

        _name = d.pop("name", UNSET)
        name: Union[Unset, Name]
        if isinstance(_name, Unset):
            name = UNSET
        else:
            name = Name.from_dict(_name)

        def _parse_type(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        type = _parse_type(d.pop("type", UNSET))

        def _parse_title(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        title = _parse_title(d.pop("title", UNSET))

        def _parse_country(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        country = _parse_country(d.pop("country", UNSET))

        def _parse_gender(data: object) -> Union["GenderType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_gender_type_0 = GenderType0.from_dict(data)

                return componentsschemas_gender_type_0
            except:  # noqa: E722
                pass
            return cast(Union["GenderType0", None, Unset], data)

        gender = _parse_gender(d.pop("gender", UNSET))

        external_organizations = []
        _external_organizations = d.pop("externalOrganizations", UNSET)
        for external_organizations_item_data in _external_organizations or []:

            def _parse_external_organizations_item(data: object) -> Union["ContentRefType0", None]:
                if data is None:
                    return data
                try:
                    if not isinstance(data, dict):
                        raise TypeError()
                    componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                    return componentsschemas_content_ref_type_0
                except:  # noqa: E722
                    pass
                return cast(Union["ContentRefType0", None], data)

            external_organizations_item = _parse_external_organizations_item(external_organizations_item_data)

            external_organizations.append(external_organizations_item)

        keyword_groups = []
        _keyword_groups = d.pop("keywordGroups", UNSET)
        for keyword_groups_item_data in _keyword_groups or []:
            keyword_groups_item = KeywordGroup.from_dict(keyword_groups_item_data)

            keyword_groups.append(keyword_groups_item)

        _workflow = d.pop("workflow", UNSET)
        workflow: Union[Unset, Workflow]
        if isinstance(_workflow, Unset):
            workflow = UNSET
        else:
            workflow = Workflow.from_dict(_workflow)

        def _parse_images(data: object) -> Union[List["ImageFile"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                images_type_0 = []
                _images_type_0 = data
                for images_type_0_item_data in _images_type_0:
                    images_type_0_item = ImageFile.from_dict(images_type_0_item_data)

                    images_type_0.append(images_type_0_item)

                return images_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ImageFile"], None, Unset], data)

        images = _parse_images(d.pop("images", UNSET))

        system_name = d.pop("systemName", UNSET)

        external_person = cls(
            pure_id=pure_id,
            uuid=uuid,
            created_by=created_by,
            created_date=created_date,
            modified_by=modified_by,
            modified_date=modified_date,
            portal_url=portal_url,
            pretty_url_identifiers=pretty_url_identifiers,
            previous_uuids=previous_uuids,
            version=version,
            identifiers=identifiers,
            name=name,
            type=type,
            title=title,
            country=country,
            gender=gender,
            external_organizations=external_organizations,
            keyword_groups=keyword_groups,
            workflow=workflow,
            images=images,
            system_name=system_name,
        )

        external_person.additional_properties = d
        return external_person

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
