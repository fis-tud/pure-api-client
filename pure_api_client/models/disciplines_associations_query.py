from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="DisciplinesAssociationsQuery")


@_attrs_define
class DisciplinesAssociationsQuery:
    """Create a query for discipline associations

    Attributes:
        uuids (Union[Unset, List[str]]):
        size (Union[Unset, int]):
        offset (Union[Unset, int]):
    """

    uuids: Union[Unset, List[str]] = UNSET
    size: Union[Unset, int] = UNSET
    offset: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        uuids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.uuids, Unset):
            uuids = self.uuids

        size = self.size

        offset = self.offset

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if uuids is not UNSET:
            field_dict["uuids"] = uuids
        if size is not UNSET:
            field_dict["size"] = size
        if offset is not UNSET:
            field_dict["offset"] = offset

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        uuids = cast(List[str], d.pop("uuids", UNSET))

        size = d.pop("size", UNSET)

        offset = d.pop("offset", UNSET)

        disciplines_associations_query = cls(
            uuids=uuids,
            size=size,
            offset=offset,
        )

        disciplines_associations_query.additional_properties = d
        return disciplines_associations_query

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
