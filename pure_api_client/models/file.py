from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.existing_file_store_definition import ExistingFileStoreDefinition
    from ..models.preserved_file_move_information import PreservedFileMoveInformation
    from ..models.temp_file_info import TempFileInfo


T = TypeVar("T", bound="File")


@_attrs_define
class File:
    """
    Attributes:
        sizelong (Union[Unset, int]):
        digest_algorithm (Union[Unset, str]):
        digest (Union[Unset, str]):
        temporary_id (Union[Unset, str]):
        temporary_info (Union[Unset, TempFileInfo]):
        existing_store_definitions (Union[Unset, List['ExistingFileStoreDefinition']]):
        migrated_file_information (Union[Unset, PreservedFileMoveInformation]):
        previous_ids (Union[Unset, List[int]]):
        size (Union[Unset, int]):
        file_name (Union[Unset, str]):
        mime_type (Union[Unset, str]):
        id (Union[Unset, int]):
    """

    sizelong: Union[Unset, int] = UNSET
    digest_algorithm: Union[Unset, str] = UNSET
    digest: Union[Unset, str] = UNSET
    temporary_id: Union[Unset, str] = UNSET
    temporary_info: Union[Unset, "TempFileInfo"] = UNSET
    existing_store_definitions: Union[Unset, List["ExistingFileStoreDefinition"]] = UNSET
    migrated_file_information: Union[Unset, "PreservedFileMoveInformation"] = UNSET
    previous_ids: Union[Unset, List[int]] = UNSET
    size: Union[Unset, int] = UNSET
    file_name: Union[Unset, str] = UNSET
    mime_type: Union[Unset, str] = UNSET
    id: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        sizelong = self.sizelong

        digest_algorithm = self.digest_algorithm

        digest = self.digest

        temporary_id = self.temporary_id

        temporary_info: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.temporary_info, Unset):
            temporary_info = self.temporary_info.to_dict()

        existing_store_definitions: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.existing_store_definitions, Unset):
            existing_store_definitions = []
            for existing_store_definitions_item_data in self.existing_store_definitions:
                existing_store_definitions_item = existing_store_definitions_item_data.to_dict()
                existing_store_definitions.append(existing_store_definitions_item)

        migrated_file_information: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.migrated_file_information, Unset):
            migrated_file_information = self.migrated_file_information.to_dict()

        previous_ids: Union[Unset, List[int]] = UNSET
        if not isinstance(self.previous_ids, Unset):
            previous_ids = self.previous_ids

        size = self.size

        file_name = self.file_name

        mime_type = self.mime_type

        id = self.id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if sizelong is not UNSET:
            field_dict["sizelong"] = sizelong
        if digest_algorithm is not UNSET:
            field_dict["digestAlgorithm"] = digest_algorithm
        if digest is not UNSET:
            field_dict["digest"] = digest
        if temporary_id is not UNSET:
            field_dict["temporaryId"] = temporary_id
        if temporary_info is not UNSET:
            field_dict["temporaryInfo"] = temporary_info
        if existing_store_definitions is not UNSET:
            field_dict["existingStoreDefinitions"] = existing_store_definitions
        if migrated_file_information is not UNSET:
            field_dict["migratedFileInformation"] = migrated_file_information
        if previous_ids is not UNSET:
            field_dict["previousIds"] = previous_ids
        if size is not UNSET:
            field_dict["size"] = size
        if file_name is not UNSET:
            field_dict["fileName"] = file_name
        if mime_type is not UNSET:
            field_dict["mimeType"] = mime_type
        if id is not UNSET:
            field_dict["id"] = id

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.existing_file_store_definition import ExistingFileStoreDefinition
        from ..models.preserved_file_move_information import PreservedFileMoveInformation
        from ..models.temp_file_info import TempFileInfo

        d = src_dict.copy()
        sizelong = d.pop("sizelong", UNSET)

        digest_algorithm = d.pop("digestAlgorithm", UNSET)

        digest = d.pop("digest", UNSET)

        temporary_id = d.pop("temporaryId", UNSET)

        _temporary_info = d.pop("temporaryInfo", UNSET)
        temporary_info: Union[Unset, TempFileInfo]
        if isinstance(_temporary_info, Unset):
            temporary_info = UNSET
        else:
            temporary_info = TempFileInfo.from_dict(_temporary_info)

        existing_store_definitions = []
        _existing_store_definitions = d.pop("existingStoreDefinitions", UNSET)
        for existing_store_definitions_item_data in _existing_store_definitions or []:
            existing_store_definitions_item = ExistingFileStoreDefinition.from_dict(
                existing_store_definitions_item_data
            )

            existing_store_definitions.append(existing_store_definitions_item)

        _migrated_file_information = d.pop("migratedFileInformation", UNSET)
        migrated_file_information: Union[Unset, PreservedFileMoveInformation]
        if isinstance(_migrated_file_information, Unset):
            migrated_file_information = UNSET
        else:
            migrated_file_information = PreservedFileMoveInformation.from_dict(_migrated_file_information)

        previous_ids = cast(List[int], d.pop("previousIds", UNSET))

        size = d.pop("size", UNSET)

        file_name = d.pop("fileName", UNSET)

        mime_type = d.pop("mimeType", UNSET)

        id = d.pop("id", UNSET)

        file = cls(
            sizelong=sizelong,
            digest_algorithm=digest_algorithm,
            digest=digest,
            temporary_id=temporary_id,
            temporary_info=temporary_info,
            existing_store_definitions=existing_store_definitions,
            migrated_file_information=migrated_file_information,
            previous_ids=previous_ids,
            size=size,
            file_name=file_name,
            mime_type=mime_type,
            id=id,
        )

        file.additional_properties = d
        return file

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
