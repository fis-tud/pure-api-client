from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.name import Name
    from ..models.researcher_commitment_type_0 import ResearcherCommitmentType0


T = TypeVar("T", bound="AbstractApplicantAssociation")


@_attrs_define
class AbstractApplicantAssociation:
    """An association between an application and a person that has applied for it.

    Attributes:
        role (Union['ClassificationRefType0', None]): A reference to a classification value
        type_discriminator (str):
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        external_organizations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of external
            organization affiliations.
        academic_ownership_percentage (Union[None, Unset, float]): Academic ownership of the applicant.
        planned_researcher_commitment_percentage (Union[None, Unset, float]): Planned researcher commitment of the
            applicant.
        researcher_commitments (Union[Unset, List[Union['ResearcherCommitmentType0', None]]]):
        name (Union[Unset, Name]): A name describing a person, made up of given- and family name
    """

    role: Union["ClassificationRefType0", None]
    type_discriminator: str
    pure_id: Union[Unset, int] = UNSET
    external_organizations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    academic_ownership_percentage: Union[None, Unset, float] = UNSET
    planned_researcher_commitment_percentage: Union[None, Unset, float] = UNSET
    researcher_commitments: Union[Unset, List[Union["ResearcherCommitmentType0", None]]] = UNSET
    name: Union[Unset, "Name"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.researcher_commitment_type_0 import ResearcherCommitmentType0

        role: Union[Dict[str, Any], None]
        if isinstance(self.role, ClassificationRefType0):
            role = self.role.to_dict()
        else:
            role = self.role

        type_discriminator = self.type_discriminator

        pure_id = self.pure_id

        external_organizations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.external_organizations, Unset):
            external_organizations = UNSET
        elif isinstance(self.external_organizations, list):
            external_organizations = []
            for external_organizations_type_0_item_data in self.external_organizations:
                external_organizations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(external_organizations_type_0_item_data, ContentRefType0):
                    external_organizations_type_0_item = external_organizations_type_0_item_data.to_dict()
                else:
                    external_organizations_type_0_item = external_organizations_type_0_item_data
                external_organizations.append(external_organizations_type_0_item)

        else:
            external_organizations = self.external_organizations

        academic_ownership_percentage: Union[None, Unset, float]
        if isinstance(self.academic_ownership_percentage, Unset):
            academic_ownership_percentage = UNSET
        else:
            academic_ownership_percentage = self.academic_ownership_percentage

        planned_researcher_commitment_percentage: Union[None, Unset, float]
        if isinstance(self.planned_researcher_commitment_percentage, Unset):
            planned_researcher_commitment_percentage = UNSET
        else:
            planned_researcher_commitment_percentage = self.planned_researcher_commitment_percentage

        researcher_commitments: Union[Unset, List[Union[Dict[str, Any], None]]] = UNSET
        if not isinstance(self.researcher_commitments, Unset):
            researcher_commitments = []
            for researcher_commitments_item_data in self.researcher_commitments:
                researcher_commitments_item: Union[Dict[str, Any], None]
                if isinstance(researcher_commitments_item_data, ResearcherCommitmentType0):
                    researcher_commitments_item = researcher_commitments_item_data.to_dict()
                else:
                    researcher_commitments_item = researcher_commitments_item_data
                researcher_commitments.append(researcher_commitments_item)

        name: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.name, Unset):
            name = self.name.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "role": role,
                "typeDiscriminator": type_discriminator,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if external_organizations is not UNSET:
            field_dict["externalOrganizations"] = external_organizations
        if academic_ownership_percentage is not UNSET:
            field_dict["academicOwnershipPercentage"] = academic_ownership_percentage
        if planned_researcher_commitment_percentage is not UNSET:
            field_dict["plannedResearcherCommitmentPercentage"] = planned_researcher_commitment_percentage
        if researcher_commitments is not UNSET:
            field_dict["researcherCommitments"] = researcher_commitments
        if name is not UNSET:
            field_dict["name"] = name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.name import Name
        from ..models.researcher_commitment_type_0 import ResearcherCommitmentType0

        d = src_dict.copy()

        def _parse_role(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        role = _parse_role(d.pop("role"))

        type_discriminator = d.pop("typeDiscriminator")

        pure_id = d.pop("pureId", UNSET)

        def _parse_external_organizations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                external_organizations_type_0 = []
                _external_organizations_type_0 = data
                for external_organizations_type_0_item_data in _external_organizations_type_0:

                    def _parse_external_organizations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    external_organizations_type_0_item = _parse_external_organizations_type_0_item(
                        external_organizations_type_0_item_data
                    )

                    external_organizations_type_0.append(external_organizations_type_0_item)

                return external_organizations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        external_organizations = _parse_external_organizations(d.pop("externalOrganizations", UNSET))

        def _parse_academic_ownership_percentage(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        academic_ownership_percentage = _parse_academic_ownership_percentage(
            d.pop("academicOwnershipPercentage", UNSET)
        )

        def _parse_planned_researcher_commitment_percentage(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        planned_researcher_commitment_percentage = _parse_planned_researcher_commitment_percentage(
            d.pop("plannedResearcherCommitmentPercentage", UNSET)
        )

        researcher_commitments = []
        _researcher_commitments = d.pop("researcherCommitments", UNSET)
        for researcher_commitments_item_data in _researcher_commitments or []:

            def _parse_researcher_commitments_item(data: object) -> Union["ResearcherCommitmentType0", None]:
                if data is None:
                    return data
                try:
                    if not isinstance(data, dict):
                        raise TypeError()
                    componentsschemas_researcher_commitment_type_0 = ResearcherCommitmentType0.from_dict(data)

                    return componentsschemas_researcher_commitment_type_0
                except:  # noqa: E722
                    pass
                return cast(Union["ResearcherCommitmentType0", None], data)

            researcher_commitments_item = _parse_researcher_commitments_item(researcher_commitments_item_data)

            researcher_commitments.append(researcher_commitments_item)

        _name = d.pop("name", UNSET)
        name: Union[Unset, Name]
        if isinstance(_name, Unset):
            name = UNSET
        else:
            name = Name.from_dict(_name)

        abstract_applicant_association = cls(
            role=role,
            type_discriminator=type_discriminator,
            pure_id=pure_id,
            external_organizations=external_organizations,
            academic_ownership_percentage=academic_ownership_percentage,
            planned_researcher_commitment_percentage=planned_researcher_commitment_percentage,
            researcher_commitments=researcher_commitments,
            name=name,
        )

        abstract_applicant_association.additional_properties = d
        return abstract_applicant_association

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
