import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.abstract_equipment_person_association import AbstractEquipmentPersonAssociation
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.classified_address import ClassifiedAddress
    from ..models.classified_localized_value import ClassifiedLocalizedValue
    from ..models.classified_value import ClassifiedValue
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
    from ..models.equipment_details import EquipmentDetails
    from ..models.equipment_press_media_association import EquipmentPressMediaAssociation
    from ..models.formatted_localized_string_type_0 import FormattedLocalizedStringType0
    from ..models.image_file import ImageFile
    from ..models.keyword_group import KeywordGroup
    from ..models.visibility import Visibility
    from ..models.workflow import Workflow


T = TypeVar("T", bound="Equipment")


@_attrs_define
class Equipment:
    """An equipment known to an institution

    Attributes:
        title (Union['FormattedLocalizedStringType0', None]): A set of potentially formatted string values each
            localized for a specific submission locale. Please note that invalid locale values will be ignored. Example:
            {'en_GB': 'Some text'}.
        organizations (List[Union['ContentRefType0', None]]): A collection of affiliated organizations
        managing_organization (Union['ContentRefType0', None]):
        loan_type (Union['ClassificationRefType0', None]): A reference to a classification value
        visibility (Visibility): Visibility of an object
        type (Union['ClassificationRefType0', None]): A reference to a classification value
        workflow (Workflow): Information about workflow
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        uuid (Union[Unset, str]): UUID, this is the primary identity of the entity
        created_by (Union[Unset, str]): Username of creator
        created_date (Union[Unset, datetime.datetime]): Date and time of creation
        modified_by (Union[Unset, str]): Username of the user that performed a modification
        modified_date (Union[Unset, datetime.datetime]): Date and time of last modification
        portal_url (Union[Unset, str]): URL of the content on the Pure Portal
        pretty_url_identifiers (Union[Unset, List[str]]): All pretty URLs
        previous_uuids (Union[Unset, List[str]]): UUIDs of other content items which have been merged into this content
            item (or similar)
        version (Union[None, Unset, str]): Used to guard against conflicting updates. For new content this is null, and
            for existing content the current value. The property should never be modified by a client, except in the rare
            case where the client wants to perform an update irrespective of if other clients have made updates in the
            meantime, also known as a "dirty write". A dirty write is performed by not including the property value or
            setting the property to null
        descriptions (Union[List['ClassifiedLocalizedValue'], None, Unset]): A collection of descriptions about or
            related to the equipment
        details (Union[List['EquipmentDetails'], None, Unset]): A collection of details bout or related to the equipment
        external_organizations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of external
            organization affiliations
        contact_persons (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of contact persons
        addresses (Union[List['ClassifiedAddress'], None, Unset]): A collection of classified addresses
        phone_numbers (Union[List['ClassifiedValue'], None, Unset]): A collection of classified phone numbers
        emails (Union[List['ClassifiedValue'], None, Unset]): A collection of classified e-mail addresses
        web_addresses (Union[List['ClassifiedLocalizedValue'], None, Unset]): A collection of classified web addresses
        loan_terms (Union['FormattedLocalizedStringType0', None, Unset]): A set of potentially formatted string values
            each localized for a specific submission locale. Please note that invalid locale values will be ignored.
            Example: {'en_GB': 'Some text'}.
        press_media (Union[List['EquipmentPressMediaAssociation'], None, Unset]): A collection of related press media
        keyword_groups (Union[List['KeywordGroup'], None, Unset]): A group for each type of keyword present
        images (Union[List['ImageFile'], None, Unset]): Image files with a maximum file size of 1MB
        persons (Union[List['AbstractEquipmentPersonAssociation'], None, Unset]): The persons that the equipment is
            associated with
        research_output_relations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of research
            output relations
        impact_relations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of impacts relations
        student_thesis_relations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of student
            thesis relations
        activity_relations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of activity
            relations
        parents (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of parent equipments
        category (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        custom_defined_fields (Union['CustomDefinedFieldsType0', None, Unset]): Map of CustomDefinedField values, where
            the key is the field identifier Example: { "fieldName1": "typeDiscriminator": "Integer", "value" : 1}.
        system_name (Union[Unset, str]): The content system name
    """

    title: Union["FormattedLocalizedStringType0", None]
    organizations: List[Union["ContentRefType0", None]]
    managing_organization: Union["ContentRefType0", None]
    loan_type: Union["ClassificationRefType0", None]
    visibility: "Visibility"
    type: Union["ClassificationRefType0", None]
    workflow: "Workflow"
    pure_id: Union[Unset, int] = UNSET
    uuid: Union[Unset, str] = UNSET
    created_by: Union[Unset, str] = UNSET
    created_date: Union[Unset, datetime.datetime] = UNSET
    modified_by: Union[Unset, str] = UNSET
    modified_date: Union[Unset, datetime.datetime] = UNSET
    portal_url: Union[Unset, str] = UNSET
    pretty_url_identifiers: Union[Unset, List[str]] = UNSET
    previous_uuids: Union[Unset, List[str]] = UNSET
    version: Union[None, Unset, str] = UNSET
    descriptions: Union[List["ClassifiedLocalizedValue"], None, Unset] = UNSET
    details: Union[List["EquipmentDetails"], None, Unset] = UNSET
    external_organizations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    contact_persons: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    addresses: Union[List["ClassifiedAddress"], None, Unset] = UNSET
    phone_numbers: Union[List["ClassifiedValue"], None, Unset] = UNSET
    emails: Union[List["ClassifiedValue"], None, Unset] = UNSET
    web_addresses: Union[List["ClassifiedLocalizedValue"], None, Unset] = UNSET
    loan_terms: Union["FormattedLocalizedStringType0", None, Unset] = UNSET
    press_media: Union[List["EquipmentPressMediaAssociation"], None, Unset] = UNSET
    keyword_groups: Union[List["KeywordGroup"], None, Unset] = UNSET
    images: Union[List["ImageFile"], None, Unset] = UNSET
    persons: Union[List["AbstractEquipmentPersonAssociation"], None, Unset] = UNSET
    research_output_relations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    impact_relations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    student_thesis_relations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    activity_relations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    parents: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    category: Union["ClassificationRefType0", None, Unset] = UNSET
    custom_defined_fields: Union["CustomDefinedFieldsType0", None, Unset] = UNSET
    system_name: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
        from ..models.formatted_localized_string_type_0 import FormattedLocalizedStringType0

        title: Union[Dict[str, Any], None]
        if isinstance(self.title, FormattedLocalizedStringType0):
            title = self.title.to_dict()
        else:
            title = self.title

        organizations = []
        for organizations_item_data in self.organizations:
            organizations_item: Union[Dict[str, Any], None]
            if isinstance(organizations_item_data, ContentRefType0):
                organizations_item = organizations_item_data.to_dict()
            else:
                organizations_item = organizations_item_data
            organizations.append(organizations_item)

        managing_organization: Union[Dict[str, Any], None]
        if isinstance(self.managing_organization, ContentRefType0):
            managing_organization = self.managing_organization.to_dict()
        else:
            managing_organization = self.managing_organization

        loan_type: Union[Dict[str, Any], None]
        if isinstance(self.loan_type, ClassificationRefType0):
            loan_type = self.loan_type.to_dict()
        else:
            loan_type = self.loan_type

        visibility = self.visibility.to_dict()

        type: Union[Dict[str, Any], None]
        if isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        workflow = self.workflow.to_dict()

        pure_id = self.pure_id

        uuid = self.uuid

        created_by = self.created_by

        created_date: Union[Unset, str] = UNSET
        if not isinstance(self.created_date, Unset):
            created_date = self.created_date.isoformat()

        modified_by = self.modified_by

        modified_date: Union[Unset, str] = UNSET
        if not isinstance(self.modified_date, Unset):
            modified_date = self.modified_date.isoformat()

        portal_url = self.portal_url

        pretty_url_identifiers: Union[Unset, List[str]] = UNSET
        if not isinstance(self.pretty_url_identifiers, Unset):
            pretty_url_identifiers = self.pretty_url_identifiers

        previous_uuids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.previous_uuids, Unset):
            previous_uuids = self.previous_uuids

        version: Union[None, Unset, str]
        if isinstance(self.version, Unset):
            version = UNSET
        else:
            version = self.version

        descriptions: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.descriptions, Unset):
            descriptions = UNSET
        elif isinstance(self.descriptions, list):
            descriptions = []
            for descriptions_type_0_item_data in self.descriptions:
                descriptions_type_0_item = descriptions_type_0_item_data.to_dict()
                descriptions.append(descriptions_type_0_item)

        else:
            descriptions = self.descriptions

        details: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.details, Unset):
            details = UNSET
        elif isinstance(self.details, list):
            details = []
            for details_type_0_item_data in self.details:
                details_type_0_item = details_type_0_item_data.to_dict()
                details.append(details_type_0_item)

        else:
            details = self.details

        external_organizations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.external_organizations, Unset):
            external_organizations = UNSET
        elif isinstance(self.external_organizations, list):
            external_organizations = []
            for external_organizations_type_0_item_data in self.external_organizations:
                external_organizations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(external_organizations_type_0_item_data, ContentRefType0):
                    external_organizations_type_0_item = external_organizations_type_0_item_data.to_dict()
                else:
                    external_organizations_type_0_item = external_organizations_type_0_item_data
                external_organizations.append(external_organizations_type_0_item)

        else:
            external_organizations = self.external_organizations

        contact_persons: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.contact_persons, Unset):
            contact_persons = UNSET
        elif isinstance(self.contact_persons, list):
            contact_persons = []
            for contact_persons_type_0_item_data in self.contact_persons:
                contact_persons_type_0_item: Union[Dict[str, Any], None]
                if isinstance(contact_persons_type_0_item_data, ContentRefType0):
                    contact_persons_type_0_item = contact_persons_type_0_item_data.to_dict()
                else:
                    contact_persons_type_0_item = contact_persons_type_0_item_data
                contact_persons.append(contact_persons_type_0_item)

        else:
            contact_persons = self.contact_persons

        addresses: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.addresses, Unset):
            addresses = UNSET
        elif isinstance(self.addresses, list):
            addresses = []
            for addresses_type_0_item_data in self.addresses:
                addresses_type_0_item = addresses_type_0_item_data.to_dict()
                addresses.append(addresses_type_0_item)

        else:
            addresses = self.addresses

        phone_numbers: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.phone_numbers, Unset):
            phone_numbers = UNSET
        elif isinstance(self.phone_numbers, list):
            phone_numbers = []
            for phone_numbers_type_0_item_data in self.phone_numbers:
                phone_numbers_type_0_item = phone_numbers_type_0_item_data.to_dict()
                phone_numbers.append(phone_numbers_type_0_item)

        else:
            phone_numbers = self.phone_numbers

        emails: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.emails, Unset):
            emails = UNSET
        elif isinstance(self.emails, list):
            emails = []
            for emails_type_0_item_data in self.emails:
                emails_type_0_item = emails_type_0_item_data.to_dict()
                emails.append(emails_type_0_item)

        else:
            emails = self.emails

        web_addresses: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.web_addresses, Unset):
            web_addresses = UNSET
        elif isinstance(self.web_addresses, list):
            web_addresses = []
            for web_addresses_type_0_item_data in self.web_addresses:
                web_addresses_type_0_item = web_addresses_type_0_item_data.to_dict()
                web_addresses.append(web_addresses_type_0_item)

        else:
            web_addresses = self.web_addresses

        loan_terms: Union[Dict[str, Any], None, Unset]
        if isinstance(self.loan_terms, Unset):
            loan_terms = UNSET
        elif isinstance(self.loan_terms, FormattedLocalizedStringType0):
            loan_terms = self.loan_terms.to_dict()
        else:
            loan_terms = self.loan_terms

        press_media: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.press_media, Unset):
            press_media = UNSET
        elif isinstance(self.press_media, list):
            press_media = []
            for press_media_type_0_item_data in self.press_media:
                press_media_type_0_item = press_media_type_0_item_data.to_dict()
                press_media.append(press_media_type_0_item)

        else:
            press_media = self.press_media

        keyword_groups: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.keyword_groups, Unset):
            keyword_groups = UNSET
        elif isinstance(self.keyword_groups, list):
            keyword_groups = []
            for keyword_groups_type_0_item_data in self.keyword_groups:
                keyword_groups_type_0_item = keyword_groups_type_0_item_data.to_dict()
                keyword_groups.append(keyword_groups_type_0_item)

        else:
            keyword_groups = self.keyword_groups

        images: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.images, Unset):
            images = UNSET
        elif isinstance(self.images, list):
            images = []
            for images_type_0_item_data in self.images:
                images_type_0_item = images_type_0_item_data.to_dict()
                images.append(images_type_0_item)

        else:
            images = self.images

        persons: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.persons, Unset):
            persons = UNSET
        elif isinstance(self.persons, list):
            persons = []
            for persons_type_0_item_data in self.persons:
                persons_type_0_item = persons_type_0_item_data.to_dict()
                persons.append(persons_type_0_item)

        else:
            persons = self.persons

        research_output_relations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.research_output_relations, Unset):
            research_output_relations = UNSET
        elif isinstance(self.research_output_relations, list):
            research_output_relations = []
            for research_output_relations_type_0_item_data in self.research_output_relations:
                research_output_relations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(research_output_relations_type_0_item_data, ContentRefType0):
                    research_output_relations_type_0_item = research_output_relations_type_0_item_data.to_dict()
                else:
                    research_output_relations_type_0_item = research_output_relations_type_0_item_data
                research_output_relations.append(research_output_relations_type_0_item)

        else:
            research_output_relations = self.research_output_relations

        impact_relations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.impact_relations, Unset):
            impact_relations = UNSET
        elif isinstance(self.impact_relations, list):
            impact_relations = []
            for impact_relations_type_0_item_data in self.impact_relations:
                impact_relations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(impact_relations_type_0_item_data, ContentRefType0):
                    impact_relations_type_0_item = impact_relations_type_0_item_data.to_dict()
                else:
                    impact_relations_type_0_item = impact_relations_type_0_item_data
                impact_relations.append(impact_relations_type_0_item)

        else:
            impact_relations = self.impact_relations

        student_thesis_relations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.student_thesis_relations, Unset):
            student_thesis_relations = UNSET
        elif isinstance(self.student_thesis_relations, list):
            student_thesis_relations = []
            for student_thesis_relations_type_0_item_data in self.student_thesis_relations:
                student_thesis_relations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(student_thesis_relations_type_0_item_data, ContentRefType0):
                    student_thesis_relations_type_0_item = student_thesis_relations_type_0_item_data.to_dict()
                else:
                    student_thesis_relations_type_0_item = student_thesis_relations_type_0_item_data
                student_thesis_relations.append(student_thesis_relations_type_0_item)

        else:
            student_thesis_relations = self.student_thesis_relations

        activity_relations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.activity_relations, Unset):
            activity_relations = UNSET
        elif isinstance(self.activity_relations, list):
            activity_relations = []
            for activity_relations_type_0_item_data in self.activity_relations:
                activity_relations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(activity_relations_type_0_item_data, ContentRefType0):
                    activity_relations_type_0_item = activity_relations_type_0_item_data.to_dict()
                else:
                    activity_relations_type_0_item = activity_relations_type_0_item_data
                activity_relations.append(activity_relations_type_0_item)

        else:
            activity_relations = self.activity_relations

        parents: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.parents, Unset):
            parents = UNSET
        elif isinstance(self.parents, list):
            parents = []
            for parents_type_0_item_data in self.parents:
                parents_type_0_item: Union[Dict[str, Any], None]
                if isinstance(parents_type_0_item_data, ContentRefType0):
                    parents_type_0_item = parents_type_0_item_data.to_dict()
                else:
                    parents_type_0_item = parents_type_0_item_data
                parents.append(parents_type_0_item)

        else:
            parents = self.parents

        category: Union[Dict[str, Any], None, Unset]
        if isinstance(self.category, Unset):
            category = UNSET
        elif isinstance(self.category, ClassificationRefType0):
            category = self.category.to_dict()
        else:
            category = self.category

        custom_defined_fields: Union[Dict[str, Any], None, Unset]
        if isinstance(self.custom_defined_fields, Unset):
            custom_defined_fields = UNSET
        elif isinstance(self.custom_defined_fields, CustomDefinedFieldsType0):
            custom_defined_fields = self.custom_defined_fields.to_dict()
        else:
            custom_defined_fields = self.custom_defined_fields

        system_name = self.system_name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "title": title,
                "organizations": organizations,
                "managingOrganization": managing_organization,
                "loanType": loan_type,
                "visibility": visibility,
                "type": type,
                "workflow": workflow,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if uuid is not UNSET:
            field_dict["uuid"] = uuid
        if created_by is not UNSET:
            field_dict["createdBy"] = created_by
        if created_date is not UNSET:
            field_dict["createdDate"] = created_date
        if modified_by is not UNSET:
            field_dict["modifiedBy"] = modified_by
        if modified_date is not UNSET:
            field_dict["modifiedDate"] = modified_date
        if portal_url is not UNSET:
            field_dict["portalUrl"] = portal_url
        if pretty_url_identifiers is not UNSET:
            field_dict["prettyUrlIdentifiers"] = pretty_url_identifiers
        if previous_uuids is not UNSET:
            field_dict["previousUuids"] = previous_uuids
        if version is not UNSET:
            field_dict["version"] = version
        if descriptions is not UNSET:
            field_dict["descriptions"] = descriptions
        if details is not UNSET:
            field_dict["details"] = details
        if external_organizations is not UNSET:
            field_dict["externalOrganizations"] = external_organizations
        if contact_persons is not UNSET:
            field_dict["contactPersons"] = contact_persons
        if addresses is not UNSET:
            field_dict["addresses"] = addresses
        if phone_numbers is not UNSET:
            field_dict["phoneNumbers"] = phone_numbers
        if emails is not UNSET:
            field_dict["emails"] = emails
        if web_addresses is not UNSET:
            field_dict["webAddresses"] = web_addresses
        if loan_terms is not UNSET:
            field_dict["loanTerms"] = loan_terms
        if press_media is not UNSET:
            field_dict["pressMedia"] = press_media
        if keyword_groups is not UNSET:
            field_dict["keywordGroups"] = keyword_groups
        if images is not UNSET:
            field_dict["images"] = images
        if persons is not UNSET:
            field_dict["persons"] = persons
        if research_output_relations is not UNSET:
            field_dict["researchOutputRelations"] = research_output_relations
        if impact_relations is not UNSET:
            field_dict["impactRelations"] = impact_relations
        if student_thesis_relations is not UNSET:
            field_dict["studentThesisRelations"] = student_thesis_relations
        if activity_relations is not UNSET:
            field_dict["activityRelations"] = activity_relations
        if parents is not UNSET:
            field_dict["parents"] = parents
        if category is not UNSET:
            field_dict["category"] = category
        if custom_defined_fields is not UNSET:
            field_dict["customDefinedFields"] = custom_defined_fields
        if system_name is not UNSET:
            field_dict["systemName"] = system_name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.abstract_equipment_person_association import AbstractEquipmentPersonAssociation
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.classified_address import ClassifiedAddress
        from ..models.classified_localized_value import ClassifiedLocalizedValue
        from ..models.classified_value import ClassifiedValue
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
        from ..models.equipment_details import EquipmentDetails
        from ..models.equipment_press_media_association import EquipmentPressMediaAssociation
        from ..models.formatted_localized_string_type_0 import FormattedLocalizedStringType0
        from ..models.image_file import ImageFile
        from ..models.keyword_group import KeywordGroup
        from ..models.visibility import Visibility
        from ..models.workflow import Workflow

        d = src_dict.copy()

        def _parse_title(data: object) -> Union["FormattedLocalizedStringType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_formatted_localized_string_type_0 = FormattedLocalizedStringType0.from_dict(data)

                return componentsschemas_formatted_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["FormattedLocalizedStringType0", None], data)

        title = _parse_title(d.pop("title"))

        organizations = []
        _organizations = d.pop("organizations")
        for organizations_item_data in _organizations:

            def _parse_organizations_item(data: object) -> Union["ContentRefType0", None]:
                if data is None:
                    return data
                try:
                    if not isinstance(data, dict):
                        raise TypeError()
                    componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                    return componentsschemas_content_ref_type_0
                except:  # noqa: E722
                    pass
                return cast(Union["ContentRefType0", None], data)

            organizations_item = _parse_organizations_item(organizations_item_data)

            organizations.append(organizations_item)

        def _parse_managing_organization(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        managing_organization = _parse_managing_organization(d.pop("managingOrganization"))

        def _parse_loan_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        loan_type = _parse_loan_type(d.pop("loanType"))

        visibility = Visibility.from_dict(d.pop("visibility"))

        def _parse_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        type = _parse_type(d.pop("type"))

        workflow = Workflow.from_dict(d.pop("workflow"))

        pure_id = d.pop("pureId", UNSET)

        uuid = d.pop("uuid", UNSET)

        created_by = d.pop("createdBy", UNSET)

        _created_date = d.pop("createdDate", UNSET)
        created_date: Union[Unset, datetime.datetime]
        if isinstance(_created_date, Unset):
            created_date = UNSET
        else:
            created_date = isoparse(_created_date)

        modified_by = d.pop("modifiedBy", UNSET)

        _modified_date = d.pop("modifiedDate", UNSET)
        modified_date: Union[Unset, datetime.datetime]
        if isinstance(_modified_date, Unset):
            modified_date = UNSET
        else:
            modified_date = isoparse(_modified_date)

        portal_url = d.pop("portalUrl", UNSET)

        pretty_url_identifiers = cast(List[str], d.pop("prettyUrlIdentifiers", UNSET))

        previous_uuids = cast(List[str], d.pop("previousUuids", UNSET))

        def _parse_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        version = _parse_version(d.pop("version", UNSET))

        def _parse_descriptions(data: object) -> Union[List["ClassifiedLocalizedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                descriptions_type_0 = []
                _descriptions_type_0 = data
                for descriptions_type_0_item_data in _descriptions_type_0:
                    descriptions_type_0_item = ClassifiedLocalizedValue.from_dict(descriptions_type_0_item_data)

                    descriptions_type_0.append(descriptions_type_0_item)

                return descriptions_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedLocalizedValue"], None, Unset], data)

        descriptions = _parse_descriptions(d.pop("descriptions", UNSET))

        def _parse_details(data: object) -> Union[List["EquipmentDetails"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                details_type_0 = []
                _details_type_0 = data
                for details_type_0_item_data in _details_type_0:
                    details_type_0_item = EquipmentDetails.from_dict(details_type_0_item_data)

                    details_type_0.append(details_type_0_item)

                return details_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["EquipmentDetails"], None, Unset], data)

        details = _parse_details(d.pop("details", UNSET))

        def _parse_external_organizations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                external_organizations_type_0 = []
                _external_organizations_type_0 = data
                for external_organizations_type_0_item_data in _external_organizations_type_0:

                    def _parse_external_organizations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    external_organizations_type_0_item = _parse_external_organizations_type_0_item(
                        external_organizations_type_0_item_data
                    )

                    external_organizations_type_0.append(external_organizations_type_0_item)

                return external_organizations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        external_organizations = _parse_external_organizations(d.pop("externalOrganizations", UNSET))

        def _parse_contact_persons(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                contact_persons_type_0 = []
                _contact_persons_type_0 = data
                for contact_persons_type_0_item_data in _contact_persons_type_0:

                    def _parse_contact_persons_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    contact_persons_type_0_item = _parse_contact_persons_type_0_item(contact_persons_type_0_item_data)

                    contact_persons_type_0.append(contact_persons_type_0_item)

                return contact_persons_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        contact_persons = _parse_contact_persons(d.pop("contactPersons", UNSET))

        def _parse_addresses(data: object) -> Union[List["ClassifiedAddress"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                addresses_type_0 = []
                _addresses_type_0 = data
                for addresses_type_0_item_data in _addresses_type_0:
                    addresses_type_0_item = ClassifiedAddress.from_dict(addresses_type_0_item_data)

                    addresses_type_0.append(addresses_type_0_item)

                return addresses_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedAddress"], None, Unset], data)

        addresses = _parse_addresses(d.pop("addresses", UNSET))

        def _parse_phone_numbers(data: object) -> Union[List["ClassifiedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                phone_numbers_type_0 = []
                _phone_numbers_type_0 = data
                for phone_numbers_type_0_item_data in _phone_numbers_type_0:
                    phone_numbers_type_0_item = ClassifiedValue.from_dict(phone_numbers_type_0_item_data)

                    phone_numbers_type_0.append(phone_numbers_type_0_item)

                return phone_numbers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedValue"], None, Unset], data)

        phone_numbers = _parse_phone_numbers(d.pop("phoneNumbers", UNSET))

        def _parse_emails(data: object) -> Union[List["ClassifiedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                emails_type_0 = []
                _emails_type_0 = data
                for emails_type_0_item_data in _emails_type_0:
                    emails_type_0_item = ClassifiedValue.from_dict(emails_type_0_item_data)

                    emails_type_0.append(emails_type_0_item)

                return emails_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedValue"], None, Unset], data)

        emails = _parse_emails(d.pop("emails", UNSET))

        def _parse_web_addresses(data: object) -> Union[List["ClassifiedLocalizedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                web_addresses_type_0 = []
                _web_addresses_type_0 = data
                for web_addresses_type_0_item_data in _web_addresses_type_0:
                    web_addresses_type_0_item = ClassifiedLocalizedValue.from_dict(web_addresses_type_0_item_data)

                    web_addresses_type_0.append(web_addresses_type_0_item)

                return web_addresses_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedLocalizedValue"], None, Unset], data)

        web_addresses = _parse_web_addresses(d.pop("webAddresses", UNSET))

        def _parse_loan_terms(data: object) -> Union["FormattedLocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_formatted_localized_string_type_0 = FormattedLocalizedStringType0.from_dict(data)

                return componentsschemas_formatted_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["FormattedLocalizedStringType0", None, Unset], data)

        loan_terms = _parse_loan_terms(d.pop("loanTerms", UNSET))

        def _parse_press_media(data: object) -> Union[List["EquipmentPressMediaAssociation"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                press_media_type_0 = []
                _press_media_type_0 = data
                for press_media_type_0_item_data in _press_media_type_0:
                    press_media_type_0_item = EquipmentPressMediaAssociation.from_dict(press_media_type_0_item_data)

                    press_media_type_0.append(press_media_type_0_item)

                return press_media_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["EquipmentPressMediaAssociation"], None, Unset], data)

        press_media = _parse_press_media(d.pop("pressMedia", UNSET))

        def _parse_keyword_groups(data: object) -> Union[List["KeywordGroup"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                keyword_groups_type_0 = []
                _keyword_groups_type_0 = data
                for keyword_groups_type_0_item_data in _keyword_groups_type_0:
                    keyword_groups_type_0_item = KeywordGroup.from_dict(keyword_groups_type_0_item_data)

                    keyword_groups_type_0.append(keyword_groups_type_0_item)

                return keyword_groups_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["KeywordGroup"], None, Unset], data)

        keyword_groups = _parse_keyword_groups(d.pop("keywordGroups", UNSET))

        def _parse_images(data: object) -> Union[List["ImageFile"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                images_type_0 = []
                _images_type_0 = data
                for images_type_0_item_data in _images_type_0:
                    images_type_0_item = ImageFile.from_dict(images_type_0_item_data)

                    images_type_0.append(images_type_0_item)

                return images_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ImageFile"], None, Unset], data)

        images = _parse_images(d.pop("images", UNSET))

        def _parse_persons(data: object) -> Union[List["AbstractEquipmentPersonAssociation"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                persons_type_0 = []
                _persons_type_0 = data
                for persons_type_0_item_data in _persons_type_0:
                    persons_type_0_item = AbstractEquipmentPersonAssociation.from_dict(persons_type_0_item_data)

                    persons_type_0.append(persons_type_0_item)

                return persons_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["AbstractEquipmentPersonAssociation"], None, Unset], data)

        persons = _parse_persons(d.pop("persons", UNSET))

        def _parse_research_output_relations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                research_output_relations_type_0 = []
                _research_output_relations_type_0 = data
                for research_output_relations_type_0_item_data in _research_output_relations_type_0:

                    def _parse_research_output_relations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    research_output_relations_type_0_item = _parse_research_output_relations_type_0_item(
                        research_output_relations_type_0_item_data
                    )

                    research_output_relations_type_0.append(research_output_relations_type_0_item)

                return research_output_relations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        research_output_relations = _parse_research_output_relations(d.pop("researchOutputRelations", UNSET))

        def _parse_impact_relations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                impact_relations_type_0 = []
                _impact_relations_type_0 = data
                for impact_relations_type_0_item_data in _impact_relations_type_0:

                    def _parse_impact_relations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    impact_relations_type_0_item = _parse_impact_relations_type_0_item(
                        impact_relations_type_0_item_data
                    )

                    impact_relations_type_0.append(impact_relations_type_0_item)

                return impact_relations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        impact_relations = _parse_impact_relations(d.pop("impactRelations", UNSET))

        def _parse_student_thesis_relations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                student_thesis_relations_type_0 = []
                _student_thesis_relations_type_0 = data
                for student_thesis_relations_type_0_item_data in _student_thesis_relations_type_0:

                    def _parse_student_thesis_relations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    student_thesis_relations_type_0_item = _parse_student_thesis_relations_type_0_item(
                        student_thesis_relations_type_0_item_data
                    )

                    student_thesis_relations_type_0.append(student_thesis_relations_type_0_item)

                return student_thesis_relations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        student_thesis_relations = _parse_student_thesis_relations(d.pop("studentThesisRelations", UNSET))

        def _parse_activity_relations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                activity_relations_type_0 = []
                _activity_relations_type_0 = data
                for activity_relations_type_0_item_data in _activity_relations_type_0:

                    def _parse_activity_relations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    activity_relations_type_0_item = _parse_activity_relations_type_0_item(
                        activity_relations_type_0_item_data
                    )

                    activity_relations_type_0.append(activity_relations_type_0_item)

                return activity_relations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        activity_relations = _parse_activity_relations(d.pop("activityRelations", UNSET))

        def _parse_parents(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                parents_type_0 = []
                _parents_type_0 = data
                for parents_type_0_item_data in _parents_type_0:

                    def _parse_parents_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    parents_type_0_item = _parse_parents_type_0_item(parents_type_0_item_data)

                    parents_type_0.append(parents_type_0_item)

                return parents_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        parents = _parse_parents(d.pop("parents", UNSET))

        def _parse_category(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        category = _parse_category(d.pop("category", UNSET))

        def _parse_custom_defined_fields(data: object) -> Union["CustomDefinedFieldsType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_custom_defined_fields_type_0 = CustomDefinedFieldsType0.from_dict(data)

                return componentsschemas_custom_defined_fields_type_0
            except:  # noqa: E722
                pass
            return cast(Union["CustomDefinedFieldsType0", None, Unset], data)

        custom_defined_fields = _parse_custom_defined_fields(d.pop("customDefinedFields", UNSET))

        system_name = d.pop("systemName", UNSET)

        equipment = cls(
            title=title,
            organizations=organizations,
            managing_organization=managing_organization,
            loan_type=loan_type,
            visibility=visibility,
            type=type,
            workflow=workflow,
            pure_id=pure_id,
            uuid=uuid,
            created_by=created_by,
            created_date=created_date,
            modified_by=modified_by,
            modified_date=modified_date,
            portal_url=portal_url,
            pretty_url_identifiers=pretty_url_identifiers,
            previous_uuids=previous_uuids,
            version=version,
            descriptions=descriptions,
            details=details,
            external_organizations=external_organizations,
            contact_persons=contact_persons,
            addresses=addresses,
            phone_numbers=phone_numbers,
            emails=emails,
            web_addresses=web_addresses,
            loan_terms=loan_terms,
            press_media=press_media,
            keyword_groups=keyword_groups,
            images=images,
            persons=persons,
            research_output_relations=research_output_relations,
            impact_relations=impact_relations,
            student_thesis_relations=student_thesis_relations,
            activity_relations=activity_relations,
            parents=parents,
            category=category,
            custom_defined_fields=custom_defined_fields,
            system_name=system_name,
        )

        equipment.additional_properties = d
        return equipment

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
