import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.application_cluster_status import ApplicationClusterStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.content_ref_type_0 import ContentRefType0


T = TypeVar("T", bound="ApplicationCluster")


@_attrs_define
class ApplicationCluster:
    """An application cluster groups related applications from their individual relations.

    Attributes:
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        uuid (Union[Unset, str]): UUID, this is the primary identity of the entity
        created_by (Union[Unset, str]): Username of creator
        created_date (Union[Unset, datetime.datetime]): Date and time of creation
        modified_by (Union[Unset, str]): Username of the user that performed a modification
        modified_date (Union[Unset, datetime.datetime]): Date and time of last modification
        portal_url (Union[Unset, str]): URL of the content on the Pure Portal
        pretty_url_identifiers (Union[Unset, List[str]]): All pretty URLs
        previous_uuids (Union[Unset, List[str]]): UUIDs of other content items which have been merged into this content
            item (or similar)
        version (Union[None, Unset, str]): Used to guard against conflicting updates. For new content this is null, and
            for existing content the current value. The property should never be modified by a client, except in the rare
            case where the client wants to perform an update irrespective of if other clients have made updates in the
            meantime, also known as a "dirty write". A dirty write is performed by not including the property value or
            setting the property to null
        title (Union[Unset, str]): The title of the application cluster.
        contained_applications (Union[Unset, List[Union['ContentRefType0', None]]]):
        project (Union['ContentRefType0', None, Unset]):
        status (Union[Unset, ApplicationClusterStatus]): Application cluster status value
        date_of_status_change (Union[Unset, datetime.date]): The date of the last status change of the application
            cluster.
        system_name (Union[Unset, str]): The content system name
    """

    pure_id: Union[Unset, int] = UNSET
    uuid: Union[Unset, str] = UNSET
    created_by: Union[Unset, str] = UNSET
    created_date: Union[Unset, datetime.datetime] = UNSET
    modified_by: Union[Unset, str] = UNSET
    modified_date: Union[Unset, datetime.datetime] = UNSET
    portal_url: Union[Unset, str] = UNSET
    pretty_url_identifiers: Union[Unset, List[str]] = UNSET
    previous_uuids: Union[Unset, List[str]] = UNSET
    version: Union[None, Unset, str] = UNSET
    title: Union[Unset, str] = UNSET
    contained_applications: Union[Unset, List[Union["ContentRefType0", None]]] = UNSET
    project: Union["ContentRefType0", None, Unset] = UNSET
    status: Union[Unset, ApplicationClusterStatus] = UNSET
    date_of_status_change: Union[Unset, datetime.date] = UNSET
    system_name: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.content_ref_type_0 import ContentRefType0

        pure_id = self.pure_id

        uuid = self.uuid

        created_by = self.created_by

        created_date: Union[Unset, str] = UNSET
        if not isinstance(self.created_date, Unset):
            created_date = self.created_date.isoformat()

        modified_by = self.modified_by

        modified_date: Union[Unset, str] = UNSET
        if not isinstance(self.modified_date, Unset):
            modified_date = self.modified_date.isoformat()

        portal_url = self.portal_url

        pretty_url_identifiers: Union[Unset, List[str]] = UNSET
        if not isinstance(self.pretty_url_identifiers, Unset):
            pretty_url_identifiers = self.pretty_url_identifiers

        previous_uuids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.previous_uuids, Unset):
            previous_uuids = self.previous_uuids

        version: Union[None, Unset, str]
        if isinstance(self.version, Unset):
            version = UNSET
        else:
            version = self.version

        title = self.title

        contained_applications: Union[Unset, List[Union[Dict[str, Any], None]]] = UNSET
        if not isinstance(self.contained_applications, Unset):
            contained_applications = []
            for contained_applications_item_data in self.contained_applications:
                contained_applications_item: Union[Dict[str, Any], None]
                if isinstance(contained_applications_item_data, ContentRefType0):
                    contained_applications_item = contained_applications_item_data.to_dict()
                else:
                    contained_applications_item = contained_applications_item_data
                contained_applications.append(contained_applications_item)

        project: Union[Dict[str, Any], None, Unset]
        if isinstance(self.project, Unset):
            project = UNSET
        elif isinstance(self.project, ContentRefType0):
            project = self.project.to_dict()
        else:
            project = self.project

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        date_of_status_change: Union[Unset, str] = UNSET
        if not isinstance(self.date_of_status_change, Unset):
            date_of_status_change = self.date_of_status_change.isoformat()

        system_name = self.system_name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if uuid is not UNSET:
            field_dict["uuid"] = uuid
        if created_by is not UNSET:
            field_dict["createdBy"] = created_by
        if created_date is not UNSET:
            field_dict["createdDate"] = created_date
        if modified_by is not UNSET:
            field_dict["modifiedBy"] = modified_by
        if modified_date is not UNSET:
            field_dict["modifiedDate"] = modified_date
        if portal_url is not UNSET:
            field_dict["portalUrl"] = portal_url
        if pretty_url_identifiers is not UNSET:
            field_dict["prettyUrlIdentifiers"] = pretty_url_identifiers
        if previous_uuids is not UNSET:
            field_dict["previousUuids"] = previous_uuids
        if version is not UNSET:
            field_dict["version"] = version
        if title is not UNSET:
            field_dict["title"] = title
        if contained_applications is not UNSET:
            field_dict["containedApplications"] = contained_applications
        if project is not UNSET:
            field_dict["project"] = project
        if status is not UNSET:
            field_dict["status"] = status
        if date_of_status_change is not UNSET:
            field_dict["dateOfStatusChange"] = date_of_status_change
        if system_name is not UNSET:
            field_dict["systemName"] = system_name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.content_ref_type_0 import ContentRefType0

        d = src_dict.copy()
        pure_id = d.pop("pureId", UNSET)

        uuid = d.pop("uuid", UNSET)

        created_by = d.pop("createdBy", UNSET)

        _created_date = d.pop("createdDate", UNSET)
        created_date: Union[Unset, datetime.datetime]
        if isinstance(_created_date, Unset):
            created_date = UNSET
        else:
            created_date = isoparse(_created_date)

        modified_by = d.pop("modifiedBy", UNSET)

        _modified_date = d.pop("modifiedDate", UNSET)
        modified_date: Union[Unset, datetime.datetime]
        if isinstance(_modified_date, Unset):
            modified_date = UNSET
        else:
            modified_date = isoparse(_modified_date)

        portal_url = d.pop("portalUrl", UNSET)

        pretty_url_identifiers = cast(List[str], d.pop("prettyUrlIdentifiers", UNSET))

        previous_uuids = cast(List[str], d.pop("previousUuids", UNSET))

        def _parse_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        version = _parse_version(d.pop("version", UNSET))

        title = d.pop("title", UNSET)

        contained_applications = []
        _contained_applications = d.pop("containedApplications", UNSET)
        for contained_applications_item_data in _contained_applications or []:

            def _parse_contained_applications_item(data: object) -> Union["ContentRefType0", None]:
                if data is None:
                    return data
                try:
                    if not isinstance(data, dict):
                        raise TypeError()
                    componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                    return componentsschemas_content_ref_type_0
                except:  # noqa: E722
                    pass
                return cast(Union["ContentRefType0", None], data)

            contained_applications_item = _parse_contained_applications_item(contained_applications_item_data)

            contained_applications.append(contained_applications_item)

        def _parse_project(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        project = _parse_project(d.pop("project", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, ApplicationClusterStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = ApplicationClusterStatus(_status)

        _date_of_status_change = d.pop("dateOfStatusChange", UNSET)
        date_of_status_change: Union[Unset, datetime.date]
        if isinstance(_date_of_status_change, Unset):
            date_of_status_change = UNSET
        else:
            date_of_status_change = isoparse(_date_of_status_change).date()

        system_name = d.pop("systemName", UNSET)

        application_cluster = cls(
            pure_id=pure_id,
            uuid=uuid,
            created_by=created_by,
            created_date=created_date,
            modified_by=modified_by,
            modified_date=modified_date,
            portal_url=portal_url,
            pretty_url_identifiers=pretty_url_identifiers,
            previous_uuids=previous_uuids,
            version=version,
            title=title,
            contained_applications=contained_applications,
            project=project,
            status=status,
            date_of_status_change=date_of_status_change,
            system_name=system_name,
        )

        application_cluster.additional_properties = d
        return application_cluster

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
