from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.keyword_group_configuration import KeywordGroupConfiguration


T = TypeVar("T", bound="KeywordGroupConfigurationList")


@_attrs_define
class KeywordGroupConfigurationList:
    """List of keyword group configurations

    Attributes:
        configurations (Union[Unset, List['KeywordGroupConfiguration']]):
    """

    configurations: Union[Unset, List["KeywordGroupConfiguration"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        configurations: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.configurations, Unset):
            configurations = []
            for configurations_item_data in self.configurations:
                configurations_item = configurations_item_data.to_dict()
                configurations.append(configurations_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if configurations is not UNSET:
            field_dict["configurations"] = configurations

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.keyword_group_configuration import KeywordGroupConfiguration

        d = src_dict.copy()
        configurations = []
        _configurations = d.pop("configurations", UNSET)
        for configurations_item_data in _configurations or []:
            configurations_item = KeywordGroupConfiguration.from_dict(configurations_item_data)

            configurations.append(configurations_item)

        keyword_group_configuration_list = cls(
            configurations=configurations,
        )

        keyword_group_configuration_list.additional_properties = d
        return keyword_group_configuration_list

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
