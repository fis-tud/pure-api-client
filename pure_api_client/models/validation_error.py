from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.system_localized_string_type_0 import SystemLocalizedStringType0


T = TypeVar("T", bound="ValidationError")


@_attrs_define
class ValidationError:
    """Validation error

    Attributes:
        path (Union[Unset, str]):
        code (Union[Unset, str]):
        title (Union['SystemLocalizedStringType0', None, Unset]): A set of localized string values each for a specific
            UI locale. Example: {'en_GB': 'Some text'}.
    """

    path: Union[Unset, str] = UNSET
    code: Union[Unset, str] = UNSET
    title: Union["SystemLocalizedStringType0", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.system_localized_string_type_0 import SystemLocalizedStringType0

        path = self.path

        code = self.code

        title: Union[Dict[str, Any], None, Unset]
        if isinstance(self.title, Unset):
            title = UNSET
        elif isinstance(self.title, SystemLocalizedStringType0):
            title = self.title.to_dict()
        else:
            title = self.title

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if path is not UNSET:
            field_dict["path"] = path
        if code is not UNSET:
            field_dict["code"] = code
        if title is not UNSET:
            field_dict["title"] = title

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.system_localized_string_type_0 import SystemLocalizedStringType0

        d = src_dict.copy()
        path = d.pop("path", UNSET)

        code = d.pop("code", UNSET)

        def _parse_title(data: object) -> Union["SystemLocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_system_localized_string_type_0 = SystemLocalizedStringType0.from_dict(data)

                return componentsschemas_system_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["SystemLocalizedStringType0", None, Unset], data)

        title = _parse_title(d.pop("title", UNSET))

        validation_error = cls(
            path=path,
            code=code,
            title=title,
        )

        validation_error.additional_properties = d
        return validation_error

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
