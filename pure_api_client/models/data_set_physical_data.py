from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0


T = TypeVar("T", bound="DataSetPhysicalData")


@_attrs_define
class DataSetPhysicalData:
    """A description of physical data.

    Attributes:
        title (str): Title.
        storage_location (str): Storage location.
        media (str): Media.
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        access_description (Union[None, Unset, str]): Access description.
        type (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
    """

    title: str
    storage_location: str
    media: str
    pure_id: Union[Unset, int] = UNSET
    access_description: Union[None, Unset, str] = UNSET
    type: Union["ClassificationRefType0", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0

        title = self.title

        storage_location = self.storage_location

        media = self.media

        pure_id = self.pure_id

        access_description: Union[None, Unset, str]
        if isinstance(self.access_description, Unset):
            access_description = UNSET
        else:
            access_description = self.access_description

        type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.type, Unset):
            type = UNSET
        elif isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "title": title,
                "storageLocation": storage_location,
                "media": media,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if access_description is not UNSET:
            field_dict["accessDescription"] = access_description
        if type is not UNSET:
            field_dict["type"] = type

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0

        d = src_dict.copy()
        title = d.pop("title")

        storage_location = d.pop("storageLocation")

        media = d.pop("media")

        pure_id = d.pop("pureId", UNSET)

        def _parse_access_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        access_description = _parse_access_description(d.pop("accessDescription", UNSET))

        def _parse_type(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        type = _parse_type(d.pop("type", UNSET))

        data_set_physical_data = cls(
            title=title,
            storage_location=storage_location,
            media=media,
            pure_id=pure_id,
            access_description=access_description,
            type=type,
        )

        data_set_physical_data.additional_properties = d
        return data_set_physical_data

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
