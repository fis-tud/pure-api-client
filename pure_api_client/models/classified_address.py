from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.geo_location_type_0 import GeoLocationType0


T = TypeVar("T", bound="ClassifiedAddress")


@_attrs_define
class ClassifiedAddress:
    """A physical address

    Attributes:
        address_type (Union['ClassificationRefType0', None]): A reference to a classification value
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        address_lines (Union[None, Unset, str]): Address for personal residence
        street (Union[None, Unset, str]): The name of the street
        building (Union[None, Unset, str]): The name of the building
        postalcode (Union[None, Unset, str]): The postal code of the city
        city (Union[None, Unset, str]): The name of the city
        country (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        subdivision (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        geo_location (Union['GeoLocationType0', None, Unset]): Geographical location
    """

    address_type: Union["ClassificationRefType0", None]
    pure_id: Union[Unset, int] = UNSET
    address_lines: Union[None, Unset, str] = UNSET
    street: Union[None, Unset, str] = UNSET
    building: Union[None, Unset, str] = UNSET
    postalcode: Union[None, Unset, str] = UNSET
    city: Union[None, Unset, str] = UNSET
    country: Union["ClassificationRefType0", None, Unset] = UNSET
    subdivision: Union["ClassificationRefType0", None, Unset] = UNSET
    geo_location: Union["GeoLocationType0", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.geo_location_type_0 import GeoLocationType0

        address_type: Union[Dict[str, Any], None]
        if isinstance(self.address_type, ClassificationRefType0):
            address_type = self.address_type.to_dict()
        else:
            address_type = self.address_type

        pure_id = self.pure_id

        address_lines: Union[None, Unset, str]
        if isinstance(self.address_lines, Unset):
            address_lines = UNSET
        else:
            address_lines = self.address_lines

        street: Union[None, Unset, str]
        if isinstance(self.street, Unset):
            street = UNSET
        else:
            street = self.street

        building: Union[None, Unset, str]
        if isinstance(self.building, Unset):
            building = UNSET
        else:
            building = self.building

        postalcode: Union[None, Unset, str]
        if isinstance(self.postalcode, Unset):
            postalcode = UNSET
        else:
            postalcode = self.postalcode

        city: Union[None, Unset, str]
        if isinstance(self.city, Unset):
            city = UNSET
        else:
            city = self.city

        country: Union[Dict[str, Any], None, Unset]
        if isinstance(self.country, Unset):
            country = UNSET
        elif isinstance(self.country, ClassificationRefType0):
            country = self.country.to_dict()
        else:
            country = self.country

        subdivision: Union[Dict[str, Any], None, Unset]
        if isinstance(self.subdivision, Unset):
            subdivision = UNSET
        elif isinstance(self.subdivision, ClassificationRefType0):
            subdivision = self.subdivision.to_dict()
        else:
            subdivision = self.subdivision

        geo_location: Union[Dict[str, Any], None, Unset]
        if isinstance(self.geo_location, Unset):
            geo_location = UNSET
        elif isinstance(self.geo_location, GeoLocationType0):
            geo_location = self.geo_location.to_dict()
        else:
            geo_location = self.geo_location

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "addressType": address_type,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if address_lines is not UNSET:
            field_dict["addressLines"] = address_lines
        if street is not UNSET:
            field_dict["street"] = street
        if building is not UNSET:
            field_dict["building"] = building
        if postalcode is not UNSET:
            field_dict["postalcode"] = postalcode
        if city is not UNSET:
            field_dict["city"] = city
        if country is not UNSET:
            field_dict["country"] = country
        if subdivision is not UNSET:
            field_dict["subdivision"] = subdivision
        if geo_location is not UNSET:
            field_dict["geoLocation"] = geo_location

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.geo_location_type_0 import GeoLocationType0

        d = src_dict.copy()

        def _parse_address_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        address_type = _parse_address_type(d.pop("addressType"))

        pure_id = d.pop("pureId", UNSET)

        def _parse_address_lines(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        address_lines = _parse_address_lines(d.pop("addressLines", UNSET))

        def _parse_street(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        street = _parse_street(d.pop("street", UNSET))

        def _parse_building(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        building = _parse_building(d.pop("building", UNSET))

        def _parse_postalcode(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        postalcode = _parse_postalcode(d.pop("postalcode", UNSET))

        def _parse_city(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        city = _parse_city(d.pop("city", UNSET))

        def _parse_country(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        country = _parse_country(d.pop("country", UNSET))

        def _parse_subdivision(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        subdivision = _parse_subdivision(d.pop("subdivision", UNSET))

        def _parse_geo_location(data: object) -> Union["GeoLocationType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_geo_location_type_0 = GeoLocationType0.from_dict(data)

                return componentsschemas_geo_location_type_0
            except:  # noqa: E722
                pass
            return cast(Union["GeoLocationType0", None, Unset], data)

        geo_location = _parse_geo_location(d.pop("geoLocation", UNSET))

        classified_address = cls(
            address_type=address_type,
            pure_id=pure_id,
            address_lines=address_lines,
            street=street,
            building=building,
            postalcode=postalcode,
            city=city,
            country=country,
            subdivision=subdivision,
            geo_location=geo_location,
        )

        classified_address.additional_properties = d
        return classified_address

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
