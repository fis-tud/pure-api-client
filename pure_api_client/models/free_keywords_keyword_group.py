from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.keyword import Keyword
    from ..models.localized_string_type_0 import LocalizedStringType0


T = TypeVar("T", bound="FreeKeywordsKeywordGroup")


@_attrs_define
class FreeKeywordsKeywordGroup:
    """De-normalized version of the full keyword group representation for instances where only free keywords can be
    selected

        Attributes:
            logical_name (str): Unique name of the configuration that specifies this keyword group
            type_discriminator (str):
            pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
                entity
            name (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission locale. Note:
                invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
            keywords (Union[Unset, List['Keyword']]): Free keywords
    """

    logical_name: str
    type_discriminator: str
    pure_id: Union[Unset, int] = UNSET
    name: Union["LocalizedStringType0", None, Unset] = UNSET
    keywords: Union[Unset, List["Keyword"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.localized_string_type_0 import LocalizedStringType0

        logical_name = self.logical_name

        type_discriminator = self.type_discriminator

        pure_id = self.pure_id

        name: Union[Dict[str, Any], None, Unset]
        if isinstance(self.name, Unset):
            name = UNSET
        elif isinstance(self.name, LocalizedStringType0):
            name = self.name.to_dict()
        else:
            name = self.name

        keywords: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.keywords, Unset):
            keywords = []
            for keywords_item_data in self.keywords:
                keywords_item = keywords_item_data.to_dict()
                keywords.append(keywords_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "logicalName": logical_name,
                "typeDiscriminator": type_discriminator,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if name is not UNSET:
            field_dict["name"] = name
        if keywords is not UNSET:
            field_dict["keywords"] = keywords

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.keyword import Keyword
        from ..models.localized_string_type_0 import LocalizedStringType0

        d = src_dict.copy()
        logical_name = d.pop("logicalName")

        type_discriminator = d.pop("typeDiscriminator")

        pure_id = d.pop("pureId", UNSET)

        def _parse_name(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        name = _parse_name(d.pop("name", UNSET))

        keywords = []
        _keywords = d.pop("keywords", UNSET)
        for keywords_item_data in _keywords or []:
            keywords_item = Keyword.from_dict(keywords_item_data)

            keywords.append(keywords_item)

        free_keywords_keyword_group = cls(
            logical_name=logical_name,
            type_discriminator=type_discriminator,
            pure_id=pure_id,
            name=name,
            keywords=keywords,
        )

        free_keywords_keyword_group.additional_properties = d
        return free_keywords_keyword_group

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
