from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.navigation_link import NavigationLink
    from ..models.system_localized_string_type_0 import SystemLocalizedStringType0


T = TypeVar("T", bound="ClassificationRefType0")


@_attrs_define
class ClassificationRefType0:
    """A reference to a classification value

    Attributes:
        uri (str): Classification URI of the referred classification
        link (Union[Unset, NavigationLink]): Link to resource
        term (Union['SystemLocalizedStringType0', None, Unset]): A set of localized string values each for a specific UI
            locale. Example: {'en_GB': 'Some text'}.
    """

    uri: str
    link: Union[Unset, "NavigationLink"] = UNSET
    term: Union["SystemLocalizedStringType0", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.system_localized_string_type_0 import SystemLocalizedStringType0

        uri = self.uri

        link: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.link, Unset):
            link = self.link.to_dict()

        term: Union[Dict[str, Any], None, Unset]
        if isinstance(self.term, Unset):
            term = UNSET
        elif isinstance(self.term, SystemLocalizedStringType0):
            term = self.term.to_dict()
        else:
            term = self.term

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "uri": uri,
            }
        )
        if link is not UNSET:
            field_dict["link"] = link
        if term is not UNSET:
            field_dict["term"] = term

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.navigation_link import NavigationLink
        from ..models.system_localized_string_type_0 import SystemLocalizedStringType0

        d = src_dict.copy()
        uri = d.pop("uri")

        _link = d.pop("link", UNSET)
        link: Union[Unset, NavigationLink]
        if isinstance(_link, Unset):
            link = UNSET
        else:
            link = NavigationLink.from_dict(_link)

        def _parse_term(data: object) -> Union["SystemLocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_system_localized_string_type_0 = SystemLocalizedStringType0.from_dict(data)

                return componentsschemas_system_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["SystemLocalizedStringType0", None, Unset], data)

        term = _parse_term(d.pop("term", UNSET))

        classification_ref_type_0 = cls(
            uri=uri,
            link=link,
            term=term,
        )

        classification_ref_type_0.additional_properties = d
        return classification_ref_type_0

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
