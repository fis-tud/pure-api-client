import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.abstract_contributor_association import AbstractContributorAssociation
    from ..models.additional_file_electronic_version import AdditionalFileElectronicVersion
    from ..models.article_processing_charge import ArticleProcessingCharge
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.classified_localized_value import ClassifiedLocalizedValue
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
    from ..models.electronic_version_type_0 import ElectronicVersionType0
    from ..models.formatted_string import FormattedString
    from ..models.funding_details import FundingDetails
    from ..models.identifier import Identifier
    from ..models.image_file import ImageFile
    from ..models.keyword_group import KeywordGroup
    from ..models.link import Link
    from ..models.localized_string_type_0 import LocalizedStringType0
    from ..models.project_awardable_association_type_0 import ProjectAwardableAssociationType0
    from ..models.publication_status import PublicationStatus
    from ..models.research_output_association_type_0 import ResearchOutputAssociationType0
    from ..models.visibility import Visibility
    from ..models.workflow import Workflow


T = TypeVar("T", bound="Patent")


@_attrs_define
class Patent:
    """A patent is a document that grants the rights of a piece of work/invention to its originator.

    Attributes:
        title (FormattedString): A string containing HTML formatted text
        type (Union['ClassificationRefType0', None]): A reference to a classification value
        category (Union['ClassificationRefType0', None]): A reference to a classification value
        publication_statuses (List['PublicationStatus']): Manage the research output's different statuses and dates
            these were reached.
        language (Union['ClassificationRefType0', None]): A reference to a classification value
        contributors (List['AbstractContributorAssociation']): Contributors and their affiliations
        managing_organization (Union['ContentRefType0', None]):
        type_discriminator (str):
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        uuid (Union[Unset, str]): UUID, this is the primary identity of the entity
        created_by (Union[Unset, str]): Username of creator
        created_date (Union[Unset, datetime.datetime]): Date and time of creation
        modified_by (Union[Unset, str]): Username of the user that performed a modification
        modified_date (Union[Unset, datetime.datetime]): Date and time of last modification
        portal_url (Union[Unset, str]): URL of the content on the Pure Portal
        pretty_url_identifiers (Union[Unset, List[str]]): All pretty URLs
        previous_uuids (Union[Unset, List[str]]): UUIDs of other content items which have been merged into this content
            item (or similar)
        version (Union[None, Unset, str]): Used to guard against conflicting updates. For new content this is null, and
            for existing content the current value. The property should never be modified by a client, except in the rare
            case where the client wants to perform an update irrespective of if other clients have made updates in the
            meantime, also known as a "dirty write". A dirty write is performed by not including the property value or
            setting the property to null
        sub_title (Union[Unset, FormattedString]): A string containing HTML formatted text
        peer_review (Union[None, Unset, bool]): Indicates whether the research output is peer reviewed or not. Query the
            /research-outputs/allowed-peer-review-configurations endpoint for allowed values.
        international_peer_review (Union[None, Unset, bool]): Indicates whether the research output is internationally
            peer reviewed or not. Query the /research-outputs/allowed-peer-review-configurations endpoint for allowed
            values.
        translated_title (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission
            locale. Note: invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        translated_sub_title (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each
            submission locale. Note: invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        organizations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of organizational unit
            affiliations.
        total_number_of_contributors (Union[None, Unset, int]): The total number of authors from author collaborations.
        submission_year (Union[None, Unset, int]): The submission year of the research output.
        main_research_area (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        electronic_versions (Union[List[Union['ElectronicVersionType0', None]], None, Unset]): Electronic versions of
            this research output.
        additional_files (Union[List['AdditionalFileElectronicVersion'], None, Unset]): Additional files related to this
            research output.
        links (Union[List['Link'], None, Unset]): Additional links associated with this research output.
        article_processing_charge (Union[Unset, ArticleProcessingCharge]): Holds information on the article processing
            charge
        keyword_groups (Union[List['KeywordGroup'], None, Unset]): Groups of keywords associated with the research
            output.
        bibliographical_note (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each
            submission locale. Note: invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        descriptions (Union[List['ClassifiedLocalizedValue'], None, Unset]): A list of descriptions for this research
            output. Query the relevant /research-outputs/allowed-*-description-types endpoint for allowed types.
        visibility (Union[Unset, Visibility]): Visibility of an object
        workflow (Union[Unset, Workflow]): Information about workflow
        identifiers (Union[List['Identifier'], None, Unset]): Identifiers related to the research output.
        activities (Union[List[Union['ContentRefType0', None]], None, Unset]): Activities related to the research
            output,
        equipment (Union[List[Union['ContentRefType0', None]], None, Unset]): Equipment related to the research output.
        projects (Union[List[Union['ProjectAwardableAssociationType0', None]], None, Unset]): Projects related to the
            research output.
        research_outputs (Union[List[Union['ResearchOutputAssociationType0', None]], None, Unset]): Other related
            research outputs.
        custom_defined_fields (Union['CustomDefinedFieldsType0', None, Unset]): Map of CustomDefinedField values, where
            the key is the field identifier Example: { "fieldName1": "typeDiscriminator": "Integer", "value" : 1}.
        images (Union[List['ImageFile'], None, Unset]): Image files with a maximum file size of 1MB
        funding_text (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission
            locale. Note: invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        funding_details (Union[List['FundingDetails'], None, Unset]): The funding details for the research output
        external_organizations (Union[Unset, List[Union['ContentRefType0', None]]]):
        abstract (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission locale.
            Note: invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        system_name (Union[Unset, str]): The content system name
        country (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        ipcs (Union[List[str], None, Unset]): The International Patent Classifications
        patent_number (Union[None, Unset, str]): The patent number.
        date (Union[None, Unset, datetime.date]): The filing date.
        priority_date (Union[None, Unset, datetime.date]): The priority date.
        priority_number (Union[None, Unset, str]): The priority number.
        publisher (Union['ContentRefType0', None, Unset]):
    """

    title: "FormattedString"
    type: Union["ClassificationRefType0", None]
    category: Union["ClassificationRefType0", None]
    publication_statuses: List["PublicationStatus"]
    language: Union["ClassificationRefType0", None]
    contributors: List["AbstractContributorAssociation"]
    managing_organization: Union["ContentRefType0", None]
    type_discriminator: str
    pure_id: Union[Unset, int] = UNSET
    uuid: Union[Unset, str] = UNSET
    created_by: Union[Unset, str] = UNSET
    created_date: Union[Unset, datetime.datetime] = UNSET
    modified_by: Union[Unset, str] = UNSET
    modified_date: Union[Unset, datetime.datetime] = UNSET
    portal_url: Union[Unset, str] = UNSET
    pretty_url_identifiers: Union[Unset, List[str]] = UNSET
    previous_uuids: Union[Unset, List[str]] = UNSET
    version: Union[None, Unset, str] = UNSET
    sub_title: Union[Unset, "FormattedString"] = UNSET
    peer_review: Union[None, Unset, bool] = UNSET
    international_peer_review: Union[None, Unset, bool] = UNSET
    translated_title: Union["LocalizedStringType0", None, Unset] = UNSET
    translated_sub_title: Union["LocalizedStringType0", None, Unset] = UNSET
    organizations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    total_number_of_contributors: Union[None, Unset, int] = UNSET
    submission_year: Union[None, Unset, int] = UNSET
    main_research_area: Union["ClassificationRefType0", None, Unset] = UNSET
    electronic_versions: Union[List[Union["ElectronicVersionType0", None]], None, Unset] = UNSET
    additional_files: Union[List["AdditionalFileElectronicVersion"], None, Unset] = UNSET
    links: Union[List["Link"], None, Unset] = UNSET
    article_processing_charge: Union[Unset, "ArticleProcessingCharge"] = UNSET
    keyword_groups: Union[List["KeywordGroup"], None, Unset] = UNSET
    bibliographical_note: Union["LocalizedStringType0", None, Unset] = UNSET
    descriptions: Union[List["ClassifiedLocalizedValue"], None, Unset] = UNSET
    visibility: Union[Unset, "Visibility"] = UNSET
    workflow: Union[Unset, "Workflow"] = UNSET
    identifiers: Union[List["Identifier"], None, Unset] = UNSET
    activities: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    equipment: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    projects: Union[List[Union["ProjectAwardableAssociationType0", None]], None, Unset] = UNSET
    research_outputs: Union[List[Union["ResearchOutputAssociationType0", None]], None, Unset] = UNSET
    custom_defined_fields: Union["CustomDefinedFieldsType0", None, Unset] = UNSET
    images: Union[List["ImageFile"], None, Unset] = UNSET
    funding_text: Union["LocalizedStringType0", None, Unset] = UNSET
    funding_details: Union[List["FundingDetails"], None, Unset] = UNSET
    external_organizations: Union[Unset, List[Union["ContentRefType0", None]]] = UNSET
    abstract: Union["LocalizedStringType0", None, Unset] = UNSET
    system_name: Union[Unset, str] = UNSET
    country: Union["ClassificationRefType0", None, Unset] = UNSET
    ipcs: Union[List[str], None, Unset] = UNSET
    patent_number: Union[None, Unset, str] = UNSET
    date: Union[None, Unset, datetime.date] = UNSET
    priority_date: Union[None, Unset, datetime.date] = UNSET
    priority_number: Union[None, Unset, str] = UNSET
    publisher: Union["ContentRefType0", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
        from ..models.electronic_version_type_0 import ElectronicVersionType0
        from ..models.localized_string_type_0 import LocalizedStringType0
        from ..models.project_awardable_association_type_0 import ProjectAwardableAssociationType0
        from ..models.research_output_association_type_0 import ResearchOutputAssociationType0

        title = self.title.to_dict()

        type: Union[Dict[str, Any], None]
        if isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        category: Union[Dict[str, Any], None]
        if isinstance(self.category, ClassificationRefType0):
            category = self.category.to_dict()
        else:
            category = self.category

        publication_statuses = []
        for publication_statuses_item_data in self.publication_statuses:
            publication_statuses_item = publication_statuses_item_data.to_dict()
            publication_statuses.append(publication_statuses_item)

        language: Union[Dict[str, Any], None]
        if isinstance(self.language, ClassificationRefType0):
            language = self.language.to_dict()
        else:
            language = self.language

        contributors = []
        for contributors_item_data in self.contributors:
            contributors_item = contributors_item_data.to_dict()
            contributors.append(contributors_item)

        managing_organization: Union[Dict[str, Any], None]
        if isinstance(self.managing_organization, ContentRefType0):
            managing_organization = self.managing_organization.to_dict()
        else:
            managing_organization = self.managing_organization

        type_discriminator = self.type_discriminator

        pure_id = self.pure_id

        uuid = self.uuid

        created_by = self.created_by

        created_date: Union[Unset, str] = UNSET
        if not isinstance(self.created_date, Unset):
            created_date = self.created_date.isoformat()

        modified_by = self.modified_by

        modified_date: Union[Unset, str] = UNSET
        if not isinstance(self.modified_date, Unset):
            modified_date = self.modified_date.isoformat()

        portal_url = self.portal_url

        pretty_url_identifiers: Union[Unset, List[str]] = UNSET
        if not isinstance(self.pretty_url_identifiers, Unset):
            pretty_url_identifiers = self.pretty_url_identifiers

        previous_uuids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.previous_uuids, Unset):
            previous_uuids = self.previous_uuids

        version: Union[None, Unset, str]
        if isinstance(self.version, Unset):
            version = UNSET
        else:
            version = self.version

        sub_title: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.sub_title, Unset):
            sub_title = self.sub_title.to_dict()

        peer_review: Union[None, Unset, bool]
        if isinstance(self.peer_review, Unset):
            peer_review = UNSET
        else:
            peer_review = self.peer_review

        international_peer_review: Union[None, Unset, bool]
        if isinstance(self.international_peer_review, Unset):
            international_peer_review = UNSET
        else:
            international_peer_review = self.international_peer_review

        translated_title: Union[Dict[str, Any], None, Unset]
        if isinstance(self.translated_title, Unset):
            translated_title = UNSET
        elif isinstance(self.translated_title, LocalizedStringType0):
            translated_title = self.translated_title.to_dict()
        else:
            translated_title = self.translated_title

        translated_sub_title: Union[Dict[str, Any], None, Unset]
        if isinstance(self.translated_sub_title, Unset):
            translated_sub_title = UNSET
        elif isinstance(self.translated_sub_title, LocalizedStringType0):
            translated_sub_title = self.translated_sub_title.to_dict()
        else:
            translated_sub_title = self.translated_sub_title

        organizations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.organizations, Unset):
            organizations = UNSET
        elif isinstance(self.organizations, list):
            organizations = []
            for organizations_type_0_item_data in self.organizations:
                organizations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(organizations_type_0_item_data, ContentRefType0):
                    organizations_type_0_item = organizations_type_0_item_data.to_dict()
                else:
                    organizations_type_0_item = organizations_type_0_item_data
                organizations.append(organizations_type_0_item)

        else:
            organizations = self.organizations

        total_number_of_contributors: Union[None, Unset, int]
        if isinstance(self.total_number_of_contributors, Unset):
            total_number_of_contributors = UNSET
        else:
            total_number_of_contributors = self.total_number_of_contributors

        submission_year: Union[None, Unset, int]
        if isinstance(self.submission_year, Unset):
            submission_year = UNSET
        else:
            submission_year = self.submission_year

        main_research_area: Union[Dict[str, Any], None, Unset]
        if isinstance(self.main_research_area, Unset):
            main_research_area = UNSET
        elif isinstance(self.main_research_area, ClassificationRefType0):
            main_research_area = self.main_research_area.to_dict()
        else:
            main_research_area = self.main_research_area

        electronic_versions: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.electronic_versions, Unset):
            electronic_versions = UNSET
        elif isinstance(self.electronic_versions, list):
            electronic_versions = []
            for electronic_versions_type_0_item_data in self.electronic_versions:
                electronic_versions_type_0_item: Union[Dict[str, Any], None]
                if isinstance(electronic_versions_type_0_item_data, ElectronicVersionType0):
                    electronic_versions_type_0_item = electronic_versions_type_0_item_data.to_dict()
                else:
                    electronic_versions_type_0_item = electronic_versions_type_0_item_data
                electronic_versions.append(electronic_versions_type_0_item)

        else:
            electronic_versions = self.electronic_versions

        additional_files: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.additional_files, Unset):
            additional_files = UNSET
        elif isinstance(self.additional_files, list):
            additional_files = []
            for additional_files_type_0_item_data in self.additional_files:
                additional_files_type_0_item = additional_files_type_0_item_data.to_dict()
                additional_files.append(additional_files_type_0_item)

        else:
            additional_files = self.additional_files

        links: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.links, Unset):
            links = UNSET
        elif isinstance(self.links, list):
            links = []
            for links_type_0_item_data in self.links:
                links_type_0_item = links_type_0_item_data.to_dict()
                links.append(links_type_0_item)

        else:
            links = self.links

        article_processing_charge: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.article_processing_charge, Unset):
            article_processing_charge = self.article_processing_charge.to_dict()

        keyword_groups: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.keyword_groups, Unset):
            keyword_groups = UNSET
        elif isinstance(self.keyword_groups, list):
            keyword_groups = []
            for keyword_groups_type_0_item_data in self.keyword_groups:
                keyword_groups_type_0_item = keyword_groups_type_0_item_data.to_dict()
                keyword_groups.append(keyword_groups_type_0_item)

        else:
            keyword_groups = self.keyword_groups

        bibliographical_note: Union[Dict[str, Any], None, Unset]
        if isinstance(self.bibliographical_note, Unset):
            bibliographical_note = UNSET
        elif isinstance(self.bibliographical_note, LocalizedStringType0):
            bibliographical_note = self.bibliographical_note.to_dict()
        else:
            bibliographical_note = self.bibliographical_note

        descriptions: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.descriptions, Unset):
            descriptions = UNSET
        elif isinstance(self.descriptions, list):
            descriptions = []
            for descriptions_type_0_item_data in self.descriptions:
                descriptions_type_0_item = descriptions_type_0_item_data.to_dict()
                descriptions.append(descriptions_type_0_item)

        else:
            descriptions = self.descriptions

        visibility: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.visibility, Unset):
            visibility = self.visibility.to_dict()

        workflow: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.workflow, Unset):
            workflow = self.workflow.to_dict()

        identifiers: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.identifiers, Unset):
            identifiers = UNSET
        elif isinstance(self.identifiers, list):
            identifiers = []
            for identifiers_type_0_item_data in self.identifiers:
                identifiers_type_0_item = identifiers_type_0_item_data.to_dict()
                identifiers.append(identifiers_type_0_item)

        else:
            identifiers = self.identifiers

        activities: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.activities, Unset):
            activities = UNSET
        elif isinstance(self.activities, list):
            activities = []
            for activities_type_0_item_data in self.activities:
                activities_type_0_item: Union[Dict[str, Any], None]
                if isinstance(activities_type_0_item_data, ContentRefType0):
                    activities_type_0_item = activities_type_0_item_data.to_dict()
                else:
                    activities_type_0_item = activities_type_0_item_data
                activities.append(activities_type_0_item)

        else:
            activities = self.activities

        equipment: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.equipment, Unset):
            equipment = UNSET
        elif isinstance(self.equipment, list):
            equipment = []
            for equipment_type_0_item_data in self.equipment:
                equipment_type_0_item: Union[Dict[str, Any], None]
                if isinstance(equipment_type_0_item_data, ContentRefType0):
                    equipment_type_0_item = equipment_type_0_item_data.to_dict()
                else:
                    equipment_type_0_item = equipment_type_0_item_data
                equipment.append(equipment_type_0_item)

        else:
            equipment = self.equipment

        projects: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.projects, Unset):
            projects = UNSET
        elif isinstance(self.projects, list):
            projects = []
            for projects_type_0_item_data in self.projects:
                projects_type_0_item: Union[Dict[str, Any], None]
                if isinstance(projects_type_0_item_data, ProjectAwardableAssociationType0):
                    projects_type_0_item = projects_type_0_item_data.to_dict()
                else:
                    projects_type_0_item = projects_type_0_item_data
                projects.append(projects_type_0_item)

        else:
            projects = self.projects

        research_outputs: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.research_outputs, Unset):
            research_outputs = UNSET
        elif isinstance(self.research_outputs, list):
            research_outputs = []
            for research_outputs_type_0_item_data in self.research_outputs:
                research_outputs_type_0_item: Union[Dict[str, Any], None]
                if isinstance(research_outputs_type_0_item_data, ResearchOutputAssociationType0):
                    research_outputs_type_0_item = research_outputs_type_0_item_data.to_dict()
                else:
                    research_outputs_type_0_item = research_outputs_type_0_item_data
                research_outputs.append(research_outputs_type_0_item)

        else:
            research_outputs = self.research_outputs

        custom_defined_fields: Union[Dict[str, Any], None, Unset]
        if isinstance(self.custom_defined_fields, Unset):
            custom_defined_fields = UNSET
        elif isinstance(self.custom_defined_fields, CustomDefinedFieldsType0):
            custom_defined_fields = self.custom_defined_fields.to_dict()
        else:
            custom_defined_fields = self.custom_defined_fields

        images: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.images, Unset):
            images = UNSET
        elif isinstance(self.images, list):
            images = []
            for images_type_0_item_data in self.images:
                images_type_0_item = images_type_0_item_data.to_dict()
                images.append(images_type_0_item)

        else:
            images = self.images

        funding_text: Union[Dict[str, Any], None, Unset]
        if isinstance(self.funding_text, Unset):
            funding_text = UNSET
        elif isinstance(self.funding_text, LocalizedStringType0):
            funding_text = self.funding_text.to_dict()
        else:
            funding_text = self.funding_text

        funding_details: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.funding_details, Unset):
            funding_details = UNSET
        elif isinstance(self.funding_details, list):
            funding_details = []
            for funding_details_type_0_item_data in self.funding_details:
                funding_details_type_0_item = funding_details_type_0_item_data.to_dict()
                funding_details.append(funding_details_type_0_item)

        else:
            funding_details = self.funding_details

        external_organizations: Union[Unset, List[Union[Dict[str, Any], None]]] = UNSET
        if not isinstance(self.external_organizations, Unset):
            external_organizations = []
            for external_organizations_item_data in self.external_organizations:
                external_organizations_item: Union[Dict[str, Any], None]
                if isinstance(external_organizations_item_data, ContentRefType0):
                    external_organizations_item = external_organizations_item_data.to_dict()
                else:
                    external_organizations_item = external_organizations_item_data
                external_organizations.append(external_organizations_item)

        abstract: Union[Dict[str, Any], None, Unset]
        if isinstance(self.abstract, Unset):
            abstract = UNSET
        elif isinstance(self.abstract, LocalizedStringType0):
            abstract = self.abstract.to_dict()
        else:
            abstract = self.abstract

        system_name = self.system_name

        country: Union[Dict[str, Any], None, Unset]
        if isinstance(self.country, Unset):
            country = UNSET
        elif isinstance(self.country, ClassificationRefType0):
            country = self.country.to_dict()
        else:
            country = self.country

        ipcs: Union[List[str], None, Unset]
        if isinstance(self.ipcs, Unset):
            ipcs = UNSET
        elif isinstance(self.ipcs, list):
            ipcs = self.ipcs

        else:
            ipcs = self.ipcs

        patent_number: Union[None, Unset, str]
        if isinstance(self.patent_number, Unset):
            patent_number = UNSET
        else:
            patent_number = self.patent_number

        date: Union[None, Unset, str]
        if isinstance(self.date, Unset):
            date = UNSET
        elif isinstance(self.date, datetime.date):
            date = self.date.isoformat()
        else:
            date = self.date

        priority_date: Union[None, Unset, str]
        if isinstance(self.priority_date, Unset):
            priority_date = UNSET
        elif isinstance(self.priority_date, datetime.date):
            priority_date = self.priority_date.isoformat()
        else:
            priority_date = self.priority_date

        priority_number: Union[None, Unset, str]
        if isinstance(self.priority_number, Unset):
            priority_number = UNSET
        else:
            priority_number = self.priority_number

        publisher: Union[Dict[str, Any], None, Unset]
        if isinstance(self.publisher, Unset):
            publisher = UNSET
        elif isinstance(self.publisher, ContentRefType0):
            publisher = self.publisher.to_dict()
        else:
            publisher = self.publisher

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "title": title,
                "type": type,
                "category": category,
                "publicationStatuses": publication_statuses,
                "language": language,
                "contributors": contributors,
                "managingOrganization": managing_organization,
                "typeDiscriminator": type_discriminator,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if uuid is not UNSET:
            field_dict["uuid"] = uuid
        if created_by is not UNSET:
            field_dict["createdBy"] = created_by
        if created_date is not UNSET:
            field_dict["createdDate"] = created_date
        if modified_by is not UNSET:
            field_dict["modifiedBy"] = modified_by
        if modified_date is not UNSET:
            field_dict["modifiedDate"] = modified_date
        if portal_url is not UNSET:
            field_dict["portalUrl"] = portal_url
        if pretty_url_identifiers is not UNSET:
            field_dict["prettyUrlIdentifiers"] = pretty_url_identifiers
        if previous_uuids is not UNSET:
            field_dict["previousUuids"] = previous_uuids
        if version is not UNSET:
            field_dict["version"] = version
        if sub_title is not UNSET:
            field_dict["subTitle"] = sub_title
        if peer_review is not UNSET:
            field_dict["peerReview"] = peer_review
        if international_peer_review is not UNSET:
            field_dict["internationalPeerReview"] = international_peer_review
        if translated_title is not UNSET:
            field_dict["translatedTitle"] = translated_title
        if translated_sub_title is not UNSET:
            field_dict["translatedSubTitle"] = translated_sub_title
        if organizations is not UNSET:
            field_dict["organizations"] = organizations
        if total_number_of_contributors is not UNSET:
            field_dict["totalNumberOfContributors"] = total_number_of_contributors
        if submission_year is not UNSET:
            field_dict["submissionYear"] = submission_year
        if main_research_area is not UNSET:
            field_dict["mainResearchArea"] = main_research_area
        if electronic_versions is not UNSET:
            field_dict["electronicVersions"] = electronic_versions
        if additional_files is not UNSET:
            field_dict["additionalFiles"] = additional_files
        if links is not UNSET:
            field_dict["links"] = links
        if article_processing_charge is not UNSET:
            field_dict["articleProcessingCharge"] = article_processing_charge
        if keyword_groups is not UNSET:
            field_dict["keywordGroups"] = keyword_groups
        if bibliographical_note is not UNSET:
            field_dict["bibliographicalNote"] = bibliographical_note
        if descriptions is not UNSET:
            field_dict["descriptions"] = descriptions
        if visibility is not UNSET:
            field_dict["visibility"] = visibility
        if workflow is not UNSET:
            field_dict["workflow"] = workflow
        if identifiers is not UNSET:
            field_dict["identifiers"] = identifiers
        if activities is not UNSET:
            field_dict["activities"] = activities
        if equipment is not UNSET:
            field_dict["equipment"] = equipment
        if projects is not UNSET:
            field_dict["projects"] = projects
        if research_outputs is not UNSET:
            field_dict["researchOutputs"] = research_outputs
        if custom_defined_fields is not UNSET:
            field_dict["customDefinedFields"] = custom_defined_fields
        if images is not UNSET:
            field_dict["images"] = images
        if funding_text is not UNSET:
            field_dict["fundingText"] = funding_text
        if funding_details is not UNSET:
            field_dict["fundingDetails"] = funding_details
        if external_organizations is not UNSET:
            field_dict["externalOrganizations"] = external_organizations
        if abstract is not UNSET:
            field_dict["abstract"] = abstract
        if system_name is not UNSET:
            field_dict["systemName"] = system_name
        if country is not UNSET:
            field_dict["country"] = country
        if ipcs is not UNSET:
            field_dict["ipcs"] = ipcs
        if patent_number is not UNSET:
            field_dict["patentNumber"] = patent_number
        if date is not UNSET:
            field_dict["date"] = date
        if priority_date is not UNSET:
            field_dict["priorityDate"] = priority_date
        if priority_number is not UNSET:
            field_dict["priorityNumber"] = priority_number
        if publisher is not UNSET:
            field_dict["publisher"] = publisher

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.abstract_contributor_association import AbstractContributorAssociation
        from ..models.additional_file_electronic_version import AdditionalFileElectronicVersion
        from ..models.article_processing_charge import ArticleProcessingCharge
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.classified_localized_value import ClassifiedLocalizedValue
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
        from ..models.electronic_version_type_0 import ElectronicVersionType0
        from ..models.formatted_string import FormattedString
        from ..models.funding_details import FundingDetails
        from ..models.identifier import Identifier
        from ..models.image_file import ImageFile
        from ..models.keyword_group import KeywordGroup
        from ..models.link import Link
        from ..models.localized_string_type_0 import LocalizedStringType0
        from ..models.project_awardable_association_type_0 import ProjectAwardableAssociationType0
        from ..models.publication_status import PublicationStatus
        from ..models.research_output_association_type_0 import ResearchOutputAssociationType0
        from ..models.visibility import Visibility
        from ..models.workflow import Workflow

        d = src_dict.copy()
        title = FormattedString.from_dict(d.pop("title"))

        def _parse_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        type = _parse_type(d.pop("type"))

        def _parse_category(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        category = _parse_category(d.pop("category"))

        publication_statuses = []
        _publication_statuses = d.pop("publicationStatuses")
        for publication_statuses_item_data in _publication_statuses:
            publication_statuses_item = PublicationStatus.from_dict(publication_statuses_item_data)

            publication_statuses.append(publication_statuses_item)

        def _parse_language(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        language = _parse_language(d.pop("language"))

        contributors = []
        _contributors = d.pop("contributors")
        for contributors_item_data in _contributors:
            contributors_item = AbstractContributorAssociation.from_dict(contributors_item_data)

            contributors.append(contributors_item)

        def _parse_managing_organization(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        managing_organization = _parse_managing_organization(d.pop("managingOrganization"))

        type_discriminator = d.pop("typeDiscriminator")

        pure_id = d.pop("pureId", UNSET)

        uuid = d.pop("uuid", UNSET)

        created_by = d.pop("createdBy", UNSET)

        _created_date = d.pop("createdDate", UNSET)
        created_date: Union[Unset, datetime.datetime]
        if isinstance(_created_date, Unset):
            created_date = UNSET
        else:
            created_date = isoparse(_created_date)

        modified_by = d.pop("modifiedBy", UNSET)

        _modified_date = d.pop("modifiedDate", UNSET)
        modified_date: Union[Unset, datetime.datetime]
        if isinstance(_modified_date, Unset):
            modified_date = UNSET
        else:
            modified_date = isoparse(_modified_date)

        portal_url = d.pop("portalUrl", UNSET)

        pretty_url_identifiers = cast(List[str], d.pop("prettyUrlIdentifiers", UNSET))

        previous_uuids = cast(List[str], d.pop("previousUuids", UNSET))

        def _parse_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        version = _parse_version(d.pop("version", UNSET))

        _sub_title = d.pop("subTitle", UNSET)
        sub_title: Union[Unset, FormattedString]
        if isinstance(_sub_title, Unset):
            sub_title = UNSET
        else:
            sub_title = FormattedString.from_dict(_sub_title)

        def _parse_peer_review(data: object) -> Union[None, Unset, bool]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, bool], data)

        peer_review = _parse_peer_review(d.pop("peerReview", UNSET))

        def _parse_international_peer_review(data: object) -> Union[None, Unset, bool]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, bool], data)

        international_peer_review = _parse_international_peer_review(d.pop("internationalPeerReview", UNSET))

        def _parse_translated_title(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        translated_title = _parse_translated_title(d.pop("translatedTitle", UNSET))

        def _parse_translated_sub_title(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        translated_sub_title = _parse_translated_sub_title(d.pop("translatedSubTitle", UNSET))

        def _parse_organizations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                organizations_type_0 = []
                _organizations_type_0 = data
                for organizations_type_0_item_data in _organizations_type_0:

                    def _parse_organizations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    organizations_type_0_item = _parse_organizations_type_0_item(organizations_type_0_item_data)

                    organizations_type_0.append(organizations_type_0_item)

                return organizations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        organizations = _parse_organizations(d.pop("organizations", UNSET))

        def _parse_total_number_of_contributors(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        total_number_of_contributors = _parse_total_number_of_contributors(d.pop("totalNumberOfContributors", UNSET))

        def _parse_submission_year(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        submission_year = _parse_submission_year(d.pop("submissionYear", UNSET))

        def _parse_main_research_area(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        main_research_area = _parse_main_research_area(d.pop("mainResearchArea", UNSET))

        def _parse_electronic_versions(data: object) -> Union[List[Union["ElectronicVersionType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                electronic_versions_type_0 = []
                _electronic_versions_type_0 = data
                for electronic_versions_type_0_item_data in _electronic_versions_type_0:

                    def _parse_electronic_versions_type_0_item(data: object) -> Union["ElectronicVersionType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_electronic_version_type_0 = ElectronicVersionType0.from_dict(data)

                            return componentsschemas_electronic_version_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ElectronicVersionType0", None], data)

                    electronic_versions_type_0_item = _parse_electronic_versions_type_0_item(
                        electronic_versions_type_0_item_data
                    )

                    electronic_versions_type_0.append(electronic_versions_type_0_item)

                return electronic_versions_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ElectronicVersionType0", None]], None, Unset], data)

        electronic_versions = _parse_electronic_versions(d.pop("electronicVersions", UNSET))

        def _parse_additional_files(data: object) -> Union[List["AdditionalFileElectronicVersion"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                additional_files_type_0 = []
                _additional_files_type_0 = data
                for additional_files_type_0_item_data in _additional_files_type_0:
                    additional_files_type_0_item = AdditionalFileElectronicVersion.from_dict(
                        additional_files_type_0_item_data
                    )

                    additional_files_type_0.append(additional_files_type_0_item)

                return additional_files_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["AdditionalFileElectronicVersion"], None, Unset], data)

        additional_files = _parse_additional_files(d.pop("additionalFiles", UNSET))

        def _parse_links(data: object) -> Union[List["Link"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                links_type_0 = []
                _links_type_0 = data
                for links_type_0_item_data in _links_type_0:
                    links_type_0_item = Link.from_dict(links_type_0_item_data)

                    links_type_0.append(links_type_0_item)

                return links_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Link"], None, Unset], data)

        links = _parse_links(d.pop("links", UNSET))

        _article_processing_charge = d.pop("articleProcessingCharge", UNSET)
        article_processing_charge: Union[Unset, ArticleProcessingCharge]
        if isinstance(_article_processing_charge, Unset):
            article_processing_charge = UNSET
        else:
            article_processing_charge = ArticleProcessingCharge.from_dict(_article_processing_charge)

        def _parse_keyword_groups(data: object) -> Union[List["KeywordGroup"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                keyword_groups_type_0 = []
                _keyword_groups_type_0 = data
                for keyword_groups_type_0_item_data in _keyword_groups_type_0:
                    keyword_groups_type_0_item = KeywordGroup.from_dict(keyword_groups_type_0_item_data)

                    keyword_groups_type_0.append(keyword_groups_type_0_item)

                return keyword_groups_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["KeywordGroup"], None, Unset], data)

        keyword_groups = _parse_keyword_groups(d.pop("keywordGroups", UNSET))

        def _parse_bibliographical_note(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        bibliographical_note = _parse_bibliographical_note(d.pop("bibliographicalNote", UNSET))

        def _parse_descriptions(data: object) -> Union[List["ClassifiedLocalizedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                descriptions_type_0 = []
                _descriptions_type_0 = data
                for descriptions_type_0_item_data in _descriptions_type_0:
                    descriptions_type_0_item = ClassifiedLocalizedValue.from_dict(descriptions_type_0_item_data)

                    descriptions_type_0.append(descriptions_type_0_item)

                return descriptions_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedLocalizedValue"], None, Unset], data)

        descriptions = _parse_descriptions(d.pop("descriptions", UNSET))

        _visibility = d.pop("visibility", UNSET)
        visibility: Union[Unset, Visibility]
        if isinstance(_visibility, Unset):
            visibility = UNSET
        else:
            visibility = Visibility.from_dict(_visibility)

        _workflow = d.pop("workflow", UNSET)
        workflow: Union[Unset, Workflow]
        if isinstance(_workflow, Unset):
            workflow = UNSET
        else:
            workflow = Workflow.from_dict(_workflow)

        def _parse_identifiers(data: object) -> Union[List["Identifier"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                identifiers_type_0 = []
                _identifiers_type_0 = data
                for identifiers_type_0_item_data in _identifiers_type_0:
                    identifiers_type_0_item = Identifier.from_dict(identifiers_type_0_item_data)

                    identifiers_type_0.append(identifiers_type_0_item)

                return identifiers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Identifier"], None, Unset], data)

        identifiers = _parse_identifiers(d.pop("identifiers", UNSET))

        def _parse_activities(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                activities_type_0 = []
                _activities_type_0 = data
                for activities_type_0_item_data in _activities_type_0:

                    def _parse_activities_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    activities_type_0_item = _parse_activities_type_0_item(activities_type_0_item_data)

                    activities_type_0.append(activities_type_0_item)

                return activities_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        activities = _parse_activities(d.pop("activities", UNSET))

        def _parse_equipment(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                equipment_type_0 = []
                _equipment_type_0 = data
                for equipment_type_0_item_data in _equipment_type_0:

                    def _parse_equipment_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    equipment_type_0_item = _parse_equipment_type_0_item(equipment_type_0_item_data)

                    equipment_type_0.append(equipment_type_0_item)

                return equipment_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        equipment = _parse_equipment(d.pop("equipment", UNSET))

        def _parse_projects(data: object) -> Union[List[Union["ProjectAwardableAssociationType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                projects_type_0 = []
                _projects_type_0 = data
                for projects_type_0_item_data in _projects_type_0:

                    def _parse_projects_type_0_item(data: object) -> Union["ProjectAwardableAssociationType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_project_awardable_association_type_0 = (
                                ProjectAwardableAssociationType0.from_dict(data)
                            )

                            return componentsschemas_project_awardable_association_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ProjectAwardableAssociationType0", None], data)

                    projects_type_0_item = _parse_projects_type_0_item(projects_type_0_item_data)

                    projects_type_0.append(projects_type_0_item)

                return projects_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ProjectAwardableAssociationType0", None]], None, Unset], data)

        projects = _parse_projects(d.pop("projects", UNSET))

        def _parse_research_outputs(
            data: object,
        ) -> Union[List[Union["ResearchOutputAssociationType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                research_outputs_type_0 = []
                _research_outputs_type_0 = data
                for research_outputs_type_0_item_data in _research_outputs_type_0:

                    def _parse_research_outputs_type_0_item(
                        data: object,
                    ) -> Union["ResearchOutputAssociationType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_research_output_association_type_0 = (
                                ResearchOutputAssociationType0.from_dict(data)
                            )

                            return componentsschemas_research_output_association_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ResearchOutputAssociationType0", None], data)

                    research_outputs_type_0_item = _parse_research_outputs_type_0_item(
                        research_outputs_type_0_item_data
                    )

                    research_outputs_type_0.append(research_outputs_type_0_item)

                return research_outputs_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ResearchOutputAssociationType0", None]], None, Unset], data)

        research_outputs = _parse_research_outputs(d.pop("researchOutputs", UNSET))

        def _parse_custom_defined_fields(data: object) -> Union["CustomDefinedFieldsType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_custom_defined_fields_type_0 = CustomDefinedFieldsType0.from_dict(data)

                return componentsschemas_custom_defined_fields_type_0
            except:  # noqa: E722
                pass
            return cast(Union["CustomDefinedFieldsType0", None, Unset], data)

        custom_defined_fields = _parse_custom_defined_fields(d.pop("customDefinedFields", UNSET))

        def _parse_images(data: object) -> Union[List["ImageFile"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                images_type_0 = []
                _images_type_0 = data
                for images_type_0_item_data in _images_type_0:
                    images_type_0_item = ImageFile.from_dict(images_type_0_item_data)

                    images_type_0.append(images_type_0_item)

                return images_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ImageFile"], None, Unset], data)

        images = _parse_images(d.pop("images", UNSET))

        def _parse_funding_text(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        funding_text = _parse_funding_text(d.pop("fundingText", UNSET))

        def _parse_funding_details(data: object) -> Union[List["FundingDetails"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                funding_details_type_0 = []
                _funding_details_type_0 = data
                for funding_details_type_0_item_data in _funding_details_type_0:
                    funding_details_type_0_item = FundingDetails.from_dict(funding_details_type_0_item_data)

                    funding_details_type_0.append(funding_details_type_0_item)

                return funding_details_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["FundingDetails"], None, Unset], data)

        funding_details = _parse_funding_details(d.pop("fundingDetails", UNSET))

        external_organizations = []
        _external_organizations = d.pop("externalOrganizations", UNSET)
        for external_organizations_item_data in _external_organizations or []:

            def _parse_external_organizations_item(data: object) -> Union["ContentRefType0", None]:
                if data is None:
                    return data
                try:
                    if not isinstance(data, dict):
                        raise TypeError()
                    componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                    return componentsschemas_content_ref_type_0
                except:  # noqa: E722
                    pass
                return cast(Union["ContentRefType0", None], data)

            external_organizations_item = _parse_external_organizations_item(external_organizations_item_data)

            external_organizations.append(external_organizations_item)

        def _parse_abstract(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        abstract = _parse_abstract(d.pop("abstract", UNSET))

        system_name = d.pop("systemName", UNSET)

        def _parse_country(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        country = _parse_country(d.pop("country", UNSET))

        def _parse_ipcs(data: object) -> Union[List[str], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                ipcs_type_0 = cast(List[str], data)

                return ipcs_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[str], None, Unset], data)

        ipcs = _parse_ipcs(d.pop("ipcs", UNSET))

        def _parse_patent_number(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        patent_number = _parse_patent_number(d.pop("patentNumber", UNSET))

        def _parse_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                date_type_0 = isoparse(data).date()

                return date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        date = _parse_date(d.pop("date", UNSET))

        def _parse_priority_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                priority_date_type_0 = isoparse(data).date()

                return priority_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        priority_date = _parse_priority_date(d.pop("priorityDate", UNSET))

        def _parse_priority_number(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        priority_number = _parse_priority_number(d.pop("priorityNumber", UNSET))

        def _parse_publisher(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        publisher = _parse_publisher(d.pop("publisher", UNSET))

        patent = cls(
            title=title,
            type=type,
            category=category,
            publication_statuses=publication_statuses,
            language=language,
            contributors=contributors,
            managing_organization=managing_organization,
            type_discriminator=type_discriminator,
            pure_id=pure_id,
            uuid=uuid,
            created_by=created_by,
            created_date=created_date,
            modified_by=modified_by,
            modified_date=modified_date,
            portal_url=portal_url,
            pretty_url_identifiers=pretty_url_identifiers,
            previous_uuids=previous_uuids,
            version=version,
            sub_title=sub_title,
            peer_review=peer_review,
            international_peer_review=international_peer_review,
            translated_title=translated_title,
            translated_sub_title=translated_sub_title,
            organizations=organizations,
            total_number_of_contributors=total_number_of_contributors,
            submission_year=submission_year,
            main_research_area=main_research_area,
            electronic_versions=electronic_versions,
            additional_files=additional_files,
            links=links,
            article_processing_charge=article_processing_charge,
            keyword_groups=keyword_groups,
            bibliographical_note=bibliographical_note,
            descriptions=descriptions,
            visibility=visibility,
            workflow=workflow,
            identifiers=identifiers,
            activities=activities,
            equipment=equipment,
            projects=projects,
            research_outputs=research_outputs,
            custom_defined_fields=custom_defined_fields,
            images=images,
            funding_text=funding_text,
            funding_details=funding_details,
            external_organizations=external_organizations,
            abstract=abstract,
            system_name=system_name,
            country=country,
            ipcs=ipcs,
            patent_number=patent_number,
            date=date,
            priority_date=priority_date,
            priority_number=priority_number,
            publisher=publisher,
        )

        patent.additional_properties = d
        return patent

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
