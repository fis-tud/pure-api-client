from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.content_ref_type_0 import ContentRefType0


T = TypeVar("T", bound="PrizeAwardableAssociation")


@_attrs_define
class PrizeAwardableAssociation:
    """A relation to a prize, optionally in the context of the Award that made it possible.

    Attributes:
        prize (Union['ContentRefType0', None]):
        award (Union['ContentRefType0', None, Unset]):
    """

    prize: Union["ContentRefType0", None]
    award: Union["ContentRefType0", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.content_ref_type_0 import ContentRefType0

        prize: Union[Dict[str, Any], None]
        if isinstance(self.prize, ContentRefType0):
            prize = self.prize.to_dict()
        else:
            prize = self.prize

        award: Union[Dict[str, Any], None, Unset]
        if isinstance(self.award, Unset):
            award = UNSET
        elif isinstance(self.award, ContentRefType0):
            award = self.award.to_dict()
        else:
            award = self.award

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "prize": prize,
            }
        )
        if award is not UNSET:
            field_dict["award"] = award

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.content_ref_type_0 import ContentRefType0

        d = src_dict.copy()

        def _parse_prize(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        prize = _parse_prize(d.pop("prize"))

        def _parse_award(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        award = _parse_award(d.pop("award", UNSET))

        prize_awardable_association = cls(
            prize=prize,
            award=award,
        )

        prize_awardable_association.additional_properties = d
        return prize_awardable_association

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
