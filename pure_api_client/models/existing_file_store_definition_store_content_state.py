from enum import Enum


class ExistingFileStoreDefinitionStoreContentState(str, Enum):
    AWAITING_CREATION = "AWAITING_CREATION"
    AWAITING_DELETE = "AWAITING_DELETE"
    AWAITING_UPDATE = "AWAITING_UPDATE"
    CREATION_FAILED = "CREATION_FAILED"
    CREATION_IN_PROGRESS = "CREATION_IN_PROGRESS"
    DELETED = "DELETED"
    DELETE_FAILED = "DELETE_FAILED"
    DELETE_IN_PROGRESS = "DELETE_IN_PROGRESS"
    STORED = "STORED"
    UPDATE_FAILED = "UPDATE_FAILED"
    UPDATE_IN_PROGRESS = "UPDATE_IN_PROGRESS"

    def __str__(self) -> str:
        return str(self.value)
