from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.keyword_container import KeywordContainer
    from ..models.localized_string_type_0 import LocalizedStringType0


T = TypeVar("T", bound="FullKeywordGroup")


@_attrs_define
class FullKeywordGroup:
    """Keywords of a specific type. A type of keyword can be configured to solely support free keywords (represented as the
    FreeKeywordsKeywordGroup type), to solely support structured keywords (represented as the
    ClassificationsKeywordGroup type), or to both support structured keywords supplemented by optional free keywords
    (this type)

        Attributes:
            logical_name (str): Unique name of the configuration that specifies this keyword group
            type_discriminator (str):
            pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
                entity
            name (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission locale. Note:
                invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
            keyword_containers (Union[List['KeywordContainer'], None, Unset]): Containers with structured keywords and/or
                free keywords
    """

    logical_name: str
    type_discriminator: str
    pure_id: Union[Unset, int] = UNSET
    name: Union["LocalizedStringType0", None, Unset] = UNSET
    keyword_containers: Union[List["KeywordContainer"], None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.localized_string_type_0 import LocalizedStringType0

        logical_name = self.logical_name

        type_discriminator = self.type_discriminator

        pure_id = self.pure_id

        name: Union[Dict[str, Any], None, Unset]
        if isinstance(self.name, Unset):
            name = UNSET
        elif isinstance(self.name, LocalizedStringType0):
            name = self.name.to_dict()
        else:
            name = self.name

        keyword_containers: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.keyword_containers, Unset):
            keyword_containers = UNSET
        elif isinstance(self.keyword_containers, list):
            keyword_containers = []
            for keyword_containers_type_0_item_data in self.keyword_containers:
                keyword_containers_type_0_item = keyword_containers_type_0_item_data.to_dict()
                keyword_containers.append(keyword_containers_type_0_item)

        else:
            keyword_containers = self.keyword_containers

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "logicalName": logical_name,
                "typeDiscriminator": type_discriminator,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if name is not UNSET:
            field_dict["name"] = name
        if keyword_containers is not UNSET:
            field_dict["keywordContainers"] = keyword_containers

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.keyword_container import KeywordContainer
        from ..models.localized_string_type_0 import LocalizedStringType0

        d = src_dict.copy()
        logical_name = d.pop("logicalName")

        type_discriminator = d.pop("typeDiscriminator")

        pure_id = d.pop("pureId", UNSET)

        def _parse_name(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        name = _parse_name(d.pop("name", UNSET))

        def _parse_keyword_containers(data: object) -> Union[List["KeywordContainer"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                keyword_containers_type_0 = []
                _keyword_containers_type_0 = data
                for keyword_containers_type_0_item_data in _keyword_containers_type_0:
                    keyword_containers_type_0_item = KeywordContainer.from_dict(keyword_containers_type_0_item_data)

                    keyword_containers_type_0.append(keyword_containers_type_0_item)

                return keyword_containers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["KeywordContainer"], None, Unset], data)

        keyword_containers = _parse_keyword_containers(d.pop("keywordContainers", UNSET))

        full_keyword_group = cls(
            logical_name=logical_name,
            type_discriminator=type_discriminator,
            pure_id=pure_id,
            name=name,
            keyword_containers=keyword_containers,
        )

        full_keyword_group.additional_properties = d
        return full_keyword_group

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
