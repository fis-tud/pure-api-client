from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0


T = TypeVar("T", bound="ClassificationRefList")


@_attrs_define
class ClassificationRefList:
    """List of classification references

    Attributes:
        classifications (Union[Unset, List[Union['ClassificationRefType0', None]]]):
    """

    classifications: Union[Unset, List[Union["ClassificationRefType0", None]]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0

        classifications: Union[Unset, List[Union[Dict[str, Any], None]]] = UNSET
        if not isinstance(self.classifications, Unset):
            classifications = []
            for classifications_item_data in self.classifications:
                classifications_item: Union[Dict[str, Any], None]
                if isinstance(classifications_item_data, ClassificationRefType0):
                    classifications_item = classifications_item_data.to_dict()
                else:
                    classifications_item = classifications_item_data
                classifications.append(classifications_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if classifications is not UNSET:
            field_dict["classifications"] = classifications

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0

        d = src_dict.copy()
        classifications = []
        _classifications = d.pop("classifications", UNSET)
        for classifications_item_data in _classifications or []:

            def _parse_classifications_item(data: object) -> Union["ClassificationRefType0", None]:
                if data is None:
                    return data
                try:
                    if not isinstance(data, dict):
                        raise TypeError()
                    componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                    return componentsschemas_classification_ref_type_0
                except:  # noqa: E722
                    pass
                return cast(Union["ClassificationRefType0", None], data)

            classifications_item = _parse_classifications_item(classifications_item_data)

            classifications.append(classifications_item)

        classification_ref_list = cls(
            classifications=classifications,
        )

        classification_ref_list.additional_properties = d
        return classification_ref_list

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
