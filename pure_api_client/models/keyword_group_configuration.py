from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.localized_string_type_0 import LocalizedStringType0


T = TypeVar("T", bound="KeywordGroupConfiguration")


@_attrs_define
class KeywordGroupConfiguration:
    """A specification of the allowed behavior of a specified keyword group

    Attributes:
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        target_system_name (Union[Unset, str]): The content system name this configuration applies to
        keyword_group_type (Union[Unset, str]): The OpenAPI schema type of this keyword configuration
        name (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission locale. Note:
            invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        description (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission
            locale. Note: invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        classification_scheme (Union['ContentRefType0', None, Unset]):
        allow_userdefined_keywords (Union[Unset, bool]): Defines if user defined keywords are allowed
        limit_to_leaf_selection (Union[Unset, bool]):
        logical_name (Union[Unset, str]):
    """

    pure_id: Union[Unset, int] = UNSET
    target_system_name: Union[Unset, str] = UNSET
    keyword_group_type: Union[Unset, str] = UNSET
    name: Union["LocalizedStringType0", None, Unset] = UNSET
    description: Union["LocalizedStringType0", None, Unset] = UNSET
    classification_scheme: Union["ContentRefType0", None, Unset] = UNSET
    allow_userdefined_keywords: Union[Unset, bool] = UNSET
    limit_to_leaf_selection: Union[Unset, bool] = UNSET
    logical_name: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        pure_id = self.pure_id

        target_system_name = self.target_system_name

        keyword_group_type = self.keyword_group_type

        name: Union[Dict[str, Any], None, Unset]
        if isinstance(self.name, Unset):
            name = UNSET
        elif isinstance(self.name, LocalizedStringType0):
            name = self.name.to_dict()
        else:
            name = self.name

        description: Union[Dict[str, Any], None, Unset]
        if isinstance(self.description, Unset):
            description = UNSET
        elif isinstance(self.description, LocalizedStringType0):
            description = self.description.to_dict()
        else:
            description = self.description

        classification_scheme: Union[Dict[str, Any], None, Unset]
        if isinstance(self.classification_scheme, Unset):
            classification_scheme = UNSET
        elif isinstance(self.classification_scheme, ContentRefType0):
            classification_scheme = self.classification_scheme.to_dict()
        else:
            classification_scheme = self.classification_scheme

        allow_userdefined_keywords = self.allow_userdefined_keywords

        limit_to_leaf_selection = self.limit_to_leaf_selection

        logical_name = self.logical_name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if target_system_name is not UNSET:
            field_dict["targetSystemName"] = target_system_name
        if keyword_group_type is not UNSET:
            field_dict["keywordGroupType"] = keyword_group_type
        if name is not UNSET:
            field_dict["name"] = name
        if description is not UNSET:
            field_dict["description"] = description
        if classification_scheme is not UNSET:
            field_dict["classificationScheme"] = classification_scheme
        if allow_userdefined_keywords is not UNSET:
            field_dict["allowUserdefinedKeywords"] = allow_userdefined_keywords
        if limit_to_leaf_selection is not UNSET:
            field_dict["limitToLeafSelection"] = limit_to_leaf_selection
        if logical_name is not UNSET:
            field_dict["logicalName"] = logical_name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        d = src_dict.copy()
        pure_id = d.pop("pureId", UNSET)

        target_system_name = d.pop("targetSystemName", UNSET)

        keyword_group_type = d.pop("keywordGroupType", UNSET)

        def _parse_name(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        name = _parse_name(d.pop("name", UNSET))

        def _parse_description(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        description = _parse_description(d.pop("description", UNSET))

        def _parse_classification_scheme(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        classification_scheme = _parse_classification_scheme(d.pop("classificationScheme", UNSET))

        allow_userdefined_keywords = d.pop("allowUserdefinedKeywords", UNSET)

        limit_to_leaf_selection = d.pop("limitToLeafSelection", UNSET)

        logical_name = d.pop("logicalName", UNSET)

        keyword_group_configuration = cls(
            pure_id=pure_id,
            target_system_name=target_system_name,
            keyword_group_type=keyword_group_type,
            name=name,
            description=description,
            classification_scheme=classification_scheme,
            allow_userdefined_keywords=allow_userdefined_keywords,
            limit_to_leaf_selection=limit_to_leaf_selection,
            logical_name=logical_name,
        )

        keyword_group_configuration.additional_properties = d
        return keyword_group_configuration

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
