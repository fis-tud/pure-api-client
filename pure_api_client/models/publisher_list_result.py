from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.page_information import PageInformation
    from ..models.publisher import Publisher


T = TypeVar("T", bound="PublisherListResult")


@_attrs_define
class PublisherListResult:
    """List of publishers. Can contain a subset of all items along with the full count

    Attributes:
        count (Union[Unset, int]): The full count, ignoring paging
        page_information (Union[Unset, PageInformation]): Information about pages
        items (Union[Unset, List['Publisher']]): Publishers
    """

    count: Union[Unset, int] = UNSET
    page_information: Union[Unset, "PageInformation"] = UNSET
    items: Union[Unset, List["Publisher"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        count = self.count

        page_information: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.page_information, Unset):
            page_information = self.page_information.to_dict()

        items: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.items, Unset):
            items = []
            for items_item_data in self.items:
                items_item = items_item_data.to_dict()
                items.append(items_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if count is not UNSET:
            field_dict["count"] = count
        if page_information is not UNSET:
            field_dict["pageInformation"] = page_information
        if items is not UNSET:
            field_dict["items"] = items

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.page_information import PageInformation
        from ..models.publisher import Publisher

        d = src_dict.copy()
        count = d.pop("count", UNSET)

        _page_information = d.pop("pageInformation", UNSET)
        page_information: Union[Unset, PageInformation]
        if isinstance(_page_information, Unset):
            page_information = UNSET
        else:
            page_information = PageInformation.from_dict(_page_information)

        items = []
        _items = d.pop("items", UNSET)
        for items_item_data in _items or []:
            items_item = Publisher.from_dict(items_item_data)

            items.append(items_item)

        publisher_list_result = cls(
            count=count,
            page_information=page_information,
            items=items,
        )

        publisher_list_result.additional_properties = d
        return publisher_list_result

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
