from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.date_range import DateRange
    from ..models.name import Name


T = TypeVar("T", bound="InternalDataSetPersonAssociation")


@_attrs_define
class InternalDataSetPersonAssociation:
    """An internal association with the dataset.

    Attributes:
        role (Union['ClassificationRefType0', None]): A reference to a classification value
        type_discriminator (str):
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        external_organizations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of external
            organization affiliations.
        name (Union[Unset, Name]): A name describing a person, made up of given- and family name
        period (Union[Unset, DateRange]): A date range
        person (Union['ContentRefType0', None, Unset]):
        organizations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of organizational unit
            affiliations.
    """

    role: Union["ClassificationRefType0", None]
    type_discriminator: str
    pure_id: Union[Unset, int] = UNSET
    external_organizations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    name: Union[Unset, "Name"] = UNSET
    period: Union[Unset, "DateRange"] = UNSET
    person: Union["ContentRefType0", None, Unset] = UNSET
    organizations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0

        role: Union[Dict[str, Any], None]
        if isinstance(self.role, ClassificationRefType0):
            role = self.role.to_dict()
        else:
            role = self.role

        type_discriminator = self.type_discriminator

        pure_id = self.pure_id

        external_organizations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.external_organizations, Unset):
            external_organizations = UNSET
        elif isinstance(self.external_organizations, list):
            external_organizations = []
            for external_organizations_type_0_item_data in self.external_organizations:
                external_organizations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(external_organizations_type_0_item_data, ContentRefType0):
                    external_organizations_type_0_item = external_organizations_type_0_item_data.to_dict()
                else:
                    external_organizations_type_0_item = external_organizations_type_0_item_data
                external_organizations.append(external_organizations_type_0_item)

        else:
            external_organizations = self.external_organizations

        name: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.name, Unset):
            name = self.name.to_dict()

        period: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.period, Unset):
            period = self.period.to_dict()

        person: Union[Dict[str, Any], None, Unset]
        if isinstance(self.person, Unset):
            person = UNSET
        elif isinstance(self.person, ContentRefType0):
            person = self.person.to_dict()
        else:
            person = self.person

        organizations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.organizations, Unset):
            organizations = UNSET
        elif isinstance(self.organizations, list):
            organizations = []
            for organizations_type_0_item_data in self.organizations:
                organizations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(organizations_type_0_item_data, ContentRefType0):
                    organizations_type_0_item = organizations_type_0_item_data.to_dict()
                else:
                    organizations_type_0_item = organizations_type_0_item_data
                organizations.append(organizations_type_0_item)

        else:
            organizations = self.organizations

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "role": role,
                "typeDiscriminator": type_discriminator,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if external_organizations is not UNSET:
            field_dict["externalOrganizations"] = external_organizations
        if name is not UNSET:
            field_dict["name"] = name
        if period is not UNSET:
            field_dict["period"] = period
        if person is not UNSET:
            field_dict["person"] = person
        if organizations is not UNSET:
            field_dict["organizations"] = organizations

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.date_range import DateRange
        from ..models.name import Name

        d = src_dict.copy()

        def _parse_role(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        role = _parse_role(d.pop("role"))

        type_discriminator = d.pop("typeDiscriminator")

        pure_id = d.pop("pureId", UNSET)

        def _parse_external_organizations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                external_organizations_type_0 = []
                _external_organizations_type_0 = data
                for external_organizations_type_0_item_data in _external_organizations_type_0:

                    def _parse_external_organizations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    external_organizations_type_0_item = _parse_external_organizations_type_0_item(
                        external_organizations_type_0_item_data
                    )

                    external_organizations_type_0.append(external_organizations_type_0_item)

                return external_organizations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        external_organizations = _parse_external_organizations(d.pop("externalOrganizations", UNSET))

        _name = d.pop("name", UNSET)
        name: Union[Unset, Name]
        if isinstance(_name, Unset):
            name = UNSET
        else:
            name = Name.from_dict(_name)

        _period = d.pop("period", UNSET)
        period: Union[Unset, DateRange]
        if isinstance(_period, Unset):
            period = UNSET
        else:
            period = DateRange.from_dict(_period)

        def _parse_person(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        person = _parse_person(d.pop("person", UNSET))

        def _parse_organizations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                organizations_type_0 = []
                _organizations_type_0 = data
                for organizations_type_0_item_data in _organizations_type_0:

                    def _parse_organizations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    organizations_type_0_item = _parse_organizations_type_0_item(organizations_type_0_item_data)

                    organizations_type_0.append(organizations_type_0_item)

                return organizations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        organizations = _parse_organizations(d.pop("organizations", UNSET))

        internal_data_set_person_association = cls(
            role=role,
            type_discriminator=type_discriminator,
            pure_id=pure_id,
            external_organizations=external_organizations,
            name=name,
            period=period,
            person=person,
            organizations=organizations,
        )

        internal_data_set_person_association.additional_properties = d
        return internal_data_set_person_association

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
