"""Contains all the data models used in inputs/outputs"""

from .abstract_activity_person_association import AbstractActivityPersonAssociation
from .abstract_applicant_association import AbstractApplicantAssociation
from .abstract_award_holder_association import AbstractAwardHolderAssociation
from .abstract_contributor_association import AbstractContributorAssociation
from .abstract_data_set_person_association import AbstractDataSetPersonAssociation
from .abstract_editorial_work_association import AbstractEditorialWorkAssociation
from .abstract_equipment_person_association import AbstractEquipmentPersonAssociation
from .abstract_examinee_association_type_0 import AbstractExamineeAssociationType0
from .abstract_participant_association import AbstractParticipantAssociation
from .academic_qualification import AcademicQualification
from .activities_query import ActivitiesQuery
from .activity import Activity
from .activity_abstract_association import ActivityAbstractAssociation
from .activity_awardable_association import ActivityAwardableAssociation
from .activity_event_association import ActivityEventAssociation
from .activity_external_organization_association import ActivityExternalOrganizationAssociation
from .activity_list_result import ActivityListResult
from .activity_organization_association import ActivityOrganizationAssociation
from .activity_visitor_association import ActivityVisitorAssociation
from .additional_file_electronic_version import AdditionalFileElectronicVersion
from .additional_issn_type_0 import AdditionalISSNType0
from .address_type_0 import AddressType0
from .allowed_template import AllowedTemplate
from .allowed_template_list_result import AllowedTemplateListResult
from .application import Application
from .application_cluster import ApplicationCluster
from .application_cluster_list_result import ApplicationClusterListResult
from .application_cluster_status import ApplicationClusterStatus
from .application_document import ApplicationDocument
from .application_document_file_store_locations import ApplicationDocumentFileStoreLocations
from .application_financial_funding_association import ApplicationFinancialFundingAssociation
from .application_financial_funding_collaborator_association_type_0 import (
    ApplicationFinancialFundingCollaboratorAssociationType0,
)
from .application_funding_association import ApplicationFundingAssociation
from .application_list_result import ApplicationListResult
from .application_non_financial_funding_association import ApplicationNonFinancialFundingAssociation
from .application_non_financial_funding_collaborator_association_type_0 import (
    ApplicationNonFinancialFundingCollaboratorAssociationType0,
)
from .application_status import ApplicationStatus
from .applications_query import ApplicationsQuery
from .article_processing_charge import ArticleProcessingCharge
from .article_version import ArticleVersion
from .article_version_key import ArticleVersionKey
from .assignable_role import AssignableRole
from .assignable_role_ref import AssignableRoleRef
from .attendance import Attendance
from .author_collaboration_contributor_association import AuthorCollaborationContributorAssociation
from .award import Award
from .award_cluster import AwardCluster
from .award_cluster_list_result import AwardClusterListResult
from .award_document import AwardDocument
from .award_document_file_store_locations import AwardDocumentFileStoreLocations
from .award_financial_funding_association import AwardFinancialFundingAssociation
from .award_financial_funding_collaborator_association_type_0 import AwardFinancialFundingCollaboratorAssociationType0
from .award_funding_association import AwardFundingAssociation
from .award_list_result import AwardListResult
from .award_management_application import AwardManagementApplication
from .award_management_application_funder_reply import AwardManagementApplicationFunderReply
from .award_management_award import AwardManagementAward
from .award_management_project import AwardManagementProject
from .award_non_financial_funding_association import AwardNonFinancialFundingAssociation
from .award_non_financial_funding_collaborator_association_type_0 import (
    AwardNonFinancialFundingCollaboratorAssociationType0,
)
from .award_status import AwardStatus
from .awards_query import AwardsQuery
from .basic_application import BasicApplication
from .basic_award import BasicAward
from .book_anthology import BookAnthology
from .book_series_journal_association import BookSeriesJournalAssociation
from .case_note import CaseNote
from .cerif_address_type_0 import CERIFAddressType0
from .classification_ref_list import ClassificationRefList
from .classification_ref_type_0 import ClassificationRefType0
from .classifications_keyword_group import ClassificationsKeywordGroup
from .classified_address import ClassifiedAddress
from .classified_case_note_source import ClassifiedCaseNoteSource
from .classified_id import ClassifiedId
from .classified_localized_value import ClassifiedLocalizedValue
from .classified_name import ClassifiedName
from .classified_value import ClassifiedValue
from .collaborator_association import CollaboratorAssociation
from .compound_date_range import CompoundDateRange
from .compound_date_type_0 import CompoundDateType0
from .conflict_problem_details import ConflictProblemDetails
from .consultancy import Consultancy
from .content_ref_list_result import ContentRefListResult
from .content_ref_type_0 import ContentRefType0
from .contribution_to_book_anthology import ContributionToBookAnthology
from .contribution_to_conference import ContributionToConference
from .contribution_to_journal import ContributionToJournal
from .contribution_to_memorandum import ContributionToMemorandum
from .contribution_to_periodical import ContributionToPeriodical
from .currency_amount import CurrencyAmount
from .curtailed_award_status import CurtailedAwardStatus
from .custom_defined_field_type_0 import CustomDefinedFieldType0
from .custom_defined_fields_type_0 import CustomDefinedFieldsType0
from .data_set import DataSet
from .data_set_activity_association import DataSetActivityAssociation
from .data_set_association import DataSetAssociation
from .data_set_awardable_association import DataSetAwardableAssociation
from .data_set_document import DataSetDocument
from .data_set_document_file_store_locations import DataSetDocumentFileStoreLocations
from .data_set_doi import DataSetDoi
from .data_set_equipment_association import DataSetEquipmentAssociation
from .data_set_geo_location import DataSetGeoLocation
from .data_set_impact_association import DataSetImpactAssociation
from .data_set_legal_condition import DataSetLegalCondition
from .data_set_list_result import DataSetListResult
from .data_set_physical_data import DataSetPhysicalData
from .data_set_press_media_association import DataSetPressMediaAssociation
from .data_set_prize_association import DataSetPrizeAssociation
from .data_set_project_association import DataSetProjectAssociation
from .data_set_research_output_association import DataSetResearchOutputAssociation
from .data_set_student_thesis_association import DataSetStudentThesisAssociation
from .data_sets_query import DataSetsQuery
from .date_range import DateRange
from .dependency_violation_problem_details import DependencyViolationProblemDetails
from .disciplines_association import DisciplinesAssociation
from .disciplines_association_list_result import DisciplinesAssociationListResult
from .disciplines_associations_query import DisciplinesAssociationsQuery
from .disciplines_discipline import DisciplinesDiscipline
from .disciplines_discipline_assignment import DisciplinesDisciplineAssignment
from .disciplines_discipline_list_result import DisciplinesDisciplineListResult
from .disciplines_discipline_scheme import DisciplinesDisciplineScheme
from .disciplines_discipline_scheme_list_result import DisciplinesDisciplineSchemeListResult
from .document import Document
from .document_file_store_locations import DocumentFileStoreLocations
from .editorial_work import EditorialWork
from .electronic_version_file import ElectronicVersionFile
from .electronic_version_file_file_store_locations import ElectronicVersionFileFileStoreLocations
from .electronic_version_type_0 import ElectronicVersionType0
from .equipment import Equipment
from .equipment_awardable_association import EquipmentAwardableAssociation
from .equipment_details import EquipmentDetails
from .equipment_list_result import EquipmentListResult
from .equipment_press_media_association import EquipmentPressMediaAssociation
from .equipment_query import EquipmentQuery
from .event import Event
from .event_list_result import EventListResult
from .event_membership_association import EventMembershipAssociation
from .events_query import EventsQuery
from .examination import Examination
from .existing_file_store_definition import ExistingFileStoreDefinition
from .existing_file_store_definition_store_content_state import ExistingFileStoreDefinitionStoreContentState
from .existing_file_store_definition_store_file_state import ExistingFileStoreDefinitionStoreFileState
from .external_activity_person_association import ExternalActivityPersonAssociation
from .external_applicant_association import ExternalApplicantAssociation
from .external_appointment import ExternalAppointment
from .external_award_holder_association import ExternalAwardHolderAssociation
from .external_collaborator_association import ExternalCollaboratorAssociation
from .external_contributor_association import ExternalContributorAssociation
from .external_data_set_person_association import ExternalDataSetPersonAssociation
from .external_equipment_person_association import ExternalEquipmentPersonAssociation
from .external_organization import ExternalOrganization
from .external_organization_list import ExternalOrganizationList
from .external_organization_list_result import ExternalOrganizationListResult
from .external_organization_ref_list import ExternalOrganizationRefList
from .external_organizations_query import ExternalOrganizationsQuery
from .external_participant_association import ExternalParticipantAssociation
from .external_person import ExternalPerson
from .external_person_list_result import ExternalPersonListResult
from .external_persons_query import ExternalPersonsQuery
from .file import File
from .formatted_localized_string_type_0 import FormattedLocalizedStringType0
from .formatted_string import FormattedString
from .free_keywords_keyword_group import FreeKeywordsKeywordGroup
from .full_keyword_group import FullKeywordGroup
from .funding_details import FundingDetails
from .gender_type_0 import GenderType0
from .gender_type_0_key import GenderType0Key
from .geo_location_type_0 import GeoLocationType0
from .global_role_assignment import GlobalRoleAssignment
from .highlighted_content import HighlightedContent
from .id import Id
from .identifier import Identifier
from .image_file import ImageFile
from .image_file_file_store_locations import ImageFileFileStoreLocations
from .impact_awardable_association import ImpactAwardableAssociation
from .internal_activity_person_association import InternalActivityPersonAssociation
from .internal_applicant_association import InternalApplicantAssociation
from .internal_award_holder_association import InternalAwardHolderAssociation
from .internal_collaborator_association import InternalCollaboratorAssociation
from .internal_contributor_association import InternalContributorAssociation
from .internal_data_set_person_association import InternalDataSetPersonAssociation
from .internal_equipment_person_association import InternalEquipmentPersonAssociation
from .internal_or_external_supervisor import InternalOrExternalSupervisor
from .internal_participant_association import InternalParticipantAssociation
from .internally_approved_award_status import InternallyApprovedAwardStatus
from .issn import ISSN
from .issn_ref import ISSNRef
from .journal import Journal
from .journal_association_type_0 import JournalAssociationType0
from .journal_editorial_work_association import JournalEditorialWorkAssociation
from .journal_list_result import JournalListResult
from .journal_title import JournalTitle
from .journal_title_ref import JournalTitleRef
from .journals_query import JournalsQuery
from .keyword import Keyword
from .keyword_container import KeywordContainer
from .keyword_group import KeywordGroup
from .keyword_group_configuration import KeywordGroupConfiguration
from .keyword_group_configuration_list import KeywordGroupConfigurationList
from .link import Link
from .local_role_assignment import LocalRoleAssignment
from .locales_list import LocalesList
from .localized_string_type_0 import LocalizedStringType0
from .membership import Membership
from .memorandum import Memorandum
from .metric_collection import MetricCollection
from .metric_collection_definition import MetricCollectionDefinition
from .metric_collection_definition_list import MetricCollectionDefinitionList
from .metric_collection_item import MetricCollectionItem
from .metric_definition import MetricDefinition
from .metric_value import MetricValue
from .name import Name
from .navigation_link import NavigationLink
from .non_textual import NonTextual
from .note import Note
from .note_list_result import NoteListResult
from .open_access import OpenAccess
from .orderings_list import OrderingsList
from .organization import Organization
from .organization_list_result import OrganizationListResult
from .organization_or_external_organization_ref_type_0 import OrganizationOrExternalOrganizationRefType0
from .organizations_query import OrganizationsQuery
from .other_activity import OtherActivity
from .other_contribution import OtherContribution
from .page_information import PageInformation
from .participant_time_spent import ParticipantTimeSpent
from .participant_time_tracking_association_type_0 import ParticipantTimeTrackingAssociationType0
from .patent import Patent
from .person import Person
from .person_classified_leave_of_absence import PersonClassifiedLeaveOfAbsence
from .person_list_result import PersonListResult
from .person_organization_association_type_0 import PersonOrganizationAssociationType0
from .person_supervisee_association import PersonSuperviseeAssociation
from .person_supervisee_association_list_result import PersonSuperviseeAssociationListResult
from .person_supervisor_association import PersonSupervisorAssociation
from .persons_query import PersonsQuery
from .preserved_content_move_information import PreservedContentMoveInformation
from .preserved_file_move_information import PreservedFileMoveInformation
from .press_media_awardable_association import PressMediaAwardableAssociation
from .primary_id import PrimaryId
from .prize_awardable_association import PrizeAwardableAssociation
from .problem_details import ProblemDetails
from .professional_qualification import ProfessionalQualification
from .project import Project
from .project_association import ProjectAssociation
from .project_awardable_association_type_0 import ProjectAwardableAssociationType0
from .project_curtailed import ProjectCurtailed
from .project_list_result import ProjectListResult
from .projects_query import ProjectsQuery
from .publication_series import PublicationSeries
from .publication_status import PublicationStatus
from .publisher import Publisher
from .publisher_editorial_work_association import PublisherEditorialWorkAssociation
from .publisher_list_result import PublisherListResult
from .publishers_query import PublishersQuery
from .research_output import ResearchOutput
from .research_output_association_type_0 import ResearchOutputAssociationType0
from .research_output_awardable_association import ResearchOutputAwardableAssociation
from .research_output_list_result import ResearchOutputListResult
from .research_output_peer_review_configuration import ResearchOutputPeerReviewConfiguration
from .research_output_peer_review_configuration_combination import ResearchOutputPeerReviewConfigurationCombination
from .research_output_peer_review_configuration_combination_peer_reviewable import (
    ResearchOutputPeerReviewConfigurationCombinationPeerReviewable,
)
from .research_output_peer_review_configuration_list_result import ResearchOutputPeerReviewConfigurationListResult
from .research_outputs_query import ResearchOutputsQuery
from .researcher_commitment_type_0 import ResearcherCommitmentType0
from .sherpa_romeo_open_access_permission_type_0 import SherpaRomeoOpenAccessPermissionType0
from .sherpa_romeo_policy import SherpaRomeoPolicy
from .student_thesis_awardable_association import StudentThesisAwardableAssociation
from .supervisor_association import SupervisorAssociation
from .system_currency_amount import SystemCurrencyAmount
from .system_localized_string_type_0 import SystemLocalizedStringType0
from .talk import Talk
from .temp_file_info import TempFileInfo
from .thesis import Thesis
from .uploaded_file import UploadedFile
from .user import User
from .user_list_result import UserListResult
from .user_roles import UserRoles
from .validation_error import ValidationError
from .validation_problem_details import ValidationProblemDetails
from .visibility import Visibility
from .visibility_key import VisibilityKey
from .workflow import Workflow
from .workflow_list_result import WorkflowListResult
from .working_paper import WorkingPaper
from .year_month_type_0 import YearMonthType0

__all__ = (
    "AbstractActivityPersonAssociation",
    "AbstractApplicantAssociation",
    "AbstractAwardHolderAssociation",
    "AbstractContributorAssociation",
    "AbstractDataSetPersonAssociation",
    "AbstractEditorialWorkAssociation",
    "AbstractEquipmentPersonAssociation",
    "AbstractExamineeAssociationType0",
    "AbstractParticipantAssociation",
    "AcademicQualification",
    "ActivitiesQuery",
    "Activity",
    "ActivityAbstractAssociation",
    "ActivityAwardableAssociation",
    "ActivityEventAssociation",
    "ActivityExternalOrganizationAssociation",
    "ActivityListResult",
    "ActivityOrganizationAssociation",
    "ActivityVisitorAssociation",
    "AdditionalFileElectronicVersion",
    "AdditionalISSNType0",
    "AddressType0",
    "AllowedTemplate",
    "AllowedTemplateListResult",
    "Application",
    "ApplicationCluster",
    "ApplicationClusterListResult",
    "ApplicationClusterStatus",
    "ApplicationDocument",
    "ApplicationDocumentFileStoreLocations",
    "ApplicationFinancialFundingAssociation",
    "ApplicationFinancialFundingCollaboratorAssociationType0",
    "ApplicationFundingAssociation",
    "ApplicationListResult",
    "ApplicationNonFinancialFundingAssociation",
    "ApplicationNonFinancialFundingCollaboratorAssociationType0",
    "ApplicationsQuery",
    "ApplicationStatus",
    "ArticleProcessingCharge",
    "ArticleVersion",
    "ArticleVersionKey",
    "AssignableRole",
    "AssignableRoleRef",
    "Attendance",
    "AuthorCollaborationContributorAssociation",
    "Award",
    "AwardCluster",
    "AwardClusterListResult",
    "AwardDocument",
    "AwardDocumentFileStoreLocations",
    "AwardFinancialFundingAssociation",
    "AwardFinancialFundingCollaboratorAssociationType0",
    "AwardFundingAssociation",
    "AwardListResult",
    "AwardManagementApplication",
    "AwardManagementApplicationFunderReply",
    "AwardManagementAward",
    "AwardManagementProject",
    "AwardNonFinancialFundingAssociation",
    "AwardNonFinancialFundingCollaboratorAssociationType0",
    "AwardsQuery",
    "AwardStatus",
    "BasicApplication",
    "BasicAward",
    "BookAnthology",
    "BookSeriesJournalAssociation",
    "CaseNote",
    "CERIFAddressType0",
    "ClassificationRefList",
    "ClassificationRefType0",
    "ClassificationsKeywordGroup",
    "ClassifiedAddress",
    "ClassifiedCaseNoteSource",
    "ClassifiedId",
    "ClassifiedLocalizedValue",
    "ClassifiedName",
    "ClassifiedValue",
    "CollaboratorAssociation",
    "CompoundDateRange",
    "CompoundDateType0",
    "ConflictProblemDetails",
    "Consultancy",
    "ContentRefListResult",
    "ContentRefType0",
    "ContributionToBookAnthology",
    "ContributionToConference",
    "ContributionToJournal",
    "ContributionToMemorandum",
    "ContributionToPeriodical",
    "CurrencyAmount",
    "CurtailedAwardStatus",
    "CustomDefinedFieldsType0",
    "CustomDefinedFieldType0",
    "DataSet",
    "DataSetActivityAssociation",
    "DataSetAssociation",
    "DataSetAwardableAssociation",
    "DataSetDocument",
    "DataSetDocumentFileStoreLocations",
    "DataSetDoi",
    "DataSetEquipmentAssociation",
    "DataSetGeoLocation",
    "DataSetImpactAssociation",
    "DataSetLegalCondition",
    "DataSetListResult",
    "DataSetPhysicalData",
    "DataSetPressMediaAssociation",
    "DataSetPrizeAssociation",
    "DataSetProjectAssociation",
    "DataSetResearchOutputAssociation",
    "DataSetsQuery",
    "DataSetStudentThesisAssociation",
    "DateRange",
    "DependencyViolationProblemDetails",
    "DisciplinesAssociation",
    "DisciplinesAssociationListResult",
    "DisciplinesAssociationsQuery",
    "DisciplinesDiscipline",
    "DisciplinesDisciplineAssignment",
    "DisciplinesDisciplineListResult",
    "DisciplinesDisciplineScheme",
    "DisciplinesDisciplineSchemeListResult",
    "Document",
    "DocumentFileStoreLocations",
    "EditorialWork",
    "ElectronicVersionFile",
    "ElectronicVersionFileFileStoreLocations",
    "ElectronicVersionType0",
    "Equipment",
    "EquipmentAwardableAssociation",
    "EquipmentDetails",
    "EquipmentListResult",
    "EquipmentPressMediaAssociation",
    "EquipmentQuery",
    "Event",
    "EventListResult",
    "EventMembershipAssociation",
    "EventsQuery",
    "Examination",
    "ExistingFileStoreDefinition",
    "ExistingFileStoreDefinitionStoreContentState",
    "ExistingFileStoreDefinitionStoreFileState",
    "ExternalActivityPersonAssociation",
    "ExternalApplicantAssociation",
    "ExternalAppointment",
    "ExternalAwardHolderAssociation",
    "ExternalCollaboratorAssociation",
    "ExternalContributorAssociation",
    "ExternalDataSetPersonAssociation",
    "ExternalEquipmentPersonAssociation",
    "ExternalOrganization",
    "ExternalOrganizationList",
    "ExternalOrganizationListResult",
    "ExternalOrganizationRefList",
    "ExternalOrganizationsQuery",
    "ExternalParticipantAssociation",
    "ExternalPerson",
    "ExternalPersonListResult",
    "ExternalPersonsQuery",
    "File",
    "FormattedLocalizedStringType0",
    "FormattedString",
    "FreeKeywordsKeywordGroup",
    "FullKeywordGroup",
    "FundingDetails",
    "GenderType0",
    "GenderType0Key",
    "GeoLocationType0",
    "GlobalRoleAssignment",
    "HighlightedContent",
    "Id",
    "Identifier",
    "ImageFile",
    "ImageFileFileStoreLocations",
    "ImpactAwardableAssociation",
    "InternalActivityPersonAssociation",
    "InternalApplicantAssociation",
    "InternalAwardHolderAssociation",
    "InternalCollaboratorAssociation",
    "InternalContributorAssociation",
    "InternalDataSetPersonAssociation",
    "InternalEquipmentPersonAssociation",
    "InternallyApprovedAwardStatus",
    "InternalOrExternalSupervisor",
    "InternalParticipantAssociation",
    "ISSN",
    "ISSNRef",
    "Journal",
    "JournalAssociationType0",
    "JournalEditorialWorkAssociation",
    "JournalListResult",
    "JournalsQuery",
    "JournalTitle",
    "JournalTitleRef",
    "Keyword",
    "KeywordContainer",
    "KeywordGroup",
    "KeywordGroupConfiguration",
    "KeywordGroupConfigurationList",
    "Link",
    "LocalesList",
    "LocalizedStringType0",
    "LocalRoleAssignment",
    "Membership",
    "Memorandum",
    "MetricCollection",
    "MetricCollectionDefinition",
    "MetricCollectionDefinitionList",
    "MetricCollectionItem",
    "MetricDefinition",
    "MetricValue",
    "Name",
    "NavigationLink",
    "NonTextual",
    "Note",
    "NoteListResult",
    "OpenAccess",
    "OrderingsList",
    "Organization",
    "OrganizationListResult",
    "OrganizationOrExternalOrganizationRefType0",
    "OrganizationsQuery",
    "OtherActivity",
    "OtherContribution",
    "PageInformation",
    "ParticipantTimeSpent",
    "ParticipantTimeTrackingAssociationType0",
    "Patent",
    "Person",
    "PersonClassifiedLeaveOfAbsence",
    "PersonListResult",
    "PersonOrganizationAssociationType0",
    "PersonsQuery",
    "PersonSuperviseeAssociation",
    "PersonSuperviseeAssociationListResult",
    "PersonSupervisorAssociation",
    "PreservedContentMoveInformation",
    "PreservedFileMoveInformation",
    "PressMediaAwardableAssociation",
    "PrimaryId",
    "PrizeAwardableAssociation",
    "ProblemDetails",
    "ProfessionalQualification",
    "Project",
    "ProjectAssociation",
    "ProjectAwardableAssociationType0",
    "ProjectCurtailed",
    "ProjectListResult",
    "ProjectsQuery",
    "PublicationSeries",
    "PublicationStatus",
    "Publisher",
    "PublisherEditorialWorkAssociation",
    "PublisherListResult",
    "PublishersQuery",
    "ResearcherCommitmentType0",
    "ResearchOutput",
    "ResearchOutputAssociationType0",
    "ResearchOutputAwardableAssociation",
    "ResearchOutputListResult",
    "ResearchOutputPeerReviewConfiguration",
    "ResearchOutputPeerReviewConfigurationCombination",
    "ResearchOutputPeerReviewConfigurationCombinationPeerReviewable",
    "ResearchOutputPeerReviewConfigurationListResult",
    "ResearchOutputsQuery",
    "SherpaRomeoOpenAccessPermissionType0",
    "SherpaRomeoPolicy",
    "StudentThesisAwardableAssociation",
    "SupervisorAssociation",
    "SystemCurrencyAmount",
    "SystemLocalizedStringType0",
    "Talk",
    "TempFileInfo",
    "Thesis",
    "UploadedFile",
    "User",
    "UserListResult",
    "UserRoles",
    "ValidationError",
    "ValidationProblemDetails",
    "Visibility",
    "VisibilityKey",
    "Workflow",
    "WorkflowListResult",
    "WorkingPaper",
    "YearMonthType0",
)
