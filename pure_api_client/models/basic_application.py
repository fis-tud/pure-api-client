import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.abstract_applicant_association import AbstractApplicantAssociation
    from ..models.application_funding_association import ApplicationFundingAssociation
    from ..models.application_status import ApplicationStatus
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.classified_localized_value import ClassifiedLocalizedValue
    from ..models.collaborator_association import CollaboratorAssociation
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
    from ..models.date_range import DateRange
    from ..models.identifier import Identifier
    from ..models.keyword_group import KeywordGroup
    from ..models.link import Link
    from ..models.localized_string_type_0 import LocalizedStringType0
    from ..models.system_currency_amount import SystemCurrencyAmount
    from ..models.visibility import Visibility
    from ..models.workflow import Workflow


T = TypeVar("T", bound="BasicApplication")


@_attrs_define
class BasicApplication:
    """The Basic application template is used when the Award Management module is not enabled for the Pure installation.

    Attributes:
        fundings (List['ApplicationFundingAssociation']): A collection of funding information for the application.
        organizations (List[Union['ContentRefType0', None]]): A collection of organizational unit affiliations.
        managing_organization (Union['ContentRefType0', None]):
        title (Union['LocalizedStringType0', None]): A set of string values, one for each submission locale. Note:
            invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        type (Union['ClassificationRefType0', None]): A reference to a classification value
        type_discriminator (str):
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        uuid (Union[Unset, str]): UUID, this is the primary identity of the entity
        created_by (Union[Unset, str]): Username of creator
        created_date (Union[Unset, datetime.datetime]): Date and time of creation
        modified_by (Union[Unset, str]): Username of the user that performed a modification
        modified_date (Union[Unset, datetime.datetime]): Date and time of last modification
        portal_url (Union[Unset, str]): URL of the content on the Pure Portal
        pretty_url_identifiers (Union[Unset, List[str]]): All pretty URLs
        previous_uuids (Union[Unset, List[str]]): UUIDs of other content items which have been merged into this content
            item (or similar)
        version (Union[None, Unset, str]): Used to guard against conflicting updates. For new content this is null, and
            for existing content the current value. The property should never be modified by a client, except in the rare
            case where the client wants to perform an update irrespective of if other clients have made updates in the
            meantime, also known as a "dirty write". A dirty write is performed by not including the property value or
            setting the property to null
        acronym (Union[None, Unset, str]): The acronym of the application
        applicants (Union[Unset, List['AbstractApplicantAssociation']]):
        external_organizations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of external
            organization affiliations.
        application_date (Union[Unset, datetime.date]): The date of application, ie. the date it was submitted to the
            funder.
        awards (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of related grants.
        budget_applied_amount_difference (Union[Unset, SystemCurrencyAmount]): A monetary value in the Pure
            installation's system currency as defined by the W3C's Payment Request standard: https://www.w3.org/TR/payment-
            request/#paymentcurrencyamount-dictionary
        co_managing_organizations (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of co-
            managing organizational unit affiliations.
        collaborators (Union[List['CollaboratorAssociation'], None, Unset]): A collection of collaborators.
        descriptions (Union[List['ClassifiedLocalizedValue'], None, Unset]): A collection of descriptions for the
            application. Query the /applications/allowed-description-types endpoint for allowed types.
        expected_period (Union[Unset, DateRange]): A date range
        identifiers (Union[List['Identifier'], None, Unset]): Identifiers related to the application.
        funding_opportunity (Union['ContentRefType0', None, Unset]):
        nature_types (Union[List[Union['ClassificationRefType0', None]], None, Unset]): Nature of activity types for the
            application.
        applications (Union[List[Union['ContentRefType0', None]], None, Unset]): A collection of related applications.
        short_title (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission
            locale. Note: invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        total_academic_ownership (Union[Unset, float]): Total academic ownership of the application.
        total_applied_amount (Union[Unset, SystemCurrencyAmount]): A monetary value in the Pure installation's system
            currency as defined by the W3C's Payment Request standard: https://www.w3.org/TR/payment-
            request/#paymentcurrencyamount-dictionary
        workflow (Union[Unset, Workflow]): Information about workflow
        visibility (Union[Unset, Visibility]): Visibility of an object
        links (Union[List['Link'], None, Unset]): Links associated with the application.
        keyword_groups (Union[List['KeywordGroup'], None, Unset]): Groups of keywords associated with the application.
        custom_defined_fields (Union['CustomDefinedFieldsType0', None, Unset]): Map of CustomDefinedField values, where
            the key is the field identifier Example: { "fieldName1": "typeDiscriminator": "Integer", "value" : 1}.
        cluster (Union['ContentRefType0', None, Unset]):
        system_name (Union[Unset, str]): The content system name
        application_statuses (Union[Unset, List['ApplicationStatus']]): Manage the applications different statuses and
            dates these were reached.
    """

    fundings: List["ApplicationFundingAssociation"]
    organizations: List[Union["ContentRefType0", None]]
    managing_organization: Union["ContentRefType0", None]
    title: Union["LocalizedStringType0", None]
    type: Union["ClassificationRefType0", None]
    type_discriminator: str
    pure_id: Union[Unset, int] = UNSET
    uuid: Union[Unset, str] = UNSET
    created_by: Union[Unset, str] = UNSET
    created_date: Union[Unset, datetime.datetime] = UNSET
    modified_by: Union[Unset, str] = UNSET
    modified_date: Union[Unset, datetime.datetime] = UNSET
    portal_url: Union[Unset, str] = UNSET
    pretty_url_identifiers: Union[Unset, List[str]] = UNSET
    previous_uuids: Union[Unset, List[str]] = UNSET
    version: Union[None, Unset, str] = UNSET
    acronym: Union[None, Unset, str] = UNSET
    applicants: Union[Unset, List["AbstractApplicantAssociation"]] = UNSET
    external_organizations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    application_date: Union[Unset, datetime.date] = UNSET
    awards: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    budget_applied_amount_difference: Union[Unset, "SystemCurrencyAmount"] = UNSET
    co_managing_organizations: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    collaborators: Union[List["CollaboratorAssociation"], None, Unset] = UNSET
    descriptions: Union[List["ClassifiedLocalizedValue"], None, Unset] = UNSET
    expected_period: Union[Unset, "DateRange"] = UNSET
    identifiers: Union[List["Identifier"], None, Unset] = UNSET
    funding_opportunity: Union["ContentRefType0", None, Unset] = UNSET
    nature_types: Union[List[Union["ClassificationRefType0", None]], None, Unset] = UNSET
    applications: Union[List[Union["ContentRefType0", None]], None, Unset] = UNSET
    short_title: Union["LocalizedStringType0", None, Unset] = UNSET
    total_academic_ownership: Union[Unset, float] = UNSET
    total_applied_amount: Union[Unset, "SystemCurrencyAmount"] = UNSET
    workflow: Union[Unset, "Workflow"] = UNSET
    visibility: Union[Unset, "Visibility"] = UNSET
    links: Union[List["Link"], None, Unset] = UNSET
    keyword_groups: Union[List["KeywordGroup"], None, Unset] = UNSET
    custom_defined_fields: Union["CustomDefinedFieldsType0", None, Unset] = UNSET
    cluster: Union["ContentRefType0", None, Unset] = UNSET
    system_name: Union[Unset, str] = UNSET
    application_statuses: Union[Unset, List["ApplicationStatus"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        fundings = []
        for fundings_item_data in self.fundings:
            fundings_item = fundings_item_data.to_dict()
            fundings.append(fundings_item)

        organizations = []
        for organizations_item_data in self.organizations:
            organizations_item: Union[Dict[str, Any], None]
            if isinstance(organizations_item_data, ContentRefType0):
                organizations_item = organizations_item_data.to_dict()
            else:
                organizations_item = organizations_item_data
            organizations.append(organizations_item)

        managing_organization: Union[Dict[str, Any], None]
        if isinstance(self.managing_organization, ContentRefType0):
            managing_organization = self.managing_organization.to_dict()
        else:
            managing_organization = self.managing_organization

        title: Union[Dict[str, Any], None]
        if isinstance(self.title, LocalizedStringType0):
            title = self.title.to_dict()
        else:
            title = self.title

        type: Union[Dict[str, Any], None]
        if isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        type_discriminator = self.type_discriminator

        pure_id = self.pure_id

        uuid = self.uuid

        created_by = self.created_by

        created_date: Union[Unset, str] = UNSET
        if not isinstance(self.created_date, Unset):
            created_date = self.created_date.isoformat()

        modified_by = self.modified_by

        modified_date: Union[Unset, str] = UNSET
        if not isinstance(self.modified_date, Unset):
            modified_date = self.modified_date.isoformat()

        portal_url = self.portal_url

        pretty_url_identifiers: Union[Unset, List[str]] = UNSET
        if not isinstance(self.pretty_url_identifiers, Unset):
            pretty_url_identifiers = self.pretty_url_identifiers

        previous_uuids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.previous_uuids, Unset):
            previous_uuids = self.previous_uuids

        version: Union[None, Unset, str]
        if isinstance(self.version, Unset):
            version = UNSET
        else:
            version = self.version

        acronym: Union[None, Unset, str]
        if isinstance(self.acronym, Unset):
            acronym = UNSET
        else:
            acronym = self.acronym

        applicants: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.applicants, Unset):
            applicants = []
            for applicants_item_data in self.applicants:
                applicants_item = applicants_item_data.to_dict()
                applicants.append(applicants_item)

        external_organizations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.external_organizations, Unset):
            external_organizations = UNSET
        elif isinstance(self.external_organizations, list):
            external_organizations = []
            for external_organizations_type_0_item_data in self.external_organizations:
                external_organizations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(external_organizations_type_0_item_data, ContentRefType0):
                    external_organizations_type_0_item = external_organizations_type_0_item_data.to_dict()
                else:
                    external_organizations_type_0_item = external_organizations_type_0_item_data
                external_organizations.append(external_organizations_type_0_item)

        else:
            external_organizations = self.external_organizations

        application_date: Union[Unset, str] = UNSET
        if not isinstance(self.application_date, Unset):
            application_date = self.application_date.isoformat()

        awards: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.awards, Unset):
            awards = UNSET
        elif isinstance(self.awards, list):
            awards = []
            for awards_type_0_item_data in self.awards:
                awards_type_0_item: Union[Dict[str, Any], None]
                if isinstance(awards_type_0_item_data, ContentRefType0):
                    awards_type_0_item = awards_type_0_item_data.to_dict()
                else:
                    awards_type_0_item = awards_type_0_item_data
                awards.append(awards_type_0_item)

        else:
            awards = self.awards

        budget_applied_amount_difference: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.budget_applied_amount_difference, Unset):
            budget_applied_amount_difference = self.budget_applied_amount_difference.to_dict()

        co_managing_organizations: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.co_managing_organizations, Unset):
            co_managing_organizations = UNSET
        elif isinstance(self.co_managing_organizations, list):
            co_managing_organizations = []
            for co_managing_organizations_type_0_item_data in self.co_managing_organizations:
                co_managing_organizations_type_0_item: Union[Dict[str, Any], None]
                if isinstance(co_managing_organizations_type_0_item_data, ContentRefType0):
                    co_managing_organizations_type_0_item = co_managing_organizations_type_0_item_data.to_dict()
                else:
                    co_managing_organizations_type_0_item = co_managing_organizations_type_0_item_data
                co_managing_organizations.append(co_managing_organizations_type_0_item)

        else:
            co_managing_organizations = self.co_managing_organizations

        collaborators: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.collaborators, Unset):
            collaborators = UNSET
        elif isinstance(self.collaborators, list):
            collaborators = []
            for collaborators_type_0_item_data in self.collaborators:
                collaborators_type_0_item = collaborators_type_0_item_data.to_dict()
                collaborators.append(collaborators_type_0_item)

        else:
            collaborators = self.collaborators

        descriptions: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.descriptions, Unset):
            descriptions = UNSET
        elif isinstance(self.descriptions, list):
            descriptions = []
            for descriptions_type_0_item_data in self.descriptions:
                descriptions_type_0_item = descriptions_type_0_item_data.to_dict()
                descriptions.append(descriptions_type_0_item)

        else:
            descriptions = self.descriptions

        expected_period: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.expected_period, Unset):
            expected_period = self.expected_period.to_dict()

        identifiers: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.identifiers, Unset):
            identifiers = UNSET
        elif isinstance(self.identifiers, list):
            identifiers = []
            for identifiers_type_0_item_data in self.identifiers:
                identifiers_type_0_item = identifiers_type_0_item_data.to_dict()
                identifiers.append(identifiers_type_0_item)

        else:
            identifiers = self.identifiers

        funding_opportunity: Union[Dict[str, Any], None, Unset]
        if isinstance(self.funding_opportunity, Unset):
            funding_opportunity = UNSET
        elif isinstance(self.funding_opportunity, ContentRefType0):
            funding_opportunity = self.funding_opportunity.to_dict()
        else:
            funding_opportunity = self.funding_opportunity

        nature_types: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.nature_types, Unset):
            nature_types = UNSET
        elif isinstance(self.nature_types, list):
            nature_types = []
            for nature_types_type_0_item_data in self.nature_types:
                nature_types_type_0_item: Union[Dict[str, Any], None]
                if isinstance(nature_types_type_0_item_data, ClassificationRefType0):
                    nature_types_type_0_item = nature_types_type_0_item_data.to_dict()
                else:
                    nature_types_type_0_item = nature_types_type_0_item_data
                nature_types.append(nature_types_type_0_item)

        else:
            nature_types = self.nature_types

        applications: Union[List[Union[Dict[str, Any], None]], None, Unset]
        if isinstance(self.applications, Unset):
            applications = UNSET
        elif isinstance(self.applications, list):
            applications = []
            for applications_type_0_item_data in self.applications:
                applications_type_0_item: Union[Dict[str, Any], None]
                if isinstance(applications_type_0_item_data, ContentRefType0):
                    applications_type_0_item = applications_type_0_item_data.to_dict()
                else:
                    applications_type_0_item = applications_type_0_item_data
                applications.append(applications_type_0_item)

        else:
            applications = self.applications

        short_title: Union[Dict[str, Any], None, Unset]
        if isinstance(self.short_title, Unset):
            short_title = UNSET
        elif isinstance(self.short_title, LocalizedStringType0):
            short_title = self.short_title.to_dict()
        else:
            short_title = self.short_title

        total_academic_ownership = self.total_academic_ownership

        total_applied_amount: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.total_applied_amount, Unset):
            total_applied_amount = self.total_applied_amount.to_dict()

        workflow: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.workflow, Unset):
            workflow = self.workflow.to_dict()

        visibility: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.visibility, Unset):
            visibility = self.visibility.to_dict()

        links: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.links, Unset):
            links = UNSET
        elif isinstance(self.links, list):
            links = []
            for links_type_0_item_data in self.links:
                links_type_0_item = links_type_0_item_data.to_dict()
                links.append(links_type_0_item)

        else:
            links = self.links

        keyword_groups: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.keyword_groups, Unset):
            keyword_groups = UNSET
        elif isinstance(self.keyword_groups, list):
            keyword_groups = []
            for keyword_groups_type_0_item_data in self.keyword_groups:
                keyword_groups_type_0_item = keyword_groups_type_0_item_data.to_dict()
                keyword_groups.append(keyword_groups_type_0_item)

        else:
            keyword_groups = self.keyword_groups

        custom_defined_fields: Union[Dict[str, Any], None, Unset]
        if isinstance(self.custom_defined_fields, Unset):
            custom_defined_fields = UNSET
        elif isinstance(self.custom_defined_fields, CustomDefinedFieldsType0):
            custom_defined_fields = self.custom_defined_fields.to_dict()
        else:
            custom_defined_fields = self.custom_defined_fields

        cluster: Union[Dict[str, Any], None, Unset]
        if isinstance(self.cluster, Unset):
            cluster = UNSET
        elif isinstance(self.cluster, ContentRefType0):
            cluster = self.cluster.to_dict()
        else:
            cluster = self.cluster

        system_name = self.system_name

        application_statuses: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.application_statuses, Unset):
            application_statuses = []
            for application_statuses_item_data in self.application_statuses:
                application_statuses_item = application_statuses_item_data.to_dict()
                application_statuses.append(application_statuses_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "fundings": fundings,
                "organizations": organizations,
                "managingOrganization": managing_organization,
                "title": title,
                "type": type,
                "typeDiscriminator": type_discriminator,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if uuid is not UNSET:
            field_dict["uuid"] = uuid
        if created_by is not UNSET:
            field_dict["createdBy"] = created_by
        if created_date is not UNSET:
            field_dict["createdDate"] = created_date
        if modified_by is not UNSET:
            field_dict["modifiedBy"] = modified_by
        if modified_date is not UNSET:
            field_dict["modifiedDate"] = modified_date
        if portal_url is not UNSET:
            field_dict["portalUrl"] = portal_url
        if pretty_url_identifiers is not UNSET:
            field_dict["prettyUrlIdentifiers"] = pretty_url_identifiers
        if previous_uuids is not UNSET:
            field_dict["previousUuids"] = previous_uuids
        if version is not UNSET:
            field_dict["version"] = version
        if acronym is not UNSET:
            field_dict["acronym"] = acronym
        if applicants is not UNSET:
            field_dict["applicants"] = applicants
        if external_organizations is not UNSET:
            field_dict["externalOrganizations"] = external_organizations
        if application_date is not UNSET:
            field_dict["applicationDate"] = application_date
        if awards is not UNSET:
            field_dict["awards"] = awards
        if budget_applied_amount_difference is not UNSET:
            field_dict["budgetAppliedAmountDifference"] = budget_applied_amount_difference
        if co_managing_organizations is not UNSET:
            field_dict["coManagingOrganizations"] = co_managing_organizations
        if collaborators is not UNSET:
            field_dict["collaborators"] = collaborators
        if descriptions is not UNSET:
            field_dict["descriptions"] = descriptions
        if expected_period is not UNSET:
            field_dict["expectedPeriod"] = expected_period
        if identifiers is not UNSET:
            field_dict["identifiers"] = identifiers
        if funding_opportunity is not UNSET:
            field_dict["fundingOpportunity"] = funding_opportunity
        if nature_types is not UNSET:
            field_dict["natureTypes"] = nature_types
        if applications is not UNSET:
            field_dict["applications"] = applications
        if short_title is not UNSET:
            field_dict["shortTitle"] = short_title
        if total_academic_ownership is not UNSET:
            field_dict["totalAcademicOwnership"] = total_academic_ownership
        if total_applied_amount is not UNSET:
            field_dict["totalAppliedAmount"] = total_applied_amount
        if workflow is not UNSET:
            field_dict["workflow"] = workflow
        if visibility is not UNSET:
            field_dict["visibility"] = visibility
        if links is not UNSET:
            field_dict["links"] = links
        if keyword_groups is not UNSET:
            field_dict["keywordGroups"] = keyword_groups
        if custom_defined_fields is not UNSET:
            field_dict["customDefinedFields"] = custom_defined_fields
        if cluster is not UNSET:
            field_dict["cluster"] = cluster
        if system_name is not UNSET:
            field_dict["systemName"] = system_name
        if application_statuses is not UNSET:
            field_dict["applicationStatuses"] = application_statuses

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.abstract_applicant_association import AbstractApplicantAssociation
        from ..models.application_funding_association import ApplicationFundingAssociation
        from ..models.application_status import ApplicationStatus
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.classified_localized_value import ClassifiedLocalizedValue
        from ..models.collaborator_association import CollaboratorAssociation
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.custom_defined_fields_type_0 import CustomDefinedFieldsType0
        from ..models.date_range import DateRange
        from ..models.identifier import Identifier
        from ..models.keyword_group import KeywordGroup
        from ..models.link import Link
        from ..models.localized_string_type_0 import LocalizedStringType0
        from ..models.system_currency_amount import SystemCurrencyAmount
        from ..models.visibility import Visibility
        from ..models.workflow import Workflow

        d = src_dict.copy()
        fundings = []
        _fundings = d.pop("fundings")
        for fundings_item_data in _fundings:
            fundings_item = ApplicationFundingAssociation.from_dict(fundings_item_data)

            fundings.append(fundings_item)

        organizations = []
        _organizations = d.pop("organizations")
        for organizations_item_data in _organizations:

            def _parse_organizations_item(data: object) -> Union["ContentRefType0", None]:
                if data is None:
                    return data
                try:
                    if not isinstance(data, dict):
                        raise TypeError()
                    componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                    return componentsschemas_content_ref_type_0
                except:  # noqa: E722
                    pass
                return cast(Union["ContentRefType0", None], data)

            organizations_item = _parse_organizations_item(organizations_item_data)

            organizations.append(organizations_item)

        def _parse_managing_organization(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        managing_organization = _parse_managing_organization(d.pop("managingOrganization"))

        def _parse_title(data: object) -> Union["LocalizedStringType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None], data)

        title = _parse_title(d.pop("title"))

        def _parse_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        type = _parse_type(d.pop("type"))

        type_discriminator = d.pop("typeDiscriminator")

        pure_id = d.pop("pureId", UNSET)

        uuid = d.pop("uuid", UNSET)

        created_by = d.pop("createdBy", UNSET)

        _created_date = d.pop("createdDate", UNSET)
        created_date: Union[Unset, datetime.datetime]
        if isinstance(_created_date, Unset):
            created_date = UNSET
        else:
            created_date = isoparse(_created_date)

        modified_by = d.pop("modifiedBy", UNSET)

        _modified_date = d.pop("modifiedDate", UNSET)
        modified_date: Union[Unset, datetime.datetime]
        if isinstance(_modified_date, Unset):
            modified_date = UNSET
        else:
            modified_date = isoparse(_modified_date)

        portal_url = d.pop("portalUrl", UNSET)

        pretty_url_identifiers = cast(List[str], d.pop("prettyUrlIdentifiers", UNSET))

        previous_uuids = cast(List[str], d.pop("previousUuids", UNSET))

        def _parse_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        version = _parse_version(d.pop("version", UNSET))

        def _parse_acronym(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        acronym = _parse_acronym(d.pop("acronym", UNSET))

        applicants = []
        _applicants = d.pop("applicants", UNSET)
        for applicants_item_data in _applicants or []:
            applicants_item = AbstractApplicantAssociation.from_dict(applicants_item_data)

            applicants.append(applicants_item)

        def _parse_external_organizations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                external_organizations_type_0 = []
                _external_organizations_type_0 = data
                for external_organizations_type_0_item_data in _external_organizations_type_0:

                    def _parse_external_organizations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    external_organizations_type_0_item = _parse_external_organizations_type_0_item(
                        external_organizations_type_0_item_data
                    )

                    external_organizations_type_0.append(external_organizations_type_0_item)

                return external_organizations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        external_organizations = _parse_external_organizations(d.pop("externalOrganizations", UNSET))

        _application_date = d.pop("applicationDate", UNSET)
        application_date: Union[Unset, datetime.date]
        if isinstance(_application_date, Unset):
            application_date = UNSET
        else:
            application_date = isoparse(_application_date).date()

        def _parse_awards(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                awards_type_0 = []
                _awards_type_0 = data
                for awards_type_0_item_data in _awards_type_0:

                    def _parse_awards_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    awards_type_0_item = _parse_awards_type_0_item(awards_type_0_item_data)

                    awards_type_0.append(awards_type_0_item)

                return awards_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        awards = _parse_awards(d.pop("awards", UNSET))

        _budget_applied_amount_difference = d.pop("budgetAppliedAmountDifference", UNSET)
        budget_applied_amount_difference: Union[Unset, SystemCurrencyAmount]
        if isinstance(_budget_applied_amount_difference, Unset):
            budget_applied_amount_difference = UNSET
        else:
            budget_applied_amount_difference = SystemCurrencyAmount.from_dict(_budget_applied_amount_difference)

        def _parse_co_managing_organizations(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                co_managing_organizations_type_0 = []
                _co_managing_organizations_type_0 = data
                for co_managing_organizations_type_0_item_data in _co_managing_organizations_type_0:

                    def _parse_co_managing_organizations_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    co_managing_organizations_type_0_item = _parse_co_managing_organizations_type_0_item(
                        co_managing_organizations_type_0_item_data
                    )

                    co_managing_organizations_type_0.append(co_managing_organizations_type_0_item)

                return co_managing_organizations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        co_managing_organizations = _parse_co_managing_organizations(d.pop("coManagingOrganizations", UNSET))

        def _parse_collaborators(data: object) -> Union[List["CollaboratorAssociation"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                collaborators_type_0 = []
                _collaborators_type_0 = data
                for collaborators_type_0_item_data in _collaborators_type_0:
                    collaborators_type_0_item = CollaboratorAssociation.from_dict(collaborators_type_0_item_data)

                    collaborators_type_0.append(collaborators_type_0_item)

                return collaborators_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["CollaboratorAssociation"], None, Unset], data)

        collaborators = _parse_collaborators(d.pop("collaborators", UNSET))

        def _parse_descriptions(data: object) -> Union[List["ClassifiedLocalizedValue"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                descriptions_type_0 = []
                _descriptions_type_0 = data
                for descriptions_type_0_item_data in _descriptions_type_0:
                    descriptions_type_0_item = ClassifiedLocalizedValue.from_dict(descriptions_type_0_item_data)

                    descriptions_type_0.append(descriptions_type_0_item)

                return descriptions_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["ClassifiedLocalizedValue"], None, Unset], data)

        descriptions = _parse_descriptions(d.pop("descriptions", UNSET))

        _expected_period = d.pop("expectedPeriod", UNSET)
        expected_period: Union[Unset, DateRange]
        if isinstance(_expected_period, Unset):
            expected_period = UNSET
        else:
            expected_period = DateRange.from_dict(_expected_period)

        def _parse_identifiers(data: object) -> Union[List["Identifier"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                identifiers_type_0 = []
                _identifiers_type_0 = data
                for identifiers_type_0_item_data in _identifiers_type_0:
                    identifiers_type_0_item = Identifier.from_dict(identifiers_type_0_item_data)

                    identifiers_type_0.append(identifiers_type_0_item)

                return identifiers_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Identifier"], None, Unset], data)

        identifiers = _parse_identifiers(d.pop("identifiers", UNSET))

        def _parse_funding_opportunity(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        funding_opportunity = _parse_funding_opportunity(d.pop("fundingOpportunity", UNSET))

        def _parse_nature_types(data: object) -> Union[List[Union["ClassificationRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                nature_types_type_0 = []
                _nature_types_type_0 = data
                for nature_types_type_0_item_data in _nature_types_type_0:

                    def _parse_nature_types_type_0_item(data: object) -> Union["ClassificationRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                            return componentsschemas_classification_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ClassificationRefType0", None], data)

                    nature_types_type_0_item = _parse_nature_types_type_0_item(nature_types_type_0_item_data)

                    nature_types_type_0.append(nature_types_type_0_item)

                return nature_types_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ClassificationRefType0", None]], None, Unset], data)

        nature_types = _parse_nature_types(d.pop("natureTypes", UNSET))

        def _parse_applications(data: object) -> Union[List[Union["ContentRefType0", None]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                applications_type_0 = []
                _applications_type_0 = data
                for applications_type_0_item_data in _applications_type_0:

                    def _parse_applications_type_0_item(data: object) -> Union["ContentRefType0", None]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, dict):
                                raise TypeError()
                            componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                            return componentsschemas_content_ref_type_0
                        except:  # noqa: E722
                            pass
                        return cast(Union["ContentRefType0", None], data)

                    applications_type_0_item = _parse_applications_type_0_item(applications_type_0_item_data)

                    applications_type_0.append(applications_type_0_item)

                return applications_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[Union["ContentRefType0", None]], None, Unset], data)

        applications = _parse_applications(d.pop("applications", UNSET))

        def _parse_short_title(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        short_title = _parse_short_title(d.pop("shortTitle", UNSET))

        total_academic_ownership = d.pop("totalAcademicOwnership", UNSET)

        _total_applied_amount = d.pop("totalAppliedAmount", UNSET)
        total_applied_amount: Union[Unset, SystemCurrencyAmount]
        if isinstance(_total_applied_amount, Unset):
            total_applied_amount = UNSET
        else:
            total_applied_amount = SystemCurrencyAmount.from_dict(_total_applied_amount)

        _workflow = d.pop("workflow", UNSET)
        workflow: Union[Unset, Workflow]
        if isinstance(_workflow, Unset):
            workflow = UNSET
        else:
            workflow = Workflow.from_dict(_workflow)

        _visibility = d.pop("visibility", UNSET)
        visibility: Union[Unset, Visibility]
        if isinstance(_visibility, Unset):
            visibility = UNSET
        else:
            visibility = Visibility.from_dict(_visibility)

        def _parse_links(data: object) -> Union[List["Link"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                links_type_0 = []
                _links_type_0 = data
                for links_type_0_item_data in _links_type_0:
                    links_type_0_item = Link.from_dict(links_type_0_item_data)

                    links_type_0.append(links_type_0_item)

                return links_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Link"], None, Unset], data)

        links = _parse_links(d.pop("links", UNSET))

        def _parse_keyword_groups(data: object) -> Union[List["KeywordGroup"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                keyword_groups_type_0 = []
                _keyword_groups_type_0 = data
                for keyword_groups_type_0_item_data in _keyword_groups_type_0:
                    keyword_groups_type_0_item = KeywordGroup.from_dict(keyword_groups_type_0_item_data)

                    keyword_groups_type_0.append(keyword_groups_type_0_item)

                return keyword_groups_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["KeywordGroup"], None, Unset], data)

        keyword_groups = _parse_keyword_groups(d.pop("keywordGroups", UNSET))

        def _parse_custom_defined_fields(data: object) -> Union["CustomDefinedFieldsType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_custom_defined_fields_type_0 = CustomDefinedFieldsType0.from_dict(data)

                return componentsschemas_custom_defined_fields_type_0
            except:  # noqa: E722
                pass
            return cast(Union["CustomDefinedFieldsType0", None, Unset], data)

        custom_defined_fields = _parse_custom_defined_fields(d.pop("customDefinedFields", UNSET))

        def _parse_cluster(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        cluster = _parse_cluster(d.pop("cluster", UNSET))

        system_name = d.pop("systemName", UNSET)

        application_statuses = []
        _application_statuses = d.pop("applicationStatuses", UNSET)
        for application_statuses_item_data in _application_statuses or []:
            application_statuses_item = ApplicationStatus.from_dict(application_statuses_item_data)

            application_statuses.append(application_statuses_item)

        basic_application = cls(
            fundings=fundings,
            organizations=organizations,
            managing_organization=managing_organization,
            title=title,
            type=type,
            type_discriminator=type_discriminator,
            pure_id=pure_id,
            uuid=uuid,
            created_by=created_by,
            created_date=created_date,
            modified_by=modified_by,
            modified_date=modified_date,
            portal_url=portal_url,
            pretty_url_identifiers=pretty_url_identifiers,
            previous_uuids=previous_uuids,
            version=version,
            acronym=acronym,
            applicants=applicants,
            external_organizations=external_organizations,
            application_date=application_date,
            awards=awards,
            budget_applied_amount_difference=budget_applied_amount_difference,
            co_managing_organizations=co_managing_organizations,
            collaborators=collaborators,
            descriptions=descriptions,
            expected_period=expected_period,
            identifiers=identifiers,
            funding_opportunity=funding_opportunity,
            nature_types=nature_types,
            applications=applications,
            short_title=short_title,
            total_academic_ownership=total_academic_ownership,
            total_applied_amount=total_applied_amount,
            workflow=workflow,
            visibility=visibility,
            links=links,
            keyword_groups=keyword_groups,
            custom_defined_fields=custom_defined_fields,
            cluster=cluster,
            system_name=system_name,
            application_statuses=application_statuses,
        )

        basic_application.additional_properties = d
        return basic_application

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
