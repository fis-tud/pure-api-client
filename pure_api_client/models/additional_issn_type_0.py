from typing import Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

T = TypeVar("T", bound="AdditionalISSNType0")


@_attrs_define
class AdditionalISSNType0:
    """
    Attributes:
        issn (str): The actual ISSN value
        type_discriminator (str):
    """

    issn: str
    type_discriminator: str
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        issn = self.issn

        type_discriminator = self.type_discriminator

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "issn": issn,
                "typeDiscriminator": type_discriminator,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        issn = d.pop("issn")

        type_discriminator = d.pop("typeDiscriminator")

        additional_issn_type_0 = cls(
            issn=issn,
            type_discriminator=type_discriminator,
        )

        additional_issn_type_0.additional_properties = d
        return additional_issn_type_0

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
