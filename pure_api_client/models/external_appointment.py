from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.compound_date_range import CompoundDateRange
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.localized_string_type_0 import LocalizedStringType0


T = TypeVar("T", bound="ExternalAppointment")


@_attrs_define
class ExternalAppointment:
    """An appointment held in an external organizational unit

    Attributes:
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        appointment (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        appointment_string (Union['LocalizedStringType0', None, Unset]): A set of string values, one for each submission
            locale. Note: invalid locale values will be ignored. Example: {'en_GB': 'Some text'}.
        period (Union[Unset, CompoundDateRange]): A date range of that can be defined by only year, year and month or a
            full date
        external_organization (Union['ContentRefType0', None, Unset]):
    """

    pure_id: Union[Unset, int] = UNSET
    appointment: Union["ClassificationRefType0", None, Unset] = UNSET
    appointment_string: Union["LocalizedStringType0", None, Unset] = UNSET
    period: Union[Unset, "CompoundDateRange"] = UNSET
    external_organization: Union["ContentRefType0", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        pure_id = self.pure_id

        appointment: Union[Dict[str, Any], None, Unset]
        if isinstance(self.appointment, Unset):
            appointment = UNSET
        elif isinstance(self.appointment, ClassificationRefType0):
            appointment = self.appointment.to_dict()
        else:
            appointment = self.appointment

        appointment_string: Union[Dict[str, Any], None, Unset]
        if isinstance(self.appointment_string, Unset):
            appointment_string = UNSET
        elif isinstance(self.appointment_string, LocalizedStringType0):
            appointment_string = self.appointment_string.to_dict()
        else:
            appointment_string = self.appointment_string

        period: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.period, Unset):
            period = self.period.to_dict()

        external_organization: Union[Dict[str, Any], None, Unset]
        if isinstance(self.external_organization, Unset):
            external_organization = UNSET
        elif isinstance(self.external_organization, ContentRefType0):
            external_organization = self.external_organization.to_dict()
        else:
            external_organization = self.external_organization

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if appointment is not UNSET:
            field_dict["appointment"] = appointment
        if appointment_string is not UNSET:
            field_dict["appointmentString"] = appointment_string
        if period is not UNSET:
            field_dict["period"] = period
        if external_organization is not UNSET:
            field_dict["externalOrganization"] = external_organization

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.compound_date_range import CompoundDateRange
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.localized_string_type_0 import LocalizedStringType0

        d = src_dict.copy()
        pure_id = d.pop("pureId", UNSET)

        def _parse_appointment(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        appointment = _parse_appointment(d.pop("appointment", UNSET))

        def _parse_appointment_string(data: object) -> Union["LocalizedStringType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_localized_string_type_0 = LocalizedStringType0.from_dict(data)

                return componentsschemas_localized_string_type_0
            except:  # noqa: E722
                pass
            return cast(Union["LocalizedStringType0", None, Unset], data)

        appointment_string = _parse_appointment_string(d.pop("appointmentString", UNSET))

        _period = d.pop("period", UNSET)
        period: Union[Unset, CompoundDateRange]
        if isinstance(_period, Unset):
            period = UNSET
        else:
            period = CompoundDateRange.from_dict(_period)

        def _parse_external_organization(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        external_organization = _parse_external_organization(d.pop("externalOrganization", UNSET))

        external_appointment = cls(
            pure_id=pure_id,
            appointment=appointment,
            appointment_string=appointment_string,
            period=period,
            external_organization=external_organization,
        )

        external_appointment.additional_properties = d
        return external_appointment

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
