from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.date_range import DateRange
    from ..models.internal_or_external_supervisor import InternalOrExternalSupervisor


T = TypeVar("T", bound="PersonSupervisorAssociation")


@_attrs_define
class PersonSupervisorAssociation:
    """The association data of a person and their supervisor

    Attributes:
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        period (Union[Unset, DateRange]): A date range
        supervision_percentage (Union[Unset, int]): Percentage for which the Supervisor is responsible for the total
            supervision
        supervisor_role (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        supervisor (Union[Unset, InternalOrExternalSupervisor]): A supervisor, either internal or external, use as
            mutually exclusive
    """

    pure_id: Union[Unset, int] = UNSET
    period: Union[Unset, "DateRange"] = UNSET
    supervision_percentage: Union[Unset, int] = UNSET
    supervisor_role: Union["ClassificationRefType0", None, Unset] = UNSET
    supervisor: Union[Unset, "InternalOrExternalSupervisor"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0

        pure_id = self.pure_id

        period: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.period, Unset):
            period = self.period.to_dict()

        supervision_percentage = self.supervision_percentage

        supervisor_role: Union[Dict[str, Any], None, Unset]
        if isinstance(self.supervisor_role, Unset):
            supervisor_role = UNSET
        elif isinstance(self.supervisor_role, ClassificationRefType0):
            supervisor_role = self.supervisor_role.to_dict()
        else:
            supervisor_role = self.supervisor_role

        supervisor: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.supervisor, Unset):
            supervisor = self.supervisor.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if period is not UNSET:
            field_dict["period"] = period
        if supervision_percentage is not UNSET:
            field_dict["supervisionPercentage"] = supervision_percentage
        if supervisor_role is not UNSET:
            field_dict["supervisorRole"] = supervisor_role
        if supervisor is not UNSET:
            field_dict["supervisor"] = supervisor

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.date_range import DateRange
        from ..models.internal_or_external_supervisor import InternalOrExternalSupervisor

        d = src_dict.copy()
        pure_id = d.pop("pureId", UNSET)

        _period = d.pop("period", UNSET)
        period: Union[Unset, DateRange]
        if isinstance(_period, Unset):
            period = UNSET
        else:
            period = DateRange.from_dict(_period)

        supervision_percentage = d.pop("supervisionPercentage", UNSET)

        def _parse_supervisor_role(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        supervisor_role = _parse_supervisor_role(d.pop("supervisorRole", UNSET))

        _supervisor = d.pop("supervisor", UNSET)
        supervisor: Union[Unset, InternalOrExternalSupervisor]
        if isinstance(_supervisor, Unset):
            supervisor = UNSET
        else:
            supervisor = InternalOrExternalSupervisor.from_dict(_supervisor)

        person_supervisor_association = cls(
            pure_id=pure_id,
            period=period,
            supervision_percentage=supervision_percentage,
            supervisor_role=supervisor_role,
            supervisor=supervisor,
        )

        person_supervisor_association.additional_properties = d
        return person_supervisor_association

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
