import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.application_document_file_store_locations import ApplicationDocumentFileStoreLocations
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.uploaded_file import UploadedFile
    from ..models.visibility import Visibility


T = TypeVar("T", bound="ApplicationDocument")


@_attrs_define
class ApplicationDocument:
    """An application document.

    Attributes:
        file_name (str): The documents file name
        mime_type (str): The documents mime type
        size (int): The documents size in bytes
        type (Union['ClassificationRefType0', None]): A reference to a classification value
        visibility (Visibility): Visibility of an object
        version_type (Union['ClassificationRefType0', None]): A reference to a classification value
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        file_id (Union[Unset, str]): The id of the file
        url (Union[Unset, str]): Download url for the binary file
        file_store_locations (Union[Unset, ApplicationDocumentFileStoreLocations]): Locations of the binary file in file
            stores.
        uploaded_file (Union[Unset, UploadedFile]): Information about the uploaded file
        file_data (Union[Unset, str]): Base64 encoded file data for new files. This property can be used instead of
            uploadedFile for small files
        title (Union[None, Unset, str]): Document title
        license_ (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        visible_on_portal_date (Union[Unset, datetime.date]): The date the document will be available on the portal
        creator (Union[Unset, str]): The user that created the document
        created (Union[Unset, datetime.datetime]): Create date for the document
    """

    file_name: str
    mime_type: str
    size: int
    type: Union["ClassificationRefType0", None]
    visibility: "Visibility"
    version_type: Union["ClassificationRefType0", None]
    pure_id: Union[Unset, int] = UNSET
    file_id: Union[Unset, str] = UNSET
    url: Union[Unset, str] = UNSET
    file_store_locations: Union[Unset, "ApplicationDocumentFileStoreLocations"] = UNSET
    uploaded_file: Union[Unset, "UploadedFile"] = UNSET
    file_data: Union[Unset, str] = UNSET
    title: Union[None, Unset, str] = UNSET
    license_: Union["ClassificationRefType0", None, Unset] = UNSET
    visible_on_portal_date: Union[Unset, datetime.date] = UNSET
    creator: Union[Unset, str] = UNSET
    created: Union[Unset, datetime.datetime] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0

        file_name = self.file_name

        mime_type = self.mime_type

        size = self.size

        type: Union[Dict[str, Any], None]
        if isinstance(self.type, ClassificationRefType0):
            type = self.type.to_dict()
        else:
            type = self.type

        visibility = self.visibility.to_dict()

        version_type: Union[Dict[str, Any], None]
        if isinstance(self.version_type, ClassificationRefType0):
            version_type = self.version_type.to_dict()
        else:
            version_type = self.version_type

        pure_id = self.pure_id

        file_id = self.file_id

        url = self.url

        file_store_locations: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.file_store_locations, Unset):
            file_store_locations = self.file_store_locations.to_dict()

        uploaded_file: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.uploaded_file, Unset):
            uploaded_file = self.uploaded_file.to_dict()

        file_data = self.file_data

        title: Union[None, Unset, str]
        if isinstance(self.title, Unset):
            title = UNSET
        else:
            title = self.title

        license_: Union[Dict[str, Any], None, Unset]
        if isinstance(self.license_, Unset):
            license_ = UNSET
        elif isinstance(self.license_, ClassificationRefType0):
            license_ = self.license_.to_dict()
        else:
            license_ = self.license_

        visible_on_portal_date: Union[Unset, str] = UNSET
        if not isinstance(self.visible_on_portal_date, Unset):
            visible_on_portal_date = self.visible_on_portal_date.isoformat()

        creator = self.creator

        created: Union[Unset, str] = UNSET
        if not isinstance(self.created, Unset):
            created = self.created.isoformat()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "fileName": file_name,
                "mimeType": mime_type,
                "size": size,
                "type": type,
                "visibility": visibility,
                "versionType": version_type,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if file_id is not UNSET:
            field_dict["fileId"] = file_id
        if url is not UNSET:
            field_dict["url"] = url
        if file_store_locations is not UNSET:
            field_dict["fileStoreLocations"] = file_store_locations
        if uploaded_file is not UNSET:
            field_dict["uploadedFile"] = uploaded_file
        if file_data is not UNSET:
            field_dict["fileData"] = file_data
        if title is not UNSET:
            field_dict["title"] = title
        if license_ is not UNSET:
            field_dict["license"] = license_
        if visible_on_portal_date is not UNSET:
            field_dict["visibleOnPortalDate"] = visible_on_portal_date
        if creator is not UNSET:
            field_dict["creator"] = creator
        if created is not UNSET:
            field_dict["created"] = created

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.application_document_file_store_locations import ApplicationDocumentFileStoreLocations
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.uploaded_file import UploadedFile
        from ..models.visibility import Visibility

        d = src_dict.copy()
        file_name = d.pop("fileName")

        mime_type = d.pop("mimeType")

        size = d.pop("size")

        def _parse_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        type = _parse_type(d.pop("type"))

        visibility = Visibility.from_dict(d.pop("visibility"))

        def _parse_version_type(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        version_type = _parse_version_type(d.pop("versionType"))

        pure_id = d.pop("pureId", UNSET)

        file_id = d.pop("fileId", UNSET)

        url = d.pop("url", UNSET)

        _file_store_locations = d.pop("fileStoreLocations", UNSET)
        file_store_locations: Union[Unset, ApplicationDocumentFileStoreLocations]
        if isinstance(_file_store_locations, Unset):
            file_store_locations = UNSET
        else:
            file_store_locations = ApplicationDocumentFileStoreLocations.from_dict(_file_store_locations)

        _uploaded_file = d.pop("uploadedFile", UNSET)
        uploaded_file: Union[Unset, UploadedFile]
        if isinstance(_uploaded_file, Unset):
            uploaded_file = UNSET
        else:
            uploaded_file = UploadedFile.from_dict(_uploaded_file)

        file_data = d.pop("fileData", UNSET)

        def _parse_title(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        title = _parse_title(d.pop("title", UNSET))

        def _parse_license_(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        license_ = _parse_license_(d.pop("license", UNSET))

        _visible_on_portal_date = d.pop("visibleOnPortalDate", UNSET)
        visible_on_portal_date: Union[Unset, datetime.date]
        if isinstance(_visible_on_portal_date, Unset):
            visible_on_portal_date = UNSET
        else:
            visible_on_portal_date = isoparse(_visible_on_portal_date).date()

        creator = d.pop("creator", UNSET)

        _created = d.pop("created", UNSET)
        created: Union[Unset, datetime.datetime]
        if isinstance(_created, Unset):
            created = UNSET
        else:
            created = isoparse(_created)

        application_document = cls(
            file_name=file_name,
            mime_type=mime_type,
            size=size,
            type=type,
            visibility=visibility,
            version_type=version_type,
            pure_id=pure_id,
            file_id=file_id,
            url=url,
            file_store_locations=file_store_locations,
            uploaded_file=uploaded_file,
            file_data=file_data,
            title=title,
            license_=license_,
            visible_on_portal_date=visible_on_portal_date,
            creator=creator,
            created=created,
        )

        application_document.additional_properties = d
        return application_document

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
