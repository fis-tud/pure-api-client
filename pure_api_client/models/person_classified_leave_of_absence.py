from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.date_range import DateRange


T = TypeVar("T", bound="PersonClassifiedLeaveOfAbsence")


@_attrs_define
class PersonClassifiedLeaveOfAbsence:
    """Leave of absence held by a person

    Attributes:
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        classification (Union['ClassificationRefType0', None, Unset]): A reference to a classification value
        period (Union[Unset, DateRange]): A date range
    """

    pure_id: Union[Unset, int] = UNSET
    classification: Union["ClassificationRefType0", None, Unset] = UNSET
    period: Union[Unset, "DateRange"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0

        pure_id = self.pure_id

        classification: Union[Dict[str, Any], None, Unset]
        if isinstance(self.classification, Unset):
            classification = UNSET
        elif isinstance(self.classification, ClassificationRefType0):
            classification = self.classification.to_dict()
        else:
            classification = self.classification

        period: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.period, Unset):
            period = self.period.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if classification is not UNSET:
            field_dict["classification"] = classification
        if period is not UNSET:
            field_dict["period"] = period

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.date_range import DateRange

        d = src_dict.copy()
        pure_id = d.pop("pureId", UNSET)

        def _parse_classification(data: object) -> Union["ClassificationRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None, Unset], data)

        classification = _parse_classification(d.pop("classification", UNSET))

        _period = d.pop("period", UNSET)
        period: Union[Unset, DateRange]
        if isinstance(_period, Unset):
            period = UNSET
        else:
            period = DateRange.from_dict(_period)

        person_classified_leave_of_absence = cls(
            pure_id=pure_id,
            classification=classification,
            period=period,
        )

        person_classified_leave_of_absence.additional_properties = d
        return person_classified_leave_of_absence

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
