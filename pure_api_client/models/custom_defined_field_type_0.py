from typing import Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

T = TypeVar("T", bound="CustomDefinedFieldType0")


@_attrs_define
class CustomDefinedFieldType0:
    """Map of CustomDefinedField values, where the key is the field identifier

    Example:
        { "fieldName1": "typeDiscriminator": "Integer", "value" : 1}

    Attributes:
        type_discriminator (str):
    """

    type_discriminator: str
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        type_discriminator = self.type_discriminator

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "typeDiscriminator": type_discriminator,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        type_discriminator = d.pop("typeDiscriminator")

        custom_defined_field_type_0 = cls(
            type_discriminator=type_discriminator,
        )

        custom_defined_field_type_0.additional_properties = d
        return custom_defined_field_type_0

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
