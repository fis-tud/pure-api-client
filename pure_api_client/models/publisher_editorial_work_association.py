from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

if TYPE_CHECKING:
    from ..models.content_ref_type_0 import ContentRefType0


T = TypeVar("T", bound="PublisherEditorialWorkAssociation")


@_attrs_define
class PublisherEditorialWorkAssociation:
    """An editorial work association with a publisher.

    Attributes:
        type_discriminator (str):
        publisher (Union['ContentRefType0', None]):
    """

    type_discriminator: str
    publisher: Union["ContentRefType0", None]
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.content_ref_type_0 import ContentRefType0

        type_discriminator = self.type_discriminator

        publisher: Union[Dict[str, Any], None]
        if isinstance(self.publisher, ContentRefType0):
            publisher = self.publisher.to_dict()
        else:
            publisher = self.publisher

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "typeDiscriminator": type_discriminator,
                "publisher": publisher,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.content_ref_type_0 import ContentRefType0

        d = src_dict.copy()
        type_discriminator = d.pop("typeDiscriminator")

        def _parse_publisher(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        publisher = _parse_publisher(d.pop("publisher"))

        publisher_editorial_work_association = cls(
            type_discriminator=type_discriminator,
            publisher=publisher,
        )

        publisher_editorial_work_association.additional_properties = d
        return publisher_editorial_work_association

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
