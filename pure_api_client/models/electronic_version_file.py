from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.electronic_version_file_file_store_locations import ElectronicVersionFileFileStoreLocations
    from ..models.uploaded_file import UploadedFile


T = TypeVar("T", bound="ElectronicVersionFile")


@_attrs_define
class ElectronicVersionFile:
    """Information about an electronic version file

    Attributes:
        file_name (str): The documents file name
        mime_type (str): The documents mime type
        size (int): The documents size in bytes
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        file_id (Union[Unset, str]): The id of the file
        url (Union[Unset, str]): Download url for the binary file
        file_store_locations (Union[Unset, ElectronicVersionFileFileStoreLocations]): Locations of the binary file in
            file stores.
        uploaded_file (Union[Unset, UploadedFile]): Information about the uploaded file
        file_data (Union[Unset, str]): Base64 encoded file data for new files. This property can be used instead of
            uploadedFile for small files
    """

    file_name: str
    mime_type: str
    size: int
    pure_id: Union[Unset, int] = UNSET
    file_id: Union[Unset, str] = UNSET
    url: Union[Unset, str] = UNSET
    file_store_locations: Union[Unset, "ElectronicVersionFileFileStoreLocations"] = UNSET
    uploaded_file: Union[Unset, "UploadedFile"] = UNSET
    file_data: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        file_name = self.file_name

        mime_type = self.mime_type

        size = self.size

        pure_id = self.pure_id

        file_id = self.file_id

        url = self.url

        file_store_locations: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.file_store_locations, Unset):
            file_store_locations = self.file_store_locations.to_dict()

        uploaded_file: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.uploaded_file, Unset):
            uploaded_file = self.uploaded_file.to_dict()

        file_data = self.file_data

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "fileName": file_name,
                "mimeType": mime_type,
                "size": size,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if file_id is not UNSET:
            field_dict["fileId"] = file_id
        if url is not UNSET:
            field_dict["url"] = url
        if file_store_locations is not UNSET:
            field_dict["fileStoreLocations"] = file_store_locations
        if uploaded_file is not UNSET:
            field_dict["uploadedFile"] = uploaded_file
        if file_data is not UNSET:
            field_dict["fileData"] = file_data

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.electronic_version_file_file_store_locations import ElectronicVersionFileFileStoreLocations
        from ..models.uploaded_file import UploadedFile

        d = src_dict.copy()
        file_name = d.pop("fileName")

        mime_type = d.pop("mimeType")

        size = d.pop("size")

        pure_id = d.pop("pureId", UNSET)

        file_id = d.pop("fileId", UNSET)

        url = d.pop("url", UNSET)

        _file_store_locations = d.pop("fileStoreLocations", UNSET)
        file_store_locations: Union[Unset, ElectronicVersionFileFileStoreLocations]
        if isinstance(_file_store_locations, Unset):
            file_store_locations = UNSET
        else:
            file_store_locations = ElectronicVersionFileFileStoreLocations.from_dict(_file_store_locations)

        _uploaded_file = d.pop("uploadedFile", UNSET)
        uploaded_file: Union[Unset, UploadedFile]
        if isinstance(_uploaded_file, Unset):
            uploaded_file = UNSET
        else:
            uploaded_file = UploadedFile.from_dict(_uploaded_file)

        file_data = d.pop("fileData", UNSET)

        electronic_version_file = cls(
            file_name=file_name,
            mime_type=mime_type,
            size=size,
            pure_id=pure_id,
            file_id=file_id,
            url=url,
            file_store_locations=file_store_locations,
            uploaded_file=uploaded_file,
            file_data=file_data,
        )

        electronic_version_file.additional_properties = d
        return electronic_version_file

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
