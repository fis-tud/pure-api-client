from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.classification_ref_type_0 import ClassificationRefType0
    from ..models.keyword import Keyword


T = TypeVar("T", bound="KeywordContainer")


@_attrs_define
class KeywordContainer:
    """Container for a structured keyword and/or free keywords

    Attributes:
        structured_keyword (Union['ClassificationRefType0', None]): A reference to a classification value
        pure_id (Union[Unset, int]): Pure database ID of the object, prefer using the UUID if it is present on the
            entity
        free_keywords (Union[List['Keyword'], None, Unset]): Free keywords
    """

    structured_keyword: Union["ClassificationRefType0", None]
    pure_id: Union[Unset, int] = UNSET
    free_keywords: Union[List["Keyword"], None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.classification_ref_type_0 import ClassificationRefType0

        structured_keyword: Union[Dict[str, Any], None]
        if isinstance(self.structured_keyword, ClassificationRefType0):
            structured_keyword = self.structured_keyword.to_dict()
        else:
            structured_keyword = self.structured_keyword

        pure_id = self.pure_id

        free_keywords: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.free_keywords, Unset):
            free_keywords = UNSET
        elif isinstance(self.free_keywords, list):
            free_keywords = []
            for free_keywords_type_0_item_data in self.free_keywords:
                free_keywords_type_0_item = free_keywords_type_0_item_data.to_dict()
                free_keywords.append(free_keywords_type_0_item)

        else:
            free_keywords = self.free_keywords

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "structuredKeyword": structured_keyword,
            }
        )
        if pure_id is not UNSET:
            field_dict["pureId"] = pure_id
        if free_keywords is not UNSET:
            field_dict["freeKeywords"] = free_keywords

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.classification_ref_type_0 import ClassificationRefType0
        from ..models.keyword import Keyword

        d = src_dict.copy()

        def _parse_structured_keyword(data: object) -> Union["ClassificationRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_classification_ref_type_0 = ClassificationRefType0.from_dict(data)

                return componentsschemas_classification_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ClassificationRefType0", None], data)

        structured_keyword = _parse_structured_keyword(d.pop("structuredKeyword"))

        pure_id = d.pop("pureId", UNSET)

        def _parse_free_keywords(data: object) -> Union[List["Keyword"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                free_keywords_type_0 = []
                _free_keywords_type_0 = data
                for free_keywords_type_0_item_data in _free_keywords_type_0:
                    free_keywords_type_0_item = Keyword.from_dict(free_keywords_type_0_item_data)

                    free_keywords_type_0.append(free_keywords_type_0_item)

                return free_keywords_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Keyword"], None, Unset], data)

        free_keywords = _parse_free_keywords(d.pop("freeKeywords", UNSET))

        keyword_container = cls(
            structured_keyword=structured_keyword,
            pure_id=pure_id,
            free_keywords=free_keywords,
        )

        keyword_container.additional_properties = d
        return keyword_container

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
