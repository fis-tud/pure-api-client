from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.content_ref_type_0 import ContentRefType0


T = TypeVar("T", bound="InternalOrExternalSupervisor")


@_attrs_define
class InternalOrExternalSupervisor:
    """A supervisor, either internal or external, use as mutually exclusive

    Attributes:
        external_supervisor (Union['ContentRefType0', None, Unset]):
        supervisor (Union['ContentRefType0', None, Unset]):
    """

    external_supervisor: Union["ContentRefType0", None, Unset] = UNSET
    supervisor: Union["ContentRefType0", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.content_ref_type_0 import ContentRefType0

        external_supervisor: Union[Dict[str, Any], None, Unset]
        if isinstance(self.external_supervisor, Unset):
            external_supervisor = UNSET
        elif isinstance(self.external_supervisor, ContentRefType0):
            external_supervisor = self.external_supervisor.to_dict()
        else:
            external_supervisor = self.external_supervisor

        supervisor: Union[Dict[str, Any], None, Unset]
        if isinstance(self.supervisor, Unset):
            supervisor = UNSET
        elif isinstance(self.supervisor, ContentRefType0):
            supervisor = self.supervisor.to_dict()
        else:
            supervisor = self.supervisor

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if external_supervisor is not UNSET:
            field_dict["externalSupervisor"] = external_supervisor
        if supervisor is not UNSET:
            field_dict["supervisor"] = supervisor

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.content_ref_type_0 import ContentRefType0

        d = src_dict.copy()

        def _parse_external_supervisor(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        external_supervisor = _parse_external_supervisor(d.pop("externalSupervisor", UNSET))

        def _parse_supervisor(data: object) -> Union["ContentRefType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None, Unset], data)

        supervisor = _parse_supervisor(d.pop("supervisor", UNSET))

        internal_or_external_supervisor = cls(
            external_supervisor=external_supervisor,
            supervisor=supervisor,
        )

        internal_or_external_supervisor.additional_properties = d
        return internal_or_external_supervisor

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
