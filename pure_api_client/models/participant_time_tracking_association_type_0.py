from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.content_ref_type_0 import ContentRefType0
    from ..models.participant_time_spent import ParticipantTimeSpent


T = TypeVar("T", bound="ParticipantTimeTrackingAssociationType0")


@_attrs_define
class ParticipantTimeTrackingAssociationType0:
    """Project participant time-tracking association, which holds allocated and spent time.

    Attributes:
        award (Union['ContentRefType0', None]):
        person (Union['ContentRefType0', None]):
        allocated_time (Union[None, Unset, int]): The allocated time for an award for the participant.
        total_time_spent (Union[Unset, int]): The total time spent for an award for the participant.
        time_spent (Union[Unset, List['ParticipantTimeSpent']]):
    """

    award: Union["ContentRefType0", None]
    person: Union["ContentRefType0", None]
    allocated_time: Union[None, Unset, int] = UNSET
    total_time_spent: Union[Unset, int] = UNSET
    time_spent: Union[Unset, List["ParticipantTimeSpent"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.content_ref_type_0 import ContentRefType0

        award: Union[Dict[str, Any], None]
        if isinstance(self.award, ContentRefType0):
            award = self.award.to_dict()
        else:
            award = self.award

        person: Union[Dict[str, Any], None]
        if isinstance(self.person, ContentRefType0):
            person = self.person.to_dict()
        else:
            person = self.person

        allocated_time: Union[None, Unset, int]
        if isinstance(self.allocated_time, Unset):
            allocated_time = UNSET
        else:
            allocated_time = self.allocated_time

        total_time_spent = self.total_time_spent

        time_spent: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.time_spent, Unset):
            time_spent = []
            for time_spent_item_data in self.time_spent:
                time_spent_item = time_spent_item_data.to_dict()
                time_spent.append(time_spent_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "award": award,
                "person": person,
            }
        )
        if allocated_time is not UNSET:
            field_dict["allocatedTime"] = allocated_time
        if total_time_spent is not UNSET:
            field_dict["totalTimeSpent"] = total_time_spent
        if time_spent is not UNSET:
            field_dict["timeSpent"] = time_spent

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.content_ref_type_0 import ContentRefType0
        from ..models.participant_time_spent import ParticipantTimeSpent

        d = src_dict.copy()

        def _parse_award(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        award = _parse_award(d.pop("award"))

        def _parse_person(data: object) -> Union["ContentRefType0", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                componentsschemas_content_ref_type_0 = ContentRefType0.from_dict(data)

                return componentsschemas_content_ref_type_0
            except:  # noqa: E722
                pass
            return cast(Union["ContentRefType0", None], data)

        person = _parse_person(d.pop("person"))

        def _parse_allocated_time(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        allocated_time = _parse_allocated_time(d.pop("allocatedTime", UNSET))

        total_time_spent = d.pop("totalTimeSpent", UNSET)

        time_spent = []
        _time_spent = d.pop("timeSpent", UNSET)
        for time_spent_item_data in _time_spent or []:
            time_spent_item = ParticipantTimeSpent.from_dict(time_spent_item_data)

            time_spent.append(time_spent_item)

        participant_time_tracking_association_type_0 = cls(
            award=award,
            person=person,
            allocated_time=allocated_time,
            total_time_spent=total_time_spent,
            time_spent=time_spent,
        )

        participant_time_tracking_association_type_0.additional_properties = d
        return participant_time_tracking_association_type_0

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
